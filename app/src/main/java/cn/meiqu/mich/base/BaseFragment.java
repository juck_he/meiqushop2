package cn.meiqu.mich.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * 在oncreate中就可以拿到view
 * 1.修复状态丢失
 * 2.getActivity()返回空
 * 3.增加了findviewbyid的方法
 * 4.要联网，要初始化initReceiver
 * 5.在onHttpHandle判断action和拿到数据
 * @author juck
 */
public abstract class BaseFragment extends Fragment {
    protected Context mContext;//修复getActivity返回空
    protected View view = null;
    public BroadcastReceiver receiver;
    // 页面被移除时，解除之前addView建立的关系
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            if (view != null) {
                viewGroup.removeView(view);
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            // 创建视图的抽象方法
            view = initView(inflater, container, savedInstanceState);
        }
        if(mContext == null){
            mContext = getActivity();
        }
        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }
    /**
     * 给子类去实现这个view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public abstract View initView(LayoutInflater inflater, ViewGroup container,
                                  Bundle savedInstanceState);
    /**
     * 初始化数据，不需要子类调用，只要子类实现就是行了
     */
    public void initData() {

    }

    public void initReceiver(String[] filters) {
        receiver = new ActionReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : filters) {
            filter.addAction(action);
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
    }

    class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            onHttpHandle(action, data);
        }
    }

    protected abstract void onHttpHandle(String action, String data);


    @Override
    public void startActivity(Intent intent) {
        getActivity().startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        getActivity().startActivityForResult(intent, requestCode);
    }

    public boolean getHttpStatus(String action, String data) {
        //dismissProgressDialog();
        if (data == null) {
            ToastUtil.showNetWorkFailure(getActivity());
            return false;
        } else if (JsonUtil.getStatusLegal(data)) {
            return true;
        } else {
            ToastUtil.show(getActivity(), JsonUtil.getErroMsg(data));
            return false;
        }
    }


    public View findViewById(int id) {
        return view.findViewById(id);
    }

    public void toast(String text) {
        if (getActivity() != null)
            ToastUtil.show(getActivity(), text);
    }
}
