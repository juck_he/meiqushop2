package cn.meiqu.mich.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;


import com.umeng.analytics.MobclickAgent;

import cn.meiqu.mich.R;
import cn.meiqu.mich.dialogs.LoadingDialog;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/17.
 * 1.加入了联网在oncreate中加入initReceiver，在onHttpHandle获取到数据
 * 2.initFragment中把Fragment加入进来
 */
public abstract class BaseActivity extends AppCompatActivity {
    public FragmentManager fm;
    public BroadcastReceiver receiver;
    public int containerId;
    public LoadingDialog progressDialog = null;

    public static String action_exitApplication = "action_exitApplication";
    public ExitBroadCase exitBroadCase = new ExitBroadCase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO 友盟发送策略
        MobclickAgent.updateOnlineConfig(this);
        fm = getSupportFragmentManager();
        LocalBroadcastManager.getInstance(this).registerReceiver(exitBroadCase, new IntentFilter(action_exitApplication));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exitBroadCase);
        if (receiver != null)
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void showProgressDialog(String content) {
        if (progressDialog == null) {
            progressDialog = new LoadingDialog(this);
        }
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public abstract void initFragment(int containerId);

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }

    public void showFirst(Fragment f) {
        fm.beginTransaction().add(containerId, f, f.getClass().getName())
                .commit();
    }

    public void showNoPop(Fragment f) {
        fm.beginTransaction().replace(containerId, f, f.getClass().getName())
                .commit();
    }

    public void showAndPop(Fragment f, int containerId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            fm.beginTransaction().setCustomAnimations(R.anim.push_right_in, R.anim.fragment_fade_out, R.anim.activity_fade_in, R.anim.push_right_out).replace(containerId, f, f.getClass().getName())
                    .addToBackStack(null).commit();
        } else {
            fm.beginTransaction().replace(containerId, f, f.getClass().getName())
                    .addToBackStack(null).commit();
        }
    }

    public void showAndPop(Fragment f) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            fm.beginTransaction().setCustomAnimations(R.anim.push_right_in, R.anim.fragment_fade_out, R.anim.activity_fade_in, R.anim.push_right_out).replace(containerId, f, f.getClass().getName())
                    .addToBackStack(null).commit();
        } else {
            fm.beginTransaction().replace(containerId, f, f.getClass().getName())
                    .addToBackStack(null).commit();
        }
    }

    public void showAndPopWithAnimation(Fragment f) {
        fm.beginTransaction().setCustomAnimations(R.anim.activity_fade_in, R.anim.fragment_fade_out).replace(containerId, f, f.getClass().getName())
                .addToBackStack(null).commit();
    }

    public void add(Fragment f) {
        fm.beginTransaction().add(containerId, f, f.getClass().getName())
                .commit();
    }

    public void popBack() {
        if (fm.getBackStackEntryCount() > 0)
            fm.popBackStack();
        else
            finish();

    }


    public abstract void onHttpHandle(String action, String data);

    public boolean getHttpStatus(String action, String data) {
        //dismissProgressDialog();
        if (data == null) {
            ToastUtil.showNetWorkFailure(this);
            return false;
        } else if (JsonUtil.getStatusLegal(data)) {
            return true;
        } else {
            ToastUtil.show(this, JsonUtil.getErroMsg(data));
            return false;
        }
    }

    public void initReceiver(String[] filters) {
        receiver = new ActionReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : filters) {
            filter.addAction(action);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            onHttpHandle(action, data);
        }
    }


    class ExitBroadCase extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(action_exitApplication)) {
                finish();
            }
        }
    }

//    //TODO 加入友盟统计
//    public void onResume() {
//        super.onResume();
//        MobclickAgent.onResume(this);
//    }
//
//    public void onPause() {
//        super.onPause();
//        MobclickAgent.onPause(this);
//    }

}
