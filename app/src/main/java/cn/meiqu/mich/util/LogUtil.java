package cn.meiqu.mich.util;

import android.util.Log;

import cn.meiqu.mich.BuildConfig;

/**
 * Created by Fatel on 15-4-3.
 */
public class LogUtil {
   // private  static final boolean DEBUG = true;

    public static String tag = "fatel";

    public static void log(String text) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, text);
        }
    }
}
