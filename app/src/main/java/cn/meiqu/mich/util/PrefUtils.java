package cn.meiqu.mich.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import cn.meiqu.mich.bean.User;

/**
 * SharePreference封装
 *
 * @author Kevin
 */

public class PrefUtils {

    public static final String PREF_NAME = "config";
    public static String userJson = "user";
    public static String adImageUri = "ad_img_uri";
    public static String cache_data = "cachedata";
    public static String stylead_position = "style_ad_position";
    public static String image_path = "image_path";

    public static boolean getBoolean(Context ctx, String key,
                                     boolean defaultValue) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, defaultValue);
    }

    public static void setBoolean(Context ctx, String key, boolean value) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static String getString(Context ctx, String key, String defaultValue) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    public static void setString(Context ctx, String key, String value) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putString(key, value).commit();
    }

    public static void setUser(Context ctx, User user) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putString(userJson, new Gson().toJson(user)).commit();
    }

    public static User getUser(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        User user = null;
        String data = sp.getString(userJson, "");
        if (!StringUtil.isEmpty(data)) {
            user = new Gson().fromJson(data, User.class);
        }
        return user;
    }

    /**
     * 设置图片的Uri路径
     */
    public static void setAdImageUri(Context ctx, String uri) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putString(adImageUri, uri).commit();
    }

    public static String getAdImageUri(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        String uri = sp.getString(adImageUri, "");
        if (!TextUtils.isEmpty(uri)) {
            return uri;
        }
        return uri;
    }

    /**
     * 设置缓存轮番图数据
     * @param ctx
     * @param data
     */
    public static void setTurnsPhotoJson(Context ctx , String data){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        sp.edit().putString(cache_data,data).commit();
    }

    /**
     * 获取轮番图数据
     * @param ctx
     */
    public static String getTurnsPhotoJson(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        String uri = sp.getString(cache_data, "");
        if (!TextUtils.isEmpty(uri)) {
            return uri;
        }
        return uri;
    }

    /**
     * 形象展示 list -location 的位置
     * @param ctx
     * @param data
     */
    public static void setStyleAdListPosition(Context ctx,int data){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        sp.edit().putInt(stylead_position,data).commit();
    }

    /**
     * 获取最后一次点击的位置
     * @param ctx
     * @return
     */
    public static int getStyleListPosiotion(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        int position = sp.getInt(stylead_position, 0);
        return position;
    }

    /**
     *  存储上次存放过的图片
     */
      public static void  setImagePath(Context mContext, String dataJson){
          LogUtil.log("---------dataJson" + dataJson);
          SharedPreferences sp = mContext.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
          sp.edit().putString(image_path,dataJson).commit();
      }

    /**
     *  获取上次获取存放过的图片的路径
     */
     public static String getImagePath(Context mContext){
         SharedPreferences sp = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
         return sp.getString(image_path,null);
}

}
