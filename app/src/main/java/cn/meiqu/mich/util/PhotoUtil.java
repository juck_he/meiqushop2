package cn.meiqu.mich.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import cn.meiqu.mich.Sdcard;

/**
 * Created by juck on 15/12/24.
 */
public class PhotoUtil {
    //
    public final static int SELECT_PIC_KITKAT = 99;
    public final static int SELECT_PIC = 98;
    //
    public final static int OPEN_CAMERA = 97;
    //
    public final static int CUT_OUT_PHOTO = 96;

    /**
     * 打开相册
     *
     * @param activity
     */
    public static void openAlbum(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);//ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/jpeg");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            //Android 4.4以上的系统
            activity.startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            //Android 4.3以下的系统
            activity.startActivityForResult(intent, SELECT_PIC);
        }
    }

    /**
     * 打开相机
     *
     * @param activity
     */
    public static void openCamera(Activity activity, String image) {
        Intent intentFromCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentFromCapture.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        // 判断存储卡是否可用，存储照片文件
        if (hasSdcard()) {
            intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT, Uri
                    .fromFile(new File(Environment.getExternalStorageDirectory(), image)));
        }
        activity.startActivityForResult(intentFromCapture, OPEN_CAMERA);
    }

    /**
     * 检查设备是否存在SDCard的工具方法
     */
    public static boolean hasSdcard() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            // 有存储的SDCard
            return true;
        } else {
            return false;
        }
    }

    /**
     * 裁剪原始的图片
     *
     * @param activity
     * @param uri
     * @param aspectX
     * @param aspectY
     * @param outputX
     * @param outputY
     */
    public static void cropRawPhoto(Activity activity, Uri uri, int aspectX, int aspectY, int outputX, int outputY) {
        if (uri == null) {
            return;
        }
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            String url = GetPhotoForAlbum.getPath(activity, uri);
            // 4.4 以上的系统
            intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");
        } else {
            // 4.4 以下的系统
            intent.setDataAndType(uri, "image/*");
        }
        // 设置裁剪 (设置显示View是可以裁剪的)
        intent.putExtra("crop", "true");
        // aspectX , aspectY :我们要裁剪宽高的比例
        intent.putExtra("aspectX", 2);
        intent.putExtra("aspectY", 3);
        // outputX , outputY : 裁剪图片宽高
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 380);
        //背景图是可以缩放的
        intent.putExtra("scale", true);
        //表示裁剪之后的图片是通过intent返回回来的
        intent.putExtra("return-data", true);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());//返回格式
//        //不启用人脸识别
//        intent.putExtra("noFaceDetection", true);//若为false则表示不返回数据
        activity.startActivityForResult(intent, CUT_OUT_PHOTO);
    }


    /**
     * 上存图片,内存卡不存就会返回null
     *
     * @param mBitmap
     */
    public static String setPicToView(Bitmap mBitmap, String fileName) {
        File fileNamePath ;
        if (mBitmap == null) {
            return null;
        }
        String sdStatus = Environment.getExternalStorageState();
        // 检测sd是否可用
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) {
            // TODO 内存卡不存在
            return null;
        }
        //
        FileOutputStream os = null;
        //
        File file = new File(Sdcard.User_Root_Dir);
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {// 如果文件夹不存在, 创建文件夹
            parentFile.mkdirs();
        }
        //
        fileNamePath = new File(Sdcard.User_Root_Dir, fileName);
        try {
            os = new FileOutputStream(fileNamePath);
            // 把数据写入文件
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //关闭流z
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileNamePath.toString();
    }


}
