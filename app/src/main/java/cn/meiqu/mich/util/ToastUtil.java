package cn.meiqu.mich.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import cn.meiqu.mich.R;

public class ToastUtil {

    static String netWorkFailure = "网络出错了";
    static Toast toast;
    static TextView tv;
    static TextView tv2;
    static TextView tvToastShow;

    public static void show(Context mContext, String text) {
//        if(toast!=null){
//            toast.cancel();
//            toast = null;
//        }
        if (toast == null) {
            toast = new Toast(mContext);
            ViewGroup v = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.toast, null);
            tv = (TextView) v.getChildAt(0);
            tv2 = (TextView) v.getChildAt(1);
            toast.setView(v);
        }
        tv.setText(text);
        tv2.setText(text);
        toast.show();
    }
//    public static void showTip(Context mContext, String message) {
//        if(toast!=null){
//            toast.cancel();
//            toast = null;
//        }
//        if(toast ==null) {
//            toast = new Toast(mContext);
//        }
//
//        toast.setDuration(Toast.LENGTH_SHORT);
//        ViewGroup toastView = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.view_toast, null);
//       // View toastView = View.inflate(mContext, R.layout.view_toast, null);
//        toast.setView(toastView);
//        tvToastShow = (TextView) toastView.findViewById(R.id.tv_toast_show);
//        tvToastShow.setText(message);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//    }

    public static void showNetWorkFailure(Context mContext) {
        show(mContext, netWorkFailure);
    }

    public static void log(String text) {
        System.out.println(text);
    }
}
