package cn.meiqu.mich.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

import java.io.File;

import cn.meiqu.mich.API;
import cn.meiqu.mich.httpGet.HttpGetController;


/**
 * Created by Fatel on 15-4-3.
 */
public class UploadUtil {
    String className = getClass().getName();
    String action_getToken = getClass().getName() + API.get_qiniu_token;
    ActionBroadCaseReceiver receiver;
    Context mContext;
    private QiuNiuUpLoadListener loadListener;
    String token = "";
    String filePath = "";

    public QiuNiuUpLoadListener getLoadListener() {
        return loadListener;
    }

    public void setLoadListener(QiuNiuUpLoadListener loadListener) {
        this.loadListener = loadListener;
    }


    static UploadManager uploadManager = new UploadManager();
    public static String bucketName = "shuntai-meiqu";
    public static String domain = bucketName + ".qiniudn.com";

    public interface QiuNiuUpLoadListener {
        public void onUpLoadSucceed(String url);

        public void onUpLoadFailure(String erroMessage);
    }

    public UploadUtil(Context mContext, QiuNiuUpLoadListener loadListener) {
        this.mContext = mContext;
        this.loadListener = loadListener;
        LogUtil.log("------action-normal:"+action_getToken);
        initReceiver();
    }

    public void initReceiver() {
        receiver = new ActionBroadCaseReceiver();
        IntentFilter filter = new IntentFilter(action_getToken);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, filter);
    }

    public void unRegisterReceiver() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);
    }

    public void getQiNiuToken() {
        HttpGetController.getInstance().getQiuniuToken(className);
    }

    public void upLoad(String filePath) {
        this.filePath = filePath;
        if (StringUtil.isEmpty(token)) {
            //获取 Token
            LogUtil.log("-------获取token");
            getQiNiuToken();
        } else {
            upLoadImage();
        }
    }

    private void upLoadImage() {
        LogUtil.log("filePath=" + filePath);
        UploadManager uploadManager = new UploadManager();
        String key = null;
        uploadManager.put(new File(filePath), key, token,
                new UpCompletionHandler() {
                    @Override
                    public void complete(String key, ResponseInfo info, JSONObject res) {
                        if (info.isOK()) {
                            LogUtil.log("上传成功" + res);
                            String hash = res.optString("hash", "");
                            String redirect = "http://" + domain + "/" + hash;
                            handleSucceed(redirect);
                        } else {
                            LogUtil.log("上传失败" + res);
                            unRegisterReceiver();
                            handleFailure("上传失败");
                        }
                        LogUtil.log("qiniu=" + key + ",\r\n " + info + ",\r\n " + res);
                    }
                }, null);

    }

    public void handleToken(String data) {
        if (data != null) {
//            if (JsonUtil.getStatusLegal(data)) {
            token = JsonUtil.getJsonObject(data).optJSONObject("info").optString("uploadtoken");
            LogUtil.log("--------获取到七牛上存token"+token);
            upLoadImage();
//            } else {
//                handleFailure("系统出错");
//            }
        } else {
            handleFailure("网络出错了");
        }
    }

    public void handleFailure(String erroMessage) {
        unRegisterReceiver();
        loadListener.onUpLoadFailure(erroMessage);
    }

    public void handleSucceed(String url) {
        unRegisterReceiver();
        loadListener.onUpLoadSucceed(url);
    }

    class ActionBroadCaseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtil.log("-------action:"+intent.getAction());
            if (intent.getAction().equals(action_getToken)) {
                LogUtil.log("------七牛获取到数据："+intent.getStringExtra("data"));
                handleToken(intent.getStringExtra("data"));
            }
        }
    }

}
