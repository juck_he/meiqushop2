package cn.meiqu.mich.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by juck on 15/12/17.
 */
public class DialogUtils  {

    public AlertDialog alertDialog;

    public void showDialog(Context mContext, String str) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(mContext).setNegativeButton("取消",null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (onClickDialogOkListen!=null){
                        onClickDialogOkListen.onClickOk();
                    }
                }
            }).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    public interface OnClickDialogOkListen{
        void onClickOk();
    }
    private OnClickDialogOkListen onClickDialogOkListen;

    public void setOnClickDialogOkListen(OnClickDialogOkListen onClickDialogOkListen){
        this.onClickDialogOkListen = onClickDialogOkListen;
    }
}
