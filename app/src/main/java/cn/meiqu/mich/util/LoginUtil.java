package cn.meiqu.mich.util;//package cn.meiqu.think.util;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.content.LocalBroadcastManager;
//import android.widget.Toast;
//
//import com.google.gson.Gson;
//import com.umeng.socialize.bean.SHARE_MEDIA;
//import com.umeng.socialize.bean.SocializeEntity;
//import com.umeng.socialize.controller.UMServiceFactory;
//import com.umeng.socialize.controller.UMSocialService;
//import com.umeng.socialize.controller.listener.SocializeListeners;
//import com.umeng.socialize.exception.SocializeException;
//import com.umeng.socialize.weixin.controller.UMWXHandler;
//
//import java.util.Map;
//import java.util.Set;
//
//import cn.meiqu.bean.User;
//import cn.meiqu.dao.SettingDao;
//import cn.meiqu.entrepreneur.index.LoginActivity;
//
///**
// * Created by Fatel on 15-7-15.
// */
//public class LoginUtil {
//    Context mContext;
//    UMSocialService mController;
//    public static final String wx_app_id = "wx80a06daa44e8bb70";
//    public static final String wx_screct_id = "1b33a0f6d5c2b47186c72f6492f76cd7";
//
//    public LoginUtil(Context mContext) {
//        this.mContext = mContext;
//
//        mController = UMServiceFactory.getUMSocialService("com.umeng.login");
//        mController.getConfig().setSsoHandler(new UMWXHandler(mContext, wx_app_id, wx_screct_id));
//        // mController.deleteOauth();
//    }
//
//    public void logout_wx() {
//        mController.deleteOauth(mContext, SHARE_MEDIA.WEIXIN, new SocializeListeners.SocializeClientListener() {
//            @Override
//            public void onStart() {
//
//            }
//
//            @Override
//            public void onComplete(int status, SocializeEntity socializeEntity) {
////                if (status == 200) {
////                    Toast.makeText(mContext, "删除成功.",
////                            Toast.LENGTH_SHORT).show();
////                } else {
////                    Toast.makeText(mContext, "删除失败",
////                            Toast.LENGTH_SHORT).show();
////                }
//            }
//        });
//    }
//
//    public void login_wx() {
//        mController.doOauthVerify(mContext, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMAuthListener() {
//            @Override
//            public void onStart(SHARE_MEDIA platform) {
//                // Toast.makeText(mContext, "授权开始", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(SocializeException e, SHARE_MEDIA platform) {
//                //Toast.makeText(mContext, "授权错误", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onComplete(final Bundle value, SHARE_MEDIA platform) {
//                // Toast.makeText(mContext, "授权完成", Toast.LENGTH_SHORT).show();
//                //获取相关授权信息
//                LogUtil.log("value=" + value.toString());
//                mController.getPlatformInfo(mContext, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMDataListener() {
//                    @Override
//                    public void onStart() {
//                        Toast.makeText(mContext, "获取平台数据开始...", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onComplete(int status, Map<String, Object> info) {
//                        if (status == 200 && info != null) {
//                            StringBuilder sb = new StringBuilder();
//                            Set<String> keys = info.keySet();
//                            for (String key : keys) {
//                                sb.append(key + "=" + info.get(key).toString() + "\r\n");
//                            }
//                            LogUtil.log("TestData" + sb.toString());
//                            User user = new User();
//                            user.access_token = value.getString("access_token");
//                            user.unionid = (String) info.get("unionid");
//                            user.nick_name = (String) info.get("nickname");
//                            user.city = (String) info.get("city");
//                            user.province = (String) info.get("province");
//                            user.head_pic = (String) info.get("headimgurl");
//                            user.headimgurl = (String) info.get("headimgurl");
//                            user.sex = (int) info.get("sex") + "";
//                            user.openid = (String) info.get("openid");
//                            SettingDao.getInstance().setUserJson(new Gson().toJson(user));
//                            Intent intent = new Intent(LoginActivity.action_wxLogin);
//                            intent.putExtra("data", "succeed");
//                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
//                        } else {
//                            LogUtil.log("TestData" + "发生错误：" + status);
//                            Intent intent = new Intent(LoginActivity.action_wxLogin);
//                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
//                        }
//
//                    }
//                });
//            }
//
//            @Override
//            public void onCancel(SHARE_MEDIA platform) {
//                // Toast.makeText(mContext, "授权取消", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//    }
//}
