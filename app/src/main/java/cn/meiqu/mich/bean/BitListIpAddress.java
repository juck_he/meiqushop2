package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/24.
 */
public class BitListIpAddress {
//    {
//        "status": 1,
//            "info": {
//        "wifi_info": {
//            "wifi_ssid": "meiqu99",
//                    "wifi_pwd": "mich150809",
//                    "wifi_ssid_1": "",
//                    "wifi_pwd_1": "",
//                    "wifi_ssid_2": "",
//                    "wifi_pwd_2": ""
//        },
//        "device_list": [
//        {
//            "device_number": "08:d8:33:2f:c3:ab",
//                "device_alias": "1",
//                "ip_addr": "",
//                "is_host": "0",
//                "boot_time": "08:45",
//                "shutdown_time": "21:30",
//                "last_online_time": "1421745920",
//                "status": 0
//        },
//        {
//            "device_number": "08:d8:33:05:03:fc",
//                "device_alias": "2",
//                "ip_addr": "",
//                "is_host": "0",
//                "boot_time": "08:45",
//                "shutdown_time": "21:30",
//                "last_online_time": "1427193215",
//                "status": 0
//        },
//        {
//            "device_number": "08:d8:33:05:39:17",
//                "device_alias": "3",
//                "ip_addr": "",
//                "is_host": "0",
//                "boot_time": "08:45",
//                "shutdown_time": "21:30",
//                "last_online_time": "1423926628",
//                "status": 0
//        },
//        {
//            "device_number": "98:3b:16:c6:dc:36",
//                "device_alias": "4",
//                "ip_addr": "192.168.66.7",
//                "is_host": "1",
//                "boot_time": "08:45",
//                "shutdown_time": "21:30",
//                "last_online_time": "1442304942",
//                "status": 0
//        },
//        {
//            "device_number": "98:3b:16:c6:f3:e5",
//                "device_alias": "12",
//                "ip_addr": "192.168.10.3",
//                "is_host": "1",
//                "boot_time": "08:45",
//                "shutdown_time": "21:30",
//                "last_online_time": "1443088535",
//                "status": 1
//        },
//        {
//            "device_number": "8c:18:d9:2b:dd:3d",
//                "device_alias": "6",
//                "ip_addr": "192.168.66.2",
//                "is_host": "1",
//                "boot_time": "09:00",
//                "shutdown_time": "21:15",
//                "last_online_time": "1443088609",
//                "status": 1
//        },
//        {
//            "device_number": "7c:c7:09:28:85:c1",
//                "device_alias": "7",
//                "ip_addr": "192.168.66.5",
//                "is_host": "0",
//                "boot_time": "09:00",
//                "shutdown_time": "23:00",
//                "last_online_time": "1443088534",
//                "status": 1
//        },
//        {
//            "device_number": "98:3b:16:14:5e:11",
//                "device_alias": "8",
//                "ip_addr": "192.168.1.103",
//                "is_host": "1",
//                "boot_time": "00:01",
//                "shutdown_time": "23:59",
//                "last_online_time": "1443056236",
//                "status": 1
//        }
//        ]
//    },
//        "did": "",
//            "sertime": 1443088687
//    }

    public String status;
    public BitListInfo info;
    private String did;
    public String sertime;

    public class BitListInfo{
        public BitWifiInfo wifi_info;
        public List<BitDeviceList>  device_list;
    }

    public class BitWifiInfo{
        public String wifi_ssid;
        public String wifi_pwd;
        public String wifi_ssid_1;
        public String wifi_pwd_1;
        public String wifi_ssid_2;
        public String wifi_pwd_2;
    }


    public class BitDeviceList{
        public String device_number;
        public String device_alias;
        public String ip_addr;
        public String is_host;
        public String boot_time;
        public String shutdown_time;
        public String last_online_time;
        public int status;
       //public Boolean isShowStatus = false;
    }
}
