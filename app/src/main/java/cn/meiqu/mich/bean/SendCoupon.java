package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/10/9.
 */
public class SendCoupon {
    /**
     * status : 1
     * info : {"voucher_id":22}
     * did : 0
     * sertime : 1444374089
     */

    public int status;
    public InfoEntity info;
    public int did;
    public int sertime;

    public static class InfoEntity {
        /**
         * voucher_id : 22
         */

        public int voucher_id;
    }
}
