package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by juck on 15/12/8.
 */
public class ProMubanlist {

    /**
     * status : 1
     * info : {"list":[{"id":"3","template_name":"项目模板1","title_color":"e69769","bg_image":"http://shuntai-meiqu.qiniudn.com/566524e0c8ace","status":"1","list_order":"0","is_delete":"0","update_time":"1449469152","create_time":"1449469153"},{"id":"4","template_name":"项目模板2","title_color":"ed3274","bg_image":"http://shuntai-meiqu.qiniudn.com/56652519d724e","status":"1","list_order":"1","is_delete":"0","update_time":"1449469209","create_time":"1449469210"},{"id":"5","template_name":"项目模板3","title_color":"ed3274","bg_image":"http://shuntai-meiqu.qiniudn.com/5665255883ca6","status":"1","list_order":"2","is_delete":"0","update_time":"1449469272","create_time":"1449469272"},{"id":"6","template_name":"项目模板4","title_color":"ed3274","bg_image":"http://shuntai-meiqu.qiniudn.com/5665257bd45ca","status":"1","list_order":"3","is_delete":"0","update_time":"1449469307","create_time":"1449469308"},{"id":"7","template_name":"项目模板5","title_color":"312d21","bg_image":"http://shuntai-meiqu.qiniudn.com/566525a6bbd08","status":"1","list_order":"4","is_delete":"0","update_time":"1449469350","create_time":"1449469351"},{"id":"8","template_name":"项目模板6","title_color":"352719","bg_image":"http://shuntai-meiqu.qiniudn.com/566525e139488","status":"1","list_order":"5","is_delete":"0","update_time":"1449469409","create_time":"1449469409"},{"id":"9","template_name":"项目模板7","title_color":"241e20","bg_image":"http://shuntai-meiqu.qiniudn.com/5665260d4afd1","status":"1","list_order":"6","is_delete":"0","update_time":"1449469453","create_time":"1449469453"},{"id":"10","template_name":"项目模板8","title_color":"5d4035","bg_image":"http://shuntai-meiqu.qiniudn.com/5665262beb15a","status":"1","list_order":"7","is_delete":"0","update_time":"1449469483","create_time":"1449469484"},{"id":"11","template_name":"项目模板9","title_color":"3591b9","bg_image":"http://shuntai-meiqu.qiniudn.com/5665265a94d26","status":"1","list_order":"8","is_delete":"0","update_time":"1449469530","create_time":"1449469530"},{"id":"12","template_name":"项目模板10","title_color":"5d3f58","bg_image":"http://shuntai-meiqu.qiniudn.com/56652688995ef","status":"1","list_order":"9","is_delete":"0","update_time":"1449469576","create_time":"1449469576"}]}
     * did :
     * sertime : 1449560074
     */

    public int status;
    public InfoEntity info;
    public String did;
    public int sertime;

    public static class InfoEntity {
        /**
         * id : 3
         * template_name : 项目模板1
         * title_color : e69769
         * bg_image : http://shuntai-meiqu.qiniudn.com/566524e0c8ace
         * status : 1
         * list_order : 0
         * is_delete : 0
         * update_time : 1449469152
         * create_time : 1449469153
         */

        public List<ListEntity> list;

        public static class ListEntity {
            public String id;
            public String template_name;
            public String title_color;
            public String bg_image;
            public String status;
            public String list_order;
            public String is_delete;
            public String update_time;
            public String create_time;
        }
    }
}
