package cn.meiqu.mich.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/11/16.
 */
public class EditTextPhoto implements Serializable{

    public int currentItem;
    public String deviceId;
    public String headUrl;
    public String contentName;
    public String contentDesc;
    public String typeId;

}
