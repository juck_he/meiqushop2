package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/9/30.
 */
public class DateInfo {

    public String dateMonths;       //月的日期
    public String dateWeeks;        //星期几
    public int state;               //状态 1 =选中 ，2表示没有选中


    public DateInfo(String dateMonths, String dateWeeks, int state) {
        this.dateMonths = dateMonths;
        this.dateWeeks = dateWeeks;
        this.state = state;
    }
}
