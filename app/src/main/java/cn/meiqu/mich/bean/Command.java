package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/11/27.
 */
public class Command {

    public Command(String command,long currentTime) {
        this.currentTime = currentTime;
        this.command = command;
    }

    public long currentTime;
    public String command;
}
