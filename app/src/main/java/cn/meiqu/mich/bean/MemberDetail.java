package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/10/12.
 */
public class MemberDetail {

    /**
     * status : 1
     * info : {"cost_money":"0.010000","pay_money":"0.00","total_money":"0.01","user_id":"169","head_pic":"http://shuntai-meiqu.qiniudn.com/FowpA2fUA9DuNoGh15R8-HT5TrMY","user_name":"美渠 谭杰超","telephone":"15018709437","address":"","introduction":"","sex":"1","province":"","city":"","area":"","province_id":"0","city_id":"0","area_id":"0","birth":"1989-06","focus_company_id":"133","tags":"","is_stylist":"0","stylist_company_id":"0","manager_company_id":"0","follow_time":"1421374145"}
     * did :
     * sertime : 1444638617
     */

    public int status;
    public InfoEntity info;
    public String did;
    public int sertime;

    public static class InfoEntity {
        /**
         * cost_money : 0.010000
         * pay_money : 0.00
         * total_money : 0.01
         * user_id : 169
         * head_pic : http://shuntai-meiqu.qiniudn.com/FowpA2fUA9DuNoGh15R8-HT5TrMY
         * user_name : 美渠 谭杰超
         * telephone : 15018709437
         * address :
         * introduction :
         * sex : 1
         * province :
         * city :
         * area :
         * province_id : 0
         * city_id : 0
         * area_id : 0
         * birth : 1989-06
         * focus_company_id : 133
         * tags :
         * is_stylist : 0
         * stylist_company_id : 0
         * manager_company_id : 0
         * follow_time : 1421374145
         */

        public String cost_money;
        public String pay_money;
        public String total_money;
        public String user_id;
        public String head_pic;
        public String user_name;
        public String telephone;
        public String address;
        public String introduction;
        public String sex;
        public String province;
        public String city;
        public String area;
        public String province_id;
        public String city_id;
        public String area_id;
        public String birth;
        public String focus_company_id;
        public String tags;
        public String is_stylist;
        public String stylist_company_id;
        public String manager_company_id;
        public String follow_time;
    }
}
