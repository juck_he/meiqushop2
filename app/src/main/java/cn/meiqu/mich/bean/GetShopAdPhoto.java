package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/22.
 */
public class GetShopAdPhoto {
//    {
//        "status":1,
//            "info":[
//        {
//            "ad_url":"http://shuntai-meiqu.qiniudn.com/FlRWYC-sS-9DvAg-8X9rU2A7ajoc",
//                "link_url":null
//        }
//        ],
//        "did":null,
//            "sertime":1442894483
//    }

    public String status;
    public List<ShopPhotoInfo> info;
    public String did;
    public String sertime;

    public class ShopPhotoInfo{
        public String ad_url;
        public String link_url;
    }

}
