package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/9/18.
 */
public class LargeScreenData  {
//    {
//        "status": 1,
//            "info": {
//        "content": "哥哥哥哥噶个again啊"
//    },
//        "did": 0,
//            "sertime": 1442570343
//    }
    public String status;
    public LargeScreenContent info;
    public String did;
    public String sertime;

    public class LargeScreenContent{
        public String content;
    }
}
