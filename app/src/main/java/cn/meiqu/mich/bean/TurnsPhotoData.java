package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/22.
 */
public class TurnsPhotoData  {


    /**
     * status : 1
     * info : {"img_list":[{"id":"1214","company_id":"6626","device_number":"8c:18:d9:4e:74:d4","title":"傻豪","desc":"傻。。。","head_pic":"http://shuntai-meiqu.qiniudn.com/FmooTt0HMk5aXnavJOXJ4vA_onBU","img_url":"http://shuntai-meiqu.qiniudn.com/Fkc9UCiRIxuz8ifDckh2mpmPaqbM","template_id":"2","status":"1","update_time":"1448155126","create_time":"1448155126","device_alias":"1"}]}
     * did : 0
     * sertime : 1448155458
     */

    public int status;
    public InfoEntity info;
    public int did;
    public int sertime;

    public static class InfoEntity {
        /**
         * img_list : [{"id":"1214","company_id":"6626","device_number":"8c:18:d9:4e:74:d4","title":"傻豪","desc":"傻。。。","head_pic":"http://shuntai-meiqu.qiniudn.com/FmooTt0HMk5aXnavJOXJ4vA_onBU","img_url":"http://shuntai-meiqu.qiniudn.com/Fkc9UCiRIxuz8ifDckh2mpmPaqbM","template_id":"2","status":"1","update_time":"1448155126","create_time":"1448155126","device_alias":"1"}]
         */

        public List<ImgListEntity> img_list;

        public static class ImgListEntity {
            /**
             * id : 1214
             * company_id : 6626
             * device_number : 8c:18:d9:4e:74:d4
             * title : 傻豪
             * desc : 傻。。。
             * head_pic : http://shuntai-meiqu.qiniudn.com/FmooTt0HMk5aXnavJOXJ4vA_onBU
             * img_url : http://shuntai-meiqu.qiniudn.com/Fkc9UCiRIxuz8ifDckh2mpmPaqbM
             * template_id : 2
             * status : 1
             * update_time : 1448155126
             * create_time : 1448155126
             * device_alias : 1
             */

            public String id;
            public String company_id;
            public String device_number;
            public String title;
            public String desc;
            public String head_pic;
            public String img_url;
            public String template_id;
            public String status;
            public String update_time;
            public String create_time;
            public String device_alias;
        }
    }
}
