package cn.meiqu.mich.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/11/26.
 */
public class ImageAd implements Serializable {
    public String filePathList;
    public boolean isCanAddPhtoto;

    public ImageAd(String filePathList, boolean isCanAddPhtoto) {
        this.filePathList = filePathList;
        this.isCanAddPhtoto = isCanAddPhtoto;
    }
}
