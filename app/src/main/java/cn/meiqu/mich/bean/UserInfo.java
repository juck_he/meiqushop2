package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/10/9.
 */
public class UserInfo {
    /**
     * status : 1
     * info : [{"user_id":"98","head_pic":"http://shuntai-meiqu.qiniudn.com/FkZzWm6tC8IB0XuQ_ati728xIoRQ","user_name":"泰先生","telephone":"15989270096","address":"","introduction":"","sex":"1","province":"广东","city":"广州","area":"","province_id":"440000","city_id":"440100","area_id":"0","birth":"1991-10","focus_company_id":"25","tags":"","is_stylist":"1","stylist_company_id":"0","manager_company_id":"0","follow_time":"1440556238"},{"user_id":"2015","head_pic":"http://shuntai-meiqu.qiniudn.com/FqmMisgEl6Z8PRo4A3X_sicuiTeP","user_name":"123","telephone":"15072385577","address":"","introduction":"","sex":"1","province":"云南省","city":"丽江","area":"","province_id":"530000","city_id":"530700","area_id":"0","birth":"1993-01","focus_company_id":"1831","tags":"","is_stylist":"0","stylist_company_id":"0","manager_company_id":"0","follow_time":"1437131206"}]
     * did :
     * sertime : 1444377047
     */

    public int status;
    public String did;
    public int sertime;
    public List<InfoEntity> info;

    public static class InfoEntity {
        /**
         * user_id : 98
         * head_pic : http://shuntai-meiqu.qiniudn.com/FkZzWm6tC8IB0XuQ_ati728xIoRQ
         * user_name : 泰先生
         * telephone : 15989270096
         * address :
         * introduction :
         * sex : 1
         * province : 广东
         * city : 广州
         * area :
         * province_id : 440000
         * city_id : 440100
         * area_id : 0
         * birth : 1991-10
         * focus_company_id : 25
         * tags :
         * is_stylist : 1
         * stylist_company_id : 0
         * manager_company_id : 0
         * follow_time : 1440556238
         */

        public String user_id;
        public String head_pic;
        public String user_name;
        public String telephone;
        public String address;
        public String introduction;
        public String sex;
        public String province;
        public String city;
        public String area;
        public String province_id;
        public String city_id;
        public String area_id;
        public String birth;
        public String focus_company_id;
        public String tags;
        public String is_stylist;
        public String stylist_company_id;
        public String manager_company_id;
        public String follow_time;
        public boolean isSelect=false;
        public boolean isShowCheckBox = true;
    }
}
