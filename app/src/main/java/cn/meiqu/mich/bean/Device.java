package cn.meiqu.mich.bean;

/**
 * Created by Fatel on 15-4-9.
 */
public class Device {
    public String device_number = "";
    public String device_alias = "";
    public String ip_addr = "";
    public String is_host = "";
    public String boot_time = "";
    public String shutdown_time = "";
    public String last_online_time = "";
    public int status = 0;//0为离线，1在线，2处理中

}
