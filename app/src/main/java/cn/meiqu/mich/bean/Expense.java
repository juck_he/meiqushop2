package cn.meiqu.mich.bean;

import java.util.ArrayList;

/**
 * Created by Fatel on 15-9-29.
 */
public class Expense {
    public ArrayList<PayList> pay_list;
    public Count count;

    public class PayList {
        public String coupon_id = "";
        public String coupon_title = "";
        public String coupon_money = "";
        public String sell_num = "";
        public String coupon_img = "";
        public String use_count = "";
        public String status = "";
        public String show_time = "";
        public String show_name = "";
        public String pay_money = "";
        public String true_time = "";
        public String begin = "";
    }

    public class Count {
        public String day_sell = "";
        public String day_money = "";
        public String week_sell = "";
        public String week_money = "";
        public String month_sell = "";
        public String month_money = "";

    }
}
