package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/2.
 */
public class GetLastSetupTime {
    /**
     * status : 1
     * info : [{"device_number":"8c:18:d9:4e:74:d4","device_alias":"1","ip_addr":"192.168.10.29","is_host":"1","boot_time":"08:30","shutdown_time":"22:30","last_online_time":"1444275950","status":0},{"device_number":"98:3b:16:14:5c:82","device_alias":"2","ip_addr":"192.168.1.109","is_host":"1","boot_time":"08:30","shutdown_time":"22:30","last_online_time":"1446445657","status":1}]
     * did :
     * sertime : 1446445745
     */

    public int status;
    public String did;
    public int sertime;
    public List<InfoEntity> info;

    public static class InfoEntity {
        /**
         * device_number : 8c:18:d9:4e:74:d4
         * device_alias : 1
         * ip_addr : 192.168.10.29
         * is_host : 1
         * boot_time : 08:30
         * shutdown_time : 22:30
         * last_online_time : 1444275950
         * status : 0
         */

        public String device_number;
        public String device_alias;
        public String ip_addr;
        public String is_host;
        public String boot_time;
        public String shutdown_time;
        public String last_online_time;
        public int status;
    }
}
