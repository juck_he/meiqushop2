package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/11.
 */
public class TemplateList {

    /**
     * status : 1
     * info : {"template_list":[{"template_id":"1","type":"1","company_color":"ec3d37","title_color":"000000","title_edge_color":"ffffff","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0012.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0011.png"},{"template_id":"2","type":"1","company_color":"ffeb64","title_color":"ffeb64","title_edge_color":"ffffff","desc_color":"dd0026","show_img":"http://meiquimg.qiniudn.com/myzxjs0022.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0021.png"},{"template_id":"3","type":"1","company_color":"000000","title_color":"f19a00","title_edge_color":"ffffff","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0032.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0031.png"},{"template_id":"4","type":"1","company_color":"fef244","title_color":"fef244","title_edge_color":"8e1bb4","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0042.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0041.png"},{"template_id":"5","type":"1","company_color":"ffffff","title_color":"13c0d9","title_edge_color":"","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0052.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0051.png"},{"template_id":"6","type":"1","company_color":"ffffff","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0062.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0061.png"},{"template_id":"7","type":"2","company_color":"0576ab","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0072.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0071.png"},{"template_id":"8","type":"2","company_color":"f25c20","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0082.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0081.png"}]}
     * did : 0
     * sertime : 1447403223
     */

    public int status;
    public InfoEntity info;
    public int did;
    public int sertime;

    public static class InfoEntity {
        /**
         * template_list : [{"template_id":"1","type":"1","company_color":"ec3d37","title_color":"000000","title_edge_color":"ffffff","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0012.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0011.png"},{"template_id":"2","type":"1","company_color":"ffeb64","title_color":"ffeb64","title_edge_color":"ffffff","desc_color":"dd0026","show_img":"http://meiquimg.qiniudn.com/myzxjs0022.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0021.png"},{"template_id":"3","type":"1","company_color":"000000","title_color":"f19a00","title_edge_color":"ffffff","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0032.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0031.png"},{"template_id":"4","type":"1","company_color":"fef244","title_color":"fef244","title_edge_color":"8e1bb4","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0042.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0041.png"},{"template_id":"5","type":"1","company_color":"ffffff","title_color":"13c0d9","title_edge_color":"","desc_color":"000000","show_img":"http://meiquimg.qiniudn.com/myzxjs0052.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0051.png"},{"template_id":"6","type":"1","company_color":"ffffff","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0062.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0061.png"},{"template_id":"7","type":"2","company_color":"0576ab","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0072.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0071.png"},{"template_id":"8","type":"2","company_color":"f25c20","title_color":"ffffff","title_edge_color":"","desc_color":"ffffff","show_img":"http://meiquimg.qiniudn.com/myzxjs0082.jpg","bg_img":"http://meiquimg.qiniudn.com/myzxjs0081.png"}]
         */

        public List<TemplateListEntity> template_list;

        public static class TemplateListEntity {
            /**
             * template_id : 1
             * type : 1
             * company_color : ec3d37
             * title_color : 000000
             * title_edge_color : ffffff
             * desc_color : 000000
             * show_img : http://meiquimg.qiniudn.com/myzxjs0012.jpg
             * bg_img : http://meiquimg.qiniudn.com/myzxjs0011.png
             */

            public String template_id;
            public String type;
            public String company_color;
            public String title_color;
            public String title_edge_color;
            public String desc_color;
            public String show_img;
            public String bg_img;
            public boolean isSelect;


        }
    }
}
