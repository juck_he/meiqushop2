package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/30.
 */
public class HistoryInfo {
//    {
//        "status":1,
//            "info":{
//        "list":[
//        {
//            "id":"2158",
//                "user_id":"702",
//                "coupon_num":"4583483391",
//                "use_time":"1438596687",
//                "company_id":"25",
//                "order_id":"3818",
//                "coupon_id":"219",
//                "coupon_type":"1",
//                "in_time":"1438596609",
//                "operater_id":null,
//                "settlement_id":null,
//                "pay_money":"0.01",
//                "coupon_amount":"1",
//                "coupon_money":"0.01",
//                "coupon_title":"洗剪吹套餐，男女不限，发长不限"
//        }
//        ]
//    },
//        "did":"",
//            "sertime":1443596927
//    }

    public String status;
    public HistoryData info;
    public String did;
    public String sertime;


    public class HistoryData{
        public List<HistoryResult> list;
    }

    public class HistoryResult{
        public String id;
        public String user_id;
        public String coupon_num;
        public String use_time;
        public String company_id;
        public String order_id;
        public String coupon_id;
        public String coupon_type;
        public String in_time;
        public String operater_id;
        public String settlement_id;
        public String pay_money;
        public String coupon_amount;
        public String coupon_money;
        public String coupon_title;
    }
}
