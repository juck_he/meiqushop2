package cn.meiqu.mich.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/10/19.
 */
public class BannerPhoto {
    /**
     * status : 1
     * info : [{"ad_url":"http://shuntai-meiqu.qiniudn.com/55346c11b58ff","link_url":""}]
     * did : null
     * sertime : 1445255492
     */

    public int status;
    public Object did;
    public int sertime;
    public List<InfoEntity> info;

    public static class InfoEntity {
        /**
         * ad_url : http://shuntai-meiqu.qiniudn.com/55346c11b58ff
         * link_url :
         */

        public String ad_url;
        public String link_url;
    }
}
