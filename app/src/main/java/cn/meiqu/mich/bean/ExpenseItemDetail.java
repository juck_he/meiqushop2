package cn.meiqu.mich.bean;

import java.util.ArrayList;

/**
 * Created by Fatel on 15-9-30.
 */
public class ExpenseItemDetail {
    public ArrayList<Item> list;
    public Detail coupon_info;

    public class Item {
        public String date = "";
        public String use_count = "";
        public String use_money = "";
        public String sell_count = "";
        public String sell_money = "";

    }

    public class Detail {
        public String coupon_id = "";
        public String coupon_img = "";
        public String coupon_money = "";
        public String coupon_title = "";
        public String begin = "";
        public String use_count = "";
        public String sell_count = "";
    }
}
