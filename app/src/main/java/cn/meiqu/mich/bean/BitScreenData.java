package cn.meiqu.mich.bean;

/**
 * Created by Administrator on 2015/11/30.
 */
public class BitScreenData {

    /**
     * command : B_000
     * company_id :
     * ip : 192.168.66.2
     * device_id : fe59093c0454f44a
     * currentTime : 1448874923208
     * updateTime : 0
     */

    public String command;
    public String company_id;
    public String ip;
    public String device_id;
    public long currentTime;
    public int updateTime;
}
