package cn.meiqu.mich.httpGet;


import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.bean.Accredit;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.common.Common;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;

public class HttpGetController {

    private HttpGetController() {
    }

    private static HttpGetController controller = new HttpGetController();

    public static HttpGetController getInstance() {
        return controller;
    }

    private HttpResponController responController = HttpResponController
            .getInstance();

    private void get(String uri, RequestParams params, String action) {
        HttpGetBase.newInstance().get(uri, params, action, responController);
    }

    private void post(final String uri, final RequestParams params, final String action) {
        BaseApp.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //LogUtil.log("------" + uri + "---" + params + "" + action+responController);
                HttpGetBase.newInstance().post(uri, params, action, responController);
            }
        }, 5 * 100);
    }

    private void postNation(final String uri, final RequestParams params, final String action, final String baseUrl) {
        BaseApp.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                HttpGetBase.newInstance().postNation(uri, params, action, responController,baseUrl);
            }
        }, 5 * 100);
    }


    public RequestParams getTokenParma() {
        RequestParams params = new RequestParams();
        User user = PrefUtils.getUser(BaseApp.mContext);
        if (user != null) {
            params.put("token", user.token);
            LogUtil.log("token=" + user.token);
        }
        return params;
    }

//    public RequestParams getTokenParma(RequestParams params) {
////        User user = SettingDao.getInstance().getUser();
////        if (user != null) {
////            params.put("token", user.token);
////            LogUtil.log("token=" + user.token);
////        }
////        return params;
//    }

    public void getQiuniuToken(String action) {
        // post(API.get_qiniu_token, new RequestParams(), c.getName());
        post(API.get_qiniu_token, new RequestParams(), action);
    }


    public void login(String client_key, String device_id, String user_account, String user_pwd, String className) {
        RequestParams params = new RequestParams();
        params.put("client_key", client_key);
        params.put("device_id", device_id);
        params.put("user_account", user_account);
        params.put("user_pwd", user_pwd);
        post(API.login, params, className);
    }

    public void getArea(String className) {
        // get(API.get_area,new RequestParams(),className);
    }
//    public void getHomeBanner(String company_id, String className) {
//        RequestParams params = new RequestParams();
//        params.put("company_id", company_id);
//        post(API.get_home_banner, params, className);
//    }

    /***
     * 上存广告
     *
     * @param token
     * @param className
     */
    public void setAdToScreen(String token, String content, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("content", content);
        post(API.setAdToScreen, params, className);
    }

    /***
     * 上存广告
     *
     * @param company_id
     * @param className
     */
    public void setAdToScreen(String company_id, String device_id, String ad_id, String play_time, String play_percent, String className) {
        RequestParams params = new RequestParams();
        params.put("company_id", company_id);
        params.put("device_id", device_id);
        params.put("ad_id", ad_id);
        params.put("play_time", play_time);
        params.put("play_percent", play_percent);
        post(API.setAdToScreen, params, className);
    }

    public void changePwd(String oldPwd, String newPwd, String className) {
        RequestParams params = getTokenParma();
        params.put("password", oldPwd);
        params.put("new_password", newPwd);
        post(API.changePwd, params, className);
    }

    public void getUserInfo(String token, String company_id, String className) {
        RequestParams params = getTokenParma();
        params.put("company_id", company_id);
        post(API.getUserInfo, params, className);
    }

    public void getAdData(String token, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        post(API.sgetAdToScreen, params, className);
    }


    /***
     * 上存轮番图
     *
     * @param token
     * @param className
     */
    public void setTurnsPhoto(String token, String img_url, String device_number, String title, String desc, String head_pic, String template_id, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("img_url", img_url);
        params.put("device_number", device_number);
        params.put("title", title);
        params.put("desc", desc);
        params.put("head_pic", head_pic);
        params.put("template_id", template_id);
        post(API.setTurnsPhoto, params, className);
    }

    /**
     * 获取轮番图
     *
     * @param token
     * @param className
     */
    public void getTurnsPhoto(String token, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        post(API.getTurnsPhoto, params, className);
    }

    /**
     * 一体机-形象展示模板列表
     *
     * @param token
     * @param className
     */
    public void getTemplateList(String token, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        post(API.getTemplateList, params, className);
    }

    /**
     * 上存店家的广告图
     *
     * @param token
     * @param ad_url
     * @param className
     */
    public void setShopName(String token, String ad_url, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("ad_url", ad_url);
        post(API.setShopAdPhoto, params, className);
    }

    /**
     * 获取店家的广告图
     *
     * @param company_id
     * @param className
     */
    public void getShopPhoto(String company_id, String className) {
        RequestParams params = new RequestParams();
        params.put("company_id", company_id);
        post(API.getShopAdPhoto, params, className);
    }

    public void edtUserInfo(User user, String className) {
        RequestParams params = getTokenParma();
        params.put("company_name", user.company_name);
        params.put("company_logo", user.company_logo);
        params.put("telephone", user.telephone);
        params.put("address", user.address);
        post(API.edtUserInfo, params, className);
    }


    /**
     * 删除轮番图
     *
     * @param token
     * @param id
     * @param className
     */
    public void setDelTurnPhoro(String token, String id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("id", id);
        post(API.setDelTurnPhoro, params, className);
    }

    /**
     * 获取大屏list的数据
     *
     * @param token
     * @param className
     */
    public void getbitListInfo(String token, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        post(API.getbitListInfo, params, className);
    }

    public void getStylist(String token, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        post(API.getStyList, params, className);
    }


    public void edtStylist(Accredit accredit, String className) {
        RequestParams params = getTokenParma();
//        params.put("phone", accredit.phone);
        params.put("stylist_id", accredit.id);
        params.put("user_pwd", accredit.user_pwd);
        post(API.edtStyList, params, className);
    }

    public void addStylist(String token,Accredit accredit, String className) {
        RequestParams params = getTokenParma();
        params.put("client_key", Common.StyList_Client_Key);
//        params.put("phone", accredit.phone);
        params.put("token", token);
        params.put("phone", accredit.user_account);
        params.put("user_pwd", accredit.user_pwd);
        post(API.addStyList, params, className);
    }

    public void delStylist(String token,String stylist_id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("stylist_id", stylist_id);
        post(API.delStylist, params, className);
    }

    public void setCompanyWiFi(Class<?> c, String wifi_ssid, String wifi_pwd, String wifi_ssid_1, String wifi_pwd_1, String wifi_ssid_2, String wifi_pwd_2) {
        RequestParams params = getTokenParma();
        params.put("wifi_ssid", wifi_ssid);
        params.put("wifi_pwd", wifi_pwd);
        params.put("wifi_ssid_1", wifi_ssid_1);
        params.put("wifi_pwd_1", wifi_pwd_1);
        params.put("wifi_ssid_2", wifi_ssid_2);
        params.put("wifi_pwd_2", wifi_pwd_2);
        post(API.set_wifi_info, params, c.getName());
    }

    public void getCompanyDevices(Class<?> c) {
        post(API.get_company_devices, getTokenParma(), c.getName());
    }

    public void bindDevice(String device_id, String className) {
        RequestParams params = getTokenParma();
        params.put("device_id", device_id);
        post(API.bindDevice, params, className);
    }

    public void setRealPay(String money, String className) {
        RequestParams params = getTokenParma();
        params.put("money", money);
        post(API.setRealPay, params, className);
    }

    public void checkRealPay(String order_num, String className) {
        RequestParams params = getTokenParma();
        params.put("order_num", order_num);
        post(API.checkRealPay, params, className);
    }

    public void getRealPayHistroy(int page, int per_page, String className) {
        RequestParams params = getTokenParma();
        params.put("page", page);
        params.put("per_page", per_page);
        post(API.getRealPayHistroy, params, className);
    }

    public void getExpenseyList(String className) {
        RequestParams params = getTokenParma();
        post(API.getPayList, params, className);
    }

    public void getExpenseItemList(int page, int per_page, String className) {
        RequestParams params = getTokenParma();
        params.put("page", page);
        params.put("per_page", per_page);
        post(API.getPayItemList, params, className);
    }

    public void getExpenseItemDetail(String coupon_id, String className) {
        RequestParams params = getTokenParma();
        params.put("coupon_id", coupon_id);
        post(API.getPayItemDetail, params, className);
    }

    /**
     * 验证并使用券
     *
     * @param token
     * @param coupon_num
     * @param className
     */
    public void checkUseCoupons(String token, String coupon_num, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("coupon_num", coupon_num);
        post(API.getCheckUseCoupons, params, className);

    }

    /**
     * 验证券历史列表
     *
     * @param token
     * @param page
     * @param per_page
     * @param req_time
     * @param className
     */
    public void getCheckUseHistory(String token, String page, String per_page, String req_time, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("page", page);
        params.put("per_page", per_page);
        params.put("req_time", req_time);
        post(API.getCheckUseHistory, params, className);

    }


    public void setCoupon(String token, String title, String money, String start_date, String end_date, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("title", title);
        params.put("money", money);
        params.put("start_date", start_date);
        params.put("end_date", end_date);
        post(API.setCoupon, params, className);

    }

    /**
     * 推送优惠券给用用户
     *
     * @param token
     * @param coupon_id
     * @param give_users
     * @param className
     */
    public void setCouponToMember(String token, String coupon_id, String give_users, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("coupon_id", coupon_id);
        params.put("give_users", give_users);
        post(API.setCouponToMember, params, className);
    }

    /**
     * 获取会员列表
     *
     * @param token
     * @param page
     * @param per_page
     * @param keyword
     * @param className
     */
    public void getUserList(String token, String page, String per_page, String keyword, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("page", page);
        params.put("per_page", per_page);
        params.put("keyword", keyword);
        post(API.getUserList, params, className);
    }

    /**
     * 店家发送推送文字
     *
     * @param token
     * @param to_user_ids
     * @param news_content
     * @param news_img
     * @param template_key
     * @param coupon_id
     * @param className
     */
    public void sendTextPush(String token, String to_user_ids, String news_content, String news_img, String template_key, String coupon_id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("to_user_ids", to_user_ids);
        params.put("news_content", news_content);
        params.put("news_img", news_img);
        params.put("template_key", template_key);
        params.put("coupon_id", coupon_id);
        post(API.sendTextPush, params, className);
    }

    /**
     * 获取已经发布的优惠券
     *
     * @param token
     * @param page
     * @param per_page
     * @param no_end
     * @param status
     * @param className
     */
    public void getCouponForMySelf(String token, String page, String per_page, String no_end, String status, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("page", page);
        params.put("per_page", per_page);
        params.put("no_end", no_end);
        params.put("status", status);
        post(API.getCouponForMySelf, params, className);
    }

    /**
     * 获取已经发布的消息
     *
     * @param token
     * @param read
     * @param page
     * @param className
     */
    public void getTextPushed(String token, String read, String page, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("read", read);
        params.put("page", page);
        post(API.getTextPushed, params, className);
    }

    /**
     * 获取会员详情
     *
     * @param token
     * @param user_id
     * @param className
     */
    public void getMemberDetail(String token, String user_id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("user_id", user_id);
        post(API.getMemberDetail, params, className);
    }

    /**
     * 设置大屏开关机时间
     *
     * @param token
     * @param boot_time
     * @param shutdown_time
     * @param className
     */
    public void setScreenWorkTime(String token, String boot_time, String shutdown_time, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("boot_time", boot_time);
        params.put("shutdown_time", shutdown_time);
        post(API.setScreenWorkTime, params, className);
    }

    public void checkDeviceHasAd(String host, String className) {
        LogUtil.log("checkDeviceHasAd");
        host = "http://" + host + ":" + "7001/";
        RequestParams params = new RequestParams();
        HttpGetBase.newInstance().get(host, API.checkDeviceHasAd, params, className, responController);
    }

    public void getDeviceFilmList(String className) {
        String host = "http://" + SettingDao.getInstance().getLastDeviceIp() + ":" + "7001/";
        RequestParams params = new RequestParams();
        HttpGetBase.newInstance().get(host, API.getDeviceFilmList, params, className, responController);
    }

    /**
     * 获取banner图
     *
     * @param area_id
     * @param className
     */
    public void getBeannerPhoto(String area_id, String className) {
        RequestParams params = getTokenParma();
        params.put("area_id", area_id);
        post(API.getBeannerPhoto, params, className);
    }

    /**
     * 发送短信验证码2.0
     *
     * @param phone
     * @param type
     * @param className
     */
    public void getVerificationCode(String phone, String type, String className) {
        RequestParams params = getTokenParma();
        params.put("phone", phone);
        params.put("type", type);
        post(API.getVerificationCode, params, className);
    }

    /**
     * 重置密码
     *
     * @param phone
     * @param client_key
     * @param code
     * @param newpsw
     * @param className
     */
    public void setNewPwd(String phone, String client_key, String code, String newpsw, String className) {
        RequestParams params = getTokenParma();
        params.put("phone", phone);
        params.put("client_key", client_key);
        params.put("code", code);
        params.put("newpsw", newpsw);
        post(API.setNewPwd, params, className);
    }

    /**
     * 获取大屏上次设置过的时间
     *
     * @param token
     * @param className
     */
    public void getBitScreenLastSetup(String token, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        post(API.getBitScreenLastSetup, params, className);
    }

    /**
     *  获取大屏模板列表
     * @param token
     * @param className
     */
    public void getProTemplateList(String token, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        post(API.getProTemplateList, params, className);
    }

    /**
     * 获取历史模板
     * @param token
     * @param className
     */
    public void getHistoryTemplateList(String token, int page, int per_page, int no_end, int status, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("page", page);
        params.put("per_page", per_page);
        params.put("no_end", no_end);
        params.put("status", status);
        post(API.getHistoryTemplateList, params, className);
    }

    /**
     * 编辑项目上传
     * @param token
     * @param coupon_id
     * @param coupon_title
     * @param coupon_img
     * @param coupon_money
     * @param template_id
     * @param className
     */
    public void setEditTemplateList(String token, String coupon_id, String coupon_title, String coupon_img, String coupon_money,String template_id,String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("coupon_id", coupon_id);
        params.put("coupon_title", coupon_title);
        params.put("coupon_img", coupon_img);
        params.put("coupon_money", coupon_money);
        params.put("template_id", template_id);
        post(API.setEditTemplateList, params, className);
    }

    /**
     * 上传模板
     * @param token
     * @param coupon_title
     * @param coupon_img
     * @param coupon_money
     * @param template_id
     * @param className
     */
    public void setUpdataTemplateList(String token, String coupon_title, String coupon_img, String coupon_money, String template_id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("coupon_title", coupon_title);
        params.put("coupon_img", coupon_img);
        params.put("coupon_money", coupon_money);
        params.put("template_id", template_id);
        post(API.setUpdataTemplateList, params, className);
    }

    /**
     * 删除模板
     * @param token
     * @param coupon_id
     * @param className
     */
    public void setDeteleMuban(String token, String coupon_id,  String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("coupon_id", coupon_id);
        post(API.setDeteleMuban, params, className);
    }

    /**
     * 上存图片广告
     *
     * @param host
     * @param fileName
     * @param className
     */
    public void setUpdataPhotoAd(String host, List<String> fileName, String className) {
        LogUtil.log("------fileName"+fileName.size());
        RequestParams params = getTokenParma();
        try {
            for (int i = 1; i < fileName.size() + 1; i++) {
                params.put("img" + i, new File(fileName.get(i - 1)));
            }
            LogUtil.log("---------地址：" + host + ":10003/uploadAdImage");
            postNation(host + ":10003/uploadAdImage", params, className,":10003/uploadAdImage");
        } catch (FileNotFoundException e) {
            LogUtil.log("-----文件创建失败");
            e.printStackTrace();
        }
    }

    public void setUpdataVideo(String host, String file, String className) {
        RequestParams params = getTokenParma();
        try {
            params.put("file", new File(file));
            postNation(host + ":10003/uploadAdVideo", params, className, ":10003/uploadAdVideo");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



}
