package cn.meiqu.mich.httpGet;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import cn.meiqu.mich.activity.LoginActivity;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.ToastUtil;


public class HttpResponController implements IHttpResponListener {

    private static HttpResponController responController = new HttpResponController();
    public static int token_expiry = -10006;

    private HttpResponController() {
    }

    public static HttpResponController getInstance() {
        return responController;
    }

    @Override
    public void onHttpRespon(String action, String object) {
        // TODO Auto-generated method stub
        if (object != null) {
            if (JsonUtil.getStatus(object) == -10006) {
                Intent intent = new Intent(BaseApp.mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                BaseApp.mContext.startActivity(intent);
                LocalBroadcastManager.getInstance(BaseApp.mContext).sendBroadcast(new Intent(BaseActivity.action_exitApplication));
                ToastUtil.show(BaseApp.mContext, "请重新登录");
                return;
            }
        }
        Intent intent = new Intent(action);
        intent.putExtra("data", object);
        LocalBroadcastManager.getInstance(BaseApp.mContext).sendBroadcast(intent);
    }
}
