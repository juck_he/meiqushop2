package cn.meiqu.mich.httpGet;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;

import cn.meiqu.mich.API;
import cn.meiqu.mich.util.LogUtil;


public class HttpGetBase extends AsyncHttpResponseHandler {

    public IHttpResponListener httpResponListener;
    public String uri;
    public RequestParams params;
    public String action = "";
    public boolean isTcp = false;
    public String baseUrl;
    public static AsyncHttpClient client = new AsyncHttpClient();
    HttpGetBase() {

    }

    @Override
    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
        String object = null;
        try {
            object = new String(responseBody, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        LogUtil.log("服务器成功返回-url----" + uri + "--数据为---" + object);
        if (object != null && object.startsWith("\ufeff")) {
            object = object.substring(1);
        }
        if (isTcp) {
            LogUtil.log("------action111成功"+action + baseUrl);
            httpResponListener.onHttpRespon(action + baseUrl, object);
        }else {
            httpResponListener.onHttpRespon(action + uri, object);
        }
    }

    @Override
    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
        if (responseBody != null)
            LogUtil.log("服务器失败返回-url----" + uri + "--数据为---" + new String(responseBody));
        else
            LogUtil.log("服务器失败返回-url----" + uri + "--数据为---");
        if (isTcp) {
            LogUtil.log("------action失败"+action + baseUrl);
            httpResponListener.onHttpRespon(action + baseUrl, null);
        }else {
            httpResponListener.onHttpRespon(action + uri, null);
        }

    }



    public static HttpGetBase newInstance() {
        client.setTimeout(25000);
        return new HttpGetBase();
    }


    public void get(String uri, RequestParams params, String action,
                    IHttpResponListener httpResponListener) {
        this.uri = uri;
        this.isTcp =false;
        getBase(API.getAbsolutePath(uri), params, httpResponListener, action, true);
    }

    public void post(String uri, RequestParams params, String action,
                     IHttpResponListener httpResponListener) {
        this.uri = uri;
        this.isTcp = false;
        getBase(API.getAbsolutePath(uri), params, httpResponListener, action, false);
    }

    public void postNation(String uri, RequestParams params, String action,
    IHttpResponListener httpResponListener,String baseUrl) {
        this.uri = uri;
        this.isTcp = true;
        this.baseUrl= baseUrl;
        getBase("http://"+uri, params, httpResponListener, action, false);
    }


    public void get(String host, String uri, RequestParams params, String action,
                    IHttpResponListener httpResponListener) {
        this.uri = uri;
        getBase(host + uri, params, httpResponListener, action, true);
    }

    public void getBase(String url, RequestParams params, IHttpResponListener httpResponListene, String action, boolean isGet) {
        this.httpResponListener = httpResponListene;
        this.action = action;
        System.out.println("requestParam=" + params.toString());
        if (isGet)
            client.get(url, params, this);
        else
            client.post(url, params, this);
    }


    public void disConnect(){
        client.cancelAllRequests(true);
    }

}
