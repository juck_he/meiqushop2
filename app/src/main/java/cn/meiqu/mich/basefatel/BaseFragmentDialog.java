package cn.meiqu.mich.basefatel;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import cn.meiqu.mich.R;
import cn.meiqu.mich.dialogs.LoadingDialog;
import cn.meiqu.mich.util.ScreenUtil;


/**
 * Created by juck on 15/12/8.
 */
public abstract class BaseFragmentDialog extends DialogFragment {
    public LoadingDialog progressDialog = null;
    private View view = null;
    private ActionReceiver receiver;
    public int id;

    public BaseFragmentDialog() {
        // setStyle( 风格, 主题);
        // setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_Dialog);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.myDialogList);
        view = View.inflate(getActivity(), id, null);

        builder.setView(view, ScreenUtil.px2dip(getActivity(), 40), ScreenUtil.px2dip(getActivity(), 220), ScreenUtil.px2dip(getActivity(), 40), ScreenUtil.px2dip(getActivity(), 160));

        AlertDialog alertDialog = builder.create();
        //
//        Window window = alertDialog.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//        wlp.gravity = Gravity.CENTER;
//        wlp.height = ScreenUtil.dip2px(getActivity(),515);
//        window.setAttributes(wlp);
        //
        initData();
        return alertDialog;
    }

    public void showProgressDialog(String content) {
        if (progressDialog == null) {
            progressDialog = new LoadingDialog(getActivity());
        }
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

//    public abstract View initView(int id);

    public abstract void initData();

    public View findViewById(int id) {
        return view.findViewById(id);
    }

    public void initReceiver(String[] filters) {
        receiver = new ActionReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : filters) {
            filter.addAction(action);
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
    }

    class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            onHttpHandle(action, data);
        }
    }

    protected abstract void onHttpHandle(String action, String data);

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressDialog = null;
    }
}

