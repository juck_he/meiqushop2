package cn.meiqu.mich.broadcast;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.meiqu.mich.activity.ForgetPwdActivity;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/10/20.
 */
public class SmsObserver  extends ContentObserver {

    private Context mContext;
    private Handler mHandler;

    public SmsObserver(Context context, Handler handler) {
        super(handler);
        mContext = context;
        mHandler = handler;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);

        LogUtil.log( "SMS has changed!");
        LogUtil.log(uri.toString());

        String code = "";

        if (uri.toString().equals("content://sms/raw")) {
            return;
        }

        Uri inboxUri = Uri.parse("content://sms/inbox");
        Cursor c = mContext.getContentResolver().query(inboxUri, null, null, null, "date desc");
        if (c != null) {
            if (c.moveToFirst()) {
                String address = c.getString(c.getColumnIndex("address"));
                String body = c.getString(c.getColumnIndex("body"));

                if (!address.equals("10657120593533")) {
                    return;
                }

                LogUtil.log("发件人为：" + address + " " + "短信内容为：" + body);
                //TODO 连续6个数字
                Pattern pattern = Pattern.compile("(\\d{6})");
                Matcher matcher = pattern.matcher(body);

                if (matcher.find()) {
                    code = matcher.group(0);
                    LogUtil.log("code is " + code);
                    mHandler.obtainMessage(ForgetPwdActivity.MSG_RECEIVED_CODE, code).sendToTarget();
                }

            }
            c.close();
        }

    }
}