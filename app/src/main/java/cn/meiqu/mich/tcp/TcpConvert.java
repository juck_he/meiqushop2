package cn.meiqu.mich.tcp;

import com.google.gson.Gson;

import java.io.InputStream;

import cn.meiqu.mich.util.LogUtil;

public class TcpConvert {

    public TcpCommand convert(InputStream is) throws Exception {
        final TcpCommand tcpCommand = new TcpCommand();
        Gson mGson = new Gson();
        byte[] byteBegin = new byte[TcpCommand.Command_Begin_Length];
        byte[] byteLength = new byte[TcpCommand.Command_Content_Length];
        byte[] byteContent;
        if (is != null) {
            int i = is.read(byteBegin);
            if (i != -1) {
                // System.out.println("接收到的指令----" + new String(byteBegin,
                // "utf-8"));
                is.read(byteLength);
                int length = 0;
                try {
                    length = Integer.parseInt(new String(byteLength, "utf-8"));
                    byteContent = new byte[length];
                    // System.out.println("接收到的指令长度----" + length);
                } catch (Exception e) {
                    System.out.println("接收到的指令长度有错----");
                    e.printStackTrace();
                    tcpCommand.begin = TcpCommand.B_ResponErro;
                    return tcpCommand;
                }
                is.read(byteContent);
                String begin = new String(byteBegin, "utf-8");
                String common = new String(byteContent, "utf-8");
                tcpCommand.begin = begin;
                try {
                    tcpCommand.content = mGson.fromJson(common, TcpContent.class);
                } catch (Exception e) {
                    System.out.println("接收到的指令Json数据有错---" + common);
                    e.printStackTrace();
                }
                LogUtil.log("接收到的指令" + "_begin_" + begin + "_length_"
                        + length + "_content_" + tcpCommand.content);
                System.gc();
            } else {
                return null;
            }
        }
        return tcpCommand;
    }
}
