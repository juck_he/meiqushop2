package cn.meiqu.mich.tcp;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cn.meiqu.mich.util.LogUtil;

class CheckUpLoadTimeOutRunnable implements Runnable {
    @Override
    public void run() {
        // TODO Auto-generated method stub
//		BaseApplication.getCurrentActivity().iActivityTcpRespon
//				.onRespon_FClose(null);
    }
}

/**
 *
 */
public class TcpSendController {
    public static CheckUpLoadTimeOutRunnable checkUpLoadTimeOutRunnable = new CheckUpLoadTimeOutRunnable();
    static TcpSendController sendController = new TcpSendController();

    private TcpSendController() {
    }

    public static TcpSendController getInstance() {
        return sendController;
    }

    Gson mGson = new Gson();


    public void sendLogin() {
        writeCommand(TcpCommand.H_Login, false);
    }

    public void sendLoginSync() {
        writeCommand(TcpCommand.H_Login, true);
    }

    /**
     * @param path
     */
    public void sendFilmStart(String path) {
        writeCommand(TcpCommand.H_ControllerStart, path, false);
    }

    public void sendFilmForward(String path) {
        writeCommand(TcpCommand.H_ControllerForward, path, false);
    }

    public void sendFilmBack(String path) {
        writeCommand(TcpCommand.H_ControllerBack, path, false);
    }

    public void sendFilmClose(String path) {
        writeCommand(TcpCommand.H_CloseSocket, path, false);
    }

    public void sendSoundUp() {
        write(TcpCommand.H_SoundUp, false);
    }

    public void sendSoundDown() {
        write(TcpCommand.H_SoundDown, false);
    }

    public void sendSoundOff() {
        write(TcpCommand.H_SoundOff, false);
    }

    public void sendLoginPlayAd() {
        writeCommand(TcpCommand.H_PlayAd, true);
    }

    /**
     * @param level 0到12
     */
    public void sendSoundLevel(int level) {
        writeCommand(TcpCommand.H_SoundLevel, level, false);
    }

    public void sendFeitu(String imageName) {
        writeCommand(TcpCommand.F_ShowImage, imageName, false);
    }

    public void sendUpLoadImageHttp(String imageName) {
        writeCommand(TcpCommand.F_ShowImageHttp, imageName, false);
    }

//    public void sendUpLoadImage(String imageName, Bitmap bitmap) {
//        byte[] b = ImageUtil.bitmapToByte(ImageUtil.compressImageByMaxSize(bitmap, 400));
//        sendUpLoadImage(imageName, b);
//    }

    byte[] imageByte = null;

    public void sendUpLoadImage(final String imageName, byte[] bitmapByte) {
        imageByte = bitmapByte;
        new Thread(new Runnable() {
            @Override
            public void run() {
                writeCommand(TcpCommand.F_UploadImage, imageName, imageByte.length + "", true);
                writeSync(imageByte);
            }
        }).start();
    }

    public void sendLevelImage() {
        writeCommand(TcpCommand.F_LeaveImageActivity, false);
    }

    public void sendHeartBeat() {
        writeCommand(TcpCommand.X_Send, false);
    }

    public void writeCommand(String commandBegin, boolean isSync) {
        writeCommand(commandBegin, "", isSync);
    }

    public void writeCommand(String commandBegin, String commandMediaName, String lenght, boolean isSync) {
        TcpCommand respon = new TcpCommand();
        respon.begin = commandBegin;
//        respon.content.user_id = new SharePreUser(BaseApp.mContext).getUserInfo().UserID;
//        respon.content.user_name = new SharePreUser(BaseApp.mContext).getUserInfo().UserName;
        respon.content.media_name = commandMediaName;
        respon.content.content = lenght;
        write(respon, isSync);
    }

    public void writeCommand(String commandBegin, String commandMediaName, boolean isSync) {
        TcpCommand respon = new TcpCommand();
        respon.begin = commandBegin;
//        respon.content.user_id = new SharePreUser(BaseApp.mContext).getUserInfo().UserID;
//        respon.content.user_name = new SharePreUser(BaseApp.mContext).getUserInfo().UserName;
        respon.content.media_name = commandMediaName;
        write(respon, isSync);
    }

    public void writeCommand(String commandBegin, int soundLevel, boolean isSync) {
        TcpCommand respon = new TcpCommand();
        respon.begin = commandBegin;
//        respon.content.user_id = new SharePreUser(BaseApp.mContext).getUserInfo().UserID;
//        respon.content.user_name = new SharePreUser(BaseApp.mContext).getUserInfo().UserName;
        respon.content.volumeLevel = soundLevel + "";
        write(respon, isSync);
    }

    public void write(TcpCommand command, boolean isSync) {
        String length;
        try {
            length = mGson.toJson(command.content).getBytes("utf-8").length
                    + "";
            while (length.length() < TcpCommand.Command_Content_Length) {
                length = "0" + length;
            }
            write(command.begin, length, command.content, isSync);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void write(String begin, String length, TcpContent content, boolean isSync) {
        String command = begin + length + mGson.toJson(content);
        write(command, isSync);
    }

    public void write(String command, boolean isSync) {
        LogUtil.log("发送指令---" + command.toString());
        try {
            if (isSync) {
                writeSync(command.getBytes("utf-8"));
            } else
                write(command.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public void writeSync(byte[] command) {
        try {
            if (TcpSocket.getInstance().mSocket != null) {
                TcpSocket.getInstance().mSocket.getOutputStream().write(command);
            } else {
                TcpSocket.getInstance().connect();
                if (TcpSocket.getInstance().mSocket != null) {
                    sendLoginSync();
                    TcpSocket.getInstance().mSocket.getOutputStream().write(command);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(final byte[] command) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                writeSync(command);
            }
        }).start();

    }
}
