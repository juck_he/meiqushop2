package cn.meiqu.mich.tcp;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;

import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.util.LogUtil;


public class TcpResponController {
    static TcpResponController responController = new TcpResponController();
    public static String action_tcp_respon = "action_tcp_respon";
    public static String data_begin = "data";
    public static String data_content = "data_data_content";

    private TcpResponController() {

    }

    public static TcpResponController getInstance() {
        return responController;
    }

    public void onResponse(final TcpCommand command, final byte[] b) {
        if (command == null) {
            handleDisConnect();
        } else {
            if (command.begin.equals(TcpCommand.H_Login)) {
                handleLoginSucceed(command);
            } else if (command.begin.equals(TcpCommand.F_RequstImage)) {
                handleUploadImage(command);
            } else if (command.begin.equals(TcpCommand.F_ImageSucceed) || command.begin.equals(TcpCommand.F_UploadImage)) {
                handleFeituSucceed(command);
            } else if (command.begin.equals(TcpCommand.B_ResponFailure)) {
                TcpSocket.getInstance().close();
                handleDisConnect();
            } else {
                sendBroadCase(command.begin, new Gson().toJson(command.content));
            }
        }
    }

    public void sendBroadCase(String begin, String content) {
        Intent intent = new Intent();
        intent.setAction(action_tcp_respon);
        intent.putExtra(data_begin, begin);
        intent.putExtra(data_content, content);
        LocalBroadcastManager.getInstance(BaseApp.mContext)
                .sendBroadcast(intent);
    }

    public void handleConnectFailure() {
        sendBroadCase(TcpCommand.H_ConnectFailure, null);
    }

    public void handleConnectSucceed() {
        sendBroadCase(TcpCommand.H_ConnectSucceed, null);
    }

    public void handleDisConnect() {
        TcpSocket.getInstance().close();
        sendBroadCase(TcpCommand.B_ResponFailure, null);
    }

    public void handleLoginSucceed(TcpCommand command) {
        sendBroadCase(TcpCommand.H_Login, null);
    }

    public void handleFeituSucceed(TcpCommand command) {
        sendBroadCase(TcpCommand.F_ImageSucceed, null);
    }

    public void handleUploadImage(TcpCommand command) {
        LogUtil.log("接收到上传图片指令");
        sendBroadCase(TcpCommand.F_UploadImage, command.content.media_name);
    }
}
