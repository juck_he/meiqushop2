package cn.meiqu.mich.tcp;

public class TcpContent {
	public String user_id = "";
	public String user_name = "";
	public String media_name = "";
	public String content = "";
	public String volumeLevel = "";

	@Override
	public String toString() {
		return "TcpCommon [user_id=" + user_id + ", user_name=" + user_name
				+ ", media_name=" + media_name + ", content=" + content + "]";
	}

}
