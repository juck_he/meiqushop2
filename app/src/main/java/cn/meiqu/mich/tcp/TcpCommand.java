package cn.meiqu.mich.tcp;

public class TcpCommand {
    // tcp
    public static final int Command_Begin_Length = 4;
    public static final int Command_Content_Length = 10;
    // 心跳包
    public static final String X_Send = "X000";
    // 每屏
    public static final String B_ResponSucceed = "B000";
    public static final String B_ResponFailure = "B001";
    public static final String B_ResponErro = "B002";
    // 发型师
    public static final String F_Close = "F999";
    public static final String F_Login = "F000";
    public static final String F_ShowImage = "F001";
    public static final String F_RequstImage = "F002";
    public static final String F_UploadImage = "F003";
    public static final String F_LeaveImageActivity = "F004";
    public static final String F_ImageSucceed = "F005";
    public static final String F_ShowImageHttp = "F006";
    // 会员
    public static final String H_Close = "H999";
    public static final String H_Login = "H000";
    public static final String H_ControllerEnter = "H001";
    public static final String H_ControllerStart = "H002";
    public static final String H_ControllerForward = "H003";
    public static final String H_ControllerBack = "H004";
    public static final String H_MediaComplete = "H005";
    public static final String H_CloseSocket = "H006";
    public static final String H_PlayAd = "H007";
    public static final String H_SoundUp = "H008";
    public static final String H_SoundDown = "H009";
    public static final String H_SoundOff = "H011";
    public static final String H_SoundLevel = "H010";
    public static final String H_ConnectFailure = "-111H";
    public static final String H_ConnectSucceed = "-000H";
    // 店长
    public static final String D_Close = "D999";
    public static final String D_Login = "D000";
    public static final String D_Bind = "D001";
    public TcpContent content = new TcpContent();
    public String begin = "";
}
