package cn.meiqu.mich.tcp;


import java.io.IOException;
import java.net.Socket;

import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.StringUtil;


public class TcpSocket {
    public Socket mSocket;
    public String ip;
    private int port = 8001;
    public static TcpSocket tcpSocket = new TcpSocket();
    private boolean isClosed = false;
    private int timeOut = 10 * 1000;
    int heart = 50 * 1000;

    public static TcpSocket getInstance() {
        return tcpSocket;
    }

    public CheckUpLoadTimeOutRunnable checkUpLoadTimeOutRunnable = new CheckUpLoadTimeOutRunnable();
    HeartBeatRunnable heartBeatRunnable = new HeartBeatRunnable();

    class CheckUpLoadTimeOutRunnable implements Runnable {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            TcpResponController.getInstance().handleConnectFailure();
        }
    }

    class HeartBeatRunnable implements Runnable {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            TcpSendController.getInstance().sendHeartBeat();
            BaseApp.mHandler.postDelayed(this, heart);
        }
    }

    /**
     *
     * @param ip
     */
    public void connect(final String ip) {

        this.ip = ip;
        SettingDao.getInstance().setLastDeviceIp(ip);
        new Thread(new Runnable() {
            @Override
            public void run() {
                connect();
                TcpSendController.getInstance().sendLoginSync();
                TcpSendController.getInstance().sendLoginPlayAd();
            }
        }).start();
    }

    public void connect() {
        if (!StringUtil.isEmpty(ip)) {
            LogUtil.log("正在连接ip=" + ip);
            try {
                close();
                BaseApp.mHandler.removeCallbacks(checkUpLoadTimeOutRunnable);
                BaseApp.mHandler.postDelayed(checkUpLoadTimeOutRunnable, timeOut);
                mSocket = new Socket(ip, port);
                BaseApp.mHandler.removeCallbacks(checkUpLoadTimeOutRunnable);
                mSocket.setSoLinger(true, 0);
                mSocket.setKeepAlive(true);
                new ReadThread().start();
                BaseApp.mHandler.removeCallbacks(heartBeatRunnable);
                BaseApp.mHandler.postDelayed(heartBeatRunnable, heart);
                TcpResponController.getInstance().handleConnectSucceed();
                LogUtil.log("连接ip=" + ip + "成功");
            } catch (Exception e) {
                e.printStackTrace();
                TcpResponController.getInstance().handleConnectFailure();
                close();
            }
        } else
            TcpResponController.getInstance().handleConnectFailure();

    }

    public boolean isConnected() {
        return mSocket != null;
    }

    public void close() {
        BaseApp.mHandler.removeCallbacks(checkUpLoadTimeOutRunnable);
        isClosed = true;
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mSocket = null;
        }
        BaseApp.mHandler.removeCallbacks(heartBeatRunnable);
    }

    class ReadThread extends Thread {
        public void run() {
            while (true) {
                try {
                    if (mSocket != null) {
                        TcpCommand command = new TcpConvert().convert(mSocket.getInputStream());
                        if (command == null) {
                            break;
                        } else if (command.begin.equals(TcpCommand.B_ResponErro)) {
                            break;
                        } else {
                            TcpResponController.getInstance().onResponse(command, null);
                        }
                    } else {
                        break;
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    if (!isClosed) {
                        LogUtil.log("-----------Exception e");
                        TcpResponController.getInstance().handleDisConnect();
                    }
                    break;
                }
            }
            close();
            System.gc();
        }
    }

}
