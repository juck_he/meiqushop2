package cn.meiqu.mich.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.shopad.async.ImageLoadAsync;
import cn.meiqu.mich.activity.shopad.async.MediaAsync;
import cn.meiqu.mich.activity.shopad.async.VideoLoadAsync;
import cn.meiqu.mich.activity.shopad.video.FragmentVideo;
import cn.meiqu.mich.bean.MediaModel;

/**
 * Created by Administrator on 2015/11/25.
 */
public class GridViewAdapter extends ArrayAdapter<MediaModel> {
    public FragmentVideo videoFragment;

    private Context mContext;
    private List<MediaModel> mGalleryModelList;
    private int mWidth;
    private boolean mIsFromVideo;

    public GridViewAdapter(Context context, int resource, List<MediaModel> categories, boolean isFromVideo) {
        super(context, resource, categories);
        mGalleryModelList = categories;
        mContext          = context;
        mIsFromVideo      = isFromVideo;
    }

    public int getCount() {
        return mGalleryModelList.size();
    }

    @Override
    public MediaModel getItem(int position) {
        return mGalleryModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
            mWidth = display.getWidth();  // deprecated
            //
            LayoutInflater viewInflater;
            viewInflater = LayoutInflater.from(getContext());
            convertView = viewInflater.inflate(R.layout.view_vedio_item, parent, false);
            //
            holder = new ViewHolder();
            holder.checkBoxTextView   = (ImageView) convertView.findViewById(R.id.checkTextViewFromMediaChooserGridItemRowView);
            holder.imageView          = (ImageView) convertView.findViewById(R.id.imageViewFromMediaChooserGridItemRowView);
            //
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //
        RelativeLayout.LayoutParams imageParams = (RelativeLayout.LayoutParams) holder.imageView.getLayoutParams();
        imageParams.width  = mWidth/2;
        imageParams.height = mWidth/2;

        holder.imageView.setLayoutParams(imageParams);

        if(mIsFromVideo){
            new VideoLoadAsync(videoFragment, holder.imageView, false, mWidth/2).executeOnExecutor(MediaAsync.THREAD_POOL_EXECUTOR, mGalleryModelList.get(position).url.toString());
        }else{

            ImageLoadAsync loadAsync = new ImageLoadAsync(mContext, holder.imageView, mWidth/2);
            loadAsync.executeOnExecutor(MediaAsync.THREAD_POOL_EXECUTOR, mGalleryModelList.get(position).url);
        }
        //
        if(mIsFromVideo) {
            holder.checkBoxTextView.setVisibility(View.GONE);
        }else {
            holder.checkBoxTextView.setVisibility(View.VISIBLE);
            if (mGalleryModelList.get(position).status){
                holder.checkBoxTextView.setImageResource(R.drawable.image_check);
                holder.imageView.setColorFilter(Color.parseColor("#77000000"));
            }else {
                holder.checkBoxTextView.setImageResource(R.drawable.image_uncheck);
                holder.imageView.setColorFilter(null);
            }
        }
        return convertView;
    }

    class ViewHolder {
        ImageView imageView;
        ImageView checkBoxTextView;
    }

}
