package cn.meiqu.mich.activity.inshopmanege;

import android.os.Bundle;

import com.umeng.analytics.MobclickAgent;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;


/**
 * Created by Administrator on 2015/9/16.
 */
public class ActivityManage extends BaseActivity {
    private ManageFragment mManageFragment;

    @Override
    public void onHttpHandle(String action, String data) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        initFragment();
    }

    @Override
    public void initFragment() {
        mManageFragment  = new ManageFragment();
        showFirst(mManageFragment);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onResume(this);
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPause(this);
    }


}
