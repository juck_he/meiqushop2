package cn.meiqu.mich.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.shopad.async.ImageLoadAsync;
import cn.meiqu.mich.activity.shopad.async.MediaAsync;
import cn.meiqu.mich.bean.ImageAd;

/**
 * Created by Administrator on 2015/11/25.
 */
public class MediaGridViewAdapter extends ArrayAdapter<ImageAd> {

    //public VideoFragment videoFragment;

    private Context mContext;
    private List<ImageAd> mediaFilePathList;
    private int mWidth;

    public MediaGridViewAdapter(Context context, int resource, List<ImageAd> filePathList) {
        super(context, resource, filePathList);
        mediaFilePathList = filePathList;
        mContext = context;
    }

    public int getCount() {
        return mediaFilePathList.size();
    }

    @Override
    public ImageAd getItem(int position) {
        return mediaFilePathList.get(position);
    }

    /**
     * 添加数据
     *
     * @param mediaFile
     */
    public void addAll(List<ImageAd> mediaFile) {
        if (mediaFile != null) {
            int count = mediaFile.size();
            //mediaFilePathList.clear();
//            for (int i = 0; i < count; i++) {
//                for (int j = 0; j < mediaFilePathList.size(); j++) {
//                    if (mediaFilePathList.get(j).filePathList.equals(mediaFile.get(i).filePathList)) {
//                        break;
//                    }
//                    if (j == (mediaFilePathList.size() - 1)) {
//                        mediaFilePathList.add(mediaFile.get(i));
//                    }
//                }
//            }
            mediaFilePathList.clear();
            for (int i = 0; i < count; i++) {
                mediaFilePathList.add(mediaFile.get(i));
            }

//            //添加到最后
//            for (int i = 0; i < mediaFilePathList.size(); i++) {
//                if (mediaFilePathList.get(i).filePathList.equals("isGetPhoto")) {
//                    mediaFilePathList.remove(i);
//                    break;
//                }
//            }
            mediaFilePathList.add(new ImageAd("isGetPhoto", true));
        }
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            //
            Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
            mWidth = display.getWidth();
            //
            LayoutInflater viewInflater;
            viewInflater = LayoutInflater.from(mContext);
            //
            convertView = viewInflater.inflate(R.layout.view_photo_item, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageViewFromGridItemRowView);
//            holder.nameTextView = (TextView) convertView.findViewById(R.id.nameTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        RelativeLayout.LayoutParams imageParams = (RelativeLayout.LayoutParams) holder.imageView.getLayoutParams();
        imageParams.width = mWidth / 3;
        imageParams.height = mWidth / 3;
        //
        holder.imageView.setLayoutParams(imageParams);
        String filePathList = mediaFilePathList.get(position).filePathList;
        if (filePathList.equals("isGetPhoto")) {
            holder.imageView.setImageResource(R.drawable.i_get_photo);
        } else {
            File mediaFile = new File(filePathList);
            //
            if (mediaFile.exists()) {
                if (mediaFile.getPath().contains("mp4") || mediaFile.getPath().contains("wmv") ||
                        mediaFile.getPath().contains("avi") || mediaFile.getPath().contains("3gp")) {
                    holder.imageView.setImageBitmap(null);
                    holder.imageView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.i_def_logo));
                }
                //
                else {
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inPurgeable = true;
//                    options.inSampleSize = 2;
//                    Bitmap myBitmap = BitmapFactory.decodeFile(mediaFile.getAbsolutePath(), options);
//                    holder.imageView.setImageBitmap(myBitmap);

                    ImageLoadAsync loadAsync = new ImageLoadAsync(mContext, holder.imageView, mWidth/3);
                    loadAsync.executeOnExecutor(MediaAsync.THREAD_POOL_EXECUTOR,mediaFile.getAbsolutePath());
                }
                //
//                holder.nameTextView.setText(mediaFile.getName());
            }
        }
        return convertView;
    }


    class ViewHolder {
        ImageView imageView;
        TextView nameTextView;
    }
}