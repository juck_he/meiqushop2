package cn.meiqu.mich.activity.me.membermanger;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.Fatel.utils.ImageLoadHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.ListMemberDetaiAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.MemberDetail;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.view.CircleImageView;

/**
 * Created by Administrator on 2015/10/12.
 */
public class FragmentMemberDetail extends BaseFragment {
    String className = getClass().getName();
    String action_getMemberDetail = className + API.getMemberDetail;

    private Context mContext;
    private static String userId;
    MemberDetail.InfoEntity userData ;
    List<String > list;
    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getMemberDetail)) {
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "网络连接失败...");
            } else {
                handlerData(data);
                LogUtil.log("--------会员详情信息"+data);
            }
        }
    }

    private void handlerData(String data) {
        if (data != null) {
            dismissMessage();
        } else {
            showNoMessage("没有内容哦!");
        }
        MemberDetail memberDetail = new Gson().fromJson(data, MemberDetail.class);
        userData = memberDetail.info;
        //显示用户头像
        View hairView = View.inflate(mContext, R.layout.item_hair_member_datail, null);
        CircleImageView hairImage = (CircleImageView) hairView.findViewById(R.id.civ_hair_icon);
        //Glide.with(mContext).load(userData.head_pic).into(hairImage);
        ImageLoadHelper.displayImage(userData.head_pic,hairImage);
        mListMemberDetail.addHeaderView(hairView);
        //把数据存放在list集合里面
        list = new ArrayList<String>();
        list.add(userData.user_name);
        list.add(userData.sex);
        list.add(userData.telephone);
        list.add(userData.total_money);
        list.add(userData.birth);
        list.add(userData.follow_time);
        list.add(userData.address);
        list.add(userData.province+" "+userData.city+" "+userData.area);
        list.add(userData.tags);
        //显示listview
        mListMemberDetail.setAdapter(new ListMemberDetaiAdapter(mContext,list));
    }

    public static FragmentMemberDetail newInstance(String user_id) {
        userId = user_id;
        FragmentMemberDetail fragmentMemberDetail = new FragmentMemberDetail();
        return fragmentMemberDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mContext = getActivity();
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_menber_detail, null);
            assignViews();
        }
        //todo 发送Http请求获取会员详情信息
        reqestHttp();
        return contain;
    }

    private void reqestHttp() {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getMemberDetail(token,userId,className);
    }

    private ListView mListMemberDetail;

    private void assignViews() {
        initTitle("会员信息");
        mListMemberDetail = (ListView) findViewById(R.id.list_member_detail);
    }

    private void initReceiver() {
        initReceiver(new String[]{action_getMemberDetail});
    }
}
