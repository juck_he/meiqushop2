package cn.meiqu.mich.activity.me.device;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.ListDeviceAdapter;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Device;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.dialogs.SetDeviceOnlineDialog;
import cn.meiqu.mich.httpGet.HttpGetBase;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.NetWrokUtils;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.util.WifiUtil;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Fatel on 15-4-9.
 */
public class FragmentDeviceList extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener, SetDeviceOnlineDialog.OnDialogClickListener {
    public String className = getClass().getName();
    public String action_getDeviceList = className + API.get_company_devices;
    ArrayList<Device> devices = new ArrayList<Device>();
    ListDeviceAdapter adapter;
    Device currentDevice;
    Handler mHandler;

    public static FragmentDeviceList newInstance() {
        FragmentDeviceList fragmentDeviceList = new FragmentDeviceList();
        return fragmentDeviceList;
    }


    //
    private ListView mListDevice;
    String ssid;

    private void assignViews() {
        //
        initTitle("美屏列表");
        setSwipeRefresh(R.id.swiperRef_device, this);
        mListDevice = (ListView) findViewById(R.id.list_device);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_device_list, null);
            assignViews();
            initDeviceList();
            requestDeviceList();
        } else {
            ((ViewGroup) contain.getParent()).removeView(contain);
        }
        startRefreshTimer();
        initReceiver();
        return contain;
    }

    public void initDeviceList() {
        adapter = new ListDeviceAdapter(getActivity(), devices);
        mListDevice.addHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.list_device_top, null));
        mListDevice.setAdapter(adapter);
        mListDevice.setOnItemClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopRefreshTimer();
        unregisterWifi();
    }

    public void unregisterWifi() {
        if (wifiChangeReceiver != null) {
            try {
                getActivity().unregisterReceiver(wifiChangeReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void initReceiver() {
        initReceiver(new String[]{action_getDeviceList});
    }


    Timer timer = new Timer();

    public void startRefreshTimer() {
        if (mHandler == null) {
            mHandler = new Handler();
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (NetWrokUtils.isNetAvailable(BaseApp.mContext)) {
                            HttpGetController.getInstance().getCompanyDevices(FragmentDeviceList.this.getClass());
                        }
                    }
                });

            }
        }, 10 * 1000, 10 * 1000);
    }

    public void stopRefreshTimer() {
        timer.cancel();
    }

    public void requestDeviceList() {
        showProgressDialog("");
        HttpGetController.getInstance().getCompanyDevices(getClass());
    }

    boolean isFirst = true;

    public void handleDeviceList(String data) {
        if (!isConnect) {
            setSwipeRefreshing(false);
            dismissProgressDialog();
        }
        if (data == null) {
            if (isFirst)
                ToastUtil.showNetWorkFailure(getActivity());
            isFirst = false;
        } else {
            if (JsonUtil.getStatusLegal(data)) {
                String deviceData = JsonUtil.getJsonObject(data).optJSONObject("info").optString("device_list");
                JSONObject wifiInfo = JsonUtil.getJsonObject(data).optJSONObject("info").optJSONObject("wifi_info");
                if (wifiInfo != null) {
                    SettingDao.getInstance().setWifiSSid(wifiInfo.optString("wifi_ssid"));
                    SettingDao.getInstance().setWifiPwd(wifiInfo.optString("wifi_pwd"));
                }
                ArrayList<Device> temp = new Gson().fromJson(deviceData, new TypeToken<ArrayList<Device>>() {
                }.getType());
                if (temp.isEmpty()) {
                    toast("没有找到设备");
                } else {
                    devices.clear();
                    devices.addAll(temp);
                    adapter.notifyDataSetChanged();
                }
            } else {
                toast("请求失败");
            }
        }
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getDeviceList)) {
            handleDeviceList(data);
        }
    }


    @Override
    public void onRefresh() {
        requestDeviceList();
    }

    SetDeviceOnlineDialog setDeviceOnlineDialog;

    public void showSetDeviceOnLineDialog() {
        if (setDeviceOnlineDialog == null) {
            setDeviceOnlineDialog = new SetDeviceOnlineDialog(getActivity());
            setDeviceOnlineDialog.setOnDialogClickListener(this);
        }
        if (!setDeviceOnlineDialog.isShowing()) {
            setDeviceOnlineDialog.show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            Device device = devices.get(position - 1);
            if (device.status == 0) {
                currentDevice = device;
                showSetDeviceOnLineDialog();
            }
        }
    }

    WifiChangeReceiver wifiChangeReceiver;

    public void registerWifiReceiver() {
        unregisterWifi();
        wifiChangeReceiver = new WifiChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiChangeReceiver.net_connect);
        filter.addAction(WifiChangeReceiver.wifi_change);
        getActivity().registerReceiver(
                wifiChangeReceiver, filter);
    }

    String input_ssid;
    String input_pwd;
    boolean isConnect = false;

    @Override
    public void onClickCommit(boolean b) {
        isConnect = true;
        LogUtil.log(currentDevice.device_number);
        if (b) {
            input_ssid = setDeviceOnlineDialog.getSSid();
            input_pwd = setDeviceOnlineDialog.getPwd();
            if (StringUtil.isEmpty(input_ssid)) {
                toast("wifi名字不能为空");
            } else {
                showProgressDialog("");
                ssid = "meiqu" + currentDevice.device_number.replace(":", "");
                String pwd = "michzz1408";
                String type = "2";
                int netId = WifiUtil.isConfiguration(ssid);
                boolean conectSuccceed = false;
                if (netId == -1) {
                    netId = WifiUtil.addWifiConfig(ssid, pwd, type);
                    LogUtil.log("netId=" + netId);
                    conectSuccceed = WifiUtil.connectWifi(netId);
                } else {
                    if (WifiUtil.getCurrentNetId() == netId) {
                        setDeviceWifi(input_ssid, input_pwd);
                    } else {
                        conectSuccceed = WifiUtil.connectWifi(netId);
                        LogUtil.log("conectSuccceed=" + conectSuccceed);
                    }
                }
                registerWifiReceiver();
                mHandler.postDelayed(checkConnectTimeOutRunable, 20 * 1000);
                if (conectSuccceed) {
                    toast("连接中，请稍后");
                } else {
                    dismissProgressDialog();
                }
            }
        } else {
            setDeviceOnlineDialog.dismiss();
        }
    }

    CheckConnectTimeOutRunable checkConnectTimeOutRunable = new CheckConnectTimeOutRunable();

    class CheckConnectTimeOutRunable implements Runnable {

        @Override
        public void run() {
            setFailure();
        }
    }

    public void setDeviceFinish() {
        unregisterWifi();
        dismissProgressDialog();
        setDeviceOnlineDialog.dismiss();
    }

    public void setDeviceWifi(String wifi_ssid, String wifi_pwd) {
        showProgressDialog("");
        mHandler.removeCallbacks(checkConnectTimeOutRunable);
        currentDevice.status = 2;
        adapter.notifyDataSetChanged();
        RequestParams params = new RequestParams();
        params.put("wifi_ssid", wifi_ssid);
        params.put("wifi_pwd", wifi_pwd);
        LogUtil.log(WifiUtil.getLocalIPAddress());
        HttpGetBase.client.get("http://192.168.43.1:7001/" + API.set_device_info, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                setSucceed();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                setFailure();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                setFailure();
            }

            @Override
            public void onFinish() {
                super.onFinish();

            }
        });
//        HttpGetBase.client.get("http://192.168.43.1:7001/" + API.set_device_info, params, new JsonHttpResponseHandler(){
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                super.onSuccess(statusCode, headers, response);
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                super.onFailure(statusCode, headers, throwable, errorResponse);
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                super.onFailure(statusCode, headers, throwable, errorResponse);
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                super.onFailure(statusCode, headers, responseString, throwable);
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                super.onSuccess(statusCode, headers, responseString);
//            }
//        });
    }

    public void setSucceed() {
        setDeviceFinish();
        toast("设置成功");
        isConnect = false;
        LogUtil.log("onFinish");
    }

    public void setFailure() {
        setDeviceFinish();
        toast("设置失败");
        currentDevice.status = 0;
        adapter.notifyDataSetChanged();
    }

    public class WifiChangeReceiver extends BroadcastReceiver {
        public static final String net_connect = WifiManager.NETWORK_STATE_CHANGED_ACTION;
        public static final String wifi_change = WifiManager.WIFI_STATE_CHANGED_ACTION;

        @Override
        public void onReceive(Context context, Intent intent) {
            // 监听WIFI状态变化
            if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {// wifi打开与否
            } else if (intent.getAction().equals(
                    WifiManager.NETWORK_STATE_CHANGED_ACTION)) {// wifi连接上与否
                NetworkInfo info = intent
                        .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info.getState().equals(NetworkInfo.State.DISCONNECTED)) {
                    LogUtil.log("wifi网络连接断开");
                } else if (info.getState().equals(NetworkInfo.State.CONNECTED)) {
                    if (isConnect) {
                        setDeviceWifi(input_ssid, input_pwd);
                        LogUtil.log("连接成功");
                    }
                }


            }
        }


    }

}
