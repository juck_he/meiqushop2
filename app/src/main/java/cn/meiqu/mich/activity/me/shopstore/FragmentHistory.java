package cn.meiqu.mich.activity.me.shopstore;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.MubanHistoryAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.MubanHistory;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.view.superrecyclerview.OnMoreListener;
import cn.meiqu.mich.view.superrecyclerview.SuperRecyclerView;
import cn.meiqu.mich.view.superrecyclerview.swipe.SparseItemRemoveAnimator;
import cn.meiqu.mich.view.superrecyclerview.swipe.SwipeDismissRecyclerViewTouchListener;


/**
 * Created by juck on 15/12/8.
 */
public class FragmentHistory extends BaseFragment implements MubanHistoryAdapter.OnRecycleClickListener, SwipeRefreshLayout.OnRefreshListener, OnMoreListener, SwipeDismissRecyclerViewTouchListener.DismissCallbacks {
    private Context mContext;
    private SuperRecyclerView mRvHistoryMuban;
    private MubanHistoryAdapter mubanHistoryAdapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private List<MubanHistory.InfoEntity.ListEntity> listHistory = new ArrayList<MubanHistory.InfoEntity.ListEntity>();
    private String className = getClass().getName();
    private String action_getHistoryTemplateList = className+ API.getHistoryTemplateList;
    private String action_setDeteleMuban = className+ API.setDeteleMuban;
    private int pageNum =1;
    private int prePageCount = 10;
    private int deletePositon = 0 ;
    private boolean isFirstCome = true;
    public static FragmentHistory newInstance() {
        FragmentHistory fragmentHistory = new FragmentHistory();
        return fragmentHistory;
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getHistoryTemplateList)){
            dismissProgressDialog();
            mRvHistoryMuban.setRefreshing(false);
            mRvHistoryMuban.setLoadingMore(false);
            LogUtil.log("-----------历史数据:"+data);
            if (TextUtils.isEmpty(data)){
                toast("网络连接失败...");
            }else {
                handlerHistoryMuban(data);
            }
        }if (action.equals(action_setDeteleMuban)){
            dismissProgressDialog();
            if (TextUtils.isEmpty(data)){
                toast("网络连接失败...");
            }else {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.getInt("status")==1){
                        ToastUtil.show(mContext, "删除成功...");
                        //
                        listHistory.remove(deletePositon);
                        mubanHistoryAdapter.notifyDataSetChanged();
                    }else {
                        ToastUtil.show(mContext, "删除失败哦...");
                    }
                } catch (JSONException e) {
                    ToastUtil.show(mContext, "删除失败哦...");
                }
            }
        }
    }

    /**
     * 处理历史数据
     * @param data
     */
    private void handlerHistoryMuban(String data) {
        MubanHistory mubanHistory = new Gson().fromJson(data, MubanHistory.class);
        List<MubanHistory.InfoEntity.ListEntity> list = mubanHistory.info.list;
        if (pageNum == 1) {
            listHistory.clear();
        }
        listHistory.addAll(list);
        mubanHistoryAdapter.notifyDataSetChanged();
        if (list.isEmpty() || list.size() < prePageCount) {
           // toast("没有更多数据了...");
            mRvHistoryMuban.removeMoreListener();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_history_muban, null);
            assignViews();
            getHistoryTemplateList(pageNum,prePageCount);
            isFirstCome = false;
        }
        return contain;
    }

    private void initReceiver() {
        initReceiver(new String[]{action_getHistoryTemplateList,action_setDeteleMuban});
    }

    private void assignViews() {
        initTitle("店铺项目");
        mRvHistoryMuban = (SuperRecyclerView) findViewById(R.id.rv_history_muban);
     //   mRvHistoryMuban.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRvHistoryMuban.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRvHistoryMuban.setRefreshListener(this);
        mRvHistoryMuban.setupMoreListener(this, 1);
        mubanHistoryAdapter = new MubanHistoryAdapter(getActivity(), listHistory);
        mubanHistoryAdapter.setOnRecycleClickListener(this);
        mRvHistoryMuban.setAdapter(mubanHistoryAdapter);
        //
        //mRvHistoryMuban.setupSwipeToDismiss(this);
        mSparseAnimator = new SparseItemRemoveAnimator();
        mRvHistoryMuban.getRecyclerView().setItemAnimator(mSparseAnimator);
        //
        View emptyView = mRvHistoryMuban.getEmptyView();
        emptyView.setVisibility(View.GONE);
    }

    /**
     * 加载历史列表
     * @param page
     * @param pre_page
     */
    private void getHistoryTemplateList(int page , int pre_page){
        //mRvHistoryMuban.setRefreshing(true);
        if (isFirstCome){
            showProgressDialog("");
        }
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getHistoryTemplateList(token, page, pre_page,1,1,className);
    }




    /**
     * recycle的点击事件
     * @param position
     */
    @Override
    public void onRecycleClick(View veiw,int position) {
        if (veiw.getId()==R.id.ed_muban) {
            // 1打开编辑界面
            ShopStoreActivity shopStoreActivity = (ShopStoreActivity) getActivity();
            shopStoreActivity.openEditPro(listHistory.get(position));
        }else if(veiw.getId() == R.id.ed_muban_delete){
            //删除模板
            deletePositon = position;
            showDialog("你确定删除吗?");
        }
    }

    private void setDetaleMuban(String proId) {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().setDeteleMuban(token,proId,className);
    }

    /**
     * 下拉刷新
     */
    @Override
    public void onRefresh() {
        //TODO 下拉刷新
        pageNum = 1;
        mRvHistoryMuban.setupMoreListener(this, 1);
        getHistoryTemplateList(pageNum, prePageCount);

    }

    public void refHistoryList(){
        pageNum = 1;
        mRvHistoryMuban.setupMoreListener(this, 1);
        getHistoryTemplateList(pageNum, prePageCount);
    }

    /**
     * 加载更多
     * @param overallItemsCount
     * @param itemsBeforeMore
     * @param maxLastVisiblePosition for staggered grid this is max of all spans
     */
    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        //TODO 加载更多
        pageNum++;
        getHistoryTemplateList(pageNum, prePageCount);
    }

    @Override
    public boolean canDismiss(int position) {
        return true;
    }

    @Override
    public void onDismiss(RecyclerView recyclerView, int[] reverseSortedPositions) {
        for (int position : reverseSortedPositions) {
            mSparseAnimator.setSkipNext(true);
            listHistory.remove(position);
            mubanHistoryAdapter.notifyDataSetChanged();
        }
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(mContext).setNegativeButton("取消",null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String proId = listHistory.get(deletePositon).id;
                    setDetaleMuban(proId);
                }
            }).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }
}
