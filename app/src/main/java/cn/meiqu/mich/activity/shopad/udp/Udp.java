package cn.meiqu.mich.activity.shopad.udp;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import cn.meiqu.mich.bean.Command;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/11/27.
 */
public class Udp {
    private static DatagramSocket mDatagramSocket;
    private String hostIp;
    public boolean isReceviceOk = false;
    private String host = "255.255.255.255";


    private Udp() {
    }
    private static Udp instance = null;
    public static Udp newInstace(int port) {
        if (instance == null) {
            synchronized (Udp.class) {
                if (instance == null) {
                    try {
                        instance = new Udp();
                        mDatagramSocket = new DatagramSocket(port);
                    } catch (SocketException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return instance;
    }


    public void closeUdp() {
        try {
            isReceviceOk = true;
            if (mDatagramSocket != null) {
                if (mDatagramSocket.isConnected()) {
                    mDatagramSocket.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public interface OnReceiveListen {
        void getReceive(String data ,String ip);
    }

    private OnReceiveListen mOnReceiveListe = null;

    public void setOnReceiveListen(OnReceiveListen mOnReceiveListe) {
        this.mOnReceiveListe = mOnReceiveListe;
    }

    public void send(String host) {
        try {
            Gson gson = new Gson();
            String dataJson = gson.toJson(new Command("B_000", System.currentTimeMillis()));
            LogUtil.log("------dataJson" + dataJson);
            byte[] command = dataJson.getBytes();
            for (int i = 0; i < 3; i++) {
                DatagramPacket datagramPacket = new DatagramPacket(command, command.length, InetAddress.getByName(host), 9001);
                mDatagramSocket.send(datagramPacket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void receive() {
        while (!isReceviceOk) {
            try {
                byte[] buf = new byte[1024];
                DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length);
                //开始计时，超过2S退出
                //mDatagramSocket.setSoTimeout(5000);
                LogUtil.log("------等待的udp数据。。。");
                mDatagramSocket.receive(datagramPacket);
                //
                LogUtil.log("------接收的udp数据。。。");
                hostIp = datagramPacket.getAddress().getHostAddress();
                String data = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                int port = datagramPacket.getPort();
                if (mOnReceiveListe!=null){
                    mOnReceiveListe.getReceive(data,hostIp);
                }
                LogUtil.log("--------接收到UDP的数据：ip" + hostIp + "-----data" + data + "------port" + port);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发送UDP
     */
    class SendThread extends Thread {
        public void run() {
            send(host);
        }
    }

    /**
     * 接收UDP
     */
    class ReceviceThread extends Thread {
        SendThread sendUdp;
        public ReceviceThread(SendThread bt) {
            this.sendUdp = bt;
        }
        public void run() {
            try {
                //接收Udp必须要等待发送udp完成后才执行
                sendUdp.join();
                receive();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void toRunUdp(){
        try {
            SendThread sendThread = new SendThread();
            ReceviceThread receviceThread = new ReceviceThread(sendThread);
            //isReceviceOk = false;
            //发UDP
            sendThread.start();
            //接收UDP
            receviceThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
