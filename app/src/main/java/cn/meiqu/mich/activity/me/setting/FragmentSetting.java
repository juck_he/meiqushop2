package cn.meiqu.mich.activity.me.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.LoginActivity;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.util.PackageUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.UpdateUtil;

/**
 * Created by Fatel on 15-9-21.
 */
public class FragmentSetting extends BaseFragment implements View.OnClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_setting, null);
            assignViews();
        }
        this.mContext = getActivity();
        return contain;
    }

    private LinearLayout mLinModifyPwd;
    private LinearLayout mLinClearCache;
    private LinearLayout mLinCheckUpdate;
    private LinearLayout mLinLogout;
    private TextView mTvVersion;
    private Context mContext;

    private void assignViews() {
        initTitle("设置");

       // mImgVTitleBack.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),R.drawable.i_can_paycheck));

        mLinModifyPwd = (LinearLayout) findViewById(R.id.lin_modifyPwd);
        mLinClearCache = (LinearLayout) findViewById(R.id.lin_clearCache);
        mLinCheckUpdate = (LinearLayout) findViewById(R.id.lin_checkUpdate);
        mLinLogout = (LinearLayout) findViewById(R.id.lin_logout);
        mTvVersion = (TextView) findViewById(R.id.tv_version);
        mLinModifyPwd.setOnClickListener(this);
        mLinClearCache.setOnClickListener(this);
        mLinCheckUpdate.setOnClickListener(this);
        mLinLogout.setOnClickListener(this);
        mTvVersion.setText("v " + PackageUtil.getVersionName(getActivity()));
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mLinCheckUpdate.getId()) {
            UpdateUtil.checkUpdate(BaseApp.mContext, true);
        } else if (v.getId() == mLinModifyPwd.getId()) {
            ((SettingActivity) getActivity()).showChangePwd();
        } else if (v.getId() == mLinClearCache.getId()) {
            toast("缓存清除成功");
        } else if (v.getId() == mLinLogout.getId()) {
            User user = PrefUtils.getUser(getActivity());
            user.token = null;
            PrefUtils.setUser(getActivity(), user);
            ((BaseActivity) getActivity()).jump(LoginActivity.class);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(BaseActivity.action_exitApplication));
        }
    }
}
