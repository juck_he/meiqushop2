package cn.meiqu.mich.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by Administrator on 2015/9/17.
 */
public class StartActivity extends Activity {
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtil.fullScreenCompat(this);
        setContentView(R.layout.activity_start);
        linearLayout = (LinearLayout) findViewById(R.id.ll_start);
        startAnim();

    }

    /**
     * 开启动画
     */
    private void startAnim() {
        // 动画集合
        AnimationSet set = new AnimationSet(false);
        // 缩放动画
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        scale.setDuration(1000);// 动画时间
        scale.setFillAfter(true);// 保持动画状态

        // 渐变动画
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        alpha.setDuration(2000);// 动画时间
        alpha.setFillAfter(true);// 保持动画状态
        set.addAnimation(scale);
        set.addAnimation(alpha);

        // 设置动画监听
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            // 动画执行结束
            @Override
            public void onAnimationEnd(Animation animation) {
                jumpNextPage();
            }
        });

        linearLayout.startAnimation(set);
    }

    /**
     * 跳转下一个页面
     */
    private void jumpNextPage() {
        // 判断之前有没有登录
        if (PrefUtils.getUser(StartActivity.this) != null) {
            User user = PrefUtils.getUser(StartActivity.this);
            if (TextUtils.isEmpty(user.token)) {
                //跳转到登录界面
                startActivity(new Intent(StartActivity.this, LoginActivity.class));
            } else {
                //直接跑到主界面
                startActivity(new Intent(StartActivity.this, MainActivity.class));
            }
        } else {
            //跳转到登录界面
            startActivity(new Intent(StartActivity.this, LoginActivity.class));
        }
        finish();
    }

}
