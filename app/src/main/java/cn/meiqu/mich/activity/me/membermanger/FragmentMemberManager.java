package cn.meiqu.mich.activity.me.membermanger;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.Calendar;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.me.membermanger.history.HistoryActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.SendCoupon;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.view.wheelview.NumericWheelAdapter;
import cn.meiqu.mich.view.wheelview.OnWheelScrollListener;
import cn.meiqu.mich.view.wheelview.WheelView;

/**
 * Created by Administrator on 2015/10/8.
 */
public class FragmentMemberManager extends BaseFragment implements View.OnClickListener {
    private WheelView year;
    private WheelView month;
    private WheelView day;

    private LinearLayout mLlShow;
    private TextView mTvCheckMemberlist;
    private TextView mTvPushtext;
    private TextView mTvPreviewPrice;
    private LinearLayout mLlPriceName;
    private EditText mEdInputName;
    private LinearLayout mLlPrice;
    private EditText mEdInpeutPrice;
    private EditText mEdInputContent;
    private TextView mTvStartDate;
    private TextView mTvEndDate;
    int width, height;
    String className = getClass().getName();
    String action_setCoupon = className + API.setCoupon;
    private LinearLayout popLayout;
    private Context mContext;
    private int norYear;//当前年
    private int norMonth;//当前月
    private int norDay;//当前日
    private int curremtStartYear; //选择起始到的年
    private int curremtStartMonth;//选择起始到的月
    private int curremtStartDay;  //选择起始到的日

    private int curremtEndYear; //选择结束到的年
    private int curremtEndMonth;//选择结束到的月
    private int curremtEndDay;  //选择结束到的日

    private boolean isStartTime = false;
    public int voucher_id;//优惠券的ID

    public static FragmentMemberManager newInstance() {
        FragmentMemberManager fragmentMemberManager = new FragmentMemberManager();
        return fragmentMemberManager;
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if(action.equals(action_setCoupon)){
            if(data!=null) {
                //发送优惠券成功，进去会员列表推送给那个会员
                SendCoupon sendCoupon = new Gson().fromJson(data, SendCoupon.class);
                voucher_id = sendCoupon.info.voucher_id;
                // TODO 打开要推送的用户列表
                MemberActivity memberActivity = (MemberActivity) getActivity();
                memberActivity.fragmentUserList.isPush = true;
                memberActivity.showUserList();
            }
            else {
                ToastUtil.show(mContext,"网络请求失败,请检查网络是否正常...");
            }
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_membermanager, null);
            assignViews();
        }
        initReceiver();
        return contain;
    }

    public void initReceiver() {
        initReceiver(new String[]{action_setCoupon});
    }


    private void assignViews() {
        initTitle("会员管理");
        setTitleRight("历史", this);
        mTvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(mContext, HistoryActivity.class));
            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();
        // 获取弹出的layout
        popLayout = (LinearLayout) findViewById(R.id.ll_show);
        mLlShow = (LinearLayout) findViewById(R.id.ll_show);
        mTvCheckMemberlist = (TextView) findViewById(R.id.tv_check_memberlist);
        mTvPushtext = (TextView) findViewById(R.id.tv_pushtext);
        mTvPreviewPrice = (TextView) findViewById(R.id.tv_preview_price);
        mLlPriceName = (LinearLayout) findViewById(R.id.ll_price_name);
        mEdInputName = (EditText) findViewById(R.id.ed_input_name);
        mEdInputContent = (EditText) findViewById(R.id.ed_inpeutcontent);
        mLlPrice = (LinearLayout) findViewById(R.id.ll_price);
        mEdInpeutPrice = (EditText) findViewById(R.id.ed_inpeut_price);
        mTvStartDate = (TextView) findViewById(R.id.tv_start_date);
        mTvEndDate = (TextView) findViewById(R.id.tv_end_date);

        mTvCheckMemberlist.setOnClickListener(this);//会员列表
        mTvPushtext.setOnClickListener(this);//推送文字
        mTvPreviewPrice.setOnClickListener(this);//推送优惠券

        mTvStartDate.setOnClickListener(this);
        mTvEndDate.setOnClickListener(this);
        getCurremtTime();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_date:
                //显示起始时间选择
                isStartTime = true;
                showPopupWindow();
                break;
            case R.id.tv_end_date:
                //显示选择结束时间
                isStartTime = false;
                showPopupWindow();
                break;
            case R.id.tv_check_memberlist:
                //跳到会员列表
                //TODO 跳到会员列表
                MemberActivity  memberActivity= (MemberActivity) getActivity();
                memberActivity.fragmentUserList.isShowCheckBox =false;
                //memberActivity.fragmentUserList.content = pushText;
                memberActivity.showUserList();
                break;
            case R.id.tv_pushtext:
                //1获取文字
                String pushText = mEdInputContent.getText().toString().trim();
                if (TextUtils.isEmpty(pushText)) {
                    ToastUtil.show(mContext, "请输入要推送的文字");
                } else {
                    //TODO 打开会员列表
                    MemberActivity  activityMember= (MemberActivity) getActivity();
                    activityMember.fragmentUserList.isPush =false;
                    activityMember.fragmentUserList.isShowCheckBox = true;

                    activityMember.fragmentUserList.content = pushText;
                    activityMember.showUserList();
                }
                break;
            case R.id.tv_preview_price:
                //获取优惠券的名称
                String inputName = mEdInputName.getText().toString().trim();
                String inputPrice = mEdInpeutPrice.getText().toString().trim();
                if (TextUtils.isEmpty(inputName)) {
                    ToastUtil.show(mContext, "请输入优惠价的名称...");
                } else {
                    if (TextUtils.isEmpty(inputPrice)) {
                        ToastUtil.show(mContext, "请输入优惠价的价格...");
                    } else {
                        if (inputName.equals("0")) {
                            ToastUtil.show(mContext, "输入的价格不能为0...");
                        } else {
                            //提交
                            //判断起始时间一定要比结束时间要小
                            if(curremtStartYear<curremtEndYear){
                                //TODO 提交优惠劵信息
                                sendCoupon(inputName,inputPrice);
                            }else {
                                if((curremtStartYear==curremtEndYear)&&(curremtStartMonth<curremtEndMonth)){
                                    //TODO 提交优惠劵信息
                                    sendCoupon(inputName,inputPrice);
                                }else {
                                    if((curremtStartYear==curremtEndYear)&&(curremtStartMonth==curremtEndMonth)&&(curremtStartDay<curremtEndDay)){
                                        //TODO 提交优惠劵信息
                                        sendCoupon(inputName,inputPrice);
                                    }
                                    else {
                                        ToastUtil.show(mContext, "起始时间必须要小于结束时间...");
                                    }
                                }
                            }
                        }
                    }
                }
                break;
        }
    }

    /**
     * 发布优惠券
     * @param inputName
     * @param inputPrice
     */
    private void sendCoupon(String inputName, String inputPrice) {
        String token = PrefUtils.getUser(mContext).token;
        String startDate = curremtStartYear+"-"+curremtStartMonth+"-"+curremtStartDay;
        String endDate = curremtEndYear+"-"+curremtEndMonth+"-"+curremtEndDay;
        HttpGetController.getInstance().setCoupon(token,inputName,inputPrice,startDate,endDate,className);

    }

    /**
     * 显示PopupWindow
     */
    public void showPopupWindow() {
        PopupWindow popupWindow = makePopupWindow(mContext);
        int[] xy = new int[2];
        popLayout.getLocationOnScreen(xy);
        popupWindow.showAtLocation(popLayout, Gravity.CENTER | Gravity.BOTTOM, 0, -(height));
    }

    private PopupWindow makePopupWindow(Context context) {
        final PopupWindow window;
        window = new PopupWindow();
        View contentView = View.inflate(context, R.layout.view_dateselect_wheel, null);
        window.setContentView(contentView);
        Button btnSelect = (Button) contentView.findViewById(R.id.btn_select_ok);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentDate();
                if (isStartTime) {
                    LogUtil.log("n_year:" + curremtStartYear + "-----n_month:" + curremtStartMonth + "-----n_day:" + curremtStartDay);
                    mTvStartDate.setText(curremtStartYear + "-" + new DecimalFormat("00").format(curremtStartMonth) + "-" + new DecimalFormat("00").format(curremtStartDay));
                } else {
                    //判断结束时间一定要比起始时间要迟
                    LogUtil.log("-------"+curremtEndYear+"------"+curremtStartYear);
                    if (curremtEndYear > curremtStartYear) {
                        mTvEndDate.setText(curremtEndYear + "-" + new DecimalFormat("00").format(curremtEndMonth) + "-" + new DecimalFormat("00").format(curremtEndDay));
                    } else {
                        if ((curremtEndYear == curremtStartYear) && (curremtEndMonth > curremtStartMonth)) {
                            mTvEndDate.setText(curremtEndYear + "-" + new DecimalFormat("00").format(curremtEndMonth) + "-" + new DecimalFormat("00").format(curremtEndDay));
                        } else {
                            if ((curremtEndYear == curremtStartYear) && (curremtEndMonth == curremtStartMonth) && (curremtEndDay > curremtStartDay)) {
                                mTvEndDate.setText(curremtEndYear + "-" + new DecimalFormat("00").format(curremtEndMonth) + "-" + new DecimalFormat("00").format(curremtEndDay));
                            } else {
                                resetEndData();
                                ToastUtil.show(mContext, "输入的结束时间要比起始时间要大...");
                            }
                        }
                    }
                    LogUtil.log("n_year:" + curremtEndYear + "-----n_month:" + curremtEndMonth + "-----n_day:" + curremtEndDay);
                }
                window.dismiss();
            }
        });

        int curYear = norYear;
        int curMonth = norMonth + 1;
        int curDate = norDay;
        LogUtil.log("curYear:" + curYear + "-----" + "curMonth:" + curMonth + "-----" + "curDatecurDate:" + curDate + "-----" + "norYear" + norYear + "-----norMonth:" + norMonth + "----norDay:" + norDay);


        year = (WheelView) contentView.findViewById(R.id.year);
        year.setVisibleItems(7);
        year.setMinimumHeight(80);
        //从1950 ----2015年 修改 2015 --2115年
        year.setAdapter(new NumericWheelAdapter(norYear, norYear + 100));
        year.setLabel("年");
        year.setCyclic(true);
        year.addScrollingListener(scrollListener);

        month = (WheelView) contentView.findViewById(R.id.month);
        month.setVisibleItems(7);
        month.setMinimumHeight(80);
        month.setAdapter(new NumericWheelAdapter(1, 12, "%02d"));
        month.setLabel("月");
        month.setCyclic(true);
        month.addScrollingListener(scrollListener);

        day = (WheelView) contentView.findViewById(R.id.day);
        day.setVisibleItems(7);
        day.setMinimumHeight(80);
        initDay(curYear, curMonth);
        day.setLabel("日");
        day.setCyclic(true);
        day.addScrollingListener(scrollListener);

        getCurrentDate();
        //设置当前的时间定位到一个时间
        if (isStartTime) {
            year.setCurrentItem(0);
            month.setCurrentItem(curMonth - 1);
            day.setCurrentItem(curDate-1);
        } else {
            year.setCurrentItem(0);
            month.setCurrentItem(curMonth - 1);
            day.setCurrentItem(curDate);
        }

        window.setWidth(width);
        contentView.measure(0, 0);
        window.setHeight(contentView.getMeasuredHeight());
        // 设置PopupWindow外部区域是否可触摸
        window.setFocusable(true); //设置PopupWindow可获得焦点
        window.setTouchable(true); //设置PopupWindow可触摸
        window.setBackgroundDrawable(new BitmapDrawable());
        window.setOutsideTouchable(true); //设置非PopupWindow区域可触摸
        return window;
    }

    OnWheelScrollListener scrollListener = new OnWheelScrollListener() {
        @Override
        public void onScrollingStarted(WheelView wheel) {

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            getCurrentDate();
        }
    };

    /**
     * 获取到当前时间
     */
    private void getCurrentDate() {
        if (isStartTime) {

            curremtStartYear = year.getCurrentItem() + norYear;
            curremtStartMonth = month.getCurrentItem() + 1;
            initDay(curremtStartYear, curremtStartMonth);
            curremtStartDay = day.getCurrentItem() + 1;

        } else {
            curremtEndYear = year.getCurrentItem() + norYear;
            curremtEndMonth = month.getCurrentItem() + 1;
            initDay(curremtEndYear, curremtEndMonth);
            curremtEndDay = day.getCurrentItem() + 1;
        }

        //String birthday=new StringBuilder().append((year.getCurrentItem())).append("-").append((month.getCurrentItem() + 1) < 10 ? "0" + (month.getCurrentItem() + 1) : (month.getCurrentItem() + 1)).append("-").append((day.getCurrentItem() + 1 < 10) ? "0" + day.getCurrentItem() + 1 : day.getCurrentItem() + 1).toString();
    }

    /**
     * 重新设置结束时间
     */
    public void resetEndData() {
        curremtEndYear = curremtStartYear;
        curremtEndMonth = curremtStartMonth;
        initDay(curremtEndYear, curremtEndMonth);
        curremtEndDay = day.getCurrentItem() + 1;
        mTvEndDate.setText(curremtEndYear + "-" + new DecimalFormat("00").format(curremtEndMonth) + "-" + new DecimalFormat("00").format(curremtEndDay));
    }

    private void initDay(int arg1, int arg2) {
        day.setAdapter(new NumericWheelAdapter(1, getDay(arg1, arg2), "%02d"));
    }

    /**
     * 获取和设置当前时间
     */
    public void getCurremtTime() {
        Calendar c = Calendar.getInstance();
        norYear = c.get(Calendar.YEAR);
        norMonth = c.get(Calendar.MONTH);
        norDay = c.get(Calendar.DAY_OF_MONTH);

        //设置优惠劵的起始时间为今天
        mTvStartDate.setText(norYear + "-" + new DecimalFormat("00").format(norMonth + 1) + "-" + new DecimalFormat("00").format(norDay));
        //设置优惠劵的结束时间为明天
        mTvEndDate.setText(norYear + "-" + new DecimalFormat("00").format(norMonth + 1) + "-" + new DecimalFormat("00").format(norDay + 1));

        //赋值---->起始时间
        curremtStartYear = norYear;
        curremtStartMonth = norMonth + 1;
        curremtStartDay = norDay;
        //赋值---->结束时间
        curremtEndYear = norYear;
        curremtEndMonth = norMonth + 1;
        curremtEndDay = norDay + 1;


    }


    private int getDay(int year, int month) {
        int day = 30;
        boolean flag = false;
        switch (year % 4) {
            case 0:
                flag = true;
                break;
            default:
                flag = false;
                break;
        }
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 2:
                day = flag ? 29 : 28;
                break;
            default:
                day = 30;
                break;
        }
        return day;
    }

    /**
     * 获取优惠券的ID信息
     * @return
     */
    public int getCouponId(){
        return voucher_id;
    }

}
