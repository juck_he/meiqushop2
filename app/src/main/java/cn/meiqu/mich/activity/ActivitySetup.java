package cn.meiqu.mich.activity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.GetLastSetupTime;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSendController;
import cn.meiqu.mich.tcp.TcpSocket;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

public class ActivitySetup extends BaseActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private ImageView mIvBack;
    private ImageView mIvVoice;
    private ImageView mIvVoiceAdd;
    private ImageView mIvVoiceCut;
    private TextView mTvTitle;
    private TextView mTvMore;
    private TextView mTvOnTime;
    private TextView mTvOffTime;
    private RelativeLayout mRlSelectVolAdd;
    private RelativeLayout mRlSelectVolCut;
    private SeekBar mSbVol;
    private Context mContext;
    private TcpSendController sendController;
    //默认开关机时间
    private int onHourSelect = 0, onMinuteSelect = 0, offHourSelect = 0, offMinuteSelect = 0;
    private String className = getClass().getName();
    private String action_setScreenWorkTime = className + API.setScreenWorkTime;
    private String action_getBitScreenLastSetup = className + API.getBitScreenLastSetup;
    private int[] images = new int[]{R.drawable.i_vol, R.drawable.i_vol_mute};
    private boolean isVoice = true;
    private boolean isVoiceChange = false;
    private int curremtVoice;
    private int seekBarProgress;

    @Override
    public void onHttpHandle(String action, String data) {
        LogUtil.log("-----action:" + action + "-------" + data);
        if (action.equals(action_setScreenWorkTime)) {
            dismissProgressDialog();
            if (data != null) {
                LogUtil.log("data:" + data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        ToastUtil.show(mContext, "设置成功...");
                    } else {
                        ToastUtil.show(mContext, "设置失败...");
                    }
                } catch (JSONException e) {
                    ToastUtil.show(mContext, "设置失败...");
                    e.printStackTrace();
                }

            } else {
                ToastUtil.show(mContext, "设置失败，请检查你的网络...");
            }
        }
        if (action.equals(TcpResponController.action_tcp_respon)) {
            LogUtil.log("声音控制..." + data);
            dismissProgressDialog();
            if (data.equals(TcpCommand.H_ConnectFailure)) {
                if (isVoiceChange) {
                    isVoiceChange = false;
                    mSbVol.setProgress(seekBarProgress);
                }
                showDialog("美屏和手机不在同WiFi下，不能设置声音...");

            } else if (data.equals(TcpCommand.H_SoundLevel)) {
                if (isVoiceChange) {
                    //TODO 设置有声状态
                    isVoice = true;
                    isVoiceChange = false;
                    //modifiVoiceState();
                }
                modifiVoiceState();
            }
        }
        if(action.equals(action_getBitScreenLastSetup)){
            LogUtil.log("--------获取到上次设置的时间："+data);
            if(data!=null){
                handleLastSetupdata(data);
            }else {
                toast("网络连接失败...");
            }
        }
    }

        private void handleLastSetupdata(String data) {
            GetLastSetupTime getLastSetupTime = new Gson().fromJson(data, GetLastSetupTime.class);
            //修改默认时间
            if(getLastSetupTime.info.isEmpty()){
                return;
            }
            GetLastSetupTime.InfoEntity infoEntity = getLastSetupTime.info.get(0);
            //开机时间
            String boot_time = infoEntity.boot_time;
            LogUtil.log("--------开始时间的长度："+boot_time.length());
            String[] splitBootTime = boot_time.split(":");
            onHourSelect = Integer.parseInt(splitBootTime[0]);
            onMinuteSelect = Integer.parseInt(splitBootTime[1]);
            LogUtil.log("-------上次开机的时间"+onHourSelect+":"+onMinuteSelect);
            mTvOnTime.setText(appendZero(onHourSelect,2)+":"+appendZero(onMinuteSelect,2));
            //关机时间
            String shutdown_time = infoEntity.shutdown_time;
            String[] splitSgudowm = shutdown_time.split(":");
            offHourSelect = Integer.parseInt(splitSgudowm[0]);
            offMinuteSelect = Integer.parseInt(splitSgudowm[1]);
            LogUtil.log("-------上次关机的时间"+offHourSelect+":"+offMinuteSelect);
            mTvOffTime.setText(appendZero(offHourSelect, 2) + ":" + appendZero(offMinuteSelect, 2));

        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        this.mContext = this;
        initReceiver(new String[]{action_setScreenWorkTime, TcpResponController.action_tcp_respon, action_getBitScreenLastSetup});
        getLastSetup();
        initView();
        initData();
    }

    @Override
    public void initFragment() {

    }

    private void initData() {
        mTvTitle.setText("美屏设置");
        mTvMore.setText("确定");
        //TODO 设置为有声音
        sendController = TcpSendController.getInstance();
        curremtVoice = mSbVol.getProgress();
        modifiVoice();

        mIvBack.setOnClickListener(this);
        mRlSelectVolAdd.setOnClickListener(this);
        mRlSelectVolCut.setOnClickListener(this);
        mTvMore.setOnClickListener(this);
        mIvVoice.setOnClickListener(this);
        mIvVoiceAdd.setOnClickListener(this);
        mIvVoiceCut.setOnClickListener(this);
        mSbVol.setOnSeekBarChangeListener(this);
    }

    private void modifiVoice() {
        if (!TcpSocket.getInstance().isConnected()) {
            showDialog("美屏未被控制，不能设置声音...");
            return;
        }
        showProgressDialog("");
        if (isVoice) {
            LogUtil.log("----音量开");
            sendController.sendSoundLevel(mSbVol.getProgress());
        } else {
            LogUtil.log("音量关----");
            sendController.sendSoundLevel(0);
        }
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(mContext).setPositiveButton("确定", null).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    private void modifiVoiceState() {
        //showProgressDialog("");
        if (isVoice) {
            LogUtil.log("----音量开");
            mIvVoice.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[0]));
        } else {
            LogUtil.log("音量关----");
            mIvVoice.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[1]));
        }
    }

    /**
     * 修改音量的大小
     *
     * @param curremtVoice
     */
    private void modifiChangVoice(int curremtVoice) {
        if (!TcpSocket.getInstance().isConnected()) {
            mSbVol.setProgress(seekBarProgress);
            showDialog("美屏未被控制，不能设置声音...");
            return;
        }
        showProgressDialog("");
        LogUtil.log("------------当前的音量为：" + curremtVoice);
        isVoiceChange = true;
        sendController.sendSoundLevel(mSbVol.getProgress());
        mSbVol.setProgress(curremtVoice);
    }


    private void initView() {
        mIvVoice = (ImageView) findViewById(R.id.iv_voice);
        mIvVoiceAdd = (ImageView) findViewById(R.id.iv_voice_add);
        mIvVoiceCut = (ImageView) findViewById(R.id.iv_voice_cut);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        mTvOnTime = (TextView) findViewById(R.id.tv_on_time);
        mTvOffTime = (TextView) findViewById(R.id.tv_off_time);
        mRlSelectVolAdd = (RelativeLayout) findViewById(R.id.rl_select_vol_add);
        mRlSelectVolCut = (RelativeLayout) findViewById(R.id.rl_select_vol_cut);
        mSbVol = (SeekBar) findViewById(R.id.sb_vol);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_select_vol_add:
                showDialogTimeON();
                break;
            case R.id.rl_select_vol_cut:
                showDialogTimeOFF();
                break;
            case R.id.tv_more:
                //TODO 获取开机和关机时间
                showProgressDialog("");
//                if(onHourSelect==0&&(onMinuteSelect==0)&&(offHourSelect==0)&&(offHourSelect==0)){
//                    ToastUtil.show(mContext,"选择开关机时间...");
//                    return;
//                }
                modifiDevice();
                break;
            case R.id.iv_voice:
                if (isVoice) {
                    isVoice = false;
                    //TODO 静音
                    modifiVoice();
                } else {
                    isVoice = true;
                    //TODO 有声音
                    modifiVoice();
                }
                break;
            case R.id.iv_voice_add:
                curremtVoice = curremtVoice + 1;
                if (curremtVoice >= 12) {
                    curremtVoice = 12;
                }
                //modifiChangVoice(curremtVoice);
                seekBarProgress = mSbVol.getProgress();
                mSbVol.setProgress(curremtVoice);
                break;
            case R.id.iv_voice_cut:
                curremtVoice = curremtVoice - 1;
                if (curremtVoice <= 0) {
                    curremtVoice = 0;
                }
                //modifiChangVoice(curremtVoice);
                seekBarProgress = mSbVol.getProgress();
                mSbVol.setProgress(curremtVoice);
                break;
        }
    }

    private void modifiDevice() {
        String token = PrefUtils.getUser(mContext).token;
        String onHour = appendZero(onHourSelect, 2);
        String onMinu = appendZero(onMinuteSelect, 2);
        String offHour = appendZero(offHourSelect, 2);
        String offMinu = appendZero(offMinuteSelect, 2);

        LogUtil.log("-----开机时间：" + onHour + ":" + onMinu);
        LogUtil.log("-----关机时间：" + offHour + ":" + offMinu);
        HttpGetController.getInstance().setScreenWorkTime(token, onHour + ":" + onMinu, offHour + ":" + offMinu, className);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        LogUtil.log("-----------当前进度：" + progress);
        modifiChangVoice(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        seekBarProgress = seekBar.getProgress();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    /**
     * 选择开机时间
     */
    private void showDialogTimeON() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View viewDialog = View.inflate(mContext, R.layout.view_date_select, null);
        builder.setView(viewDialog, 0, 0, 0, 0);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        final TimePicker mTimePicker = (TimePicker) viewDialog.findViewById(R.id.timePicker);
        //修改为24小时制
        mTimePicker.setIs24HourView(true);

        mTimePicker.setCurrentHour(onHourSelect);
        mTimePicker.setCurrentMinute(onMinuteSelect);

        TextView mTvCancle = (TextView) viewDialog.findViewById(R.id.tv_cancle);
        TextView mTvSure = (TextView) viewDialog.findViewById(R.id.tv_sure);
        TextView mTitle = (TextView) viewDialog.findViewById(R.id.tv_title);

        mTitle.setText("选择大屏开机时间");
        alertDialog.show();
        mTvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHourSelect = mTimePicker.getCurrentHour();
                onMinuteSelect = mTimePicker.getCurrentMinute();
                alertDialog.dismiss();

                if (onHourSelect >= 10) {
                    if (onHourSelect == 10)
                        if (onMinuteSelect <= 30) {
                            mTvOnTime.setText(appendZero(onHourSelect, 2) + ":" + appendZero(onMinuteSelect, 2));
                        } else {
                            onHourSelect = 10;
                            onMinuteSelect = 30;
                            mTvOnTime.setText(appendZero(onHourSelect, 2) + ":" + appendZero(onMinuteSelect, 2));
                        }
                    else {
                        onHourSelect = 10;
                        onMinuteSelect = 30;
                        mTvOnTime.setText(appendZero(onHourSelect, 2) + ":" + appendZero(onMinuteSelect, 2));
                    }
                } else {
                    mTvOnTime.setText(appendZero(onHourSelect, 2) + ":" + appendZero(onMinuteSelect, 2));
                }
            }
        });
        mTvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                //选择开机的时间必须是10点30分之前
                LogUtil.log("----hourOfDay:" + hourOfDay + "=====minute:" + minute);
                //如果时是大于等于10点
                if (hourOfDay >= 10) {
                    //如果时是10:30分之前
                    if (hourOfDay == 10) {
                        if (minute <= 30) {
                        } else {
                            ToastUtil.show(mContext, "开机时间是10:30之前哦！");
                        }
                    }
                    else {
                        ToastUtil.show(mContext, "开机时间是10:30之前哦！");
                    }
                }
            }
        });
    }

    /**
     * 选择关机时间
     */
    private void showDialogTimeOFF() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View viewDialog = View.inflate(mContext, R.layout.view_date_select, null);
        builder.setView(viewDialog, 0, 0, 0, 0);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        final TimePicker mTimePicker = (TimePicker) viewDialog.findViewById(R.id.timePicker);

        mTimePicker.setIs24HourView(true);
        mTimePicker.setCurrentHour(offHourSelect);
        mTimePicker.setCurrentMinute(offMinuteSelect);

        TextView mTvCancle = (TextView) viewDialog.findViewById(R.id.tv_cancle);
        TextView mTvSure = (TextView) viewDialog.findViewById(R.id.tv_sure);
        TextView mTitle = (TextView) viewDialog.findViewById(R.id.tv_title);
        mTitle.setText("选择大屏关机时间");
        alertDialog.show();
        mTvSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offHourSelect = mTimePicker.getCurrentHour();
                offMinuteSelect = mTimePicker.getCurrentMinute();
                alertDialog.dismiss();

                if (offHourSelect <= 22) {
                    if (offHourSelect == 22)
                        if (offMinuteSelect >= 30) {
                            mTvOffTime.setText(appendZero(offHourSelect, 2) + ":" + appendZero(offMinuteSelect, 2));
                        } else {
                            offHourSelect = 22;
                            offMinuteSelect = 30;
                            mTvOffTime.setText(appendZero(offHourSelect, 2) + ":" + appendZero(offMinuteSelect, 2));
                        }
                    else {
                        offHourSelect = 22;
                        offMinuteSelect = 30;
                        mTvOffTime.setText(appendZero(offHourSelect, 2) + ":" + appendZero(offMinuteSelect, 2));
                    }
                } else {
                    mTvOffTime.setText(appendZero(offHourSelect, 2) + ":" + appendZero(offMinuteSelect, 2));
                }
            }
        });
        mTvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                //选择开机的时间必须是22:30之前
                LogUtil.log("----hourOfDay:" + hourOfDay + "=====minute:" + minute);
                //如果时是22点之前
                if (hourOfDay <= 22) {
                    //如果时是20:30分之后
                    if (hourOfDay == 22) {
                        if (minute >= 30) {
                        } else {
                            ToastUtil.show(mContext, "关机时间是22:30之后！");
                        }
                    }
                    else {
                        ToastUtil.show(mContext, "关机时间是22:30之后！");
                    }
                }
            }
        });
    }

    public String appendZero(int num, int length) {
        //String.valueOf()是用来将其他类型的数据转换为string型数据的
        String tmpString = String.valueOf(num);
        for (int i = tmpString.length(); i < length; i++) {
            tmpString = "0" + tmpString;
        }
        return tmpString;
    }

    public void getLastSetup() {
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getBitScreenLastSetup(token,className);
    }
}
