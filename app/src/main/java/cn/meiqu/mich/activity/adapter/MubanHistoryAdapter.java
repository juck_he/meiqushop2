package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.MubanHistory;
import cn.meiqu.mich.util.ImageLoadHelper;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by juck on 15/12/9.
 */
public class MubanHistoryAdapter extends RecyclerView.Adapter<MubanHistoryAdapter.Holder> {
    Context mContext;
    List<MubanHistory.InfoEntity.ListEntity> listHistory;

    private OnRecycleClickListener onRecycleClickListener;


    public OnRecycleClickListener getOnRecycleClickListener() {
        return onRecycleClickListener;
    }

    public void setOnRecycleClickListener(OnRecycleClickListener onRecycleClickListener) {
        this.onRecycleClickListener = onRecycleClickListener;
    }

    public interface OnRecycleClickListener {
        public void onRecycleClick(View view , int position);
    }


    public MubanHistoryAdapter(FragmentActivity activity, List<MubanHistory.InfoEntity.ListEntity> listHistory) {
        this.mContext = activity;
        this.listHistory = listHistory;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.item_history_muban, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return listHistory == null ? 0 : listHistory.size();
    }

    class Holder extends BaseHolder {
        private ImageView mIvImageMubanUrl;
        private TextView mTvMubanTitle;
        private TextView mTvMubanPrice;
        private TextView mEdMuban;
        private CardView mRlHistory;
        private RelativeLayout mRlData;
        private TextView mEdMubanDetale;
        private TextView mTvShopName;
        private TextView mTvShopName2;

        public Holder(View itemView) {
            super(itemView);

        }

        public void assignViews() {
            mRlHistory = (CardView) findViewById(R.id.rl_history);
            mRlData = (RelativeLayout) findViewById(R.id.rl_data);
            mIvImageMubanUrl = (ImageView) findViewById(R.id.iv_image_muban_url);
            mTvMubanTitle = (TextView) findViewById(R.id.tv_muban_title);
            mTvMubanPrice = (TextView) findViewById(R.id.tv_muban_price);
            mEdMuban = (TextView) findViewById(R.id.ed_muban);
            mEdMubanDetale = (TextView) findViewById(R.id.ed_muban_delete);
            mTvShopName = (TextView) findViewById(R.id.tv_shop_name);
            mTvShopName2 = (TextView) findViewById(R.id.tv_shop_name2);
        }

        @Override
        public void instanceView(final int position) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(ScreenUtil.dip2px(mContext, 10), ScreenUtil.dip2px(mContext, 10), ScreenUtil.dip2px(mContext, 10), 0);
            mRlHistory.setLayoutParams(layoutParams);
           // mRlHistory.setPadding(ScreenUtil.dip2px(mContext,5),ScreenUtil.dip2px(mContext,5),ScreenUtil.dip2px(mContext,5),0);
            //显示背景图
            ImageLoadHelper.displayImage(listHistory.get(position).coupon_img, mIvImageMubanUrl);
            //设置标题
            mTvMubanTitle.setText(listHistory.get(position).coupon_title);
            //设置价格
            mTvMubanPrice.setText("￥ " + listHistory.get(position).coupon_money);
            //
            mEdMuban.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecycleClickListener != null) {
                        //ToastUtil.show(mContext, "点击了:" + position);
                        onRecycleClickListener.onRecycleClick(mEdMuban,getAdapterPosition());
                    }
                }
            });
            String company_name = PrefUtils.getUser(mContext).company_name;
//            String company_name = "傻豪傻豪豪尚·发型定制（天河南店）";
            mTvShopName2.setVisibility(View.GONE);
            LogUtil.log("----------company_name.length:" + company_name.length());
            if (company_name.length()>9){
                mTvShopName.setTextSize(ScreenUtil.px2sp(mContext, 30));
                String name1= company_name.substring(0, 8);
                String name2 = company_name.substring(9,company_name.length());
                mTvShopName.setText(name1);
                mTvShopName2.setVisibility(View.VISIBLE);
                mTvShopName2.setText(name2);
            }else {
                mTvShopName.setText(company_name);
            }
            mEdMubanDetale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecycleClickListener != null) {
                        onRecycleClickListener.onRecycleClick(mEdMubanDetale,getAdapterPosition());
                    }
                }
            });
        }
    }
}