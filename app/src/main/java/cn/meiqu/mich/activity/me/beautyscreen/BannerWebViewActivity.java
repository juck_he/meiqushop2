package cn.meiqu.mich.activity.me.beautyscreen;


import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/10/10.
 */
public class BannerWebViewActivity extends BaseActivity {
    public String url;

    @Override
    public void onHttpHandle(String action, String data) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_webview);
        initFragment();
        url = getIntent().getStringExtra("url_ad");
    }

    @Override
    public void initFragment() {
        setContainerId(containerId);
        FragmentBannerDetail fragmentBannerDetail = FragmentBannerDetail.newInstance();
        showFirst(fragmentBannerDetail);
    }



}
