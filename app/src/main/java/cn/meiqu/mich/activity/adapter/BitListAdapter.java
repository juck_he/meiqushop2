package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.BitListIpAddress;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/9/24.
 */
public class BitListAdapter extends BaseAdapter {
    private Context mContext;
    private List<BitListIpAddress.BitDeviceList> device_list;
    List<Integer> list;
//    boolean isSingSelcet = false;
//    boolean isDouSelect = false;
    boolean status = false;

    public BitListAdapter(Context mCotext, List<BitListIpAddress.BitDeviceList> device_list,boolean status) {
        if (mCotext != null) {
            this.mContext = mCotext;
        }
        if (device_list != null) {
            this.device_list = device_list;
        }
        this.status =status;
    }

    @Override
    public int getCount() {
        return device_list == null ? 0 : device_list.size();
    }

    @Override
    public Object getItem(int position) {
        return device_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int thisNumber = position + 1;
        list = new ArrayList<Integer>();
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_bitlist, null);
        }
        TextView mTvSingNumber = (TextView) convertView.findViewById(R.id.tv_sing_number);
        TextView mTvDouNumber = (TextView) convertView.findViewById(R.id.tv_dou_number);
        TextView mTvSingStatus = (TextView) convertView.findViewById(R.id.tv_sig_status);
        TextView mTvDouStatus = (TextView) convertView.findViewById(R.id.tv_dou_status);

        RelativeLayout mLlSigLinear = (RelativeLayout) convertView.findViewById(R.id.ll_sig_linear);
        TextView mTvSigIpaddress = (TextView) convertView.findViewById(R.id.tv_sig_ipaddress);
        // final ImageView mIvSigCheck = (ImageView) convertView.findViewById(R.id.iv_sig_check);
        RelativeLayout mLlDouLinear = (RelativeLayout) convertView.findViewById(R.id.ll_dou_linear);
        TextView mTvDouIpaddress = (TextView) convertView.findViewById(R.id.tv_dou_ipaddress);
        ImageView mIvDouCheck = (ImageView) convertView.findViewById(R.id.iv_dou_check);
//        mLlDouLinear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!isSingSelcet){
//                    mLlDouLinear.setSelected(true);
//                    mIvSigCheck.setVisibility(View.VISIBLE);
//                    list.add(position);
//                }else {
//                    if (list.contains(position)){
//                        list.remove(position);
//                    }
//                }
//
//            }
//        });
        LogUtil.log("------" + list);
        if (thisNumber % 2 != 0) {
            //把第二个隐藏
            mLlDouLinear.setVisibility(View.GONE);
            //显示数据
            mLlSigLinear.setBackgroundResource(R.color.white);
            mTvSingNumber.setText("大屏" + device_list.get(position).device_alias + "号");
            mTvSigIpaddress.setText(device_list.get(position).ip_addr);
            if (status) {
                LogUtil.log("-----------------"+device_list.get(position).status);
                mTvSingStatus.setVisibility(View.VISIBLE);
                if (device_list.get(position).status==1) {
                    mTvSingStatus.setText("在线");
                } else {
                    mTvSingStatus.setText("离线");
                }
            } else {
                mTvSingStatus.setVisibility(View.GONE);
            }

        } else {
            //把第yi个隐藏
            mLlSigLinear.setVisibility(View.GONE);
            //显示数据
            mLlDouLinear.setBackgroundResource(R.color.ll_listIP);
            mTvDouNumber.setText("大屏" + device_list.get(position).device_alias + "号");
            mTvDouIpaddress.setText(device_list.get(position).ip_addr);
            if (status) {
                mTvDouStatus.setVisibility(View.VISIBLE);
                if (device_list.get(position).status==1) {
                    mTvDouStatus.setText("在线");
                } else {
                    mTvDouStatus.setText("离线");
                }
            } else {
                mTvDouStatus.setVisibility(View.GONE);
            }
        }
        return convertView;
    }
}
