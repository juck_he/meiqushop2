package cn.meiqu.mich.activity.me;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.activity.adapter.BannerAdapter;
import cn.meiqu.mich.activity.me.accredit.AccreditActivity;
import cn.meiqu.mich.activity.me.checkpay.PayTestActivity;
import cn.meiqu.mich.activity.me.device.DeviceActivity;
import cn.meiqu.mich.activity.me.expense.ExpenseActivity;
import cn.meiqu.mich.activity.me.membermanger.MemberActivity;
import cn.meiqu.mich.activity.me.realpay.RealPayActivity;
import cn.meiqu.mich.activity.me.setting.SettingActivity;
import cn.meiqu.mich.activity.me.shopstore.ShopStoreActivity;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.BannerPhoto;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.view.RippleView;
import cn.meiqu.mich.view.ViewPagerTouch;

public class MeActivity extends BaseActivity implements RippleView.OnRippleCompleteListener, View.OnClickListener {

    int[] imgIds = new int[]{R.id.ll_wifisetup, R.id.ll_screen_list};
    String className = getClass().getName();
    String action_bindDevice = className + API.bindDevice;
    public String action_getBeannerPhoto = className + API.getBeannerPhoto;

    private ViewPagerTouch mVpShPhoto;
    private RippleView mRipplePayCheck;
    private RippleView mRipplePayRel;
    private RippleView mRippleExpenseCount;
    private RippleView mRippleScreenSetup;
    private RippleView mRippleMemberManger;
    private RippleView mRippleShopSetup;
    private ImageView mSpinnerImageView;
    private AnimationDrawable spinner;
    List<BannerPhoto.InfoEntity> listInfo;
    private List<View> views;
    private List<ImageView> dot_layouts;
    private LinearLayout dot_layout;
    private int dotselect = 0;
    private ImageView mIvClose;
    ArrayList<LinearLayout> listImg;
    private FrameLayout mFlFlLayoutShow;
    private static final int DELAY_TIME = 6000;
    public static final int SCROLL_WHAT = 0;
    private RippleView mRippleMessageInfo;
    private int firstToClose;
    private int lastToClose;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            LogUtil.log("-------接收到信息");
            mVpShPhoto.setCurrentItem(mVpShPhoto.getCurrentItem() + 1);
            mHandler.removeMessages(SCROLL_WHAT);
            mHandler.sendEmptyMessageDelayed(SCROLL_WHAT, DELAY_TIME);
        }
    };

    private void assignViews() {
        initTitle("我的");
        setTitleRight("更多", this);
        mVpShPhoto = (ViewPagerTouch) findViewById(R.id.vp_sh_photo);
        mRipplePayCheck = (RippleView) findViewById(R.id.ripple_payCheck);
        mRipplePayRel = (RippleView) findViewById(R.id.ripple_payRel);
        mRippleExpenseCount = (RippleView) findViewById(R.id.ripple_expenseCount);
        mRippleScreenSetup = (RippleView) findViewById(R.id.ripple_screen_setup);
        mRippleMemberManger = (RippleView) findViewById(R.id.ripple_member_manger);
        mRippleMessageInfo = (RippleView) findViewById(R.id.ripple_message_info);
        mRippleShopSetup = (RippleView) findViewById(R.id.ripple_shop_fitment);
        dot_layout = (LinearLayout) findViewById(R.id.ll_dot_layout);
        mFlFlLayoutShow = (FrameLayout) findViewById(R.id.fl_layout_show);
        mSpinnerImageView = (ImageView) findViewById(R.id.spinnerImageView);
        mSpinnerImageView.setVisibility(View.VISIBLE);
        // 获取ImageView上的动画背景
        spinner = (AnimationDrawable) mSpinnerImageView.getBackground();
        // 开始动画
        spinner.start();
    }

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        mContext = this;
        assignViews();
        //TODO 发送http请求 ,获取到bean图
        requestData();
        initReceiver(new String[]{action_bindDevice, action_getBeannerPhoto, action_bindDevice});
    }

    private void requestData() {
        HttpGetController.getInstance().getBeannerPhoto("", className);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        //自定义事件的代码需要放在Activity里的onResume方法后面，不支持放在onCreat()方法中。
        initEvnet();
        //自动翻转图片
        startAutoScrolled();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopAutoScrolled();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


    public void initEvnet() {
        mRipplePayCheck.setOnRippleCompleteListener(this);
        mRipplePayRel.setOnRippleCompleteListener(this);
        mRippleExpenseCount.setOnRippleCompleteListener(this);
        mRippleScreenSetup.setOnRippleCompleteListener(this);
        mRippleMemberManger.setOnRippleCompleteListener(this);
        mRippleShopSetup.setOnRippleCompleteListener(this);
        mRippleMessageInfo.setOnRippleCompleteListener(this);

        mIvClose = (ImageView) findViewById(R.id.iv_close);
        listImg = new ArrayList<LinearLayout>();
        for (int i = 0; i < imgIds.length; i++) {
            LinearLayout linear = (LinearLayout) findViewById(imgIds[i]);
            linear.setVisibility(View.INVISIBLE);
            linear.setOnClickListener(this);
            listImg.add(linear);
        }
        mIvClose.setVisibility(View.INVISIBLE);
        mFlFlLayoutShow.setVisibility(View.INVISIBLE);
        mIvClose.setOnClickListener(this);
        mFlFlLayoutShow.setOnClickListener(this);
    }

    @Override
    public void initFragment() {

    }

    public void requestBindDevice(String deviceId) {
        showProgressDialog("");
        HttpGetController.getInstance().bindDevice(deviceId, className);
    }

    public void handleBindDevice(String data) {
        toast("绑定成功");
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_bindDevice)) {
                handleBindDevice(data);
            }
        }
        if (action.equals(action_getBeannerPhoto)) {
            handlerBannerPhoto(data);
        }
        if (getHttpStatus(action, data)) {
            if (action.equals(action_bindDevice)) {
                handleBindDevice(data);
            }
        }
    }


    @Override
    public void onComplete(RippleView v) {
//        if (v.getId() == mRippleViewMainImage.getId()) {
//            //用户信息
//            jump(UserInfoActivity.class);
//        }
//        if (v.getId() == mRippleAccreditManger.getId()) {
//            //授权管理
//            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_SuperUser);
//            jump(AccreditActivity.class);
//        }
//        else if (v.getId() == mRippleWifiSetting.getId()) {
//            //WiFi设置
//            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_WiFi_SetUp);
//            Intent intent = new Intent(this, DeviceActivity.class);
//            intent.putExtra("type", DeviceActivity.openWifiSetting);
//            jump(intent);
//        }
//        else if (v.getId() == mRippleDeviceList.getId()) {
//            //大屏列表
//            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Beauty_List);
//            Intent intent = new Intent(this, DeviceActivity.class);
//            intent.putExtra("type", DeviceActivity.openDevivceList);
//            jump(intent);
//        }
//        else if (v.getId() == mRippleDeviceBind.getId()) {
//            //绑定大屏
//            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Bingding_Screen);
//            jumpForResult(ScanActivity.class, ScanActivity.requestCode);
//        }
        if (v.getId() == mRipplePayRel.getId()) {
            //实时收款
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Real_Time_Gathering);
            jump(RealPayActivity.class);
        } else if (v.getId() == mRippleExpenseCount.getId()) {
            //消费统计
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Custom_Count);
            jump(ExpenseActivity.class);
        } else if (v.getId() == mRippleScreenSetup.getId()) {
            //美屏设置
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Beauty_Setup);
            //jump(BeautyScreenActivity.class);
            openImgView();
        } else if (v.getId() == mRipplePayCheck.getId()) {
            //支付验证
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Pay_Verify);
            jump(PayTestActivity.class);
        } else if (v.getId() == mRippleMemberManger.getId()) {
            //会员管理
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Member_Manger);
            jump(MemberActivity.class);
        } else if (v.getId() == mRippleShopSetup.getId()) {
            //店家商铺
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Shop_Store);
            jump(ShopStoreActivity.class);
        } else if (v.getId() == mRippleMessageInfo.getId()) {
//            ToastUtil.showTip(mContext, "敬请期待");
//            showDialog("敬请期待");
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_SuperUser);
            jump(AccreditActivity.class);
        }
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(mContext).setPositiveButton("确定", null).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_title_more) {
            jump(SettingActivity.class);
        } else if (v.getId() == R.id.fl_layout_show) {
            closeImgView();
        } else if (v.getId() == R.id.iv_close) {
            closeImgView();
        } else if (v.getId() == R.id.ll_wifisetup) {
            //打开WiFi设置界面
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_WiFi_SetUp);
            Intent intent = new Intent(mContext, DeviceActivity.class);
            intent.putExtra("type", DeviceActivity.openWifiSetting);
            jump(intent);
        }

//        else if (v.getId() == R.id.ll_banner_screen) {
//            //打开绑定大屏
//            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Bingding_Screen);
//            jumpForResult(ScanActivity.class, ScanActivity.requestCode);
//        }

        else if (v.getId() == R.id.ll_screen_list) {
            //大屏列表
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Beauty_List);
            Intent beautyScreenIntent = new Intent(mContext, DeviceActivity.class);
            beautyScreenIntent.putExtra("type", DeviceActivity.openDevivceList);
            jump(beautyScreenIntent);
        }
    }

    private void handlerBannerPhoto(final String data) {
        if (spinner != null) {
            spinner = null;
        }
        mSpinnerImageView.setVisibility(View.GONE);
        dot_layouts = new ArrayList<ImageView>();
        views = new ArrayList<View>();
        if (TextUtils.isEmpty(data)) {
            toast("网络连接失败");
            //mVpShPhoto.setOffscreenPageLimit(1);
            for (int i = 0; i < 2; i++) {
                views.add(View.inflate(mContext, R.layout.item_banner, null));
            }
            dot_layout.removeAllViews();
            for (int i = 0; i < 2; i++) {
                ImageView image = new ImageView(mContext);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15,
                        15);
                if (i != 0) {
                    params.leftMargin = 10;
                }
                image.setLayoutParams(params);
                image.setBackgroundResource(R.drawable.selector_dot);
//                image.setSelected(false);
                dot_layout.addView(image);
                dot_layouts.add(image);
            }
        } else {
            BannerPhoto bannerPhoto = new Gson().fromJson(data, BannerPhoto.class);
            listInfo = bannerPhoto.info;
            if (listInfo != null) {
                // 为viewpager准备好填充的view和点
                for (int i = 0; i < listInfo.size(); i++) {
                    views.add((RelativeLayout) View.inflate(mContext, R.layout.item_banner, null));
                }
                dot_layout.removeAllViews();
                for (int i = 0; i < listInfo.size(); i++) {
                    ImageView image = new ImageView(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15,
                            15);
                    if (i != 0) {
                        params.leftMargin = 10;
                    }
                    image.setLayoutParams(params);
                    image.setBackgroundResource(R.drawable.selector_dot);
                    //image.setSelected(false);
                    dot_layout.addView(image);
                    dot_layouts.add(image);
                }
            }
        }
        dot_layouts.get(dotselect).setSelected(true);
        mVpShPhoto.setOnSlideListen(new ViewPagerTouch.OnSlideListen() {
            @Override
            public void slideImage(int x, int y) {
                startAutoScrolled();
            }

            @Override
            public void slide() {
                stopAutoScrolled();
            }
        });
        mVpShPhoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        stopAutoScrolled();
                        break;
                    case MotionEvent.ACTION_UP:
                        startAutoScrolled();
                        break;
                }
                return false;
            }
        });
        mVpShPhoto.setAdapter(new BannerAdapter(mContext, listInfo, views));
        mVpShPhoto.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                LogUtil.log("-----------前一个点：" + dotselect);
                if (data != null) {
                    dot_layouts.get(dotselect).setSelected(false);
                    dotselect = position % listInfo.size();
                    LogUtil.log("-----------后一个点：" + dotselect);
                    // 把下一次选进来
                    dot_layouts.get(dotselect).setSelected(true);
                } else {
                    dot_layouts.get(dotselect).setSelected(false);
                    dotselect = position % 2;
                    LogUtil.log("-----------后一个点：" + dotselect);
                    dot_layouts.get(dotselect).setSelected(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    public void handleScan(String result) {
        if (!StringUtil.isEmpty(result)) {
            if (result.contains("device_id")) {
                String device_id = StringUtil.getParam(result).get("device_id");
                requestBindDevice(device_id);
            } else {
                toast("二维码不正确");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ScanActivity.requestCode) {
                String result = data.getStringExtra(ScanActivity.result);
                handleScan(result);
                LogUtil.log("result=" + result);
            }
        }
    }

    private void startAutoScrolled() {
        mHandler.sendEmptyMessageDelayed(SCROLL_WHAT, DELAY_TIME);
    }

    private void stopAutoScrolled() {
        mHandler.removeMessages(SCROLL_WHAT);
    }

    private void closeImgView() {
        //lastToClose = (int) (mIvClose.getX() - listImg.get(1).getX());
        //firstToClose = (int) (mIvClose.getX() - listImg.get(0).getX());
        AnimatorSet animatorSet =  new AnimatorSet();
        for (int i = listImg.size()-1; i >= 0; i--) {
            float posttion4 = 0f;
            float posttion2 = ScreenUtil.dip2px(mContext,30);
            float posttion3 = -ScreenUtil.dip2px(mContext,30);
            float posttion1 = -((ScreenUtil.dip2px(mContext,100)+ScreenUtil.dip2px(mContext,100)*i));
            //ObjectAnimator alpha = Obje
            LogUtil.log("----posttion1:"+posttion1+"-----posttion2:"+posttion2+"-----posttion3:"+posttion3+"-----posttion4:"+posttion4);
            ObjectAnimator translationY = ObjectAnimator.ofFloat(listImg.get(i), "translationY", posttion1, posttion2, posttion3, posttion4);
            ObjectAnimator alpha = ObjectAnimator.ofFloat(listImg.get(i), "alpha", 1, 0);
            animatorSet.playTogether(translationY,alpha);
            animatorSet.setDuration(10000);
            animatorSet.setStartDelay(i * 1000);
            animatorSet.start();
        }
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mIvClose.setVisibility(View.INVISIBLE);
                mFlFlLayoutShow.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void openImgView() {
        firstToClose = (int) (mIvClose.getY() - listImg.get(0).getY());
        mFlFlLayoutShow.setVisibility(View.VISIBLE);
        mIvClose.setVisibility(View.VISIBLE);
        //
        LogUtil.log("--------firstToClose:"+firstToClose);
        AnimatorSet animatorSet = null;
        for (int i = 0; i < imgIds.length; i++) {
            listImg.get(i).setVisibility(View.VISIBLE);
            ObjectAnimator translationY;
          //  translationY = ObjectAnimator.ofFloat(listImg.get(i), "translationY",);
            ObjectAnimator alpha = ObjectAnimator.ofFloat(listImg.get(i), "alpha", 0, 1);
            animatorSet = new AnimatorSet();
           // animatorSet.playTogether(translationY, alpha);
          //  animatorSet.playTogether(translationY,alpha);
            animatorSet.setDuration(1000);
            animatorSet.setStartDelay(i * 100);
            //设置差值期
            // animatorSet.setInterpolator(new BounceInterpolator());
            animatorSet.start();
        }

    }
}
