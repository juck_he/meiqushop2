package cn.meiqu.mich.activity;

import android.animation.ValueAnimator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.broadcast.SmsObserver;
import cn.meiqu.mich.common.Common;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.view.ClearEditText;

/**
 * Created by Administrator on 2015/10/20.
 */
public class ForgetPwdActivity extends BaseActivity implements View.OnClickListener {
    public static final int MSG_RECEIVED_CODE = 33;
    private SmsObserver mObserver;
    private ClearEditText mEdInpeutPhone;
    private Button mBtnSendCode;
    private ClearEditText mEdCode;
    private ClearEditText mEdInpeutPwd;
    private ClearEditText mEdInpeutRepwd;
    private Button mBtnOk;
    String className = getClass().getName();
    String action_getVerificationCode = className + API.getVerificationCode;
    String action_setNewPwd = className + API.setNewPwd;

    private void assignViews() {
        initTitle("找回密码");
        mEdInpeutPhone = (ClearEditText) findViewById(R.id.ed_inpeut_phone);
        mBtnSendCode = (Button) findViewById(R.id.btn_send_code);
        mEdCode = (ClearEditText) findViewById(R.id.ed_code);
        mEdInpeutPwd = (ClearEditText) findViewById(R.id.ed_inpeut_pwd);
        mEdInpeutRepwd = (ClearEditText) findViewById(R.id.ed_inpeut_repwd);
        mBtnOk = (Button) findViewById(R.id.btn_ok);
        mBtnSendCode.setEnabled(false);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_RECEIVED_CODE) {
                String code = (String) msg.obj;
                //update the UI
                mEdCode.setText(code);
            }
        }
    };

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getVerificationCode)) {
            LogUtil.log("-----action_getVerificationCode" + data);
            if (data == null) {
                toast("网络连接失败...");
            } else {
                toast("验证码正在发送....");
            }
        }
        if (action.equals(action_setNewPwd)) {
            LogUtil.log("-----action_setNewPwd" + data);
            dismissProgressDialog();
            if (data == null) {
                toast("网络连接失败...");
            } else {
                try {
                    //13719031054
                    JSONObject jsonObject = new JSONObject(data);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        toast("密码修改成功...");
                        finish();
                    }else {
                       showDialog(jsonObject.getString("error_msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {

        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(ForgetPwdActivity.this).setPositiveButton("确定", null).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    @Override
    public void initFragment() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_pwd);
        assignViews();
        initReceiver(new String[]{action_getVerificationCode, action_setNewPwd});
        initEvent();
        mObserver = new SmsObserver(ForgetPwdActivity.this, mHandler);
        Uri uri = Uri.parse("content://sms");
        getContentResolver().registerContentObserver(uri, true, mObserver);
    }

    private void initEvent() {
        mEdInpeutPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LogUtil.log("--------------s" + s);
                if (s.length() == 11) {
                    mBtnSendCode.setEnabled(true);
                } else {
                    mBtnSendCode.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBtnSendCode.setOnClickListener(this);
        mBtnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_code:
                //TODO 修改状态
                //1.拿着手机号码发送Http请求
                String phoneNumber = mEdInpeutPhone.getText().toString().trim();
                //修改button的文字
                getValues();
                ScreenUtil.hideKeyBroad(ForgetPwdActivity.this);
                //发送请求
                getVerificationCode(phoneNumber);

                break;
            case R.id.btn_ok:
                String phone = mEdInpeutPhone.getText().toString().trim();
                String code = mEdCode.getText().toString().trim();
                String inputPwd = mEdInpeutPwd.getText().toString().trim();
                String inputRePwd = mEdInpeutRepwd.getText().toString().trim();

                if (TextUtils.isEmpty(phone)) {
                    toast("请输入手机号码...");
                    return;
                }
                if (phone.length() == 11) {

                }else {
                    toast("输入的手机号码有误...");
                    return;
                }
                if (TextUtils.isEmpty(code)) {
                    toast("请入验证码...");
                    return;
                }
                if (!(code.length() == 6)) {
                    toast("验证码有误...");
                    return;
                }
                if (TextUtils.isEmpty(inputPwd)) {
                    toast("请输入密码...");
                    return;
                }
                if (TextUtils.isEmpty(inputRePwd)) {
                    toast("请再次输入密码...");
                    return;
                }
                if (!inputPwd.equals(inputRePwd)) {
                    toast("两次输入的密码不一样...");
                    return;
                }
                setNewPwd(phone, code, inputPwd);
                break;
        }
    }

    private void setNewPwd(String phone, String code, String inputPwd) {
        showProgressDialog("");
        HttpGetController.getInstance().setNewPwd(phone, Common.CLIENT_KEY, code, inputPwd, className);
    }

    private void getVerificationCode(String phoneNumber) {
        HttpGetController.getInstance().getVerificationCode(phoneNumber, "4", className);
    }

    public void getValues() {
        //设置这个数值发生器值的范围是0-60
        mBtnSendCode.setEnabled(false);
        mBtnSendCode.setText("重新发送 (60s)");
        ValueAnimator values = ValueAnimator.ofInt(1, 60);
        //设置数值发生器多长时间变化从0变化到60
        values.setDuration(59000);
        values.start();
        values.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //获取这数值
                Integer intteger = (Integer) animation.getAnimatedValue();
                LogUtil.log("--------" + intteger);
                mBtnSendCode.setText("重新发送 (" + (60 - intteger) + "s)");
                if ((60 - intteger) == 0) {
                    if (mEdInpeutPhone.length() == 11) {
                        mBtnSendCode.setEnabled(true);
                    }
                    mBtnSendCode.setText("发送验证码");
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(mObserver);
    }

}
