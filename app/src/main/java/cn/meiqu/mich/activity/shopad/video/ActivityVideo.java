package cn.meiqu.mich.activity.shopad.video;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/11/25.
 */
public class ActivityVideo extends BaseActivity {
    private FragmentVideo mFragmentViedo;
    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void initFragment() {
        mFragmentViedo = new FragmentVideo();
        showFirst(mFragmentViedo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        initFragment();
    }
}
