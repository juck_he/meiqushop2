package cn.meiqu.mich.activity.me.accredit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Accredit;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.PatternUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Fatel on 15-9-23.
 */
public class FragmentAccreditEdit extends BaseFragment implements View.OnClickListener {
    String className = getClass().getName();
    String action_add = className + API.addStyList;
    String action_edt = className + API.edtStyList;
    private EditText mTvAccount;
    private EditText mTvPassword;
//    private EditText mTvPhone;
    private Button mBtnSave;
    private Context mContext;
    public static Accredit accredit = new Accredit();
    boolean isAdd = true;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_add, action_edt});
        mContext = getActivity();
        if (contain == null) {

            contain = inflater.inflate(R.layout.f_accredit_edt, null);
            assignViews();
        }

        return contain;
    }

    private void assignViews() {
        if (accredit == null) {
            accredit = new Accredit();
            isAdd = true;
        } else {
            isAdd = false;
        }

        mTvAccount = (EditText) findViewById(R.id.tv_account);
        mTvPassword = (EditText) findViewById(R.id.tv_password);
//        mTvPhone = (EditText) findViewById(R.id.tv_phone);
        mBtnSave = (Button) findViewById(R.id.btn_save);
        mBtnSave.setOnClickListener(this);
        if (isAdd) {
            initTitle("增加账号");
            mBtnSave.setText("生成");
        } else {
            initTitle("修改授权账号");
            mBtnSave.setText("保存");
            mTvAccount.setText(accredit.user_account + "");
            mTvPassword.setText("");
            mTvAccount.setEnabled(false);
        }
    }

    public void requsetAdd() {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().addStylist(token,accredit, className);
    }

    public void handleAdd(String data) {
        toast("增加账号成功");
        ((BaseActivity) getActivity()).popBack();
    }

    public void requestEdt() {
        showProgressDialog("");
        HttpGetController.getInstance().edtStylist(accredit, className);
    }

    public void handleEdt(String data) {
        toast("修改账号成功");
        ((BaseActivity) getActivity()).popBack();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_add)) {
                handleAdd(data);
            } else if (action.equals(action_edt)) {
                handleEdt(data);
            }
        }
    }

    public void checkInput() {
        accredit.user_account = mTvAccount.getText().toString();
        accredit.user_pwd = mTvPassword.getText().toString();
//        accredit.phone = mTvPhone.getText().toString();

        if (StringUtil.isBlank(accredit.user_account)) {
            toast("请输入授权手机号");
            return;
        }

        if (!PatternUtil.isPhone(accredit.user_account)) {
            toast("手机号输入格式不正确");
            return;
        }

        if (StringUtil.isBlank(accredit.user_pwd)) {
            toast("请输入密码");
            return;
        }
//        if (StringUtil.isBlank(accredit.phone)) {
//            toast("请输入授权用户的手机号");
//            return;
//        }

//        if (!PatternUtil.isPhone(accredit.phone)) {
//            toast("手机号输入格式不正确");
//            return;
//        }
        if (isAdd) {
            requsetAdd();
        } else {
            requestEdt();
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnSave.getId()) {
            checkInput();
        }
    }
}
