package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.ProMubanlist;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by juck on 15/12/8.
 */
public class MubanListAdapter extends RecyclerView.Adapter<MubanListAdapter.Holder> {
    Context mContext;
    List<ProMubanlist.InfoEntity.ListEntity> mubanList;

    public MubanListAdapter(Context mContext, List<ProMubanlist.InfoEntity.ListEntity> mubanLists) {
        this.mContext = mContext;
        this.mubanList = mubanLists;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.item_muban, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return mubanList.size();
    }

    class Holder extends BaseHolder {

        private ImageView mSpinnerImageView;
        private TextView mTvShopName2;

        public Holder(View itemView) {
            super(itemView);
        }

        private ImageView mIvMubanItem;
        private TextView mTvShopName;
        private RelativeLayout mRlItemList;

        public void assignViews() {
            mIvMubanItem = (ImageView) findViewById(R.id.iv_muban_item);
            mSpinnerImageView = (ImageView)findViewById(R.id.spinner_muban_list);
            mTvShopName = (TextView) findViewById(R.id.tv_shop_name);
            mTvShopName2 = (TextView) findViewById(R.id.tv_shop_name2);
            mRlItemList = (RelativeLayout) findViewById(R.id.rl_item_list);
        }

        @Override
        public void instanceView(final int position) {
            String company_name = PrefUtils.getUser(mContext).company_name;
//            String company_name = "傻豪傻豪豪尚·发型定制（天河南店）";
            mTvShopName2.setVisibility(View.GONE);
            LogUtil.log("----------company_name.length:" + company_name.length());
            if (company_name.length()>9){
                mTvShopName.setTextSize(ScreenUtil.px2sp(mContext, 30));
                String name1= company_name.substring(0, 8);
                String name2 = company_name.substring(9,company_name.length());
                mTvShopName.setText(name1);
                mTvShopName2.setVisibility(View.VISIBLE);
                mTvShopName2.setText(name2);
            }else {
                mTvShopName.setText(company_name);
            }
            String textColor = "#"+mubanList.get(position).title_color;
            mTvShopName.setTextColor(Color.parseColor(textColor));
            mTvShopName2.setTextColor(Color.parseColor(textColor));
            ImageLoadHelper.displayImage(mubanList.get(position).bg_image, null, new MyImgeViewLoader(mSpinnerImageView, mIvMubanItem));
            mRlItemList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickLitener!=null){
                        mOnItemClickLitener.onItemClick(mRlItemList,position);
                    }
                }
            });
        }
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    class  MyImgeViewLoader implements ImageLoadingListener {
        private ImageView loaderImageView;
        private AnimationDrawable spinner;
        private ImageView dataView;

        public MyImgeViewLoader(ImageView view,ImageView data){
            this.loaderImageView = view;
            this.dataView = data;
        }
        @Override
        public void onLoadingStarted(String s, View view) {
            loaderImageView.setVisibility(View.VISIBLE);
            // 获取ImageView上的动画背景
            spinner = (AnimationDrawable) loaderImageView.getBackground();
            // 开始动画
            spinner.start();
        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {
            spinner.stop();
            loaderImageView.setVisibility(View.GONE);
        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            spinner.stop();
            loaderImageView.setVisibility(View.GONE);
            dataView.setImageBitmap(bitmap);
        }

        @Override
        public void onLoadingCancelled(String s, View view) {

        }
    }


}
