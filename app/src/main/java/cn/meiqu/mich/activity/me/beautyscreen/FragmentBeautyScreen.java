package cn.meiqu.mich.activity.me.beautyscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.activity.adapter.BannerAdapter;
import cn.meiqu.mich.activity.me.accredit.AccreditActivity;
import cn.meiqu.mich.activity.me.device.DeviceActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.BannerPhoto;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/10/19.
 */
public class FragmentBeautyScreen extends BaseFragment implements View.OnClickListener {

    private Context mContext;
    private ViewPager mVpBeanPhoto;
    private RelativeLayout mLlSetupWifi;
    private RelativeLayout mLlBingScreen;
    private RelativeLayout mLlScreenList;
    private RelativeLayout mLlSuperUser;
    public String className = getClass().getName();
    public String action_getBeannerPhoto = className + API.getBeannerPhoto;
    List<BannerPhoto.InfoEntity> listInfo;
    private List<View> views;
    private List<ImageView> dot_layouts;
    private LinearLayout dot_layout;
    private int dotselect = 0;

    private void assignViews() {
        initTitle("美屏设置");
        mVpBeanPhoto = (ViewPager) findViewById(R.id.vp_bean_photo);
        mLlSetupWifi = (RelativeLayout) findViewById(R.id.ll_setup_wifi);
        mLlBingScreen = (RelativeLayout) findViewById(R.id.ll_bing_screen);
        mLlScreenList = (RelativeLayout) findViewById(R.id.ll_screen_list);
        mLlSuperUser = (RelativeLayout) findViewById(R.id.ll_super_user);
        dot_layout = (LinearLayout) findViewById(R.id.ll_dot_layout);
    }


    public static FragmentBeautyScreen newInstance() {
        FragmentBeautyScreen fragmentBeautyScreen = new FragmentBeautyScreen();
        return fragmentBeautyScreen;
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getBeannerPhoto)) {
            handlerBannerPhoto(data);
        }
    }

    private void handlerBannerPhoto(final String data) {
        dot_layouts = new ArrayList<ImageView>();
        views = new ArrayList<View>();
        if(TextUtils.isEmpty(data)) {
            toast("网络连接失败");
            //mVpShPhoto.setOffscreenPageLimit(1);
            for (int i = 0; i < 2; i++) {
                views.add(View.inflate(mContext, R.layout.item_banner, null));
            }
            dot_layout.removeAllViews();
            for (int i = 0; i < 2; i++) {
                ImageView image = new ImageView(mContext);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15,
                        15);
                if (i != 0) {
                    params.leftMargin = 10;
                }
                image.setLayoutParams(params);
                image.setBackgroundResource(R.drawable.selector_dot);
//                image.setSelected(false);
                dot_layout.addView(image);
                dot_layouts.add(image);
            }
        } else {
            BannerPhoto bannerPhoto = new Gson().fromJson(data, BannerPhoto.class);
            listInfo = bannerPhoto.info;
            if (listInfo != null) {
                // 为viewpager准备好填充的view和点
                for (int i = 0; i < listInfo.size(); i++) {
                    views.add((RelativeLayout) View.inflate(mContext, R.layout.item_banner, null));
                }
                dot_layout.removeAllViews();
                for (int i = 0; i < listInfo.size(); i++) {
                    ImageView image = new ImageView(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15,
                            15);
                    if (i != 0) {
                        params.leftMargin = 10;
                    }
                    image.setLayoutParams(params);
                    image.setBackgroundResource(R.drawable.selector_dot);
                    //image.setSelected(false);
                    dot_layout.addView(image);
                    dot_layouts.add(image);
                }
            }
        }
        dot_layouts.get(dotselect).setSelected(true);
        mVpBeanPhoto.setAdapter(new BannerAdapter(mContext, listInfo, views));
        mVpBeanPhoto.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LogUtil.log("-----------前一个点：" + dotselect);
                if (data!=null) {
                    dot_layouts.get(dotselect).setSelected(false);
                    dotselect = position % listInfo.size();
                    LogUtil.log("-----------后一个点：" + dotselect);
                    // 把下一次选进来
                    dot_layouts.get(dotselect).setSelected(true);
                }else {
                    dot_layouts.get(dotselect).setSelected(false);
                    dotselect = position % 2;
                    LogUtil.log("-----------后一个点：" + dotselect);
                    dot_layouts.get(dotselect).setSelected(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_screen_setup, null);
            assignViews();
        }
        initReceiver();
        //TODO 发送http请求 ,获取到bean图
        requestData();
        return contain;
    }

    private void requestData() {
//        User user = PrefUtils.getUser(mContext);
        HttpGetController.getInstance().getBeannerPhoto("", className);
    }


    private void initReceiver() {

        initReceiver(new String[]{action_getBeannerPhoto});
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initEvent();

    }

    private void initEvent() {
        mLlSetupWifi.setOnClickListener(this);
        mLlBingScreen.setOnClickListener(this);
        mLlScreenList.setOnClickListener(this);
        mLlSuperUser.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        BeautyScreenActivity beautyScreenActivity = (BeautyScreenActivity) getActivity();
        switch (v.getId()) {
            case R.id.ll_setup_wifi:
                //设置wifi
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_WiFi_SetUp);
                Intent intent = new Intent(mContext, DeviceActivity.class);
                intent.putExtra("type", DeviceActivity.openWifiSetting);
                beautyScreenActivity.jump(intent);
                break;
            case R.id.ll_bing_screen:
                //绑定大屏
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Bingding_Screen);
                beautyScreenActivity.jumpForResult(ScanActivity.class, ScanActivity.requestCode);
                break;
            case R.id.ll_screen_list:
                //大屏列表
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Beauty_List);
                Intent beautyScreenIntent = new Intent(beautyScreenActivity, DeviceActivity.class);
                beautyScreenIntent.putExtra("type", DeviceActivity.openDevivceList);
                beautyScreenActivity.jump(beautyScreenIntent);
                break;
            case R.id.ll_super_user:
                //授权管理
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_SuperUser);
                beautyScreenActivity.jump(AccreditActivity.class);
                break;
        }
    }
}
