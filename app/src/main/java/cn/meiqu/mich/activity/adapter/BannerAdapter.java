package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Fatel.utils.ImageLoadHelper;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.me.beautyscreen.BannerWebViewActivity;
import cn.meiqu.mich.bean.BannerPhoto;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/10/19.
 */
public class BannerAdapter extends PagerAdapter {
    private Context mContext;
    private List<BannerPhoto.InfoEntity> infoData;
    private List<View> views;
    private int[] images = new int[]{R.drawable.banner,R.drawable.banner2};
    public BannerAdapter(Context mContext, List<BannerPhoto.InfoEntity> infoData, List<View> views) {
        this.mContext = mContext;
        // if (infoData != null) {
        this.infoData = infoData;
        this.views = views;
        //}

    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * 类似于BaseAdapger的getView方法 用了将数据设置给view 由于它最多就3个界面，不需要viewHolder
     */
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LogUtil.log("-------------原来：" + position);
        LogUtil.log("-------------" + position % views.size());
        //final int thisposition = position % views.size();

        View view = View.inflate(mContext, R.layout.item_banner, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.iv_banner);
        if (infoData != null) {
            LogUtil.log("--------ad_url" + infoData.get(position % infoData.size()).ad_url);
            //Glide.with(mContext).load(infoData.get(position % infoData.size()).ad_url).into(imageView);
            ImageLoadHelper.displayImage(infoData.get(position % infoData.size()).ad_url,imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ToastUtil.show(mContext, "没有数据...");
                    if (TextUtils.isEmpty((infoData.get(position % views.size()).link_url))) {
                        //ToastUtil.show(mContext, "没有数据...");
                        return;
                    } else {
                        //显示banner的详情
                        LogUtil.log("--------link_url"+infoData.get(position % views.size()).link_url);
                        Intent intent = new Intent(mContext, BannerWebViewActivity.class);
                        intent.putExtra("url_ad", infoData.get(position % views.size()).link_url);
                        mContext.startActivity(intent);
                    }
                }
            });
        }
        else {
            imageView.setImageResource(images[ position % views.size()]);
        }

        container.addView(view);// 一定不能少，将view加入到viewPager中
        return view;
    }

    // 销毁page position： 当前需要消耗第几个page object:当前需要消耗的page
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}