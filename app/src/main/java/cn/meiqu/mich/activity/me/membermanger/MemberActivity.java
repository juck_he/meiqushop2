package cn.meiqu.mich.activity.me.membermanger;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.me.membermanger.history.InterfaceMemberDetail;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/10/8.
 */
public class MemberActivity extends BaseActivity implements InterfaceMemberDetail
{

    public FragmentMemberManager fragmentMemberManager;
    public FragmentUserList fragmentUserList;
    @Override
    public void onHttpHandle(String action, String data) {

    }


    @Override
    public void initFragment() {
        setContainerId(containerId);
        fragmentMemberManager = FragmentMemberManager.newInstance();
        showFirst(fragmentMemberManager);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membermanager);
        initFragment();
    }

    /**
     * 打开user list表
     */
    public void showUserList() {
        fragmentUserList = fragmentUserList.newInstance();
        showAndPop(fragmentUserList);
    }

    @Override
    public void openMemberDetail(String user_id) {
        FragmentMemberDetail fragmentMemberDetail = FragmentMemberDetail.newInstance(user_id);
        showAndPop(fragmentMemberDetail);
    }

//    public void showHistory() {
//        showAndPop(new FragmentHistory());
//    }

}
