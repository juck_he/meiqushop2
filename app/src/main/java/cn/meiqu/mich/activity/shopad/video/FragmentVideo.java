package cn.meiqu.mich.activity.shopad.video;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.GridViewAdapter;
import cn.meiqu.mich.activity.shopad.async.MediaChooserConstants;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.MediaModel;
import cn.meiqu.mich.common.MediaChooser;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/11/25.
 */
public class FragmentVideo extends BaseFragment implements AbsListView.OnScrollListener {
    private final static Uri MEDIA_EXTERNAL_CONTENT_URI = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    private final static String MEDIA_DATA = MediaStore.Video.Media.DATA;
    private Context mContext;
    private Cursor mCursor;
    private GridViewAdapter mVideoAdapter;
    //    private OnVideoSelectedListener mCallback;
    private int mDataColumnIndex;
    private ArrayList<MediaModel> mGalleryModelList;
    private ArrayList<String> mSelectedItems = new ArrayList<String>();

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (view == mVideoGridView) {
            if (scrollState == SCROLL_STATE_FLING) {
            } else {
                mVideoAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

//    public interface OnVideoSelectedListener {
//        void onVideoSelected(int count);
//    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mCallback = (OnVideoSelectedListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString() + " must implement OnVideoSelectedListener");
//        }
//    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    /**
     * 在Activity被销毁的时候，Fragment不会被销毁
     */
    public FragmentVideo() {
        setRetainInstance(true);
    }

    private GridView mVideoGridView;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_video, null);
            assignViews();
            initVideos();
        }
        mContext = getActivity();
        return contain;
    }

    private void assignViews() {
        initTitle("选择视频");
        mVideoGridView = (GridView) findViewById(R.id.gridViewFromMediaChooser);
    }

    /**
     * 获取到视频文件
     */
    public void initVideos() {
        try {
            final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
            //Here we set up a string array of the thumbnail ID column we want to get back
            String[] proj = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};
            //
            mCursor = getActivity().getContentResolver().query(MEDIA_EXTERNAL_CONTENT_URI, proj, null, null, orderBy + " DESC");
            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        int count = mCursor.getCount();
        LogUtil.log("------count" + count);
        if (count > 0) {
            mGalleryModelList = new ArrayList<MediaModel>();
            //
            mDataColumnIndex = mCursor.getColumnIndex(MEDIA_DATA);
            mCursor.moveToFirst();
            //
            for (int i = 0; i < count; i++) {
                mCursor.moveToPosition(i);
                String url = mCursor.getString(mDataColumnIndex);
                //
                mGalleryModelList.add(new MediaModel(url, false));
            }
            //
            mVideoAdapter = new GridViewAdapter(getActivity(), 0, mGalleryModelList, true);
            mVideoAdapter.videoFragment = this;
            //
            mVideoGridView.setAdapter(mVideoAdapter);
            mVideoGridView.setOnScrollListener(this);
        } else {
            //TODO 手机内没有视频数据
            Toast.makeText(getActivity(), "没有数据...", Toast.LENGTH_SHORT).show();
        }
        //TODO 点击事件
        mVideoGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // 更新状态
                GridViewAdapter adapter = (GridViewAdapter) parent.getAdapter();
                MediaModel galleryModel = (MediaModel) adapter.getItem(position);

                long size = MediaChooserConstants.ChekcMediaFileSize(new File(galleryModel.url.toString()), true);
                if (size != 0) {
                    //TODO 选择的视频超过了默认的大小
                    ToastUtil.show(mContext, "选择的视频不能超过20M了");
                    LogUtil.log("---------选择的视频大小" + size);
                    return;
                }
                mSelectedItems.add(galleryModel.url.toString());
                Intent videoIntent = new Intent();
                videoIntent.setAction(MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
                videoIntent.putStringArrayListExtra("list", mSelectedItems);
                getActivity().sendBroadcast(videoIntent);
                getActivity().finish();
            }
        });
    }

    public GridViewAdapter getAdapter() {
        if (mVideoAdapter != null) {
            return mVideoAdapter;
        }
        return null;
    }


}
