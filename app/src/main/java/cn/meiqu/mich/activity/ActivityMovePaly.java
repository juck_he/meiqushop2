package cn.meiqu.mich.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSendController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.view.VerticalSeekBar;

/**
 * Created by Administrator on 2015/10/13.
 */
public class ActivityMovePaly extends BaseActivity implements View.OnClickListener, View.OnLongClickListener {
    private String moveId;
    private String moveName;
    private String movePath;
    private int[] images = new int[]{R.drawable.i_paly_icon, R.drawable.img_stop_icon, R.drawable.i_voice, R.drawable.i_voice_mute};

    private ImageView mIvBackout;
    private TextView mTvMoveName;
    private ImageView mIvRetreat;
    private ImageView mIvPlay;
    private ImageView mIvAdvance;
    private ImageView mIvVoiceAdd;
    private ImageView mIvVoiceCut;
    private ImageView mIvVoiceContorl;
    private VerticalSeekBar mSbVoice;
    private boolean isPlay = true;
    private boolean isVoice = true;
    private int curremtVoice;
    private TcpSendController sendController;
    private int seekBarProgress;
    private boolean isVoiceChange = false;
    private boolean isPlayUp = false;

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(TcpResponController.action_tcp_respon)) {
            LogUtil.log("data -----" + data);
            //String begin = action;
            dismissProgressDialog();
            if (data.equals(TcpCommand.H_ConnectFailure)) {
                if (isVoiceChange) {
                    isVoiceChange = false;
                    mSbVoice.setProgress(seekBarProgress);
                }
                ToastUtil.show(ActivityMovePaly.this, "美屏与手机不在同一WiFi下...");
            } else if (data.equals(TcpCommand.H_ControllerStart)) {
                modifiMoveState();
            } else if (data.equals(TcpCommand.H_SoundLevel)) {
                if (isVoiceChange) {
                    //TODO 设置有声状态
                    isVoiceChange = false;
                    isVoice = true;
                }
                modifiVoiceState();
            } else if (data.equals(TcpCommand.H_ControllerForward)) {
                if (isPlayUp) {
                    isPlayUp = false;
                    isPlay = true;
                    modifiMoveState();
                }
            } else if (data.equals(TcpCommand.H_ControllerBack)) {
                if (isPlayUp) {
                    isPlayUp = false;
                    isPlay = true;
                    modifiMoveState();
                }
            }
        }
    }

    AlertDialog alertDialog;

    public void showDialogBack(String str) {

        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(ActivityMovePaly.this).setNegativeButton("否",null).setPositiveButton("是", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendController.sendFilmClose(movePath);
                    finish();
                }
            }).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ScreenUtil.fullScreenCompat(this);
        setContentView(R.layout.activity_moveplay_face);
        initReceiver(new String[]{TcpResponController.action_tcp_respon});
        getMoveData();
        initView();
        initData();
        LogUtil.log("------onCreate");
    }

    @Override
    public void initFragment() {

    }

    private void initData() {
        sendController = TcpSendController.getInstance();
        //TODO 修改初始化状态为播放状态
        modifiMove();
        modifiVoice();
        //获取当前音量的值并修改
        curremtVoice = mSbVoice.getProgress();

        mIvBackout.setOnClickListener(this);
        mTvMoveName.setText(moveName);
        mIvPlay.setOnClickListener(this);
        mIvVoiceContorl.setOnClickListener(this);
        mIvVoiceAdd.setOnClickListener(this);
        mIvVoiceCut.setOnClickListener(this);
        mIvRetreat.setOnLongClickListener(this);
        mIvAdvance.setOnLongClickListener(this);
        mIvRetreat.setOnClickListener(this);
        mIvAdvance.setOnClickListener(this);
        mSbVoice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                LogUtil.log("--------------" + progress);
                modifiChangVoice(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarProgress = seekBar.getProgress();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initView() {
        mIvBackout = (ImageView) findViewById(R.id.iv_backout);
        mTvMoveName = (TextView) findViewById(R.id.tv_move_name);
        mIvRetreat = (ImageView) findViewById(R.id.iv_retreat);
        mIvPlay = (ImageView) findViewById(R.id.iv_play);
        mIvAdvance = (ImageView) findViewById(R.id.iv_advance);
        mIvVoiceAdd = (ImageView) findViewById(R.id.iv_voice_add);
        mIvVoiceCut = (ImageView) findViewById(R.id.iv_voice_cut);
        mIvVoiceContorl = (ImageView) findViewById(R.id.iv_voice_contorl);
        mSbVoice = (VerticalSeekBar) findViewById(R.id.sb_voice);
    }

    private void getMoveData() {
        Intent intent = getIntent();
        moveId = intent.getStringExtra("moveId");
        moveName = intent.getStringExtra("moveName");
        movePath = intent.getStringExtra("movePath");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_backout:
                showDialogBack("电影正在播放，是否要退出...");
                break;
            case R.id.iv_play:
                if (isPlay) {
                    isPlay = false;
                    //TODO 暂停
                    modifiMove();
                } else {
                    isPlay = true;
                    //TODO 播放
                    modifiMove();
                }
                break;
            case R.id.iv_voice_contorl:
                if (isVoice) {
                    isVoice = false;
                    //TODO 静音
                    modifiVoice();
                } else {
                    isVoice = true;
                    //TODO 有声音
                    modifiVoice();
                }
                break;
            case R.id.iv_voice_add:
                curremtVoice = curremtVoice + 1;
                if (curremtVoice >= 12) {
                    curremtVoice = 12;
                }
                //modifiChangVoice(curremtVoice);
                seekBarProgress = mSbVoice.getProgress();
                mSbVoice.setProgress(curremtVoice);
                break;
            case R.id.iv_voice_cut:
                curremtVoice = curremtVoice - 1;
                if (curremtVoice <= 0) {
                    curremtVoice = 0;
                }
                //modifiChangVoice(curremtVoice);
                seekBarProgress = mSbVoice.getProgress();
                mSbVoice.setProgress(curremtVoice);
                break;
            case R.id.iv_retreat:
                showProgressDialog("");
                isPlayUp = true;
                sendController.sendFilmBack(movePath);
                LogUtil.log("----快退");
                break;
            case R.id.iv_advance:
                showProgressDialog("");
                isPlayUp = true;
                sendController.sendFilmForward(movePath);
                LogUtil.log("----快进");
                break;
        }
    }

    /**
     * 修改音量的大小
     *
     * @param curremtVoice
     */
    private void modifiChangVoice(int curremtVoice) {
        showProgressDialog("");
        LogUtil.log("------------当前的音量为：" + curremtVoice);
        isVoiceChange = true;
        sendController.sendSoundLevel(mSbVoice.getProgress());
        mSbVoice.setProgress(curremtVoice);
    }

    private void modifiMove() {
        showProgressDialog("");
        if (isPlay) {
            LogUtil.log("播放电影----");
            sendController.sendFilmStart(movePath);
            //mIvPlay.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[0]));
        } else {
            LogUtil.log("停止播放----");
            sendController.sendFilmStart(movePath);
            //mIvPlay.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[1]));
        }
    }

    private void modifiMoveState() {
        if (isPlay) {
            LogUtil.log("播放电影状态----");
            //sendController.sendFilmStart(movePath);
            mIvPlay.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[0]));
        } else {
            LogUtil.log("停止播放状态----");
            //sendController.sendFilmStart(movePath);
            mIvPlay.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[1]));
        }
    }

    private void modifiVoice() {
        showProgressDialog("");
        if (isVoice) {
            LogUtil.log("----音量开");
            sendController.sendSoundLevel(mSbVoice.getProgress());
            //mIvVoiceContorl.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[2]));
        } else {
            LogUtil.log("音量关----");
            sendController.sendSoundLevel(0);
            //mIvVoiceContorl.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[3]));
        }
    }

    private void modifiVoiceState() {
        //showProgressDialog("");
        if (isVoice) {
            LogUtil.log("----音量开");
            //sendController.sendSoundLevel(mSbVoice.getProgress());
            mIvVoiceContorl.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[2]));
        } else {
            LogUtil.log("音量关----");
            //sendController.sendSoundLevel(0);
            mIvVoiceContorl.setImageBitmap(BitmapFactory.decodeResource(getResources(), images[3]));
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
//            case R.id.iv_retreat:
//                showProgressDialog("");
//                sendController.sendFilmBack(movePath);
//                LogUtil.log("----快退");
//                break;
//            case R.id.iv_advance:
//                showProgressDialog("");
//                sendController.sendFilmForward(movePath);
//                LogUtil.log("----快进");
//                break;
        }
        return true;
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        switch (event.getAction()) {
//            case KeyEvent.KEYCODE_BACK:
//               showDialogBack("电影正在播放，是否要退出...");
//                break;
//        }
//        return true;
//    }

    @Override
    public void onBackPressed() {
        showDialogBack("电影正在播放，是否要退出...");
    }
}
