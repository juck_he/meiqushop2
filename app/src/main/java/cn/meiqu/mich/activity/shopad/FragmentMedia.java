package cn.meiqu.mich.activity.shopad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.shopad.udp.Udp;
import cn.meiqu.mich.activity.shopad.video.ActivityVideo;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.BitScreenData;
import cn.meiqu.mich.common.MediaChooser;
import cn.meiqu.mich.httpGet.HttpGetBase;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/11/26.
 */
public class FragmentMedia extends BaseFragment implements View.OnClickListener, MediaPlayer.OnErrorListener {

    private Context mContext;
    private LinearLayout mLlSelectVideo;
    private String videoPath;
    private ImageView mIvViedoPlay;
    private ImageView mIvViedoPhoto;
    private int currentPosition = -1;
    private Udp mUdp;
    private String host = "255.255.255.255";
    private boolean isRec = false;
    private String hostIp;
    private Handler  mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                dismissProgressDialog();
            }
        }
    };
    private long currentTime;
    private String className = getClass().getName();
    private String action_setUpdataViedo = className + ":10003/uploadAdVideo";
    private ImageView mIvPhoto;
    private int isFrist;

    @Override
    public void onHttpHandle(String action, String data) {
        if (action_setUpdataViedo.equals(action)) {
            dismissProgressDialog();
            ToastUtil.show(mContext,"视频广告上传成功！");
        }
    }

    /**
     * 在Activity被销毁的时候，Fragment不会被销毁
     */
    public FragmentMedia() {
        setRetainInstance(true);
    }
    private VideoView mSvVideo;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_setUpdataViedo});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_shopad_viedo, null);
            assignViews();
        }
        mContext = getActivity();
        return contain;
    }

    private void assignViews() {
        initTitle("广告制作");
        mSvVideo = (VideoView) findViewById(R.id.sv_video);
        mLlSelectVideo = (LinearLayout) findViewById(R.id.ll_select_viedo);
        mIvViedoPlay = (ImageView) findViewById(R.id.iv_video_play);
        mIvViedoPhoto = (ImageView) findViewById(R.id.iv_photo_vieo);
        mIvPhoto = (ImageView) findViewById(R.id.iv_photo);
        mLlSelectVideo.setOnClickListener(this);
        IntentFilter videoIntentFilter = new IntentFilter(MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
        getActivity().registerReceiver(videoBroadcastReceiver, videoIntentFilter);
        mSvVideo.setVisibility(View.GONE);
        mSvVideo.setOnErrorListener(this);
        mSvVideo.setOnClickListener(this);
        mIvPhoto.setOnClickListener(this);
        mIvViedoPlay.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSvVideo.canPause()) {
            mSvVideo.pause();
            currentPosition = mSvVideo.getCurrentPosition();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentPosition > 0) {
            mSvVideo.seekTo(currentPosition);
            mSvVideo.start();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitleRight("上传", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(videoPath)) {
                    showProgressDialog("");
                    LogUtil.log("---------发送数据");
                    mUdp = Udp.newInstace(9001);
                    mUdp.setOnReceiveListen(new Udp.OnReceiveListen() {
                        @Override
                        public void getReceive(String data, String ip) {
                            isRec = true;
                            //判断是不是同一个WiFi
                            LogUtil.log("------本地IP：" + getLocalIpAddress(BaseApp.mContext) + "--------服务i" + ip);
                            if (getLocalIpAddress(BaseApp.mContext).equals(ip)) {
                            } else {
                                LogUtil.log("--------data" + data);
                                BitScreenData bitScreenData = new Gson().fromJson(data, BitScreenData.class);
                                if (bitScreenData.currentTime != currentTime) {
                                    mUdp.isReceviceOk = true;
                                    mUdp.closeUdp();
                                    currentTime = bitScreenData.currentTime;
                                    HttpGetController.getInstance().setUpdataVideo(ip, videoPath, className);
                                }
                            }
                        }
                    });
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isRec) {
                                if (mUdp.isReceviceOk) {
                                    mUdp.closeUdp();
                                }
                            } else {
                                //再发一次UDP
                                if (isFrist == 1) {
                                    isFrist = 2;
                                    mUdp.toRunUdp();
                                } else {
                                    Message message = Message.obtain();
                                    message.what = 1;
                                    mHandler.sendMessage(message);
                                    mUdp.closeUdp();
                                    ToastUtil.show(mContext, "不在同一个WiFi下");
                                }
                            }
                        }
                    }, 10000);
                    isFrist =1;
                    mUdp.toRunUdp();
                } else {
                    ToastUtil.show(mContext, "至少要选择一张图片!");
                }
            }
        });
    }

    /**
     * 获取到视频的广播
     */
    BroadcastReceiver videoBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<String> list = intent.getStringArrayListExtra("list");
            LogUtil.log("-------视频的路径：" + list.get(0));
            videoPath = list.get(0);
            mIvViedoPhoto.setVisibility(View.VISIBLE);
            mSvVideo.setVisibility(View.GONE);
            mIvViedoPlay.setVisibility(View.VISIBLE);
            if (new File(videoPath).exists()) {
                mIvViedoPhoto.setImageBitmap(getVideoThumbnail(videoPath));
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_select_viedo:
                //
                Intent intentGetImage = new Intent(mContext, ActivityVideo.class);
                startActivity(intentGetImage);
                break;
            case R.id.iv_photo:
                //
                if (mSvVideo.canPause()) {
                    mSvVideo.pause();
                    currentPosition = mSvVideo.getCurrentPosition();
                }
                mIvViedoPlay.setVisibility(View.VISIBLE);
                mIvPhoto.setVisibility(View.GONE);
                break;
            case R.id.iv_video_play:
                //
                play(videoPath,currentPosition);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUdp != null) {
            mUdp.closeUdp();
        }
        HttpGetBase.newInstance().disConnect();
        getActivity().unregisterReceiver(videoBroadcastReceiver);
    }


    private void play(String path,int position ) {
        if (TextUtils.isEmpty(path)) {
            ToastUtil.show(mContext, "还没有视频文件，请点击选择视频！");
            return;
        }
        mIvPhoto.setVisibility(View.VISIBLE);
        mSvVideo.setVisibility(View.VISIBLE);
        mIvViedoPhoto.setVisibility(View.GONE);
        if (mSvVideo != null && mSvVideo.isPlaying()) {
            mIvViedoPlay.setVisibility(View.VISIBLE);
            mIvPhoto.setVisibility(View.GONE);
            mSvVideo.setEnabled(false);
            mSvVideo.stopPlayback();
            return;
        }
        File file = new File(path);
        if (!file.exists()) {
            ToastUtil.show(mContext, "文件不存在！");
            return;
        }
        mSvVideo.setVideoPath(path);
        if (position > 0) {
            mSvVideo.seekTo(position);
            mSvVideo.start();
        }else {
            mSvVideo.start();
        }
        //
        mIvViedoPlay.setVisibility(View.GONE);
        mIvPhoto.setVisibility(View.VISIBLE);
        mSvVideo.setEnabled(true);
        mSvVideo.requestFocus();
        mSvVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mIvViedoPlay.setVisibility(View.VISIBLE);
                mIvPhoto.setVisibility(View.GONE);
                mSvVideo.setEnabled(false);
            }
        });
    }

    /**
     * 视频播放失败
     * @param mp
     * @param what
     * @param extra
     * @return
     */
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        ToastUtil.show(mContext, "你选择的视频不能播放！");
        mIvViedoPlay.setVisibility(View.VISIBLE);
        mSvVideo.setEnabled(false);
        return true;
    }

    public Bitmap getVideoThumbnail(String filePath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(filePath);
            bitmap = retriever.getFrameAtTime();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    /**
     * 获取当前ip地址
     *
     * @param context
     * @return
     */
    public static String getLocalIpAddress(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int i = wifiInfo.getIpAddress();
            return int2ip(i);
        } catch (Exception ex) {
            return " 获取IP出错鸟!!!!请保证是WIFI,或者请重新打开网络!\n" + ex.getMessage();
        }
    }

    /**
     * 将ip的整数形式转换成ip形式
     * @param ipInt
     * @return
     */
    public static String int2ip(int ipInt) {
        StringBuilder sb = new StringBuilder();
        sb.append(ipInt & 0xFF).append(".");
        sb.append((ipInt >> 8) & 0xFF).append(".");
        sb.append((ipInt >> 16) & 0xFF).append(".");
        sb.append((ipInt >> 24) & 0xFF);
        return sb.toString();
    }

    /**
     * 取消连接
     */
    public void disConnect() {
        if (mUdp != null) {
            mUdp.closeUdp();
        }
        HttpGetBase.newInstance().disConnect();
    }

}
