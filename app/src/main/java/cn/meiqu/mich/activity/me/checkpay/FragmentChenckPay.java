package cn.meiqu.mich.activity.me.checkpay;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/28.
 */
public class FragmentChenckPay extends BaseFragment implements View.OnClickListener {
    public static final int SCANCODE =99;
    public String className = getClass().getName();
    public String action_getCheckUseCoupons = className + API.getCheckUseCoupons;
    private EditText mEdCheckpayPwd;
    private ImageView mIvDeletePwd;
    private LinearLayout mIvNumberOne;
    private LinearLayout mIvNumberTwo;
    private LinearLayout mIvNumberThree;
    private LinearLayout mIvNumberFour;
    private LinearLayout mIvNumberFive;
    private LinearLayout mIvNumberSix;
    private LinearLayout mIvNumberSeven;
    private LinearLayout mIvNumberEight;
    private LinearLayout mIvNumberNight;
    private LinearLayout mIvNumberHistory;
    private LinearLayout mIvNumberZero;
    private LinearLayout mIvNumberCheck;
    private String mEditStr;
    private int location = 0;// 记录光标的位置
    private Context mContext;
    private ProgressDialog mProgressDialog;

    public static FragmentChenckPay newInstance() {
        FragmentChenckPay fragmentChenckPay = new FragmentChenckPay();
        return fragmentChenckPay;
    }

    private void assignViews() {
        initTitle("支付验证");
        mTvRight.setVisibility(View.VISIBLE);
        mTvRight.setText("");
        mTvRight.setPadding(6, 3, 6, 3);
//        RelativeLayout.LayoutParams  lp= new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
//        lp.setMargins(0,0,30,0);
//        mTvRight.setLayoutParams(lp);
        mTvRight.setBackgroundResource(R.drawable.i_can_paycheck);
        mTvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ToastUtil.show(mContext, "打开扫一扫...");
                PayTestActivity payTestActivity = (PayTestActivity) getActivity();
                Intent intent = new Intent(payTestActivity,ScanActivity.class);
                payTestActivity.startActivityForResult(intent,SCANCODE);
            }
        });
        mIvNumberOne = (LinearLayout) findViewById(R.id.iv_number_one);
        mIvNumberTwo = (LinearLayout) findViewById(R.id.iv_number_two);
        mIvNumberThree = (LinearLayout) findViewById(R.id.iv_number_three);
        mIvNumberFour = (LinearLayout) findViewById(R.id.iv_number_four);
        mIvNumberFive = (LinearLayout) findViewById(R.id.iv_number_five);
        mIvNumberSix = (LinearLayout) findViewById(R.id.iv_number_six);
        mIvNumberSeven = (LinearLayout) findViewById(R.id.iv_number_seven);
        mIvNumberEight = (LinearLayout) findViewById(R.id.iv_number_eight);
        mIvNumberNight = (LinearLayout) findViewById(R.id.iv_number_night);
        mIvNumberHistory = (LinearLayout) findViewById(R.id.iv_number_history);
        mIvNumberZero = (LinearLayout) findViewById(R.id.iv_number_zero);
        mIvNumberCheck = (LinearLayout) findViewById(R.id.iv_number_check);
        mEdCheckpayPwd = (EditText) findViewById(R.id.ed_checkpay_pwd);
        mIvDeletePwd = (ImageView) findViewById(R.id.iv_delete_pwd);
        mIvNumberOne.setOnClickListener(this);
        mIvNumberTwo.setOnClickListener(this);
        mIvNumberThree.setOnClickListener(this);
        mIvNumberFour.setOnClickListener(this);
        mIvNumberFive.setOnClickListener(this);
        mIvNumberSix.setOnClickListener(this);
        mIvNumberSeven.setOnClickListener(this);
        mIvNumberEight.setOnClickListener(this);
        mIvNumberNight.setOnClickListener(this);
        mIvNumberHistory.setOnClickListener(this);
        mIvNumberZero.setOnClickListener(this);
        mIvNumberCheck.setOnClickListener(this);
        mIvDeletePwd.setOnClickListener(this);
        mEdCheckpayPwd.addTextChangedListener(new TextChang());
        mEdCheckpayPwd.setInputType(InputType.TYPE_NULL);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_check_pay, null);
            assignViews();
        }
        initReceiver();
        return contain;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.log("------data:"+requestCode+"-----"+requestCode);
        if(requestCode == SCANCODE){
            if(data==null){
                return;
            }
            String result = data.getStringExtra("result");
            if(TextUtils.isEmpty(result)){
                ToastUtil.show(mContext,"二维码有误...");
                return;
            }else {
                //TODO 去验证信息
                LogUtil.log("result:" + result);
                Map<String, String> param = StringUtil.getParam(result);
                String pay_id = param.get("pay_id");
                if(TextUtils.isEmpty(pay_id)){
                    ToastUtil.show(mContext,"二维码有误...");
                }else {
                    //ToastUtil.show(mContext, pay_id);
                    showProgreeDialog();
                    checkUseCOupons(pay_id.trim());
                }
            }
        }
    }

    public void initReceiver() {
        initReceiver(new String[]{action_getCheckUseCoupons});
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getCheckUseCoupons)) {
            if (data == null) {

                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                ToastUtil.show(mContext, "网络连接失败，请检查你的网络...");
            } else {
                handleMessage(data);
            }
        }
        //LogUtil.log("action"+action+"-----data:"+data);
    }

    private void handleMessage(String data) {
        try {
            JSONObject json = new JSONObject(data);
            int status = json.getInt("status");
            if (status == -40001) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    //显示验证码错误的对话框
                }
                showTipDialog("你输入的验证码有误,请查询后重新输入");
            } else {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                showTipDialog("验证成功...");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示提示对话框
     */
    private void showTipDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.myDialog);
        View viewDialog = View.inflate(mContext, R.layout.view_tip_upto_bitscreen, null);
        builder.setView(viewDialog, 0, 0, 0, 0);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView title = (TextView) viewDialog.findViewById(R.id.tv_dialog_title);
        title.setText("支付验证");
        TextView message = (TextView) viewDialog.findViewById(R.id.tv_message);
        message.setText(str);
        LinearLayout llBtnOk = (LinearLayout) viewDialog.findViewById(R.id.ll_btnok);
        llBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_number_one:
                setmEditStr(1);
                break;
            case R.id.iv_number_two:
                setmEditStr(2);
                break;
            case R.id.iv_number_three:
                setmEditStr(3);
                break;
            case R.id.iv_number_four:
                setmEditStr(4);
                break;
            case R.id.iv_number_five:
                setmEditStr(5);
                break;
            case R.id.iv_number_six:
                setmEditStr(6);
                break;
            case R.id.iv_number_seven:
                setmEditStr(7);
                break;
            case R.id.iv_number_eight:
                setmEditStr(8);
                break;
            case R.id.iv_number_night:
                setmEditStr(9);
                break;
            case R.id.iv_number_zero:
                setmEditStr(0);
                break;
            case R.id.iv_number_history:
                PayTestActivity payTestActivity = (PayTestActivity) getActivity();
                payTestActivity.showHistory();
                break;
            case R.id.iv_number_check:
                String checkNumber = mEdCheckpayPwd.getText().toString().trim();
                if (TextUtils.isEmpty(checkNumber)) {
                    toast("请输入验证码...");
                    return;
                }
                //发送请求
                checkUseCOupons(checkNumber);
                //弹出progressDialog
                showProgreeDialog();
                break;
            case R.id.iv_delete_pwd:
                String trim = mEdCheckpayPwd.getText().toString().trim();
                if (TextUtils.isEmpty(trim)) {
                    return;
                }
                mEdCheckpayPwd.setText(trim.substring(0, trim.length() - 1));
                break;

        }

    }

    /**
     * 获取editview的字符串
     *
     * @return
     */
    public void setmEditStr(int number) {
        String trim = mEdCheckpayPwd.getText().toString().trim();
        if (trim.length()>13){
            toast("验证码的长度最多为12位哦！");
            return;
        }
        mEdCheckpayPwd.setText(trim + number);
    }

    /**
     * 连网请求查询数据
     */
    public void checkUseCOupons(String pay_id) {
        HttpGetController instance = HttpGetController.getInstance();
        instance.checkUseCoupons(PrefUtils.getUser(mContext).token, pay_id, className);
    }

    /**
     * 显示不确定进度对话框
     */
    public void showProgreeDialog() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("正在验证中...");
        mProgressDialog.show();
    }

    class TextChang implements TextWatcher {
        int beforeTextLength = 0;
        int onTextLength = 0;
        boolean isChanged = false;

        private char[] tempChar;
        private StringBuffer buffer = new StringBuffer();
        int konggeNumberB = 0;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
            onTextLength = s.length();
            buffer.append(s.toString());
            //长度是0，onTextLength<=3,（挡长度变为4的时候，ischanged =true）
            if (onTextLength == beforeTextLength || onTextLength <= 3 || isChanged) {
                isChanged = false;
                return;
            }
            isChanged = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
            beforeTextLength = s.length();
            if (buffer.length() > 0) {
                buffer.delete(0, buffer.length());
            }
            konggeNumberB = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ' ') {
                    konggeNumberB++;
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            if (isChanged) {
                location = mEdCheckpayPwd.getSelectionEnd();
                int index = 0;
                while (index < buffer.length()) {
                    if (buffer.charAt(index) == ' ') {
                        buffer.deleteCharAt(index);
                    } else {
                        index++;
                    }
                }

                index = 0;
                int konggeNumberC = 0;
                while (index < buffer.length()) {
                    if (index % 5 == 4) {
                        buffer.insert(index, ' ');
                        konggeNumberC++;
                    }
                    index++;
                }

                if (konggeNumberC > konggeNumberB) {
                    location += (konggeNumberC - konggeNumberB);
                }

                tempChar = new char[buffer.length()];
                buffer.getChars(0, buffer.length(), tempChar, 0);
                mEditStr = buffer.toString();
                if (location > mEditStr.length()) {
                    location = mEditStr.length();
                } else if (location < 0) {
                    location = 0;
                }

                mEdCheckpayPwd.setText(mEditStr);
                Editable etable = mEdCheckpayPwd.getText();
                Selection.setSelection(etable, location);
                isChanged = false;
            }
        }
    }
}
