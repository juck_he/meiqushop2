package cn.meiqu.mich.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;
import com.umeng.analytics.MobclickAgent;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.util.UploadUtil;

/**
 * Created by Administrator on 2015/9/18.
 */
public class ActivityPreView extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMore;
    private ImageView mIvPerviewImg;
    private Context mContext;
    private File file;
    private boolean isUrl = false;

    String ad_img_url;
    String ad_img_uri;
    String className = getClass().getName();
    String action_setShopAdPhoto = className + API.setShopAdPhoto;


    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            dismissProgressDialog();
            if (action.equals(action_setShopAdPhoto)) {
                ToastUtil.show(mContext, "上传轮番图成功...");

            } else {
                ToastUtil.show(mContext, "上传轮番图失败...");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        initReceiver(new String[]{action_setShopAdPhoto});
        this.mContext = this;
        ad_img_uri = getIntent().getStringExtra("ad_img_uri");
        ad_img_url = getIntent().getStringExtra("ad_img_url");
        initView();
        initEvent();

        if (!TextUtils.isEmpty(ad_img_uri)) {
            file = new File(ad_img_uri);
            if (file.exists()) {
                try {
                    byte[] bytes = readStream(file);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    mIvPerviewImg.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (!TextUtils.isEmpty(ad_img_url)) {
                isUrl = true;
                ImageLoadHelper.displayImage(ad_img_url,mIvPerviewImg);
               // Glide.with(this).load(ad_img_url).into(mIvPerviewImg);
            }
        }
    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onResume(this);
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPause(this);
    }
    @Override
    public void initFragment() {

    }

    private void initEvent() {
        mTvMore.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    private void initView() {
        mIvPerviewImg = (ImageView) findViewById(R.id.iv_perview_img);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        mTvTitle.setText("预览效果");
        mTvMore.setText("上存");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_more:
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.ADManager_UploadMemberBannerPic);
                //在这里上存图片
                if (isUrl) {
                    LogUtil.log("----------------");
                    upPhotoUrl(ad_img_url);
                } else {
                    new MyTask().execute(file.toString());
                }
                break;
        }
    }

    public byte[] readStream(File fl) throws Exception {
        FileInputStream inStream = new FileInputStream(fl);
        byte[] buffer = new byte[1024];
        int len = -1;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        byte[] data = outStream.toByteArray();
        outStream.close();
        inStream.close();
        return data;
    }


    public void submitHeadIcon(String str) {
        LogUtil.log("-------------传递过来的路径" + str);

        UploadUtil uploadUtil = new UploadUtil(mContext, new UploadUtil.QiuNiuUpLoadListener() {
            @Override
            public void onUpLoadSucceed(String url) {
               dismissProgressDialog();
                //PrefUtils.setString(mContext, "head_pic", url);//存取head_pic
                upPhotoUrl(url);
                LogUtil.log("--------------返回来的Url" + url);
                ToastUtil.show(mContext, "上传轮番图成功...");
            }

            @Override
            public void onUpLoadFailure(String erroMessage) {
                ToastUtil.show(mContext, "上传轮番图失败...");
                dismissProgressDialog();
            }
        });
        uploadUtil.initReceiver();
        uploadUtil.upLoad(str);
    }


    public class MyTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("");
        }

        @Override
        protected Void doInBackground(String... params) {
            submitHeadIcon(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // mProgressDialog.dismiss();
        }
    }

    /**
     * 上存图片
     *
     * @param url
     */
    public void upPhotoUrl(String url) {
        HttpGetController instance = HttpGetController.getInstance();
        User user = PrefUtils.getUser(mContext);
        LogUtil.log("-------user.token:" + user.token);
        if (isUrl) {
            showProgressDialog("");
        }
        LogUtil.log("user.token:" + user.token + "--------url" + url + "-----+className" + className);
        instance.setShopName(user.token, url, className);
    }
}
