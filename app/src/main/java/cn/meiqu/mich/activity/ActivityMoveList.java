package cn.meiqu.mich.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.RecyclerAdapterMoveLiset;
import cn.meiqu.mich.base.BaseActivity;
import cn.meiqu.mich.bean.DeviceFilm;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ToastUtil;

public class ActivityMoveList extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMore;

    private SwipeRefreshLayout mSwipeMoveList;
    private RecyclerView mMyRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerAdapterMoveLiset adapter;
    private String className = getClass().getName();
    private String action_getDeviceFilmList = className+ API.getDeviceFilmList;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_list);
        this.mContext = this;
        initReceiver(new String[]{action_getDeviceFilmList});
        initView();
        initData();
        requestData();

    }

    @Override
    public void initFragment(int containerId) {

    }

    @Override
    public void onHttpHandle(String action, String data) {
        if(action.equals(action_getDeviceFilmList)){
            LogUtil.log("电影列表data"+data);
            if (mSwipeMoveList.isRefreshing()) {
                mSwipeMoveList.setRefreshing(false);
            }
            if(data==null){
                ToastUtil.show(mContext,"美屏和手机不在同一WiFi下...");
            }else {
                LogUtil.log("data"+data);
                handleMoveList(data);
            }
        }
    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onResume(this);
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /**
     * 处理电影列表数据
     * @param data
     */
    private void handleMoveList(String data) {
        String info= JsonUtil.getJsonObject(data).optString("info");
        final ArrayList<DeviceFilm> deviceFilms = new Gson().fromJson(info,new TypeToken<ArrayList<DeviceFilm>>(){}.getType());
        adapter = new RecyclerAdapterMoveLiset(this,deviceFilms);
        mMyRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickLitener(new RecyclerAdapterMoveLiset.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                //ToastUtil.show(mContext, "点击了" + position);
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Movie_Play);
                Intent intent = new Intent(ActivityMoveList.this, ActivityMovePaly.class);
                intent.putExtra("moveId",deviceFilms.get(position).ad_id);
                intent.putExtra("moveName",deviceFilms.get(position).ad_name);
                intent.putExtra("movePath",deviceFilms.get(position).ad_path);
                startActivity(intent);
            }
        });
    }

    private void initData() {

        mIvBack.setOnClickListener(this);
        mTvTitle.setText("电影列表");
        mTvMore.setVisibility(View.GONE);

        mMyRecyclerView.setHasFixedSize(true);
        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
        mLayoutManager = new LinearLayoutManager(this);
        mMyRecyclerView.setLayoutManager(mLayoutManager);
        mMyRecyclerView.setVerticalScrollBarEnabled(false);

        mSwipeMoveList.setColorSchemeResources(android.R.color.holo_red_light);
        //mSwipeMoveList.setRefreshing(true);
        mSwipeMoveList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 请求数据获取电影列表
                requestData();
            }
        });

    }

    /**
     * 获取电影列表的请求
     */
    private void requestData() {
        HttpGetController.getInstance().getDeviceFilmList(className);
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        mSwipeMoveList = (SwipeRefreshLayout) findViewById(R.id.swipe_move_list);
        mMyRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }


}
