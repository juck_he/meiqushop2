package cn.meiqu.mich.activity.shopad;

import android.content.Context;
import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/11/25.
 */
public class ActivityShopAd extends BaseActivity {
    private FragmentImage mFragmentImage;
    private FragmentMedia mFragmentMedia;
    private static boolean isSelectImage = false;
    private Context mContext;
    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void initFragment() {
        if (isSelectImage) {
            mFragmentImage = new FragmentImage();
            showFirst(mFragmentImage);
        }else {
            mFragmentMedia = new FragmentMedia();
            showFirst(mFragmentMedia);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_shopap);
        isSelectImage = getIntent().getBooleanExtra("isSelectImage",false);
        initFragment();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isSelectImage){
            LogUtil.log("-------取消连接");
            mFragmentImage.disConnect();
        }else {
            LogUtil.log("-------取消连接");
            mFragmentMedia.disConnect();
        }
    }
}
