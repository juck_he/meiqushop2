package cn.meiqu.mich.activity.wifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.util.List;

/**
 * Created by Administrator on 2015/10/21.
 */
public class WifiAdmin {

    private WifiManager wifiManager;
    private List<WifiConfiguration> configuratedList;

    public WifiAdmin(Context context) {
        // TODO Auto-generated constructor stub
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    /**
     * 获取之前连接过的WiFi，可以不用输入密码
     * @return
     */
    public List<WifiConfiguration> getConfiguratedList(){
        configuratedList = wifiManager.getConfiguredNetworks();
        return configuratedList;
    }

    /**
     * 连接到WiFi
     * @param netId
     */
    public void connectWifi(int netId){
        wifiManager.enableNetwork(netId, true);
    }

    /**
     * 获取到当前WiFi的连接结果
     * @return
     */
    public boolean getPingResult(){

        return wifiManager.pingSupplicant();
    }

    /**
     * 判断WiFi是否打开了
     * @return
     */
    public boolean isWifiEnable(){
        return wifiManager.isWifiEnabled();
    }

    /**
     *获取到当前扫描到的WiFi
     * @return
     */
    public List<ScanResult> getScanResultList(){
        wifiManager.startScan();
        return wifiManager.getScanResults();
    }

    /**
     * 获取当前WiFi的连接状态
     * @return
     */
    public WifiInfo getCurrentWifiInfo(){
        return wifiManager.getConnectionInfo();
    }
}
