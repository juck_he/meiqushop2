package cn.meiqu.mich.activity.me.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.LoginActivity;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.PatternUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Fatel on 15-9-22.
 */
public class FragmentChangePwd extends BaseFragment implements View.OnClickListener {
    String className = getClass().getName();
    String action_changePwd = className + API.changePwd;
    private EditText mEdtOldPw;
    private EditText mEdtNewPw;
    private EditText mEdtConfirmPw;
    private Button mBtnCommit;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_change_pwd, null);
            assignViews();
        }
        return contain;
    }

    public void initReceiver() {
        initReceiver(new String[]{action_changePwd});
    }

    private void assignViews() {
        initTitle("修改密码");
        mEdtOldPw = (EditText) findViewById(R.id.edt_old_pw);
        mEdtNewPw = (EditText) findViewById(R.id.edt_new_pw);
        mEdtConfirmPw = (EditText) findViewById(R.id.edt_confirm_pw);
        mBtnCommit = (Button) findViewById(R.id.btn_commit);
        mBtnCommit.setOnClickListener(this);
    }

    public void requestChangePwd(String oldPwd, String newPwd) {
        showProgressDialog("");
        HttpGetController.getInstance().changePwd(oldPwd, newPwd, className);
    }

    public void handleChangePwd(String data) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setMessage("密码修改成功，请重新登录").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                User user = PrefUtils.getUser(getActivity());
                user.token = null;
                PrefUtils.setUser(getActivity(), user);
                ((BaseActivity) getActivity()).jump(LoginActivity.class);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(BaseActivity.action_exitApplication));
            }
        }).setCancelable(false).create();
        alertDialog.show();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_changePwd)) {
                handleChangePwd(data);
            }
        } else {
            toast("当前密码不正确");
        }

    }

    public void checkInput() {
        String newpw = mEdtNewPw.getText().toString();
        String confirmpw = mEdtConfirmPw.getText().toString();
        String oldpw = mEdtOldPw.getText().toString();

        if (StringUtil.isBlank(newpw)
                || StringUtil.isBlank(confirmpw)
                || StringUtil.isBlank(oldpw)) {
            toast("请填写完整");
            return;
        }

        if (!newpw.equals(confirmpw)) {
            toast("输入密码不一致,请确认");
            return;
        }

        if (newpw.equals(oldpw)) {
            toast("新密码不能和旧密码一样");
            return;
        }
        if (newpw.length() < 6) {
            toast("请输入6~18位密码");
            return;
        }

        if (!PatternUtil.isPassword(newpw)) {
            toast("请检查密码格式");
            return;
        }
        requestChangePwd(oldpw, newpw);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnCommit.getId()) {
            checkInput();
        }
    }
}
