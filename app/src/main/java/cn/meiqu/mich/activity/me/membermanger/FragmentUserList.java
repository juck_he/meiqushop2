package cn.meiqu.mich.activity.me.membermanger;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.UserListAdapter;
import cn.meiqu.mich.activity.me.membermanger.history.InterfaceMemberDetail;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.UserInfo;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/10/9.
 */
public class FragmentUserList extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private Context mContext;
    String className = getClass().getName();
    String action_getUserList = className + API.getUserList;
    String action_setCouponToMember = className + API.setCouponToMember;
    String action_sendTextPush = className + API.sendTextPush;

    private List<UserInfo.InfoEntity> list = new ArrayList<UserInfo.InfoEntity>();
    private ListView mListUser;
    private CheckBox mCbSelectAll;
    private UserListAdapter adapter;
    private TextView mIvCommit;
    private TextView mIvTotal;
    private LinearLayout mLlBottomFrame;
    List<String> listUserName = new ArrayList<String>();

    // private RecyclerView mRecyclerView;
    // private SwipeRefreshLayout mSwiperRefUser;

    public static FragmentUserList newInstance() {
        FragmentUserList fragmentUserList = new FragmentUserList();
        return fragmentUserList;
    }

    public static boolean isShowCheckBox = true;
    public static boolean isPush = false;
    public static String content = "";

    private void assignViews() {
        initTitle("会员列表");

        //setSwipeRefresh(R.id.swiperRef_user, this);
        //mSwiperRefUser = (SwipeRefreshLayout) findViewById(R.id.swiperRef_user);
        mListUser = (ListView) findViewById(R.id.list_user);
        //mRecyclerView = (RecyclerView) findViewById(R.id.rv_pricelist);
        mCbSelectAll = (CheckBox) findViewById(R.id.cb_select_all);
        mIvCommit = (TextView) findViewById(R.id.iv_commit);
        mIvTotal = (TextView) findViewById(R.id.tv_total);
        mLlBottomFrame = (LinearLayout) findViewById(R.id.ll_bottom_frame);


        //TODO 是否要不要显示底部的选择框（查看会员列表时不要显示）
        if (!isShowCheckBox) {
            mLlBottomFrame.setVisibility(View.GONE);
        } else {
            mLlBottomFrame.setVisibility(View.VISIBLE);
        }
        mIvCommit.setOnClickListener(this);
        //mSwiperRefUser.setColorSchemeResources(android.R.color.holo_red_light);
//        mSwiperRefUser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //加载数据
//                //请求完数据后关闭下拉刷新
//                requestData();
//            }
//        });
        mListUser.setOnItemClickListener(this);
        mCbSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //选中全部
                    selectAll();
                } else {
                    //取消选中
                    selectNull();
                }
            }
        });

    }

    /**
     * 取消选中
     */
    private void selectNull() {
        //修改选中人数
        if (list.size() < 0) {
            return;
        }
        mIvTotal.setText("合计：0人");
        int childCount = list.size();
        for (int i = 0; i < childCount; i++) {
            list.get(i).isSelect = false;
        }
        listUserName.clear();//清空集合
        adapter.notifyDataSetChanged();
        mCbSelectAll.setChecked(false);
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getUserList)) {
            //mSwiperRefUser.setRefreshing(false);
            LogUtil.log("-------------+"+data);
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "网络请求失败,请检查网络是否正常...");
                return;
            }
            handleUserData(data);
        }
        if (action.equals(action_setCouponToMember)) {
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "网络请求失败,请检查网络是否正常...");
                return;
            }
            ((BaseActivity) getActivity()).popBack();
        }
        if (action.equals(action_sendTextPush)) {
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "网络请求失败,请检查网络是否正常...");
                return;
            }
            ((BaseActivity) getActivity()).popBack();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assignViews();
        //initData();
        requestData();
    }

    private void handleUserData(String data) {
        if (data != null) {
            UserInfo userInfo = new Gson().fromJson(data, UserInfo.class);
            if (userInfo.info != null) {
                //list.clear();
                // list.addAll(userInfo.info);
                list = userInfo.info;
                adapter = new UserListAdapter(mContext, list);
                mListUser.setAdapter(adapter);
                if (isShowCheckBox) {
                    //全部选中
                    selectAll();
                    mCbSelectAll.setChecked(true);
                } else {
                    //每个item不要显示checkbox
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).isShowCheckBox = false;
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    /**
     * 选中全部
     */
    private void selectAll() {
        //获取listview的总数
        if (list.size() <= 0) {
            return;
        }
        int childCount = list.size();
        listUserName.clear();
        mIvTotal.setText("合计：" + childCount + " 人");
        for (int i = 0; i < childCount; i++) {
            list.get(i).isSelect = true;
            //同时把userID给添加进来
            listUserName.add(list.get(i).user_id);
        }
        adapter.notifyDataSetChanged();
        mCbSelectAll.setChecked(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_user_list, null);

        }
        return contain;
    }

//    private void initData() {
//
//
////        mRecyclerView.setHasFixedSize(true);
////        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
////        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
////        mRecyclerView.setLayoutManager(mLayoutManager);
////        mRecyclerView.setVerticalScrollBarEnabled(false);
////        mRecyclerView.setAdapter(adapter);
////        adapter.setOnItemClickLitener(this);
////        adapter = new UserListAdapter(mContext, list);
////        mListUser.setAdapter(adapter);
//
//
//    }

    public void initReceiver() {
        initReceiver(new String[]{action_getUserList, action_setCouponToMember, action_sendTextPush});
    }


    /**
     * 请求数据
     */
    private void requestData() {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getUserList(token, null, null, null, className);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // CheckBox cbSelect = (CheckBox) view.findViewById(R.id.cb_select);
        //toast("position" + position);
        if (isShowCheckBox) {
            if (!listUserName.contains(list.get(position).user_id)) {
                listUserName.add(list.get(position).user_id);
                list.get(position).isSelect = true;
                adapter.notifyDataSetChanged();
            } else {
                listUserName.remove(list.get(position).user_id);
                list.get(position).isSelect = false;
                adapter.notifyDataSetChanged();
            }
            //更新选中人数
            mIvTotal.setText("合计：" + listUserName.size() + " 人");
            //LogUtil.log("------"+list.toString());
        } else {
            //TODO 显示会员详情信息
            if(mContext instanceof InterfaceMemberDetail){
                InterfaceMemberDetail listener = (InterfaceMemberDetail) mContext;
                listener.openMemberDetail(list.get(position).user_id);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_commit:
                if (listUserName.size() <= 0) {
                    toast("请选中推送给那些会员...");
                    return;
                }
                StringBuffer userIds = new StringBuffer();
                for (int i = 0; i < listUserName.size(); i++) {
                    if (i == (listUserName.size() - 1)) {
                        userIds.append(listUserName.get(i));

                    } else {
                        userIds.append(listUserName.get(i) + ",");
                    }

                }
                if (isPush) {
                    //TODO 优惠券推送
                    LogUtil.log("-------优惠券推送");
                    pushCoupon(userIds.toString());
                } else {
                    //TODO 文字推送
                    LogUtil.log("-------文字推送");
                    pushText(content, userIds.toString());
                }

                break;
        }
    }

    private void pushText(String content, String userId) {
        showProgressDialog("");
        //MemberActivity memberActivity = (MemberActivity) getActivity();
        String token = PrefUtils.getUser(mContext).token;
        //String coupon_id = memberActivity.fragmentMemberManager.getCouponId() + "";
        LogUtil.log("-------" + content);
        HttpGetController.getInstance().sendTextPush(token, userId, content, null, null, null, className);
    }

    /**
     * 推送优惠券给会员
     */
    private void pushCoupon(String userId) {
        showProgressDialog("");
        MemberActivity memberActivity = (MemberActivity) getActivity();
        String token = PrefUtils.getUser(mContext).token;
        String coupon_id = memberActivity.fragmentMemberManager.getCouponId() + "";
        LogUtil.log("-------" + userId + "=======coupon_id:" + coupon_id);
        HttpGetController.getInstance().setCouponToMember(token, coupon_id, userId, className);
    }


//    @Override
//    public void onItemClick(View view, int position) {
//
//    }
//
//    @Override
//    public void onItemLongClick(View view, int position) {
//
//    }
}
