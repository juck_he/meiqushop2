package cn.meiqu.mich.activity.me.realpay;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.RecycleRealPayHistoryAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.RealPayHistroy;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.view.superrecyclerview.OnMoreListener;
import cn.meiqu.mich.view.superrecyclerview.SuperRecyclerView;

/**
 * Created by Fatel on 15-9-28.
 */
public class FragmentRealPayHistroy extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {
    String className = getClass().getName();
    String action_histroy = className + API.getRealPayHistroy;
    int page = 1;
    int per_page = 20;
    private SuperRecyclerView mRecycleV;
    RecycleRealPayHistoryAdapter adapter;
    ArrayList<RealPayHistroy> realPayHistroys = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_histroy});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_realpay_history, null);
            assignViews();
            showProgressDialog("");
            requestHistroy(page, per_page);
        }
        return contain;
    }


    private void assignViews() {
        initTitle("实时收款历史");
        mRecycleV = (SuperRecyclerView) findViewById(R.id.recycleV);
        mRecycleV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecycleV.setRefreshListener(this);
        mRecycleV.setupMoreListener(this, 1);
        adapter = new RecycleRealPayHistoryAdapter(getActivity(), realPayHistroys);
        mRecycleV.setAdapter(adapter);
    }

    public void requestHistroy(int page, int per_page) {
        HttpGetController.getInstance().getRealPayHistroy(page, per_page, className);
    }

    public void handleHistroy(String data) {
//        if (TextUtils.isEmpty(data)){
//            showNoMessage("没有内容哦!");
//        }else {
//            dismissMessage();
//        }
        ArrayList<RealPayHistroy> temps = new Gson().fromJson(JsonUtil.getJsonObject(data).optJSONObject("info").optString("list"), new TypeToken<ArrayList<RealPayHistroy>>() {
        }.getType());
        if (page == 1) {
            realPayHistroys.clear();
        }
        realPayHistroys.addAll(temps);
        adapter.notifyDataSetChanged();
        if (temps.isEmpty() || temps.size() < per_page) {
            mRecycleV.removeMoreListener();
        }
    }

    @Override
    public void onHttpHandle(String action, String data) {
        mRecycleV.setRefreshing(false);
        if (getHttpStatus(action, data)) {
            if (action.equals(action_histroy)) {
                handleHistroy(data);
            }
        }
    }

    @Override
    public void onRefresh() {
        page = 1;
        mRecycleV.setupMoreListener(this, 1);
        showProgressDialog("");
        requestHistroy(page, per_page);
    }

    @Override
    public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
        page++;
        requestHistroy(page, per_page);
    }
}
