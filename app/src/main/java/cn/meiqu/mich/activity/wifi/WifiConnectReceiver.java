package cn.meiqu.mich.activity.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.os.Handler;
import android.os.Message;

import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/10/21.
 */
public class WifiConnectReceiver extends BroadcastReceiver {
    private  WifiAdmin wifiAdmin;
    private WifiInfo currentWifiInfo;
    private Handler handler;
    public WifiConnectReceiver(WifiAdmin wifiAdmin, Handler handler) {
        this.wifiAdmin = wifiAdmin;
        this.handler = handler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            Message message = Message.obtain();
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo notewokInfo = manager.getActiveNetworkInfo();

            if (notewokInfo != null) {
                //当前连接的有cmwap 如果是WiFi的话直接显示SSID
                LogUtil.log("------->"+notewokInfo.getExtraInfo());
                currentWifiInfo = wifiAdmin.getCurrentWifiInfo();
                LogUtil.log("currentWifiInfo.getSSID()----->" + currentWifiInfo.getSSID());

                if(currentWifiInfo.getSSID().equals("<unknown ssid>")){
                    //运营商网络
                    message.what=2;
                }else {
                    //Wifi 网络
                    message.what=1;
                    //message.obj = currentWifiInfo.getSSID();
                }
            } else {
                //表示当前没有网络
                LogUtil.log("notewokInfo is null");
                //没有网络状态
                message.what=3;
            }
            handler.sendMessage(message);
        }
    }
}
