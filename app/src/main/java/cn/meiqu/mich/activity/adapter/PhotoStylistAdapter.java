package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.TurnsPhotoData;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/9/2.
 */
public class PhotoStylistAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<View> views;
    private List<TurnsPhotoData.InfoEntity.ImgListEntity> list;

    public PhotoStylistAdapter(Context mContext, List<TurnsPhotoData.InfoEntity.ImgListEntity> img_list, ArrayList<View> views) {
        this.mContext = mContext;
        if (img_list != null) {
            this.list = img_list;
        }
        if (views != null) {
            this.views = views;
        }
    }


    /**
     * PagerAdapter管理数据大小
     */
    @Override
    public int getCount() {
        return list != null ? list.size() : 0;
    }

    /**
     * 关联key 与 obj是否相等，即是否为同一个对象
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * 销毁当前page的相隔2个及2个以上的item时调用
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    /**
     * 当前的page的前一页和后一页也会被调用，如果还没有调用或者已经调用了destroyItem
     */
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final int thisPosition = position;
        LogUtil.log("发型相册viewPager position：" + position);
        View view = views.get(position);
        //
        ImageView hairStylePhoto = (ImageView) view.findViewById(R.id.iv_hairstyle_photoalbum);
        ImageLoadHelper.displayImage(list.get(position).img_url, hairStylePhoto);
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.log("------------点击了" + thisPosition);
//                Intent intent = new Intent(mContext, ActivityPhoto.class);
//                intent.putExtra("url", photoAlbum[thisPosition]);
//                mContext.startActivity(intent);
            }
        });
        //
        TextView mTvConnectToBit = (TextView) view.findViewById(R.id.tv_connect_to_bit);
        mTvConnectToBit.setText("所属美屏：" + list.get(position).device_alias);
        //
        ImageView mBtnEdit = (ImageView) view.findViewById(R.id.btn_edit);
        mBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickPressListen != null) {
                    mOnClickPressListen.onClickEdit(views.get(position), position);
                }
            }
        });
        //
        ImageView mBtnDetele = (ImageView) view.findViewById(R.id.btn_detele);
        mBtnDetele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickPressListen != null) {
                    mOnClickPressListen.onClickDel(views.get(position), position);
                }
            }
        });

        container.addView(view);
        return views.get(position); // 返回该view对象，作为key
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    public interface OnClickPressListen {
        void onClickDel(View view, int position);

        void onClickEdit(View view, int position);
    }

    private OnClickPressListen mOnClickPressListen = null;

    public void setOnClickBtnListen(OnClickPressListen mOnClickPressListen) {
        this.mOnClickPressListen = mOnClickPressListen;
    }


}