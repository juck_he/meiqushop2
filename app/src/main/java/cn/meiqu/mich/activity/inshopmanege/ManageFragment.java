package cn.meiqu.mich.activity.inshopmanege;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.imageshow.ActivityIpList;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.LargeScreenData;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by juck on 15/12/5.
 */
public class ManageFragment extends BaseFragment implements View.OnClickListener {
    private TextView mTvUpdateAd;
    private EditText mEdEditAd;
    private Context mContext;
    //
    String className = getClass().getName();
    String action_setAdToScreen = className + API.setAdToScreen;
    String action_getAdData = className + API.sgetAdToScreen;
    String action_getAdPhoto = className + API.getShopAdPhoto;
    private HttpGetController instance;
    private LinearLayout mTvUpDataText;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getAdData)) {
                //获取到店家的广告
                handerAdData(data);
            } else if (action.equals(action_setAdToScreen)) {
                dismissProgressDialog();
                ToastUtil.show(mContext, "更新广告成功...");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_setAdToScreen, action_getAdData, action_getAdPhoto});
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_manage, null);
            assignViews();
            initEven();
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        requestData();
        return contain;
    }

    private void assignViews() {
        initTitle("字幕广告");
        mTvUpdateAd = (TextView) findViewById(R.id.tv_update_ad);
        mEdEditAd = (EditText) findViewById(R.id.ed_edit_ad);
        mTvUpDataText = (LinearLayout)findViewById(R.id.tv_updata_text);
    }


    /**
     * 把之前的广告何图片给获取下来显示
     */
    private void requestData() {
        instance = HttpGetController.getInstance();
        //获取大屏数据
        instance.getAdData(PrefUtils.getUser(mContext).token, className);
        //获取轮番图
        LogUtil.log("------------user_id:" + PrefUtils.getUser(mContext).user_id);
    }

    private void initEven() {
        mTvUpdateAd.setOnClickListener(this);
        mTvUpDataText.setOnClickListener(this);//查看大屏是否在线
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_update_ad:
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.ADManager_UpdateADTitle);
                //提交广告
                //1.获取广告
                String adContent = mEdEditAd.getText().toString().trim();
                if (TextUtils.isEmpty(adContent)) {
                    ToastUtil.show(mContext, "请输入广告内同...");
                }
                //提交广告
                else {
                    //更新数据大屏数据
                    showProgressDialog("");
                    updataLargeSceenData(adContent);
                    mEdEditAd.setText(adContent);
                }
                break;
            case R.id.tv_updata_text:
                MobclickAgent.onEvent(mContext, KeyCountAnalytics.ADManager_BigScreenList);
                Intent showIpList = new Intent(mContext, ActivityIpList.class);
                showIpList.putExtra("isShowList", true);
                startActivity(showIpList);
                break;
        }
    }

    /**
     * 显示店家之前的广告信息
     *
     * @param data
     */
    private void handerAdData(String data) {
        Gson gson = new Gson();
        LargeScreenData largeScreenData = gson.fromJson(data, LargeScreenData.class);
        if (TextUtils.isEmpty(largeScreenData.info.content)) {
            String company_name = PrefUtils.getUser(mContext).company_name;
            mEdEditAd.setText(company_name + "欢迎您，请尽情享受我们为您定制的美发服务，您的支持是我们进步的动力!");
        } else {
            mEdEditAd.setText(largeScreenData.info.content);
        }
    }

    /**
     * 更新大屏数据
     * @param adContent
     */
    private void updataLargeSceenData(String adContent) {
        instance.setAdToScreen(PrefUtils.getUser(mContext).token, adContent, className);

    }


}
