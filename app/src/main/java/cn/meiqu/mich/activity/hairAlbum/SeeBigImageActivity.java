package cn.meiqu.mich.activity.hairAlbum;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.Fatel.customView.PhotoViewPager;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.adapter.PagerHairAlbumAdapter;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.tcp.TcpSocket;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.StringUtil;

public class SeeBigImageActivity extends BaseActivity implements
        OnPageChangeListener, FlyImageControl.FlyImageListener {
    public static String EXTAR_IMAGEORGIN_PATH = "EXTAR_IMAGEORGIN_PATH";
    public static String EXTAR_IMAGESMALL_PATH = "EXTAR_IMAGESMALL_PATH";
    public static String EXTAR_IMAGE_ID = "ID";
    public static String EXTRA_IMAGE_CURRENT = "EXTRA_IMAGE_CURRENT";
    public static String EXTRA_IMAGE_ACTION = "EXTRA_IMAGE_ACTION";
    public static int EXTRA_ACTION_FLY = 1;
    private Context mContext;
    private TextView tv_count;
    private PhotoViewPager photoViewPager;
    private PagerHairAlbumAdapter adapter;
    ArrayList<String> imageBigs = new ArrayList<String>();
    ArrayList<String> imageSmalls = new ArrayList<String>();
    ArrayList<String> imageIds = new ArrayList<String>();
    private float touchX = 0;
    private float touchY = 0;
    Handler mHandler;
    private int currentPosition = 0;
    private int imageAction = 0;
    int imageQuality = 300;
//    private final int requestScan = 998;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtil.fullScreenCompat(this);
        setContentView(R.layout.activity_big_image);
        initView();
    }


    @Override
    public void initFragment() {

    }

    @Override
    public void onHttpHandle(String action, String data) {

    }


    public void initView() {
        mContext = this;
        tv_count = (TextView) findViewById(R.id.tv_currentCount);
        photoViewPager = (PhotoViewPager) findViewById(R.id.viewP_photo);
        imageBigs = getIntent().getStringArrayListExtra(EXTAR_IMAGEORGIN_PATH);
        imageSmalls = getIntent().getStringArrayListExtra(EXTAR_IMAGESMALL_PATH);
        currentPosition = getIntent().getIntExtra(EXTRA_IMAGE_CURRENT, 1);
        imageAction = getIntent().getIntExtra(EXTRA_IMAGE_ACTION, 0);
        imageIds = getIntent().getStringArrayListExtra(EXTAR_IMAGE_ID);
        if (imageBigs == null)
            imageBigs = new ArrayList<String>();
        if (imageSmalls == null)
            imageSmalls = new ArrayList<String>();
        adapter = new PagerHairAlbumAdapter(mContext, imageBigs, imageSmalls);
        photoViewPager.setAdapter(adapter);
        photoViewPager.setCurrentItem(currentPosition);
        photoViewPager.setOnPageChangeListener(this);
        tv_count.setText((currentPosition + 1) + "/" + imageBigs.size());
        mHandler = new Handler();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_IMAGE_CURRENT, currentPosition);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        FlyImageControl.getInstance().setFlyImageListener(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        FlyImageControl.getInstance().setFlyImageListener(null);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        FlyImageControl.getInstance().leaveImage();
        super.onDestroy();
    }


    boolean isSending = false;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (imageAction == EXTRA_ACTION_FLY) {
            // TODO Auto-generated method stub
            if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                touchX = ev.getX();
                touchY = ev.getY();
                isSending = false;
            } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
                if (Math.abs(ev.getX() - touchX) < 20 && touchY - ev.getY() > 20) {
                    if (!isSending) {
                        isSending = true;
                        sendImage();
                    }
                }
            } else if (ev.getAction() == MotionEvent.ACTION_CANCEL
                    || ev.getAction() == MotionEvent.ACTION_UP) {
            }
        }
        return super.dispatchTouchEvent(ev);
    }

//    public void gotoScan() {
//        jumpForResult(ScanActivity.class, requestScan);
//    }

    @Override
    public void onConnectSucceed() {
        dismissProgressDialog();
        toast("控制成功");
    }

    @Override
    public void onConnectFailured() {
        dismissProgressDialog();
        FlyImageControl.getInstance().showConnectDialog(this);
    }

    @Override
    public void onFlyImageSucceed() {
        dismissProgressDialog();
    }

    @Override
    public void onFlyImageFailured() {
        dismissProgressDialog();
    }

    byte[] b;

    @Override
    public void onFlyImageUpLoad(String name) {
        ImageView imageView = (ImageView) ((ViewGroup) photoViewPager.findViewById(currentPosition)).getChildAt(2);
        if (name.equals(imageName)) {
            showProgressDialog("");
            imageByte = ImageUtil.compressImageByMaxSize(ImageUtil.drawableToBitmap(imageView.getDrawable()), imageQuality);
            FlyImageControl.getInstance().upLoadImage(getImageName(), imageByte);
        }
    }

    public String getImageName() {
        return (PrefUtils.getUser(this).user_id + "_" + imageIds.get(currentPosition) + ".jpg").trim();
    }

    ImageView imageFei;
    byte[] imageByte = null;

    public void createImageFeiView(Drawable drawable) {
        if (imageFei == null) {
            imageFei = (ImageView) findViewById(R.id.imgV_feitu);
        }
        imageFei.setImageBitmap(ImageUtil.drawableToBitmap(drawable));
        imageFei.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_top_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageFei.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageFei.startAnimation(animation);
    }

//    public void showConnectDialog() {
//        AlertDialog dialog = new AlertDialog.Builder(this).setMessage("是否现在去扫二维码控制?").setTitle("美屏未控制").setPositiveButton("马上去", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                gotoScan();
//            }
//        }).setNegativeButton("不去", null).create();
//        dialog.show();
//    }

    String imageName = "";

    public void sendImage() {
        if (StringUtil.isEmpty(TcpSocket.getInstance().ip)) {
            FlyImageControl.getInstance().showConnectDialog(this);
            return;
        }
        try {
            ImageView imageView = (ImageView) ((ViewGroup) photoViewPager.findViewById(currentPosition)).getChildAt(2);
            if (imageView.getDrawable() != null) {
                imageName = getImageName();
                createImageFeiView(imageView.getDrawable());
                FlyImageControl.getInstance().flyImageWithSound(getImageName());
            }
        } catch (Exception e) {

        }
        //TcpSendController.getInstance().sendUpLoadImageHttp(WebServer.getLocalHost() + imageBigs.get(currentPosition));
    }


    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub
        currentPosition = arg0;
        tv_count.setText((arg0 + 1) + "/" + imageBigs.size());
        final ImageView imageView = (ImageView) ((ViewGroup) photoViewPager.findViewById(currentPosition)).getChildAt(2);
        if (imageView.getDrawable() != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
//                    imageByte = ImageUtil.compressImageByMaxSize(ImageUtil.drawableToBitmap(imageView.getDrawable()), imageQuality);
                }
            }).start();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.log("onActivityResult");
        FlyImageControl.getInstance().setFlyImageListener(this);
        if (resultCode == -1) {
            switch (requestCode) {
                // 如果是直接从相册获取
                case FlyImageControl.requestScan:
                    String result = data.getStringExtra(ScanActivity.result);
                    FlyImageControl.getInstance().handleScanSucceed(this, result);
                    break;
            }
        }
    }
}
