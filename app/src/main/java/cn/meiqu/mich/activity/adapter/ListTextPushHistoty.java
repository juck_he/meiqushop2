package cn.meiqu.mich.activity.adapter;


import android.content.Context;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.TextPushed;

/**
 * Created by Administrator on 2015/10/10.
 */
public class ListTextPushHistoty extends BaseAdapter {
    private Context mContext;
    private List<TextPushed.InfoEntity.ListEntity> list;

    public ListTextPushHistoty(Context mContext, List<TextPushed.InfoEntity.ListEntity> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextPushed.InfoEntity.ListEntity listEntity = list.get(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_text_push, null);
            holder.mTvDataShow = (TextView) convertView.findViewById(R.id.tv_data_show);
            holder.mTvTextpushContent = (TextView) convertView.findViewById(R.id.tv_textpush_content);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag(); // 重新获取ViewHolder
        }

        long when = Long.valueOf(listEntity.news_time) * 1000L;
        String dateStr;
        if(DateUtils.isToday(when)){
            dateStr = "今天"+DateFormat.getTimeFormat(mContext).format(when);
        }else{
            dateStr = DateFormat.getDateFormat(mContext).format(when);
        }
        holder.mTvDataShow.setText(dateStr);
        holder.mTvTextpushContent.setText(listEntity.news_content);

        return convertView;
    }

    class ViewHolder {
        private TextView mTvDataShow;
        private TextView mTvTextpushContent;

    }
}
