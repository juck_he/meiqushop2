package cn.meiqu.mich.activity.imageshow;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.AdStayListAdapter;
import cn.meiqu.mich.base.BaseActivity;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.bean.EditTextPhoto;
import cn.meiqu.mich.bean.TemplateList;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.bean.UserShow;
import cn.meiqu.mich.dialogs.ActionSheet;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.GetPhotoForAlbum;
import cn.meiqu.mich.util.ImageLoadHelper;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PhotoUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.util.UploadUtil;
import cn.meiqu.mich.view.CircleImageView;
import cn.meiqu.mich.view.MyTextView;

public class ActivityStylistAd extends BaseActivity implements ActionSheet.OnActionSheetSelected, DialogInterface.OnCancelListener, View.OnClickListener, View.OnLayoutChangeListener, AdStayListAdapter.OnItemClickLitener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMore;
    private Context mContext;
    private File fileName;
    //屏幕高度
    private int screenHeight = 0;
    //软件盘弹起后所占高度阀值
    private int keyHeight = 0;
    /* 广告文件数据像文件 */
    private static final String IMAGE_FILE_NAME = "stylist_head.jpg";
    /* 请求识别码 */
    private static final int CODE_GALLERY_REQUEST = 11;     //打开相册的请求码
    private static final int CODE_CAMERA_REQUEST = 12;      //打开相机的请求码
    private static final int CODE_RESULT_REQUEST = 13;      //剪切完图片的请求码
    private static final int SELECT_PIC_KITKAT = 14;        //高版本相册打开的请请求码

    String className = getClass().getName();
    String action_setsetTurnsPhoto = className + API.setTurnsPhoto;
    String action_getTemplateList = className + API.getTemplateList;
    //
    private LinearLayout mLlListPhoto;    //轮番图
    private RecyclerView mRvTemplate;     //模板列表
    private MyTextView mTvShoptitlename;  //店名
    private MyTextView mEdStylistName;    //标题
    private EditText mEdContent;          //描述
    private ImageView mLlAdpreviewGg;     //背景
    private FrameLayout mFlPhoto;         //生成背景图
    //
    private FrameLayout mFlPhotoOne;              //模板一
    private CircleImageView mTvStylistHeadphoto;  //头像
    //
    private FrameLayout mFlPhotoTwo;      //模板二
    private ImageView mIvUserHeadicon;    //模板二头像
    private ImageView mIvSetPhotoBg;      //模板二的选择
    //
    private FrameLayout mFlPhotoThree;                 //模板三
    private ImageView mIvStyleadTwodemansion;          //模板的二维码
    private ImageView mIvStyleadSelectTwodemansion;    //模板三的选择
    //
    private UserShow userShow;
    private int currentItemId;
    private AdStayListAdapter mAdStayLisyAdapter;
    private List<TemplateList.InfoEntity.TemplateListEntity> listMuban;
    //编辑形象展示
    private EditTextPhoto mEditTextPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;
        setContentView(R.layout.activity_stylist_ad);
        initReceiver(new String[]{action_setsetTurnsPhoto, action_getTemplateList});
        //获取屏幕高度
        screenHeight = this.getWindowManager().getDefaultDisplay().getHeight();
        //阀值设置为屏幕高度的1/3
        keyHeight = screenHeight / 6;
        //编辑形象展示
        mEditTextPhoto = (EditTextPhoto) getIntent().getSerializableExtra("editphoto");
        //
        initView();
        ininData();
        getTemplateList();
    }

    /**
     * 一体机-形象展示模板列表
     */
    private void getTemplateList() {
        showProgressDialog("");
        User user = PrefUtils.getUser(mContext);
        String token = user.token;
        HttpGetController.getInstance().getTemplateList(token, className);
    }

    @Override
    public void initFragment(int containerId) {
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            dismissProgressDialog();
            if (action.equals(action_getTemplateList)) {
                if (TextUtils.isEmpty(data)) {
                    ToastUtil.show(mContext, "网络连接失败...");
                } else {
                    LogUtil.log("-----------" + data);
                    handlerMubanList(data);
                }
            }
            if (action.equals(action_setsetTurnsPhoto)) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        ToastUtil.show(mContext, "形象更新成功...");
                        Intent intent = new Intent(mContext, ActivityTurnsPhoto.class);
                        if (mEditTextPhoto != null) {
                            intent.putExtra("intent_result_url", userShow.imgUrl);
                            intent.putExtra("intent_result_position", mEditTextPhoto.currentItem);
                            setResult(108, intent);
                            finish();
                        }
                    } else {
                        ToastUtil.log("形象更新失败...");
                    }
                } catch (JSONException e) {
                    ToastUtil.log("形象更新失败...");
                }
            }
        }
    }

    /**
     * 显示模板信息和编辑模板信息
     *
     * @param data
     */
    private void handlerMubanList(String data) {
        TemplateList templateList = new Gson().fromJson(data, TemplateList.class);
        listMuban = templateList.info.template_list;
        mAdStayLisyAdapter = new AdStayListAdapter(mContext, listMuban);
        mRvTemplate.setAdapter(mAdStayLisyAdapter);
        mAdStayLisyAdapter.setOnItemClickLitener(this);
        //
        if (mEditTextPhoto != null) {
            mEdStylistName.setText(mEditTextPhoto.contentName);
            mEdContent.setText(mEditTextPhoto.contentDesc);
            // 通过模板的唯一标识,来判断模板的移动recycleview的位置
            int positionType = -1;
            for (int i = 0; i < listMuban.size(); i++) {
                if (mEditTextPhoto.typeId.equals(listMuban.get(i).template_id)) {
                    //获取到模板的实际位置
                    positionType = i;
                    break;
                }
            }
            LinearLayoutManager lp = (LinearLayoutManager) mRvTemplate.getLayoutManager();
            //找不到
            if (positionType == -1) {
                // 移动模板的位置
                lp.scrollToPosition(0);
                //设置用户名字的颜色 和描述的颜色
                modifyColor(0);
                currentItemId = 0;
            }
            //找到
            else {
                userShow.headPic = mEditTextPhoto.headUrl;
                ImageLoadHelper.displayImage(mEditTextPhoto.headUrl, mTvStylistHeadphoto);
                ImageLoadHelper.displayImage(mEditTextPhoto.headUrl, mIvUserHeadicon);
                ImageLoadHelper.displayImage(mEditTextPhoto.headUrl,mIvStyleadTwodemansion);
//                }
                if (positionType <= listMuban.size()) {
                    //移动模板的位置
                    lp.scrollToPosition(positionType);
                    //设置用户名字的颜色 和描述的颜色
                    modifyColor(positionType);
                    currentItemId = positionType;
                } else {
                    // 移动模板的位置
                    lp.scrollToPosition(0);
                    //设置用户名字的颜色 和描述的颜色
                    modifyColor(0);
                    currentItemId = 0;
                }
            }
        }
        //
        else {
            int styleListPosiotion = PrefUtils.getStyleListPosiotion(mContext);
            LinearLayoutManager lp = (LinearLayoutManager) mRvTemplate.getLayoutManager();
            if (styleListPosiotion <= listMuban.size()) {
                //移动模板的位置
                lp.scrollToPosition(styleListPosiotion);
                //设置用户名字的颜色 和描述的颜色
                modifyColor(styleListPosiotion);
                currentItemId = styleListPosiotion;
            } else {
                //移动模板的位置
                lp.scrollToPosition(0);
                //设置用户名字的颜色 和描述的颜色
                modifyColor(0);
                currentItemId = 0;
            }
        }
        // 设置店家的名字
        String companyName = PrefUtils.getUser(mContext).company_name;
        //根据店名的长度来修改店名
        if (companyName.length() > 10) {
            mTvShoptitlename.setTieleTextSize(ScreenUtil.dip2px(mContext, 10));
        } else {
            mTvShoptitlename.setTieleTextSize(ScreenUtil.dip2px(mContext, 12));
        }
        mTvShoptitlename.setText(companyName);
    }

    /**
     * 修改用户参数
     *
     * @param positoin
     */
    public void modifyColor(int positoin) {
        //模板选择
        if (listMuban.get(positoin).type.equals("1")){
            mFlPhotoOne.setVisibility(View.VISIBLE);
            mFlPhotoTwo.setVisibility(View.GONE);
            mFlPhotoThree.setVisibility(View.GONE);
        }else if (listMuban.get(positoin).type.equals("2")){
            mFlPhotoOne.setVisibility(View.GONE);
            mFlPhotoTwo.setVisibility(View.VISIBLE);
            mFlPhotoThree.setVisibility(View.GONE);
        }else if (listMuban.get(positoin).type.equals("3")){
            mFlPhotoOne.setVisibility(View.GONE);
            mFlPhotoTwo.setVisibility(View.GONE);
            mFlPhotoThree.setVisibility(View.VISIBLE);
        }
        //设置左边的图片
        for (int i = 0; i < listMuban.size(); i++) {
            listMuban.get(i).isSelect = false;
        }
        listMuban.get(positoin).isSelect = true;
        mAdStayLisyAdapter.notifyDataSetChanged();
        //设置设置背景图
        ImageLoadHelper.displayImage(listMuban.get(positoin).bg_img, null, new MyImageLoadingListener(mLlAdpreviewGg));
        //店家名字的颜色
        mTvShoptitlename.setPaintColor(listMuban.get(positoin).title_edge_color, listMuban.get(positoin).company_color);
        mTvShoptitlename.invalidate();
        mTvShoptitlename.setFocusable(false);
        //设置用户名字的颜色
        mEdStylistName.setPaintColor(listMuban.get(positoin).title_edge_color, listMuban.get(positoin).title_color);
        mEdStylistName.invalidate();
        //设置描述的颜色
        mEdContent.setTextColor(Color.parseColor("#" + listMuban.get(positoin).desc_color));
        //判断是不是显示背景还是头像
        if (listMuban.get(positoin).type.equals("1")) {
            //是头像
            mTvStylistHeadphoto.setVisibility(View.VISIBLE); //显示头像
            mIvUserHeadicon.setVisibility(View.INVISIBLE);//隐藏背景
            mIvSetPhotoBg.setVisibility(View.GONE);//隐藏提示

        }
        if (listMuban.get(positoin).type.equals("2")) {
            //是背景
            mTvStylistHeadphoto.setVisibility(View.INVISIBLE); //hint头像
            mIvUserHeadicon.setVisibility(View.VISIBLE);//show背景
            mIvSetPhotoBg.setVisibility(View.VISIBLE);//显示提示
        }
    }

    private void ininData() {
        userShow = new UserShow();
        mIvBack.setOnClickListener(this);
        mTvTitle.setText("形象展示");
        //
        if (mEditTextPhoto != null) {
            mTvMore.setText("完成");
            mIvBack.setVisibility(View.GONE);
        } else {
            mTvMore.setText("发布");
            mIvBack.setVisibility(View.VISIBLE);
        }
        //
        mIvSetPhotoBg.setOnClickListener(this);  //模板二的选择图片
        mTvStylistHeadphoto.setOnClickListener(this); //模板一的选择图片
        mIvStyleadSelectTwodemansion.setOnClickListener(this); //模板三的选择图片
        mTvMore.setOnClickListener(this);        //发布
        mLlListPhoto.setOnClickListener(this);   //模板历史
        mEdStylistName.setOnClickListener(this); //标题编辑
        mEdContent.setOnClickListener(this);     //内容编辑

    }

    //初始化模板
    private void assignViews() {
        mLlListPhoto = (LinearLayout) findViewById(R.id.ll_list_photo);
        mRvTemplate = (RecyclerView) findViewById(R.id.rv_template);
        mFlPhoto = (FrameLayout) findViewById(R.id.fl_photo);
        //
        mTvShoptitlename = (MyTextView) findViewById(R.id.tv_shoptitlename);
        //
        mEdStylistName = (MyTextView) findViewById(R.id.ed_stylist_name);
        mEdContent = (EditText) findViewById(R.id.ed_content);
        //
        mLlAdpreviewGg = (ImageView) findViewById(R.id.ll_adpreview_gg);
        //
        mRvTemplate.setHasFixedSize(true);
        mRvTemplate.setLayoutManager(new LinearLayoutManager(mContext));
    }


    private void assignViewsImageShowOne() {
        mFlPhotoOne = (FrameLayout) findViewById(R.id.fl_photo_one);
        mTvStylistHeadphoto = (CircleImageView) findViewById(R.id.tv_stylist_headphoto);
    }

    private void assignViewsImageShowTwo() {
        mFlPhotoTwo = (FrameLayout) findViewById(R.id.fl_photo_two);
        mIvUserHeadicon = (ImageView) findViewById(R.id.iv_user_headicon);
        mIvSetPhotoBg = (ImageView) findViewById(R.id.iv_set_photo_bg);
    }
    private void assignViewsImageShowThree() {
        mFlPhotoThree = (FrameLayout) findViewById(R.id.fl_photo_three);
        mIvStyleadTwodemansion = (ImageView) findViewById(R.id.iv_stylead_twodemansion);
        mIvStyleadSelectTwodemansion = (ImageView) findViewById(R.id.iv_stylead_select_twodemansion);
    }

    //初始化view
    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        //初始化模板
        assignViews();
        //初始化模板一
        assignViewsImageShowOne();
        //初始化模板二
        assignViewsImageShowTwo();
        //初始化模板三
        assignViewsImageShowThree();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.ed_stylist_name:
                mEdStylistName.requestFocus();
                mEdStylistName.setFocusableInTouchMode(true);
                mEdStylistName.setFocusable(true);
                mEdStylistName.setEnabled(true);
                mEdStylistName.setCursorVisible(true);
                ScreenUtil.showKeyBroad(mEdStylistName);
                break;

            case R.id.ed_content:
                mEdContent.requestFocus();
                mEdContent.setFocusableInTouchMode(true);
                mEdContent.setFocusable(true);
                mEdContent.setEnabled(true);
                mEdContent.setCursorVisible(true);
                ScreenUtil.showKeyBroad(mEdContent);
                break;

            case R.id.iv_set_photo_bg: // 背景的点击事件
                //弹出对话框
                ActionSheet.showSheet(ActivityStylistAd.this, ActivityStylistAd.this, ActivityStylistAd.this, "选择照片", "拍照", "从相册选择照片");
                break;

            case R.id.tv_stylist_headphoto: //头像的点击事件
                //弹出对话框
                ActionSheet.showSheet(ActivityStylistAd.this, ActivityStylistAd.this, ActivityStylistAd.this, "选择照片", "拍照", "从相册选择照片");
                break;

            case R.id.tv_more:
                updataUerAd();
                break;

            case R.id.ll_list_photo:
                Intent intent = new Intent(mContext, ActivityTurnsPhoto.class);
                if (mEditTextPhoto != null) {
                    intent.putExtra("intent_result_url", "no_to_updata");
                    intent.putExtra("intent_result_position", mEditTextPhoto.currentItem);
                    setResult(108, intent);
                    finish();
                } else {
                    startActivity(intent);
                }
                break;

            case R.id.iv_stylead_select_twodemansion:
                //弹出对话框
                ActionSheet.showSheet(ActivityStylistAd.this, ActivityStylistAd.this, ActivityStylistAd.this, "选择二维码", null, "从相册中选择二维码");
                break;

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(mContext, ActivityTurnsPhoto.class);
        if (mEditTextPhoto != null) {
            intent.putExtra("intent_result_url", "no_to_updata");
            intent.putExtra("intent_result_position", mEditTextPhoto.currentItem);
            setResult(108, intent);
            finish();
        }else {
            finish();
        }
    }

    /**
     * 上传用户信息
     */
    private void updataUerAd() {
        //判断头像
        if (TextUtils.isEmpty(userShow.headPic)) {
            ToastUtil.show(mContext, "用户头像不能为空...");
            return;
        }
        //判读用户名字
        String name = mEdStylistName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.show(mContext, "请输入名字或者编号...");
            return;
        } else {
            userShow.titleName = name;
        }
        //内容
        String content = mEdContent.getText().toString();
        if (TextUtils.isEmpty(content)) {
            ToastUtil.show(mContext, "请输入个人说明...");
            return;
        } else {
            userShow.deceContent = content;
        }
        //位置信息
        LogUtil.log("-------当前templateId:" + currentItemId);
        userShow.currentType = listMuban.get(currentItemId).template_id;
        LogUtil.log("------currentType" + userShow.currentType);
        mTvMore.setEnabled(false);
        showProgressDialog("");
        mEdContent.setCursorVisible(false);
        mEdStylistName.setCursorVisible(false);
        mEdStylistName.setFocusable(false);
        mEdContent.setFocusable(false);
        mEdStylistName.invalidate();
        mEdContent.invalidate();
        //上存发型师广告
        mFlPhoto.destroyDrawingCache();
        mFlPhoto.buildDrawingCache();
        mFlPhoto.setDrawingCacheEnabled(true);
        BaseApp.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap drawingCache = mFlPhoto.getDrawingCache();
                //把drawingCache保存到本地
                String filePath = PhotoUtil.setPicToView(drawingCache, "headPicBit.jpg");
                //mFlPhoto.setDrawingCacheEnabled(false);
                //mFlPhoto.destroyDrawingCache();
                if (filePath == null) {
                    dismissProgressDialog();
                    mTvMore.setEnabled(true);
                    return;
                }
                new MyTask(2).execute(filePath);
            }
        }, 2500);
    }

    @Override
    public void onClickAction(int whichButton) {
        switch (whichButton) {
            case 0:
                //TODO　打开相机
                PhotoUtil.openCamera(ActivityStylistAd.this, IMAGE_FILE_NAME);
                break;
            case 1:
                //TODO 打开相册
               PhotoUtil.openAlbum(ActivityStylistAd.this);
                break;
            case 2:
                //TODO 用户取消
                break;
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
    }



    @Override
    protected void onResume() {
        super.onResume();
        // TODO 传递根布局
        // mLlRootView.addOnLayoutChangeListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        LogUtil.log("------用户的请求码：" + requestCode + "-----用户的结果码：" + resultCode);
        // 用户没有进行有效的设置操作，返回
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        switch (requestCode) {
            case PhotoUtil.SELECT_PIC:
                //相册 低版本打开的方式
                if(listMuban.get(currentItemId).type.equals("3")){
                    ImageLoadHelper.displayImage(String.valueOf(intent.getData()), mIvStyleadTwodemansion);
                    String url = GetPhotoForAlbum.getPath(mContext, intent.getData());
                    new MyTask(1).execute(url);
                }else {
                    PhotoUtil.cropRawPhoto(ActivityStylistAd.this, intent.getData(), 2, 3, 250, 380);
                }
                break;
            case PhotoUtil.SELECT_PIC_KITKAT:
                //相册 高版本打开的方式
                if(listMuban.get(currentItemId).type.equals("3")){
                    ImageLoadHelper.displayImage(String.valueOf(intent.getData()), mIvStyleadTwodemansion);
                    String url = GetPhotoForAlbum.getPath(mContext, intent.getData());
                    new MyTask(1).execute(url);
                }else {
                    PhotoUtil.cropRawPhoto(ActivityStylistAd.this,intent.getData(),2,3,250,380);
                }
                break;
            case PhotoUtil.OPEN_CAMERA:
                if (PhotoUtil.hasSdcard()) {
                    //照相机
                    File tempFile = new File(Environment.getExternalStorageDirectory(), IMAGE_FILE_NAME);
                    PhotoUtil.cropRawPhoto(ActivityStylistAd.this, Uri.fromFile(tempFile), 2, 3, 250, 380);
                } else {
                    ToastUtil.show(this, "未找到存储卡，无法存储照片！");
                }
                break;
            case PhotoUtil.CUT_OUT_PHOTO:
                // 图片编辑后使用这个方式打开
                if (intent != null) {
                    //编辑图片完成
                    setImageView(intent);
                }
                break;
        }
    }

    /**
     * 提取保存裁剪之后的图片数据，并设置头像部分的View
     */
    private void setImageView(Intent intent) {
        LogUtil.log("------intent" + intent.toString());
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");
            //BitmapFactory.de
            //TODO 把图片设置给imageView
            mIvUserHeadicon.setImageBitmap(photo);
            mTvStylistHeadphoto.setImageBitmap(photo);
            //保存图片至本地
            String filePath = PhotoUtil.setPicToView(photo,"headPicSmall.jpg");
            if (filePath == null) {
                return;
            }
            new MyTask(1).execute(filePath);
        }
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        LogUtil.log("-----oldBottom:" + oldTop + "-----bootom:" + top + "-----keyHeight" + keyHeight);
        if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom > keyHeight)) {
            LogUtil.log("--------------软键盘起来");

        } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom > keyHeight)) {
            LogUtil.log("--------------软键盘关闭");
            //TODO 设置软键盘的焦点
            mEdContent.setCursorVisible(false);
            mEdStylistName.setCursorVisible(false);
        }
    }


    /**
     * recycleView的点击事件，点击右边的更新左边的
     *
     * @param view
     * @param position
     */
    @Override
    public void onItemClick(View view, int position) {
        PrefUtils.setStyleAdListPosition(mContext, position);
        modifyColor(position);
        currentItemId = position;
    }

    class MyImageLoadingListener implements ImageLoadingListener {
        private ImageView mLlAdpreviewBg;

        public MyImageLoadingListener(ImageView mLlAdpreviewBg) {
            this.mLlAdpreviewBg = mLlAdpreviewBg;
        }

        @Override
        public void onLoadingStarted(String s, View view) {
            showProgressDialog("");
        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {
            dismissProgressDialog();
        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            dismissProgressDialog();
            mLlAdpreviewBg.setImageBitmap(bitmap);
        }

        @Override
        public void onLoadingCancelled(String s, View view) {

        }
    }


    /**
     * 长存发型师介绍的内容到七牛
     */
    public class MyTask extends AsyncTask<String, Void, Void> {
        private int status;

        public MyTask(int status) {
            this.status = status;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog("");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            //打印上存路径 图片在 sd 卡的路径
            submitHeadIcon(params[0], status);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    /**
     * 七牛上传完成返回Url的方法
     *
     * @param str
     */
    public void submitHeadIcon(String str, final int status) {
        LogUtil.log("-------------传递过来的路径" + str);
        UploadUtil uploadUtil = new UploadUtil(mContext, new UploadUtil.QiuNiuUpLoadListener() {
            @Override
            public void onUpLoadSucceed(String url) {
                dismissProgressDialog();
                //TODO 保存用户数据
                if (status == 1) {
                    userShow.headPic = url;
                    ToastUtil.show(mContext, "头像设置成功...");
                }
                if (status == 2) {
                    mTvMore.setEnabled(true);
                    userShow.imgUrl = url;
                    if (mEditTextPhoto == null) {
                        Intent intentBitList = new Intent(mContext, ActivityIpList.class);
                        intentBitList.putExtra("userShow", userShow);
                        startActivity(intentBitList);
                    } else {
                        String headImage = null;
                        //直接上存
                        headImage = userShow.headPic;
                        upPhotoUrl(userShow.imgUrl, mEditTextPhoto.deviceId, userShow.titleName, userShow.deceContent, headImage, userShow.currentType);
                    }
                }
                LogUtil.log("--------------返回来的Url" + url);
            }

            @Override
            public void onUpLoadFailure(String erroMessage) {
                mTvMore.setEnabled(true);
                dismissProgressDialog();
                if (status == 1) {
                    mIvUserHeadicon.setImageBitmap(null);
                    mTvStylistHeadphoto.setImageResource(R.drawable.i_def_icon);
                    ToastUtil.show(mContext, "头像设置失败，请检查网络...");
                }
            }
        });
        uploadUtil.upLoad(str);
    }

    /**
     * 提交数据
     */
    public void upPhotoUrl(String headImage, String device, String titleName, String titleContent, String imagePhoto, String posiionId) {
        HttpGetController instance = HttpGetController.getInstance();
        User user = PrefUtils.getUser(mContext);
        LogUtil.log("-------user.token:" + user.token);
        instance.setTurnsPhoto(user.token, headImage, device, titleName, titleContent, imagePhoto, posiionId, className);
    }

}
