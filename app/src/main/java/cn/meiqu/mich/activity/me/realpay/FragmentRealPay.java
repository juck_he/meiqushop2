package cn.meiqu.mich.activity.me.realpay;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.zxing.WriterException;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Order;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.QRCodeUtils;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.view.RippleView;

/**
 * Created by Fatel on 15-9-28.
 */
public class FragmentRealPay extends BaseFragment implements View.OnClickListener, RippleView.OnRippleCompleteListener {
    String className = getClass().getName();
    String action_pay = className + API.setRealPay;
    String action_checkPay = className + API.checkRealPay;
    private EditText mEtPrice;
    private RippleView mRippleViewGen;
    private Button mBtnGen;
    private RelativeLayout mFlQr;
    private ImageView mIvQrCode;
    private TextView mTvRefreshState;
    String order_num = "";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_checkPay, action_pay});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_realpay, null);
            assignViews();
        }
        return contain;
    }


    private void assignViews() {
        initTitle("实时收款");
        setTitleRight("历史", this);
        mEtPrice = (EditText) findViewById(R.id.et_price);
        mRippleViewGen = (RippleView) findViewById(R.id.rippleView_gen);
        mBtnGen = (Button) findViewById(R.id.btn_gen);
        mFlQr = (RelativeLayout) findViewById(R.id.fl_qr);
        mIvQrCode = (ImageView) findViewById(R.id.iv_qr_code);
        mTvRefreshState = (TextView) findViewById(R.id.tv_refresh_state);

        mRippleViewGen.setOnRippleCompleteListener(this);
        mTvRefreshState.setOnClickListener(this);
        mTvRefreshState.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        showNoMessage("您还没有生成收款二维码哦!");
        mFlQr.setVisibility(View.GONE);

    }

    public void requestQrCode(String money) {
        showProgressDialog("");
        HttpGetController.getInstance().setRealPay(money, className);
    }

    public void handleQrCode(String data) {
        try {
            order_num = JsonUtil.getJsonObject(data).optJSONObject("info").optString("order_num");
            mIvQrCode.setImageBitmap(QRCodeUtils.createQRCode("mich_action=pay&pay_id=" + order_num, 200));
            mTvRefreshState.setVisibility(View.VISIBLE);
//            findViewById(R.id.tv_qrcode).setVisibility(View.INVISIBLE);
            dismissMessage();
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void requestCheckPay() {
        showProgressDialog("");
        HttpGetController.getInstance().checkRealPay(order_num, className);
    }

    public void handleCheckPay(String data) {
        final Order order = new Gson().fromJson(JsonUtil.getJsonObject(data).optString("info"), Order.class);
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (order.is_pay.equals("1")) {
                    ((BaseActivity) getActivity()).popBack();
                }
            }
        }).create();
        if (order.is_pay.equals("0")) {
            dialog.setTitle("会员未支付");
        } else if (order.is_pay.equals("1")) {
            dialog.setTitle("支付成功");
            dialog.setMessage("收入" + order.pay_money + "元");
        } else {
            dialog.setTitle("会员支付失败");
        }
        dialog.show();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_checkPay)) {
                handleCheckPay(data);
            } else if (action.equals(action_pay)) {
                handleQrCode(data);
            }
        }
    }

    public void checkInput() {
        String money = mEtPrice.getText().toString();
        if (StringUtil.isEmpty(money)) {
            toast("请先输入收款金额");
            return;
        } else {
            dismissMessage();
            mFlQr.setVisibility(View.VISIBLE);
            requestQrCode(money);

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mTvRefreshState.getId()) {
            requestCheckPay();
        } else if (v.getId() == R.id.tv_title_more) {
            ((RealPayActivity) getActivity()).showHistory();
        }
    }

    @Override
    public void onComplete(RippleView v) {
        if (v.getId() == mRippleViewGen.getId()) {
            checkInput();
        }
    }
}
