package cn.meiqu.mich.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.common.Common;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.view.ClearEditText;

/**
 * Created by Administrator on 2015/9/17.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private ClearEditText mEdUesrmane;
    private ClearEditText mEdUserpwd;
    private TextView mTvLogin;
    private Context mContext;
    String className = getClass().getName();
    String action_login = className + API.login;
    String actiion_getInfo = className + API.getUserInfo;
    private TextView mTvForgetPwd;


    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_login)) {
            dismissProgressDialog();
            if (data != null) {
                LogUtil.log("--------------data" + data);
                try {
                    JSONObject loginData = new JSONObject(data);
                    int status = loginData.getInt("status");
                    if (status == 1) {
                        handlerUserData(data);
                    } else {
                        showDialog(loginData.getString("error_msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                toast("网络连接失败...");
            }
        }
        if (action.equals(actiion_getInfo)) {
            if (data != null) {
                handleUserInfo(data);
                LogUtil.log("--------------data1" + data);
            } else {
                toast("网络连接失败...");
            }

        }
    }

    private void handlerUserData(String data) {
        Gson gson = new Gson();
        User user = gson.fromJson(data, User.class);
        PrefUtils.setUser(mContext, user);
        requestUserInfo(user.token, user.user_id);
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {

        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(LoginActivity.this).setPositiveButton("确定", null).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtil.fullScreenCompat(this);
        setContentView(R.layout.activity_login);
        this.mContext = this;
        initReceiver(new String[]{action_login, actiion_getInfo});
        initView();
        initEvent();
    }

    @Override
    public void initFragment() {

    }

    private void initEvent() {
        mTvLogin.setOnClickListener(this);
        mTvForgetPwd.setOnClickListener(this);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        mEdUesrmane = (ClearEditText) findViewById(R.id.ed_uesrmane);
        mEdUserpwd = (ClearEditText) findViewById(R.id.ed_userpwd);
        mTvLogin = (TextView) findViewById(R.id.tv_login);
        mTvForgetPwd = (TextView) findViewById(R.id.tv_forget_pwd);
        User mUser = PrefUtils.getUser(this);
        if (mUser != null) {
            mEdUesrmane.setText(mUser.user_account + "");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_login:
                //判读手机号码
                String phoneMenber = mEdUesrmane.getText().toString().trim();
                String passWrod = mEdUserpwd.getText().toString().trim();
                phoneDeviceTd = Common.getDeviceTd(mContext);
                if (TextUtils.isEmpty(phoneMenber)) {
                    ToastUtil.show(mContext, "请输入账号");
                } else if (TextUtils.isEmpty(passWrod)) {
                    ToastUtil.show(mContext, "请输入密码");
                } else if (TextUtils.isEmpty(phoneDeviceTd)) {
                    LogUtil.log("-----------手机设别码是null");
                    phoneDeviceTd = "12345";
                    //拿账号去登录
                    LogUtil.log("-------------" + Common.getDeviceTd(mContext) + "----" + phoneMenber + "---" + passWrod + "" + className);
                    if (phoneMenber.length() == 11) {
                        //TODO 店长
                        requestData(phoneMenber,phoneDeviceTd, passWrod);
                    } else {
                        //TODO 会员账号
                        requestMember(phoneMenber,phoneDeviceTd, passWrod);
                    }
                    showProgressDialog("");
                } else {
                    //拿账号去登录
                    LogUtil.log("-------------" + Common.getDeviceTd(mContext) + "----" + phoneMenber + "---" + passWrod + "" + className);
                    if (phoneMenber.length() == 11) {
                        //TODO 店长
                        requestData(phoneMenber,phoneDeviceTd, passWrod);
                    } else {
                        //TODO 会员账号
                        requestMember(phoneMenber,phoneDeviceTd, passWrod);
                    }
                    showProgressDialog("");
                }
                break;
            case R.id.tv_forget_pwd:
                Intent intent = new Intent(mContext, ForgetPwdActivity.class);
                startActivity(intent);
                break;
        }

    }

    private  String phoneDeviceTd;
    private void requestMember(String phoneMenber,String number,String passWrod) {
        HttpGetController instance = HttpGetController.getInstance();
        instance.login(Common.StyList_Client_Key, number, phoneMenber, passWrod, className);
    }

    private void requestData(String user,String number, String pwd) {
        HttpGetController instance = HttpGetController.getInstance();
        instance.login(Common.CLIENT_KEY, number, user, pwd, className);
    }

    public void requestUserInfo(String token, String company_id) {
        HttpGetController.getInstance().getUserInfo(token, company_id, className);
    }

    public void handleUserInfo(String data) {
        User mUser = PrefUtils.getUser(this);
        User tempUser = new Gson().fromJson(JsonUtil.getJsonObject(data).optJSONArray("info").optString(0), User.class);
        mUser.company_id = tempUser.company_id;
        mUser.company_name = tempUser.company_name;
        mUser.lat = tempUser.lat;
        mUser.lng = tempUser.lng;
        mUser.like = tempUser.like;
        mUser.telephone = tempUser.telephone;
        mUser.address = tempUser.address;
        mUser.company_logo = tempUser.company_logo;
        PrefUtils.setUser(this, mUser);
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
