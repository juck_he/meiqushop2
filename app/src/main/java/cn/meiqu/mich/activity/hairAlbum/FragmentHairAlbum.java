package cn.meiqu.mich.activity.hairAlbum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;
import com.Fatel.utils.StringUtils;
import com.example.myphotoalbum.PhotoAlbumActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;
import com.yalantis.cameramodule.activity.CameraActivity;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.Sdcard;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.adapter.RecycleHairImageAdapter;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.AblumImage;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.dialogs.SelectImageDialog;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSocket;
import cn.meiqu.mich.util.DialogUtils;
import cn.meiqu.mich.util.FileUtil;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.view.RippleView;

/**
 * Created by Fatel on 15-4-24.
 */
public class FragmentHairAlbum extends BaseFragment implements View.OnClickListener, RecycleHairImageAdapter.MyOnItemClickListener, RecycleHairImageAdapter.MyOnItemDeleteListener, RippleView.OnRippleCompleteListener, FlyImageControl.FlyImageListener {

    public static FragmentHairAlbum newInstance() {
        return new FragmentHairAlbum();
    }

    private RippleView mBtnHairScan;
    ArrayList<AblumImage> ablumImages = new ArrayList<AblumImage>();
    ArrayList<AblumImage> aroundImages = new ArrayList<>();
    RecycleHairImageAdapter adapter;
    //
    //  private final int requestScan = 998;
    private final int requestFeitu = 999;
    boolean isNeedRefresh = false;

    private RecyclerView mRecyclerVHairAlbum;
    private RippleView mRippleViewHairAdd;
    private RippleView mRippleViewHairAround;
    private RippleView mRippleViewHairCompound;
    //
    private boolean isSelectStatic = false;
    private LinearLayout mLlSelectAllPhoto;
    private LinearLayout mLlSelectAll;
    private TextView mTvDetelePhoto;
    private ImageView mTvSelectStatic;
    //
    private List<AblumImage> detelePath = new ArrayList<AblumImage>();
    //
    ViewGroup mVGArround;
    ImageView mImgVHair1, mImgVHair2;
    private boolean isHairAround = false;
    private boolean isSelectAllStatic = false;
    private boolean isContented = false;

    private void assignViews() {
        initTitle("发型作品");
        setTitleRight("选择", this);
        mRecyclerVHairAlbum = (RecyclerView) findViewById(R.id.recyclerV_hair_album);
        mBtnHairScan = (RippleView) findViewById(R.id.rippleView_hair_scan);
        mBtnHairScan.setOnRippleCompleteListener(this);
        mRecyclerVHairAlbum.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        //
        mRippleViewHairAdd = (RippleView) findViewById(R.id.rippleView_hair_add);
        mRippleViewHairAround = (RippleView) findViewById(R.id.rippleView_hair_around);
        mRippleViewHairCompound = (RippleView) findViewById(R.id.rippleView_hair_compound);

        mRippleViewHairAdd.setOnRippleCompleteListener(this);
        mRippleViewHairCompound.setOnRippleCompleteListener(this);
        mRippleViewHairAround.setOnRippleCompleteListener(this);
        //
        mVGArround = (ViewGroup) findViewById(R.id.lin_arround);
        mImgVHair1 = (ImageView) findViewById(R.id.imgV_arroundHair1);
        mImgVHair2 = (ImageView) findViewById(R.id.imgV_arroundHair2);
        mVGArround.setLayoutParams(new RelativeLayout.LayoutParams(ScreenUtil.ScreenWidth(getActivity()), ScreenUtil.ScreenWidth(getActivity()) * 12 / 16));
        //
        mLlSelectAllPhoto = (LinearLayout) findViewById(R.id.ll_select_all_photo);
        mLlSelectAll = (LinearLayout) findViewById(R.id.ll_select_all);
        mTvDetelePhoto = (TextView) findViewById(R.id.tv_detele_photo);
        mTvSelectStatic = (ImageView) findViewById(R.id.iv_seleect_static);
        //
        mLlSelectAll.setOnClickListener(this);
        mTvDetelePhoto.setOnClickListener(this);

    }

    public void initListData() {
        adapter = new RecycleHairImageAdapter(ablumImages, this, this);
        mRecyclerVHairAlbum.setHasFixedSize(true);
        mRecyclerVHairAlbum.setAdapter(adapter);
        String data = SettingDao.getInstance().getHairImages();
        if (!StringUtils.isEmpty(data)) {
            LogUtil.log("----------data:" + data);
            ArrayList<AblumImage> temps = new Gson().fromJson(data, new TypeToken<ArrayList<AblumImage>>() {
            }.getType());
            addListData(temps);
        }
        //
    }

    public void addListData(ArrayList<AblumImage> temps) {
        ablumImages.addAll(0, temps);
        for (AblumImage ablumImage : ablumImages) {
            ablumImage.isSelect = false;
        }
        adapter.notifyDataSetChanged();
        if (ablumImages.size() > 10) {
            mRecyclerVHairAlbum.setItemViewCacheSize(10);
        } else {
            mRecyclerVHairAlbum.setItemViewCacheSize(ablumImages.size());
        }
        mRecyclerVHairAlbum.smoothScrollToPosition(0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_hair_album, null);
            assignViews();
            initListData();
            initReceiver(new String[]{TcpResponController.action_tcp_respon});
        }
        return contain;
    }

    @Override
    public void onPause() {
        super.onPause();
        FlyImageControl.getInstance().setFlyImageListener(null);
//        try {
//            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNeedRefresh) {
            mRecyclerVHairAlbum.smoothScrollToPosition(0);
        }
    }


//    public void initTcpReceiver() {
//        String filters[] = {TcpResponController.action_tcp_respon};
//        initReceiver(filters);
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        IntentFilter filter = new IntentFilter(CameraActivity.ACTION_TAKE_PHOTO);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(takePhotoBroadcase, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SettingDao.getInstance().setHairImages(new Gson().toJson(ablumImages));
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(takePhotoBroadcase);
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(TcpResponController.action_tcp_respon)) {
            if (data.equals(TcpCommand.H_ConnectSucceed)) {
                if (!isContented) {
                    isContented = true;
                    FlyImageControl.getInstance().isTakePhoto = true;
                    Intent intentTakePhoto = new Intent(getActivity(), CameraActivity.class);
                    intentTakePhoto.putExtra(CameraActivity.PATH, Sdcard.AppRootDir);
                    intentTakePhoto.putExtra(CameraActivity.OPEN_PHOTO_PREVIEW, false);
                    intentTakePhoto.putExtra(CameraActivity.USE_FRONT_CAMERA, false);
                    startActivity(intentTakePhoto);
                    toast("美屏连接成功!");
                }
            }
        } else if (data.equals(TcpCommand.H_ConnectFailure)) {
            toast("网络状态不好，请检查网络。");
        }
    }

    public void refreshViewToAround() {

        if (isHairAround) {
            setTitleRight("取消", this);
            initTitle("请选择2张图片");
            mRippleViewHairAround.setVisibility(View.INVISIBLE);
            mRippleViewHairAdd.setVisibility(View.INVISIBLE);
            mBtnHairScan.setVisibility(View.INVISIBLE);
            mRippleViewHairCompound.setVisibility(View.VISIBLE);
        } else {
            for (AblumImage ablumImage : aroundImages) {
                ablumImage.isSelect = false;
            }
            if (!aroundImages.isEmpty()) {
                aroundImages.clear();
                adapter.notifyDataSetChanged();
            }
            setTitleRightInVisiable();
            initTitle("发型作品");
            mRippleViewHairAround.setVisibility(View.VISIBLE);
            mRippleViewHairAdd.setVisibility(View.VISIBLE);
            mBtnHairScan.setVisibility(View.VISIBLE);
            mRippleViewHairCompound.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == mTvRight.getId()) {
//            new SelectImageDialog(getActivity()).show(true);

        if (v.getId() == R.id.tv_title_more) {
            if (isHairAround) {
                isHairAround = false;
                refreshViewToAround();
                mTvTitle.setText("发型作品");
                mTvRight.setVisibility(View.VISIBLE);
                mTvRight.setText("选择");
            } else {
                if (isSelectStatic) {
                    // 正在选择状态
                    isSelectStatic = false;
                    //
                    mTvTitle.setText("发型作品");
                    mTvRight.setText("选择");
                    mRippleViewHairAround.setVisibility(View.VISIBLE);
                    mRippleViewHairAdd.setVisibility(View.VISIBLE);
                    mLlSelectAllPhoto.setVisibility(View.GONE);
                    mBtnHairScan.setVisibility(View.VISIBLE);
                    //
                    for (int i = 0; i < ablumImages.size(); i++) {
                        ablumImages.get(i).isSelect = false;
                        detelePath.add(ablumImages.get(i));
                        adapter.notifyItemChanged(i);
                        detelePath.clear();
                    }
                } else {
                    isSelectStatic = true;
                    //
                    mTvTitle.setText("请选择发型作品");
                    mTvRight.setText("取消");
                    mBtnHairScan.setVisibility(View.GONE);
                    mRippleViewHairAround.setVisibility(View.GONE);
                    mRippleViewHairAdd.setVisibility(View.GONE);
                    mLlSelectAllPhoto.setVisibility(View.VISIBLE);
                }
            }
        } else if (v.getId() == mLlSelectAll.getId()) {
            if (isSelectAllStatic) {
                isSelectAllStatic = false;
                mTvSelectStatic.setImageResource(R.drawable.image_uncheck);
                for (int i = 0; i < ablumImages.size(); i++) {
                    ablumImages.get(i).isSelect = false;
                    detelePath.add(ablumImages.get(i));
                    adapter.notifyItemChanged(i);
                    detelePath.clear();
                }
            } else {
                if (ablumImages.size() > 0) {
                    isSelectAllStatic = true;
                    mTvSelectStatic.setImageResource(R.drawable.image_select);
                    for (int i = 0; i < ablumImages.size(); i++) {
                        ablumImages.get(i).isSelect = true;
                        detelePath.add(ablumImages.get(i));
                        adapter.notifyItemChanged(i);
                    }
                } else {
                    toast("请添加作品!");
                }
            }
        } else if (v.getId() == mTvDetelePhoto.getId()) {
            if (detelePath.size() > 0) {
                DialogUtils dialogUtils = new DialogUtils();
                dialogUtils.showDialog(getActivity(), "你确定删除吗?");
                dialogUtils.setOnClickDialogOkListen(new DialogUtils.OnClickDialogOkListen() {
                    @Override
                    public void onClickOk() {
                        for (int i = 0; i < detelePath.size(); i++) {
                            ablumImages.remove(detelePath.get(i));
                        }
                        isSelectAllStatic = false;
                        mTvSelectStatic.setImageResource(R.drawable.image_uncheck);
                        detelePath.clear();
                        adapter.notifyDataSetChanged();
                        SettingDao.getInstance().setHairImages(new Gson().toJson(ablumImages));
                    }
                });
            } else {
                toast("请选择作品!");
            }
        }
    }

    @Override
    public void onComplete(RippleView v) {
        if (v.getId() == mRippleViewHairAround.getId()) {
            MobclickAgent.onEvent(getActivity(), KeyCountAnalytics.Album_HairBeforeAfter);
            if (ablumImages.size() <= 1) {
                toast("至少要有两张图片哦！，请添加作品...");
                return;
            }
            isHairAround = true;
            refreshViewToAround();//            gotoScan();
        } else if (v.getId() == mRippleViewHairCompound.getId()) {
            MobclickAgent.onEvent(getActivity(), KeyCountAnalytics.Album_BeginCompare);
            if (aroundImages.size() == 2) {
                isHairAround = false;
                mVGArround.destroyDrawingCache();
                mVGArround.setDrawingCacheEnabled(true);
                mVGArround.buildDrawingCache();
                refreshViewToAround();
                showProgressDialog("");
                BaseApp.mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AblumImage ablumImage = new AblumImage();
                        String imgId = System.currentTimeMillis() + "";
                        String imgPath = Sdcard.AppRootDir + "/" + imgId + ".jpg";
                        FileUtil.writeBitmap(imgPath, ImageUtil.bitmapToByte(mVGArround.getDrawingCache()));
                        ablumImage.image_path = imgPath;
                        ablumImage.image_id = imgId;
                        ablumImages.add(0, ablumImage);
                        currentPosition = 0;
                        dismissProgressDialog();
                        gotoSeeBigImage();
                        adapter.notifyDataSetChanged();
                    }
                }, 1500);
            } else {
                toast("请选择两张图片");
            }
        } else if (v.getId() == mRippleViewHairAdd.getId()) {
            MobclickAgent.onEvent(getActivity(), KeyCountAnalytics.Album_AddPic);
            new SelectImageDialog(getActivity()).show(true);
        } else if (v.getId() == mBtnHairScan.getId()) {
            if (TcpSocket.getInstance().isConnected()) {
                isContented = true;
                FlyImageControl.getInstance().isTakePhoto = true;
                Intent intentTakePhoto = new Intent(getActivity(), CameraActivity.class);
                intentTakePhoto.putExtra(CameraActivity.PATH, Sdcard.AppRootDir);
                intentTakePhoto.putExtra(CameraActivity.OPEN_PHOTO_PREVIEW, false);
                intentTakePhoto.putExtra(CameraActivity.USE_FRONT_CAMERA, false);
                getActivity().startActivity(intentTakePhoto);
            } else {
                toast("请先扫描二维码，控制美屏");
                isContented = false;
                gotoScan();
            }
        }
    }

    int currentPosition = 0;

    @Override
    public void onItemClick(View v, int position) {
        currentPosition = position;
        AblumImage ablumImage = ablumImages.get(position);
        if (isHairAround) {
            if (!ablumImage.isSelect) {
                if (aroundImages.size() < 2) {
                    ablumImage.isSelect = !ablumImage.isSelect;
                    adapter.notifyItemChanged(position);
                    aroundImages.add(ablumImage);
                    if (aroundImages.size() == 1) {
                        ImageLoadHelper.displayImage("file://" + ablumImage.image_path, mImgVHair1);
                    } else if (aroundImages.size() == 2) {
                        ImageLoadHelper.displayImage("file://" + ablumImage.image_path, mImgVHair2);
                    }
                } else {
                    toast("最多选择2张图片");
                }
            } else {
                aroundImages.remove(ablumImage);
                ablumImage.isSelect = !ablumImage.isSelect;
                adapter.notifyItemChanged(position);
            }

        } else if (isSelectStatic) {
            if (!ablumImage.isSelect) {
                ablumImage.isSelect = !ablumImage.isSelect;
                detelePath.add(ablumImage);
                adapter.notifyItemChanged(position);
            } else {
                ablumImage.isSelect = !ablumImage.isSelect;
                detelePath.remove(ablumImage);
                adapter.notifyItemChanged(position);
            }
            LogUtil.log("--------size1:" + ablumImages.size() + "-----------sise2" + detelePath.size());
            if (ablumImages.size() == detelePath.size()) {
                isSelectAllStatic = true;
                mTvSelectStatic.setImageResource(R.drawable.image_select);
            } else {
                isSelectAllStatic = false;
                mTvSelectStatic.setImageResource(R.drawable.image_unselect);
            }
        } else {
            gotoSeeBigImage();
        }

    }

    @Override
    public void onItemDelete(View v, final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setMessage("是否要删除?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ablumImages.remove(position);
                adapter.notifyDataSetChanged();
            }
        }).setNegativeButton("取消", null).create();
        alertDialog.show();

    }

    @Override
    public void onConnectSucceed() {
        dismissProgressDialog();
        ((BaseActivity) getActivity()).dismissProgressDialog();
        gotoSeeBigImage();
        toast("控制成功");
    }

    @Override
    public void onConnectFailured() {
        dismissProgressDialog();
        toast("控制失败");
    }

    @Override
    public void onFlyImageSucceed() {

    }

    @Override
    public void onFlyImageFailured() {
        dismissProgressDialog();
//        toast("控制失败");
    }

    @Override
    public void onFlyImageUpLoad(String name) {

    }


    public void gotoSeeBigImage() {
        Intent intent = new Intent(getActivity(), SeeBigImageActivity.class);
        intent.putExtra(SeeBigImageActivity.EXTRA_IMAGE_ACTION, SeeBigImageActivity.EXTRA_ACTION_FLY);
        intent.putExtra(SeeBigImageActivity.EXTRA_IMAGE_CURRENT, currentPosition);
        ArrayList<String> path = new ArrayList<String>();
        ArrayList<String> ids = new ArrayList<String>();
        for (AblumImage ablumImage : ablumImages) {
            path.add(ablumImage.image_path);
            ids.add(ablumImage.image_id);
        }
        intent.putStringArrayListExtra(SeeBigImageActivity.EXTAR_IMAGEORGIN_PATH, path);
        intent.putStringArrayListExtra(SeeBigImageActivity.EXTAR_IMAGE_ID, ids);
        startActivityForResult(intent, requestFeitu);
    }

    public void gotoScan() {
        FlyImageControl.getInstance().gotoScan(((BaseActivity) getActivity()));
    }


    public void showImage(String path) {
        ArrayList<AblumImage> temps = new ArrayList<AblumImage>();
        AblumImage image = new AblumImage();
        image.image_path = path;
        image.image_id = SelectImageDialog.takePhotoId + "";
        temps.add(image);
        addListData(temps);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        FlyImageControl.getInstance().setFlyImageListener(this);
        LogUtil.log("---------requestCodeA:" + requestCode);
        if (resultCode == -1) {
            switch (requestCode) {
                // 如果是直接从相册获取
                case SelectImageDialog.ablumResult:
                    String[] imagePaths = data.getStringArrayExtra(PhotoAlbumActivity.EXTRS_IMAGE_PATH);
                    String[] imageIds = data.getStringArrayExtra(PhotoAlbumActivity.EXTRS_IMAGE_ID);
                    if (imagePaths.length > 0) {
                        ArrayList<AblumImage> temps = new ArrayList<AblumImage>();
                        for (int i = 0; i < imagePaths.length; i++) {
                            AblumImage ablumImage = new AblumImage();
                            ablumImage.image_path = imagePaths[i];
                            ablumImage.image_id = imageIds[i];
                            temps.add(ablumImage);
                        }
                        addListData(temps);
                    }
                    break;
                case SelectImageDialog.photographResult:
                    showImage(SelectImageDialog.imagePath);
                    break;
                case FlyImageControl.requestScan:
                    String result = data.getStringExtra(ScanActivity.result);
                    //FlyImageControl.getInstance().setFlyImageListener(this);
                    FlyImageControl.getInstance().handleScanSucceed(((BaseActivity) getActivity()), result);
                    break;
                case requestFeitu:
                    currentPosition = data.getIntExtra(SeeBigImageActivity.EXTRA_IMAGE_CURRENT, 0);
                    mRecyclerVHairAlbum.smoothScrollToPosition(currentPosition);

            }
        }
    }


}
