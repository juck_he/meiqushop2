package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.Fatel.utils.ImageLoadHelper;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.UserInfo;
import cn.meiqu.mich.view.CircleImageView;

/**
 * Created by Administrator on 2015/10/9.
 */
public class UserListAdapter extends BaseAdapter {
    private Context mContext;
    private List<UserInfo.InfoEntity> list;

    public UserListAdapter(Context mContext, List<UserInfo.InfoEntity> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserInfo.InfoEntity userData = list.get(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_uset_list, null);
            holder.mIvUsericon = (CircleImageView) convertView.findViewById(R.id.iv_usericon);
            holder.mTvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            holder.mCbSelect = (CheckBox) convertView.findViewById(R.id.cb_select);
            holder.mLlUserlist = (RelativeLayout) convertView.findViewById(R.id.ll_userlist);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag(); // 重新获取ViewHolder
        }
        ListView.LayoutParams layoutParams=new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120);
        holder.mLlUserlist.setLayoutParams(layoutParams);
        //Glide.with(mContext).load(userData.head_pic).into(holder.mIvUsericon);
        ImageLoadHelper.displayImage(userData.head_pic,holder.mIvUsericon);
        holder.mTvUserName.setText(userData.user_name);
        if (list.get(position).isShowCheckBox) {
            holder.mCbSelect.setVisibility(View.VISIBLE);
            if (userData.isSelect) {
                holder.mCbSelect.setChecked(true);
            } else {
                holder.mCbSelect.setChecked(false);
            }
        } else {
            holder.mCbSelect.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        private CircleImageView mIvUsericon;
        private TextView mTvUserName;
        private CheckBox mCbSelect;
        private RelativeLayout mLlUserlist;
    }
}

//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//
//import java.util.List;
//
//import cn.meiqu.mich.R;
//import cn.meiqu.mich.bean.UserInfo;
//import cn.meiqu.mich.view.CircleImageView;
//
//
//public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.Holder> {
//    private Context mContext;
//    private List<UserInfo.InfoEntity> list;
//
//    public UserListAdapter(Context mContext, List<UserInfo.InfoEntity> list) {
//        this.mContext = mContext;
//        this.list = list;
//    }
//
//
//    public interface OnItemClickLitener {
//        void onItemClick(View view, int position);
//
//        void onItemLongClick(View view, int position);
//    }
//
//    private OnItemClickLitener mOnItemClickLitener;
//
//    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
//        this.mOnItemClickLitener = mOnItemClickLitener;
//    }
//
//
//    @Override
//    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
//        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.item_uset_list, null));
//    }
//
//    @Override
//    public void onBindViewHolder(Holder holder, int position) {
//        holder.initViewDate(position);
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//    class Holder extends RecyclerView.ViewHolder {
//
//        public Holder(View itemView) {
//            super(itemView);
//        }
//
//        private CircleImageView mIvUsericon;
//        private TextView mTvUserName;
//        private CheckBox mCbSelect;
//        private RelativeLayout mLlUserlist;
//
//        public void initView() {
//            mIvUsericon = (CircleImageView) findViewById(R.id.iv_usericon);
//            mLlUserlist = (RelativeLayout) findViewById(R.id.ll_userlist);
//            mTvUserName = (TextView) findViewById(R.id.tv_user_name);
//            mCbSelect = (CheckBox) findViewById(R.id.cb_select);
//        }
//
//        public void initViewDate(int position) {
//            //初始化头布局
//            initView();
//            UserInfo.InfoEntity userData = list.get(position);
//            Glide.with(mContext).load(userData.head_pic).into(mIvUsericon);
//            mTvUserName.setText(userData.user_name);
//            if (mOnItemClickLitener != null) {
//                final int finalPosition = position;
//                mLlUserlist.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mOnItemClickLitener.onItemClick(mLlUserlist, finalPosition);
//                    }
//                });
//            }
//        }
//
//        public View findViewById(int id) {
//            return itemView.findViewById(id);
//        }
//    }
//
//}
//
