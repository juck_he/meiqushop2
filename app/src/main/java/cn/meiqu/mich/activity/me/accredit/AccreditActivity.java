package cn.meiqu.mich.activity.me.accredit;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.Accredit;

public class AccreditActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accredit);
        initFragment();
    }

    @Override
    public void initFragment() {
        showFirst(new FragmentAccreditList());
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    public void showAddAccredit() {
        FragmentAccreditEdit.accredit = null;
        showAndPop(new FragmentAccreditEdit());
    }

    public void showEdtAccredit(Accredit accredit) {
        FragmentAccreditEdit.accredit = accredit;
        showAndPop(new FragmentAccreditEdit());
    }
}
