package cn.meiqu.mich.activity.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.DateInfo;

/**
 * Created by Administrator on 2015/9/29.
 */
public class RecycleAdapterDate extends RecyclerView.Adapter<RecycleAdapterDate.MyViewHolderDate>  {
    private Context mContext;
//    private String[] dateMonths;
//    private String[] dateWeeks;
    private List<DateInfo> list;
//    public RecycleAdapterDate(Context mContext, String[] dateMonths, String[] dateWeeks) {
//        this.mContext = mContext;
//        this.dateMonths =dateMonths;
//        this.dateWeeks =dateWeeks;
//    }

    public RecycleAdapterDate(Context mCOntext, List<DateInfo> listDate) {
        this.mContext = mCOntext;
        this.list = listDate;

    }

    @Override
    public MyViewHolderDate onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderDate(LayoutInflater.from(mContext).inflate(R.layout.item_date_show, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolderDate holder, int position) {
        holder.instanceListView(position);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

       // void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    class MyViewHolderDate extends RecyclerView.ViewHolder {
        private TextView mTvWeek;
        private TextView mTvTodayData;
        private TextView mTvToday;
        private TextView mTvDate;
        private LinearLayout mLlDate;

        public void assignViews() {
            mTvWeek = (TextView) findViewById(R.id.tv_week);
            mTvTodayData = (TextView) findViewById(R.id.tv_today_data);
            mTvToday = (TextView) findViewById(R.id.tv_today);
            mTvDate = (TextView) findViewById(R.id.tv_date);
            mLlDate = (LinearLayout) findViewById(R.id.ll_date);
        }


        public MyViewHolderDate(View itemView) {
            super(itemView);
        }
        public void instanceListView(int position) {
            assignViews();
            if(position==29){
                mTvDate.setVisibility(View.GONE);
                mLlDate.setVisibility(View.VISIBLE);
                mTvTodayData.setText(list.get(position).dateMonths);
                mTvWeek.setText("  "+list.get(position).dateWeeks);
                if (mOnItemClickLitener != null) {
                    final int finalPosition = position;
                    mLlDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnItemClickLitener.onItemClick(mLlDate, finalPosition);
                        }
                    });
                }
                if(list.get(position).state==1){
                    mTvTodayData.setTextColor(mContext.getResources().getColor(R.color.title_color));
                    mTvToday.setTextColor(mContext.getResources().getColor(R.color.title_color));
                }else {
                    mTvTodayData.setTextColor(mContext.getResources().getColor(R.color.history_color));
                    mTvToday.setTextColor(mContext.getResources().getColor(R.color.history_color));
                }
            }else {
                mTvDate.setVisibility(View.VISIBLE);
                mLlDate.setVisibility(View.GONE);
                mTvDate.setText(list.get(position).dateMonths);
                mTvWeek.setText(" " + list.get(position).dateWeeks);
                if (mOnItemClickLitener != null) {
                    final int finalPosition = position;
                    mTvDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnItemClickLitener.onItemClick(mTvDate, finalPosition);
                        }
                    });
                }
                if(list.get(position).state==1){
                    mTvDate.setTextColor(mContext.getResources().getColor(R.color.title_color));
                }else {
                    mTvDate.setTextColor(mContext.getResources().getColor(R.color.history_color));
                }
            }
        }


        public View findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}


