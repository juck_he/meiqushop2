package cn.meiqu.mich.activity.me.device;

import android.os.Bundle;


import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;


public class DeviceActivity extends BaseActivity {

    public static int openWifiSetting = 0;
    public static int openDevivceList = 1;
    FragmentWIfiSetting fragmentWIfiSetting;
    FragmentDeviceList fragmentDeviceList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        initFragment();
    }

    @Override
    public void initFragment() {
        setContainerId(containerId);
        if (getIntent().getIntExtra("type", openWifiSetting) == openWifiSetting) {
            fragmentWIfiSetting = new FragmentWIfiSetting();
            showFirst(fragmentWIfiSetting);
        } else {
            fragmentDeviceList = FragmentDeviceList.newInstance();
            showFirst(fragmentDeviceList);
        }
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    public void showDeviceList() {
        if (fragmentDeviceList == null) {
            fragmentDeviceList = FragmentDeviceList.newInstance();
        }
        showNoPop(fragmentDeviceList);
    }
}
