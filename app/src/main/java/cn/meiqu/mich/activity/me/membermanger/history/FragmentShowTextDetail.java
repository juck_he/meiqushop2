package cn.meiqu.mich.activity.me.membermanger.history;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.util.PrefUtils;

/**
 * Created by Administrator on 2015/10/10.
 */
public class FragmentShowTextDetail extends BaseFragment {

    private Context mContext ;
    String className = getClass().getName();
    //String action_getTextPushed = className + API.getTextPushed;

    @Override
    public void onHttpHandle(String action, String data) {

    }


    public static FragmentShowTextDetail newInstance() {
        FragmentShowTextDetail fragmentShowTextDetail = new FragmentShowTextDetail();
        return fragmentShowTextDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_text_datail,null);
        }
        return contain;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assignViews();
        //Todo webView的数据
        //HistoryActivity historyActivity = (HistoryActivity) getActivity();
        //String url = historyActivity.fragmentTextPush.getUrl();
        WebViewActivity webViewActivity = (WebViewActivity) getActivity();
        String token = PrefUtils.getUser(mContext).token;
        mWvTextDetail.loadUrl(webViewActivity.url+token);

    }

    private void initReceiver() {
        initReceiver(new String[]{});

    }

    private WebView mWvTextDetail;

    private void assignViews() {
        initTitle("信息详情");
        mWvTextDetail = (WebView) findViewById(R.id.wv_text_detail);
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }

}
