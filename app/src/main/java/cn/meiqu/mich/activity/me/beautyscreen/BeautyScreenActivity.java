package cn.meiqu.mich.activity.me.beautyscreen;

import android.content.Intent;
import android.os.Bundle;

import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Administrator on 2015/10/19.
 */
public class BeautyScreenActivity extends BaseActivity {
//    String className = getClass().getName();
//    String action_bindDevice = className + API.bindDevice;
    private FragmentBeautyScreen fragmentBeautyScreen;

    @Override
    public void onHttpHandle(String action, String data) {
//        if (getHttpStatus(action, data)) {
//            if (action.equals(action_bindDevice)) {
//                handleBindDevice(data);
//            }
//        }
    }


    @Override
    public void initFragment() {
        setContainerId(containerId);
        fragmentBeautyScreen = FragmentBeautyScreen.newInstance();
        showFirst(fragmentBeautyScreen);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initReceiver(new String[]{action_bindDevice});
       // setContentView(R.layout.activity_beautyscreen);
       // initFragment();
    }
//  public void handleBindDevice(String data) {
//        toast("绑定成功");
//    }

    //    public void showHistory() {
//        showAndPop(new FragmentHistory());
//    }
    public void handleScan(String result) {
        if (!StringUtil.isEmpty(result)) {
            if (result.contains("device_id")) {
                String device_id = StringUtil.getParam(result).get("device_id");
                requestBindDevice(device_id);
            } else {
                toast("二维码不正确");
            }
        }
    }

    public void requestBindDevice(String deviceId) {
        showProgressDialog("");
       // HttpGetController.getInstance().bindDevice(deviceId, className);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ScanActivity.requestCode) {
                String result = data.getStringExtra(ScanActivity.result);
                handleScan(result);
                LogUtil.log("result=" + result);
            }
        }
    }


}
