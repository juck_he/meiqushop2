package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;

/**
 * Created by Administrator on 2015/10/12.
 */
public class ListMemberDetaiAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> list;

    public ListMemberDetaiAdapter(Context mContext, List<String> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_member_detail, null);
            holder.mTvNameDetail = (TextView) convertView.findViewById(R.id.tv_name_detail);
            holder.mTvContentDetail = (TextView) convertView.findViewById(R.id.tv_content_detail);
            holder.mLlShowitem = (LinearLayout) convertView.findViewById(R.id.ll_showitem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag(); // 重新获取ViewHolder
        }
        ListView.LayoutParams layoutParams = new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 110);
        holder.mLlShowitem.setLayoutParams(layoutParams);
        if (position == 0) {
            holder.mTvNameDetail.setText("昵称");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 1) {
            holder.mTvNameDetail.setText("性别");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 2) {
            holder.mTvNameDetail.setText("手机号码");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 3) {
            holder.mTvNameDetail.setText("消费金额");
            holder.mTvContentDetail.setText("￥" + list.get(position));
        } else if (position == 4) {
            holder.mTvNameDetail.setText("出生年月");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 5) {
            holder.mTvNameDetail.setText("关注时间");
            long when = Long.valueOf(list.get(position)) * 1000L;
            String dateStr = DateFormat.getDateFormat(mContext).format(when);
            holder.mTvContentDetail.setText(dateStr);
        } else if (position == 6) {
            holder.mTvNameDetail.setText("常住地址");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 7) {
            holder.mTvNameDetail.setText("所在地区");
            holder.mTvContentDetail.setText(list.get(position));
        } else if (position == 8) {
            holder.mTvNameDetail.setText("个性签名");
            holder.mTvContentDetail.setText(list.get(position));
        }
        return convertView;
    }

    class ViewHolder {
        private TextView mTvNameDetail;
        private TextView mTvContentDetail;
        private LinearLayout mLlShowitem;
    }


}
