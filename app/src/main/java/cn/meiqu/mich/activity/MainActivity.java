package cn.meiqu.mich.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.yalantis.cameramodule.activity.CameraActivity;

import cn.meiqu.mich.API;
import cn.meiqu.mich.KeyCountAnalytics;
import cn.meiqu.mich.R;
import cn.meiqu.mich.Sdcard;
import cn.meiqu.mich.activity.hairAlbum.FlyImageActivity;
import cn.meiqu.mich.activity.hairAlbum.FlyImageControl;
import cn.meiqu.mich.activity.imageshow.ActivityStylistAd;
import cn.meiqu.mich.activity.inshopmanege.ActivityManage;
import cn.meiqu.mich.activity.me.accredit.AccreditActivity;
import cn.meiqu.mich.activity.me.checkpay.PayTestActivity;
import cn.meiqu.mich.activity.me.expense.ExpenseActivity;
import cn.meiqu.mich.activity.me.membermanger.MemberActivity;
import cn.meiqu.mich.activity.me.realpay.RealPayActivity;
import cn.meiqu.mich.activity.me.setting.SettingActivity;
import cn.meiqu.mich.activity.me.shopstore.ShopStoreActivity;
import cn.meiqu.mich.activity.shopad.ActivityShopAd;
import cn.meiqu.mich.activity.wifi.WifiAdmin;
import cn.meiqu.mich.activity.wifi.WifiConnectReceiver;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSocket;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.UpdateUtil;
import cn.meiqu.mich.view.CircleImageView;
import cn.meiqu.mich.view.MainMidLinearlayout;


public class MainActivity extends BaseActivity implements View.OnClickListener, DialogInterface.OnCancelListener, DrawerLayout.DrawerListener {
    //
    private String className = getClass().getName();
    private String action_checkDeviceAd = className + API.checkDeviceHasAd;
    //
    private LinearLayout mLlPlaylist;
    private LinearLayout mLlAdMake;
    private LinearLayout mLlMakeText;
    private LinearLayout mLlStyleAd;
    private MainMidLinearlayout mLlHairstylePhotoalbum;
    private LinearLayout mLlScreenMove;
    private LinearLayout mLlWeixinCardCup;
    private LinearLayout mLlShopDecorate;
    private TextView mTvConnectTowhere;
    private TextView mTvDisconnect;
    //
    private ImageView mIvMyself;
    private TextView mIvAppTitle;
    private ImageView mIvScan;
    private DrawerLayout mDlRoot;
    private CircleImageView mCivMenuPhoto;
    private TextView mTvMenuShopname;
    private TextView mTvMenuNtegral;
    private LinearLayout mLlMenuPayCheck;
    private LinearLayout mLlMenuIntegral;
    private LinearLayout mLlMenuRealCollect;
    private LinearLayout mLlMenuConsume;
    private LinearLayout mLlMenuMenmberManager;
    private LinearLayout mLlMenuSuperuser;
    private LinearLayout mLlMenuScreenSetup;
    private LinearLayout mLlMenuSysSetup;
    //
    private boolean isOpenLeftMenu = false;
    //
    private Context mContext;
    boolean isStart = false;
    private boolean isShowMoveList = false;
    private boolean isShowFlySoon = false;
    private WifiAdmin wifiAdmin;
    private WifiConnectReceiver wifiConnectReceiver;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                LogUtil.log("-------接收到信息");
                //wifiAdmin.getCurrentWifiInfo().getSSID()
                String ssid = wifiAdmin.getCurrentWifiInfo().getSSID();
                LogUtil.log("------ssid" + ssid);
                mTvConnectTowhere.setText("当前WiFI：" + ssid.replace("\"", ""));
            } else if (msg.what == 2) {
                mTvConnectTowhere.setText("运营商网络");
            } else if (msg.what == 3) {
                //     mTvConnectTowhere.setText("当前网络不可用");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtil.fullScreenCompat(this);
        setContentView(R.layout.activity_main);
        this.mContext = this;
        UpdateUtil.checkUpdate(BaseApp.mContext, false);
        initView();
        //initEvent();
        //打印用户数据
        printUserData();
        initReceiver(new String[]{TcpResponController.action_tcp_respon, action_checkDeviceAd});
        connectDevice();
        listenerNet();
    }

    private void assignViews() {
        mIvMyself = (ImageView) findViewById(R.id.iv_myself);
        mIvScan = (ImageView) findViewById(R.id.iv_scan);
        //
        mDlRoot = (DrawerLayout) findViewById(R.id.dl_root);
        mCivMenuPhoto = (CircleImageView) findViewById(R.id.civ_menu_photo);
        // 店名
        mTvMenuShopname = (TextView) findViewById(R.id.tv_menu_shopname);
        // 当前积分
        mTvMenuNtegral = (TextView) findViewById(R.id.tv_menu_ntegral);
        //
        mLlMenuPayCheck = (LinearLayout) findViewById(R.id.ll_menu_pay_check);
        mLlMenuIntegral = (LinearLayout) findViewById(R.id.ll_menu_integral);
        mLlMenuRealCollect = (LinearLayout) findViewById(R.id.ll_menu_real_collect);
        mLlMenuConsume = (LinearLayout) findViewById(R.id.ll_menu_consume);
        mLlMenuMenmberManager = (LinearLayout) findViewById(R.id.ll_menu_menmber_manager);
        mLlMenuSuperuser = (LinearLayout) findViewById(R.id.ll_menu_superuser);
        mLlMenuScreenSetup = (LinearLayout) findViewById(R.id.ll_menu_screen_setup);
        mLlMenuSysSetup = (LinearLayout) findViewById(R.id.ll_menu_sys_setup);
    }
    private void listenerNet() {
        wifiAdmin = new WifiAdmin(mContext);
        //注册一个广播事件
        wifiConnectReceiver = new WifiConnectReceiver(wifiAdmin, handler);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(wifiConnectReceiver, filter);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isStart = true;
        MobclickAgent.onResume(this);
        //TODO 自定义事件的代码需要放在Activity里的onResume方法后面，不支持放在onCreat()方法中
        initEvent();
        //判断是否已经绑定过美屏
        if (TcpSocket.getInstance().isConnected()) {
            mTvDisconnect.setVisibility(View.VISIBLE);
        } else {
            mTvDisconnect.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        isStart = false;
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(wifiConnectReceiver);
    }

    private void printUserData() {
        User user = PrefUtils.getUser(this);
        LogUtil.log("-----userToken:" + user.token);
    }

    public void connectDevice() {
        String ip = SettingDao.getInstance().getLastDeviceIp();
        if (!StringUtil.isEmpty(ip)) {
            TcpSocket.getInstance().connect(ip);
        }
    }

    @Override
    public void initFragment() {

    }

    public void requestCheckHasAd() {
        String ip = SettingDao.getInstance().getLastDeviceIp();
        if (StringUtil.isEmpty(ip)) {
            toast("请先扫描二维码，控制美屏");
            FlyImageControl.getInstance().gotoScan(this);
        } else {
            showProgressDialog("");
            HttpGetController.getInstance().checkDeviceHasAd(ip, className);
        }
    }

    public void handleCheckHasAd(String data) {
        if (data == null) {
            toast("请先扫描二维码，控制美屏");
            FlyImageControl.getInstance().gotoScan(this);
        } else {
            if (getHttpStatus("", data)) {
//                TcpSendController.getInstance().s
            } else {
                toast("美屏暂时没有店内广告");
            }
        }
    }

    @Override
    public void onHttpHandle(String action, String begin) {
        dismissProgressDialog();
        if (isStart) {
            if (action.equals(TcpResponController.action_tcp_respon)) {
                if (begin.equals(TcpCommand.H_ConnectSucceed)) {
                    if (isShowMoveList) {
                        isShowMoveList = false;
                        //TODO 跳转到 movelist
                        Intent intentMove = new Intent(mContext, ActivityMoveList.class);
                        startActivity(intentMove);
                    }
                    if (isShowFlySoon) {
                        isShowFlySoon = false;
                        FlyImageControl.getInstance().isTakePhoto = true;
                        Intent intentTakePhoto = new Intent(this, CameraActivity.class);
                        intentTakePhoto.putExtra(CameraActivity.PATH, Sdcard.AppRootDir);
                        intentTakePhoto.putExtra(CameraActivity.OPEN_PHOTO_PREVIEW, false);
                        intentTakePhoto.putExtra(CameraActivity.USE_FRONT_CAMERA, false);
                        startActivity(intentTakePhoto);
                    }
                    toast("美屏控制成功");
                    mTvDisconnect.setVisibility(View.VISIBLE);
                } else if (begin.equals(TcpCommand.H_ConnectFailure)) {
                    toast("网络状态不好，请检查网络。");
                    mTvDisconnect.setVisibility(View.GONE);
                }
            } else if (action.equals(action_checkDeviceAd)) {
                handleCheckHasAd(begin);
            }
        }
    }


    /**
     * 初始化点击事件
     */
    private void initEvent() {
        mLlPlaylist.setOnClickListener(this);  //广告播放
        mLlAdMake.setOnClickListener(this);     //广告制作
        mLlMakeText.setOnClickListener(this);  //字幕广告
        mLlStyleAd.setOnClickListener(this);  //形象展示
        mLlHairstylePhotoalbum.setOnClickListener(this);  //发型相册
        mLlScreenMove.setOnClickListener(this);  //美屏电影
        mLlWeixinCardCup.setOnClickListener(this);  //微信卡券
        mLlShopDecorate.setOnClickListener(this);  //店铺装饰
        mTvDisconnect.setOnClickListener(this);  //断开连接
        //菜单
        mIvMyself.setOnClickListener(this);     //我的页面
        mIvScan.setOnClickListener(this);       //扫一扫
        mCivMenuPhoto.setOnClickListener(this); //菜单头像
        //
        mLlMenuIntegral.setOnClickListener(this); //美屏积分
        mLlMenuPayCheck.setOnClickListener(this); //支付验证
        mLlMenuRealCollect.setOnClickListener(this); //实时收款
        mLlMenuConsume.setOnClickListener(this); //消费统计
        mLlMenuMenmberManager.setOnClickListener(this); //会员管理
        mLlMenuSuperuser.setOnClickListener(this); //授权管理
        mLlMenuScreenSetup.setOnClickListener(this); //美屏设置
        mLlMenuSysSetup.setOnClickListener(this); //系统设置
        // 侧滑菜单拖拽效果
        mDlRoot.setDrawerListener(this);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        TextView appName = (TextView) findViewById(R.id.iv_app_title);
        if (API.staticNow == 1) {
            appName.setText("美渠店家");
        } else if (API.staticNow == 2) {
            appName.setText("美渠店家测试环境");
        }
        //初始化菜单
        assignViews();
        mLlPlaylist = (LinearLayout) findViewById(R.id.ll_playlist);
        mLlAdMake = (LinearLayout) findViewById(R.id.ll_ad_make);
        mLlMakeText = (LinearLayout) findViewById(R.id.ll_make_text);
        mLlStyleAd = (LinearLayout) findViewById(R.id.ll_style_ad);
        mLlHairstylePhotoalbum = (MainMidLinearlayout) findViewById(R.id.ll_hairstyle_photoalbum);
        mLlScreenMove = (LinearLayout) findViewById(R.id.ll_screen_move);
        mLlWeixinCardCup = (LinearLayout) findViewById(R.id.ll_weixin_card_cup);
        mLlShopDecorate = (LinearLayout) findViewById(R.id.ll_shop_decorate);
        mTvConnectTowhere = (TextView) findViewById(R.id.tv_connect_towhere);
        mTvDisconnect = (TextView) findViewById(R.id.tv_disconnect);
        //判断是会员还是店长
        int isAgentAccount = PrefUtils.getUser(mContext).is_agent_account;
        LogUtil.log("--------------isAgentAccount" + isAgentAccount);
//        if (isAgentAccount == 0) {
//            //是店长
//            mTvManage.setVisibility(View.VISIBLE);
//            mTvSetup.setVisibility(View.VISIBLE);
//            mTvAdMake.setVisibility(View.VISIBLE);
//        } else {
//            //是发型师
//            mTvManage.setVisibility(View.GONE);
//            mTvSetup.setVisibility(View.GONE);
//            mTvAdMake.setVisibility(View.GONE);
//        }

        //判断是否已经绑定过美屏
        if (TcpSocket.getInstance().isConnected()) {
            mTvDisconnect.setVisibility(View.VISIBLE);
        } else {
            mTvDisconnect.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        // //广告播放
        if (v.getId() == mLlPlaylist.getId()) {
            //友盟自定义点击事件
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Play);
            requestCheckHasAd();
        }
        // 广告制作
        else if (v.getId() == mLlAdMake.getId()) {
            Intent intentImage = new Intent(mContext, ActivityShopAd.class);
            intentImage.putExtra("isSelectImage", true);
            startActivity(intentImage);
        }
        // 字幕广告
        else if (v.getId() == mLlMakeText.getId()) {
            //友盟自定义点击事件
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Text_Ad);
            Intent intent = new Intent(mContext, ActivityManage.class);
            startActivity(intent);
        }
        // 形象展示
        else if (v.getId() == mLlStyleAd.getId()) {
            //友盟自定义点击事件
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_HairStyleer_Ad);
            Intent intentStylist = new Intent(mContext, ActivityStylistAd.class);
            startActivity(intentStylist);
        }
        // 发型作品
        else if (v.getId() == mLlHairstylePhotoalbum.getId()) {
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_HairStyle_Album);
            FlyImageControl.getInstance().isTakePhoto = false;
            jump(FlyImageActivity.class);
        }
        // 美屏电影
        else if (v.getId() == mLlScreenMove.getId()) {
            //友盟自定义更新
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_BeautyScreen_Move);
            if (TcpSocket.getInstance().isConnected()) {
                Intent intentMove = new Intent(mContext, ActivityMoveList.class);
                startActivity(intentMove);
            } else {
                isShowMoveList = true;
                toast("请先扫描二维码，控制美屏");
                FlyImageControl.getInstance().gotoScan(this);
            }
        }
        // 微信卡券
        else if (v.getId() == mLlWeixinCardCup.getId()){
            //敬请期待
        }
        // 店铺装饰
        else if(v.getId() == mLlShopDecorate.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Shop_Store);
            jump(ShopStoreActivity.class);
        }
        //断开连接
        else if (v.getId()==mTvDisconnect.getId()){
            TcpSocket.getInstance().close();
            mTvDisconnect.setVisibility(View.GONE);
        }
        // 打开我的界面
        else if (v.getId()==mIvMyself.getId()){
            if (!isOpenLeftMenu){
                isOpenLeftMenu = true;
                mDlRoot.openDrawer(Gravity.LEFT);
            }else {
                isOpenLeftMenu = false;
                mDlRoot.closeDrawer(Gravity.LEFT);
            }
        }
        // 扫一扫界面
        else if (v.getId()==mIvScan.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Home_Scan);
            FlyImageControl.getInstance().gotoScan(this);
        }
        //设置用户头像
        else if (v.getId()== mCivMenuPhoto.getId()){

        }
        // 美屏积分
        else if (v.getId() == mLlMenuIntegral.getId()){

        }
        // 支付验证
        else if (v.getId() == mLlMenuPayCheck.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Pay_Verify);
            jump(PayTestActivity.class);
        }
        // 实时收款
        else if (v.getId() == mLlMenuRealCollect.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Real_Time_Gathering);
            jump(RealPayActivity.class);
        }
        // 消费统计
        else if (v.getId()== mLlMenuConsume.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Custom_Count);
            jump(ExpenseActivity.class);
        }
        // 会员管理
        else if (v.getId()==mLlMenuMenmberManager.getId()){
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Member_Manger);
            jump(MemberActivity.class);
        }
        // 授权管理
        else if (v.getId() == mLlMenuSuperuser.getId()){
            //美屏自定义点击事件
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_SuperUser);
            jump(AccreditActivity.class);
        }
        //美屏设置
        else if (v.getId()==mLlMenuScreenSetup.getId()){
            //友盟自定义点击事件
            MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Screen_Control);
            Intent intentSetup = new Intent(mContext, ActivitySetup.class);
            startActivity(intentSetup);
        }
        //系统设置
        else if (v.getId()==mLlMenuSysSetup.getId()){
            jump(SettingActivity.class);
        }


//        switch (v.getId()) {
//            case R.id.tv_setup://打开美屏控制
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Screen_Control);
//                Intent intentSetup = new Intent(mContext, ActivitySetup.class);
//                startActivity(intentSetup);
//                break;
//          //  case R.id.tv_manage:
//                //打开字幕广告
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Text_Ad);
//                Intent intent = new Intent(mContext, ActivityManage.class);
//                startActivity(intent);
//                break;
//            case R.id.iv_myself:
//                //打开我的界面
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Open_Myself);
////                Intent mySelfIntent = new Intent(mContext, MySelfActivity.class);
////                startActivity(mySelfIntent);
//                //如果是店长打开我的界面，是会员打开更多界面
//                int isAgentAccount = PrefUtils.getUser(mContext).is_agent_account;
//                if (isAgentAccount == 0) {
//                    jump(MeActivity.class);
//                } else {
//                    jump(SettingActivity.class);
//                }
//                break;
//            case R.id.ll_beauty_move:
//                //if()
//                //打开美屏电影
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_BeautyScreen_Move);
//                //startAc
//                if (TcpSocket.getInstance().isConnected()) {
//                    Intent intentMove = new Intent(mContext, ActivityMoveList.class);
//                    startActivity(intentMove);
//                } else {
//                    isShowMoveList = true;
//                    toast("请先扫描二维码，控制美屏");
//                    FlyImageControl.getInstance().gotoScan(this);
//                }
////                Intent sacnIntent = new Intent(mContext, ScanActivity.class);
////                startActivityForResult(sacnIntent, Common.START_SCSAN_CODe);
//                // Intent intentMove = new Intent(mContext, ActivityMoveList.class);
//                // startActivity(intentMove);
//                break;
//            case R.id.hairstyleer_ad:
//                //打开发型师广告页面
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_HairStyleer_Ad);
//                Intent intentStylist = new Intent(mContext, ActivityStylistAd.class);
//                startActivity(intentStylist);
//                break;
//            case R.id.ll_hairstyle_photoalbum:
//                //打开发型相册
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_HairStyle_Album);
//                FlyImageControl.getInstance().isTakePhoto = false;
//                jump(FlyImageActivity.class);
//                break;
//            case R.id.ll_flyimage:
//                //打开即拍即飞
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Take_Fly_Soon);
//                if (TcpSocket.getInstance().isConnected()) {
//                    FlyImageControl.getInstance().isTakePhoto = true;
//                    Intent intentTakePhoto = new Intent(this, CameraActivity.class);
//                    intentTakePhoto.putExtra(CameraActivity.PATH, Sdcard.AppRootDir);
//                    intentTakePhoto.putExtra(CameraActivity.OPEN_PHOTO_PREVIEW, false);
//                    intentTakePhoto.putExtra(CameraActivity.USE_FRONT_CAMERA, false);
//                    startActivity(intentTakePhoto);
//                } else {
//                    isShowFlySoon = true;
//                    toast("请先扫描二维码，控制美屏");
//                    FlyImageControl.getInstance().gotoScan(this);
//                }
//                break;
//            case R.id.iv_scan:
//                //主页扫一扫
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Home_Scan);
//                FlyImageControl.getInstance().gotoScan(this);
//                break;
//          //  case R.id.tv_playlist:
//                //广告播放
//                MobclickAgent.onEvent(mContext, KeyCountAnalytics.Key_Play);
//                requestCheckHasAd();
//                break;
//            case R.id.tv_disconnect:
//                TcpSocket.getInstance().close();
//                mTvDisconnect.setVisibility(View.GONE);
//                break;
//          //  case R.id.tv_ad_make:
//                //
//                Intent intentImage = new Intent(mContext, ActivityShopAd.class);
//                intentImage.putExtra("isSelectImage", true);
//                startActivity(intentImage);
//                break;
//        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean b = ScreenUtil.onKeyDown(this, event);
        if (!b) {
            return super.onKeyDown(keyCode, event);
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.log("---------requestCode111:" + requestCode);
        if (resultCode == -1) {
            switch (requestCode) {
                // 如果是直接从相册获取
                case FlyImageControl.requestScan:
                    String result = data.getStringExtra(ScanActivity.result);
                    FlyImageControl.getInstance().handleScanSucceed(this, result);
                    break;
            }
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        isOpenLeftMenu =true;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        isOpenLeftMenu = false;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
