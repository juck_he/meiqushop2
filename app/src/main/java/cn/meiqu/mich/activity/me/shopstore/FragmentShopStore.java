package cn.meiqu.mich.activity.me.shopstore;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.Sdcard;
import cn.meiqu.mich.activity.ActivityPreView;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.GetShopAdPhoto;
import cn.meiqu.mich.dialogs.ActionSheet;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.GetPhotoForAlbum;
import cn.meiqu.mich.util.ImageLoadHelper;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/10/19.
 */
public class FragmentShopStore extends BaseFragment implements ActionSheet.OnActionSheetSelected, DialogInterface.OnCancelListener, View.OnClickListener {
    private ImageView mIvPhotoShop;
    private TextView mTvPreviewShop;
    private RelativeLayout mRlSelectImgShop;
    private TextView mIvHistory;
    private TextView mIvUpdata;
    private EditText mEdInputProname;
    private EditText mEdInputProprice;
    private ImageView mIvSeleectMuban;
    private static ImageView mIvShowMuban;
    private LinearLayout mLlMubanName;
    private TextView mTvShopName;
    private TextView mTvShopName2;
    /* 广告文件数据像文件 */
    private static final String IMAGE_FILE_NAME = "ad_img1.jpg";
    /* 请求识别码 */
    private static final int CODE_GALLERY_REQUEST = 11;//打开相册的请求码
    private static final int CODE_CAMERA_REQUEST = 17; //打开相机的请求码
    private static final int CODE_RESULT_REQUEST = 13; //剪切完图片的请求码
    private static final int SELECT_PIC_KITKAT = 14;   //高版本相册打开的请请求码
    private Context mContext;
    private HttpGetController instance;
    private boolean isShowPhoto = false;
    private File fileName;//图片名字
    private GetShopAdPhoto getShopAdPhoto;
    String className = getClass().getName();
    String action_getAdPhoto = className + API.getShopAdPhoto;
    String action_setUpdataTemplateList = className + API.setUpdataTemplateList;
    String action_setEditTemplateList = className +API.setEditTemplateList;
    //
    public static String templateId;
    public static String imageUrl;
    public boolean isEditStatic = false;
    public static String proId;
    public static String title;
    public static String price;
    private RelativeLayout mRlshopPhto;
    private LinearLayout mLlBgPro;
    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getAdPhoto)) {
            dismissProgressDialog();
            if (data != null) {
                LogUtil.log("----------广告图信息" + data);
                handerAdPhoto(data);
            } else {
                ToastUtil.show(mContext, "网络连接失败...");
            }
        }
        if (action.equals(action_setUpdataTemplateList)) {
            dismissProgressDialog();
            LogUtil.log("----------muban" + data);
            if (data != null) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.getInt("status")==1){
                        ToastUtil.show(mContext, "上传成功...");
                    }else {
                        ToastUtil.show(mContext, "上传失败哦...");
                    }
                } catch (JSONException e) {
                    ToastUtil.show(mContext, "上传失败...");
                }
            } else {
                ToastUtil.show(mContext, "网络连接失败...");
            }
        }
        if (action_setEditTemplateList.equals(action)){
            dismissProgressDialog();
            if (TextUtils.isEmpty(data)){
                ToastUtil.show(mContext, "网络连接失败...");
            }else {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.getInt("status")==1){
                        ToastUtil.show(mContext, "编辑成功...");
                        //
                        ((BaseActivity) getActivity()).popBack();
                        ((ShopStoreActivity) getActivity()).refHistoryData();
                    }else {
                        ToastUtil.show(mContext, "编辑失败哦...");
                    }
                } catch (JSONException e) {
                    ToastUtil.show(mContext, "编辑失败...");
                }
            }
        }
    }

    private void handerAdPhoto(String data) {
        Gson gson = new Gson();
        getShopAdPhoto = gson.fromJson(data, GetShopAdPhoto.class);
        String ad_url = getShopAdPhoto.info.get(0).ad_url;
        LogUtil.log("---------ad_url:" + ad_url);
        if (!TextUtils.isEmpty(ad_url)) {
            isShowPhoto = true;
            ImageLoadHelper.displayImage(ad_url, mIvPhotoShop);
            //Glide.with(mContext).load(ad_url).into(mIvPhotoShop);
            mIvPhotoShop.setVisibility(View.VISIBLE);
            mRlSelectImgShop.setVisibility(View.GONE);
        }
    }


    public static FragmentShopStore newInstance() {
        FragmentShopStore fragmentShopStore = new FragmentShopStore();
        return fragmentShopStore;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_menber_banner, null);
        }
        return contain;
    }

    private void requestData() {
        showProgressDialog("");
        instance = HttpGetController.getInstance();
        //获取轮番图
        LogUtil.log("------------user_id:" + PrefUtils.getUser(mContext).user_id);
        instance.getShopPhoto(PrefUtils.getUser(mContext).user_id, className);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assignViews();
        initEvent();
    }

    private void initEvent() {
        mIvPhotoShop.setOnClickListener(this);
        mTvPreviewShop.setOnClickListener(this);
        mRlSelectImgShop.setOnClickListener(this);
        mIvHistory.setOnClickListener(this);
        mIvUpdata.setOnClickListener(this);
        mIvSeleectMuban.setOnClickListener(this);
        mIvShowMuban.setOnClickListener(this);
    }

    private void initReceiver() {
        initReceiver(new String[]{action_getAdPhoto, action_setUpdataTemplateList, action_setEditTemplateList});
    }

    private void assignViews() {
        mIvPhotoShop = (ImageView) findViewById(R.id.iv_photo_shop);
        mLlBgPro = (LinearLayout) findViewById(R.id.ll_bg_pro);
        mTvPreviewShop = (TextView) findViewById(R.id.tv_preview_shop);
        mRlSelectImgShop = (RelativeLayout) findViewById(R.id.rl_select_img_shop);
        mIvHistory = (TextView) findViewById(R.id.iv_history);
        mIvUpdata = (TextView) findViewById(R.id.iv_updata);
        mEdInputProname = (EditText) findViewById(R.id.ed_input_proname);
        mEdInputProprice = (EditText) findViewById(R.id.ed_input_proprice);
        mIvSeleectMuban = (ImageView) findViewById(R.id.iv_seleect_muban);
        mIvShowMuban = (ImageView) findViewById(R.id.iv_show_muban);
        mRlshopPhto = (RelativeLayout) findViewById(R.id.rl_shop_photo);
        mLlMubanName = (LinearLayout) findViewById(R.id.ll_muban_name);
        mTvShopName = (TextView) findViewById(R.id.tv_shop_name);
        mTvShopName2 = (TextView) findViewById(R.id.tv_shop_name2);
        if (isEditStatic){
            initTitleWithoutBack("编辑项目");
            mIvHistory.setVisibility(View.INVISIBLE);
            mIvUpdata.setText("完成");
            setTitleRight("取消", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) getActivity()).popBack();
                }
            });
            mEdInputProname.setText(title);
            mEdInputProprice.setText(price);
            mIvSeleectMuban.setVisibility(View.GONE);
            mIvShowMuban.setVisibility(View.VISIBLE);
            showShopName();
            LogUtil.log("-------------------ge" + imageUrl);
            ImageLoadHelper.displayImage(imageUrl, mIvShowMuban);
            mRlshopPhto.setVisibility(View.INVISIBLE);
            mLlBgPro.setBackgroundResource(R.drawable.bg_frame_pro);
        }else {
            LogUtil.log("-------重新创建");
            initTitle("店铺装饰");
            mRlshopPhto.setVisibility(View.VISIBLE);
            requestData();
            mLlBgPro.setBackgroundResource(R.drawable.bg_outline);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }

    @Override
    public void onClickAction(int whichButton) {
        ShopStoreActivity shopStoreActivity = (ShopStoreActivity) getActivity();
        switch (whichButton) {
            case 0:
                //TODO　打开相机
                Intent intentFromCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentFromCapture.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                // 判断存储卡是否可用，存储照片文件
                if (hasSdcard()) {
                    intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT, Uri
                            .fromFile(new File(Environment.getExternalStorageDirectory(), IMAGE_FILE_NAME)));
                }
                startActivityForResult(intentFromCapture, CODE_CAMERA_REQUEST);
                break;

            case 1:
                //TODO 打开相册
                Intent intentFromGallery = new Intent(Intent.ACTION_GET_CONTENT);
                intentFromGallery.addCategory(Intent.CATEGORY_OPENABLE);
                // 设置文件类型
                intentFromGallery.setType("image/*");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    startActivityForResult(intentFromGallery, SELECT_PIC_KITKAT);
                } else {
                    startActivityForResult(intentFromGallery, CODE_GALLERY_REQUEST);
                }
                break;
            case 2:
                //TODO 用户取消
                break;
        }
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        ToastUtil.show(mContext, "取消...");
    }

    /**
     * 检查设备是否存在SDCard的工具方法
     */
    public static boolean hasSdcard() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            // 有存储的SDCard
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        LogUtil.log("------用户的请求码：" + requestCode + "-----用户的结果码：" + resultCode);
        // 用户没有进行有效的设置操作，返回
        if (resultCode == 0) {
            return;
        }
        switch (requestCode) {
            case CODE_GALLERY_REQUEST:
                //相册 低版本打开的方式
                cropRawPhoto(intent.getData());
                break;
            case SELECT_PIC_KITKAT:
                //相册 高版本打开的方式
                cropRawPhoto(intent.getData());
                break;
            case CODE_CAMERA_REQUEST:
                if (hasSdcard()) {
                    //照相机
                    File tempFile = new File(Environment.getExternalStorageDirectory(), IMAGE_FILE_NAME);
                    cropRawPhoto(Uri.fromFile(tempFile));
                } else {
                    ToastUtil.show(mContext, "未找到存储卡，无法存储照片！");
                }
                break;
            case CODE_RESULT_REQUEST:
                // 图片编辑后使用这个方式打开
                if (intent != null) {
                    //编辑图片完成
                    setImageView(intent);
                }
                break;
        }
    }

    public void cropRawPhoto(Uri uri) {
        if (uri == null) {
            return;
        }
        LogUtil.log("----------开始剪切");
        Intent intent = new Intent("com.android.camera.action.CROP");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            String url = GetPhotoForAlbum.getPath(mContext, uri);
            LogUtil.log("------Android4.4系统....");
            intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");
        } else {
            LogUtil.log("------4.4以下...");
            intent.setDataAndType(uri, "image/*");
        }
        //TODO 方法二
        /*创建一个指向需要操作文件（filename）的文件流。（可解决无法“加载问题”）*/
        String url = GetPhotoForAlbum.getPath(mContext, uri);
        intent.setDataAndType(Uri.fromFile(new File(url)), "image/*");
        // 设置裁剪
        intent.putExtra("crop", "true");
        // aspectX , aspectY :宽高的比例
        intent.putExtra("aspectX", 2);
        intent.putExtra("aspectY", 1);
        // outputX , outputY : 裁剪图片宽高
        intent.putExtra("outputX", 240);
        intent.putExtra("outputY", 120);

        intent.putExtra("scale", true);
        intent.putExtra("return-data", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());//返回格式
//        //不启用人脸识别
//        intent.putExtra("noFaceDetection", true);//若为false则表示不返回数据
        startActivityForResult(intent, CODE_RESULT_REQUEST);
    }



    /**
     * 提取保存裁剪之后的图片数据，并设置头像部分的View
     */
    private void setImageView(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");
            mIvPhotoShop.setVisibility(View.VISIBLE);
            mIvPhotoShop.setImageBitmap(photo);
            mRlSelectImgShop.setVisibility(View.GONE);
            //保存图片至本地
            setPicToView(photo);
        }
    }

    /**
     * 上存图片
     *
     * @param mBitmap
     */
    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream os = null;
        File file = new File(Sdcard.User_Root_Dir);
        file.mkdirs();// 创建文件夹
        fileName = new File(Sdcard.User_Root_Dir, "ad.jpg");
        LogUtil.log("------上存头像的路径" + fileName.toString());
        //PrefUtils.setAdImageUri(mContext, fileName.toString());
        try {
            os = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                //关闭流
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_select_img_shop:
                //弹出对话框
                ActionSheet.showSheet(mContext, FragmentShopStore.this, FragmentShopStore.this, "选择照片", "拍照", "从相册选择照片");
                break;
            case R.id.iv_photo_shop:
                //弹出对话框
                ActionSheet.showSheet(mContext, FragmentShopStore.this, FragmentShopStore.this, "选择照片", "拍照", "从相册选择照片");
                break;
            case R.id.tv_preview_shop:
                //把图片数据给传递过去
                //String adImageUri = PrefUtils.getAdImageUri(mContext);
                Intent intent = new Intent(mContext, ActivityPreView.class);
                if ((fileName != null) && fileName.exists()) {
                    intent.putExtra("ad_img_uri", fileName.toString());
                    startActivity(intent);
                } else if (getShopAdPhoto != null) {
                    if (!TextUtils.isEmpty(getShopAdPhoto.info.get(0).ad_url)) {
                        intent.putExtra("ad_img_url", getShopAdPhoto.info.get(0).ad_url);
                        startActivity(intent);
                    } else {
                        ToastUtil.show(mContext, "请选择图片...");
                    }
                } else {
                    ToastUtil.show(mContext, "请选择图片...");
                }
                break;
            case R.id.iv_seleect_muban:
                showMubanList();
                break;
            case R.id.iv_updata:
                String title = mEdInputProname.getText().toString().trim();
                String price = mEdInputProprice.getText().toString().trim();
                if (TextUtils.isEmpty(title)) {
                    toast("请输入项目的名字!");
                    return;
                }
                if (TextUtils.isEmpty(price)) {
                    toast("请输入项目的价格!");
                    return;
                }
                if (TextUtils.isEmpty(imageUrl)) {
                    toast("请选择模板图片!");
                    return;
                }
                //
                if (isEditStatic){
                    LogUtil.log("-------------------ge"+imageUrl);
                    setEditTemplateList(proId,title,imageUrl,price,templateId);
                }
                else {
                    setUpdataTemplateList(title, imageUrl, price, templateId);
                }
                break;
            case R.id.iv_show_muban:
                showMubanList();
                break;
            case R.id.iv_history:
                ShopStoreActivity shopStoreActivity = (ShopStoreActivity) mContext;
                shopStoreActivity.openHistoryMuban();
                break;
        }
    }

    /**
     * 编辑项目上传
     */
    private void setEditTemplateList(String proId,String title,String imgUrl,String money,String templateId) {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().setEditTemplateList(token,proId,title,imgUrl,money,templateId,className);
    }

    private void setUpdataTemplateList(String coupon_title, String coupon_img, String coupon_money, String template_id) {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().setUpdataTemplateList(token, coupon_title, coupon_img, coupon_money, template_id, className);
    }

    private MubanListDialog mMubanListDialog;

    private void showMubanList() {
        mMubanListDialog = new MubanListDialog(R.layout.view_muban_list);
        mMubanListDialog.show(getFragmentManager(), "ShowMubanList");
    }

    public void setIdImageUrl(String id, String image) {
        this.templateId = id;
        this.imageUrl = image;
        //显示
        if (imageUrl != null) {
            mIvSeleectMuban.setVisibility(View.GONE);
            mIvShowMuban.setVisibility(View.VISIBLE);
            ImageLoadHelper.displayImage(imageUrl, mIvShowMuban);
            showShopName();
            LogUtil.log("-------------------ge" + imageUrl);
        } else {
            mLlMubanName.setVisibility(View.GONE);
            mIvSeleectMuban.setVisibility(View.VISIBLE);
            mIvShowMuban.setVisibility(View.GONE);
        }

    }

    public void showShopName(){
        String company_name = PrefUtils.getUser(mContext).company_name;
//            String company_name = "傻豪傻豪豪尚·发型定制（天河南店）";
        mTvShopName2.setVisibility(View.GONE);
        mLlMubanName.setVisibility(View.VISIBLE);
        LogUtil.log("----------company_name.length:" + company_name.length());
        if (company_name.length()>9){
            mTvShopName.setTextSize(ScreenUtil.px2sp(mContext, 30));
            String name1= company_name.substring(0, 8);
            String name2 = company_name.substring(9,company_name.length());
            mTvShopName.setText(name1);
            mTvShopName2.setVisibility(View.VISIBLE);
            mTvShopName2.setText(name2);
        }else {
            mTvShopName.setText(company_name);
        }
    }

}
