package cn.meiqu.mich.activity.me.expense;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.RecycleExpendAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Expense;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.view.superrecyclerview.SuperRecyclerView;

/**
 * Created by Fatel on 15-9-29.
 */
public class FragmentExpense extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    String className = getClass().getName();
    String action_getPayList = className + API.getPayList;
    private RelativeLayout mCfl01;
    private TextView mTvTodayNum;
    private TextView mTvTodayIncome;
    private RelativeLayout mCfl02;
    private TextView mTvWeekNum;
    private TextView mTvWeekIncome;
    private RelativeLayout mCfl03;
    private TextView mTvMonthNum;
    private TextView mTvMonthIncome;
    private SuperRecyclerView mRecycleV;
    RecycleExpendAdapter adapter;
    ArrayList<Expense.PayList> playLists = new ArrayList<>();
    ArrayList<Expense.PayList> playListsTemp = new ArrayList<>();
    RelativeLayout[] cfls = new RelativeLayout[3];
    Expense expense;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_getPayList});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_expense, null);
            assignViews();
            initRecycle();
            requestList();
        }
        return contain;
    }

    private void assignViews() {
        initTitle("消费统计");
        setTitleRight("按项目", this);
        mCfl01 = (RelativeLayout) findViewById(R.id.cfl_01);
        mTvTodayNum = (TextView) findViewById(R.id.tv_today_num);
        mTvTodayIncome = (TextView) findViewById(R.id.tv_today_income);
        mCfl02 = (RelativeLayout) findViewById(R.id.cfl_02);
        mTvWeekNum = (TextView) findViewById(R.id.tv_week_num);
        mTvWeekIncome = (TextView) findViewById(R.id.tv_week_income);
        mCfl03 = (RelativeLayout) findViewById(R.id.cfl_03);
        mTvMonthNum = (TextView) findViewById(R.id.tv_month_num);
        mTvMonthIncome = (TextView) findViewById(R.id.tv_month_income);
        mRecycleV = (SuperRecyclerView) findViewById(R.id.recycleV);
        cfls[0] = mCfl01;
        cfls[1] = mCfl02;
        cfls[2] = mCfl03;
        for (int i = 0; i < cfls.length; i++) {
            cfls[i].setOnClickListener(this);
        }
        mCfl03.setSelected(true);
    }

    public void initRecycle() {
        mRecycleV = (SuperRecyclerView) findViewById(R.id.recycleV);
        mRecycleV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecycleV.setRefreshListener(this);
        adapter = new RecycleExpendAdapter(getActivity(), playListsTemp);
        mRecycleV.setAdapter(adapter);
    }

    public void refreshView() {
        if (expense != null) {
            Expense.Count count = expense.count;
            mTvTodayNum.setText(count.day_sell + "笔");
            mTvTodayIncome.setText("￥ " + StringUtil.getLess(count.day_money));
            mTvWeekNum.setText(count.week_sell + "笔");
            mTvWeekIncome.setText("￥ " + StringUtil.getLess(count.week_money));
            mTvMonthNum.setText(count.month_sell + "笔");
            mTvMonthIncome.setText("￥ " + StringUtil.getLess(count.month_money));
            if (expense.pay_list != null) {
                playLists.clear();
                playLists.addAll(expense.pay_list);
                filterTime(lastS);
            }
        }
    }

    public void requestList() {
        showProgressDialog("");
        HttpGetController.getInstance().getExpenseyList(className);
    }

    public void handleList(String data) {
//        if(data!=null){
//            dismissMessage();
//        }else{
//            showNoMessage("没有内容哦!");
//        }
        expense = new Gson().fromJson(JsonUtil.getJsonObject(data).optString("info"), Expense.class);
        refreshView();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        mRecycleV.setRefreshing(false);
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getPayList)) {
                handleList(data);
            }
        }
    }

    private int lastId = R.id.cfl_03;//默认是本月
    private int lastS = -1;//默认是本月

    public void filter(View view) {
        if (lastId == view.getId())
            return;
        lastId = view.getId();
        for (int i = 0; i < cfls.length; i++) {
            cfls[i].setSelected(false);
        }
//        ((Checkable) view).setChecked(true);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        switch (view.getId()) {
            case R.id.cfl_01://今天
                lastS = (int) (c.getTimeInMillis() / 1000);
                cfls[0].setSelected(true);
                break;
            case R.id.cfl_02://本周
                int dd = c.get(Calendar.DAY_OF_WEEK) - 1;
                c.add(Calendar.DATE, -dd);
                lastS = (int) (c.getTimeInMillis() / 1000);
                cfls[1].setSelected(true);
                break;
            case R.id.cfl_03://本月
                lastS = -1;
                cfls[2].setSelected(true);
                break;
        }
        filterTime(lastS);
        mRecycleV.getRecyclerView().scrollToPosition(0);
    }

    public void filterTime(int starttime) {
        playListsTemp.clear();
        if (playLists != null && !playLists.isEmpty()) {
            if (starttime == -1) {
                playListsTemp.addAll(playLists);
            } else {
                int size = playLists.size();
                for (int i = 0; i < size; i++) {
                    try {
                        int t = Integer.parseInt(playLists.get(i).show_time);
                        if (t >= starttime) {
                            playListsTemp.add(playListsTemp.get(i));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_title_more) {
            ((ExpenseActivity) getActivity()).showItem();
        } else {
            filter(v);
        }
    }

    @Override
    public void onRefresh() {
        requestList();
    }
}
