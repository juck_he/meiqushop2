package cn.meiqu.mich.activity.me.accredit;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.ListAccreditAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Accredit;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;

/**
 * Created by Fatel on 15-9-23.
 */
public class FragmentAccreditList extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, ListAccreditAdapter.OnAccreditClickListener {
    private ListView mListView;
    String className = getClass().getName();
    String action_getList = className + API.getStyList;
    String action_del = className + API.delStylist;
    ArrayList<Accredit> accredits = new ArrayList<>();
    ListAccreditAdapter adapter;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_del, action_getList});
        mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_accredit_list, null);
            assignView();
        }
        requestList();
        return contain;
    }


    protected void assignView() {
        initTitle("授权管理");
        setTitleRight("新增", this);
        setSwipeRefresh(R.id.srl_refresh, this);
        mListView = (ListView) findViewById(R.id.lv_content);
        adapter = new ListAccreditAdapter(getActivity(), accredits);
        mListView.setAdapter(adapter);
        adapter.setOnAccreditClickListener(this);
    }

    public void requestList() {
        showProgressDialog("");
        setSwipeRefreshing(true);
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getStylist(token, className);
    }

    public void handlList(String data) {

        accredits.clear();
        ArrayList<Accredit> temp = new Gson().fromJson(JsonUtil.getJsonObject(data).optJSONObject("info").optString("stylists"), new TypeToken<ArrayList<Accredit>>() {
        }.getType());
        accredits.addAll(temp);
        if (accredits.size()>0) {
            dismissMessage();
        } else {
            showNoMessage("没有内容哦!");
        }
        adapter.notifyDataSetChanged();
    }

    public void requestDel(String styId) {
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().delStylist(token,styId, className);
    }

    public void handleDel(String data) {

    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_del)) {
                handleDel(data);
            } else if (action.equals(action_getList)) {
                LogUtil.log("------用户列表"+data);
                handlList(data);
            }
        }
//        overlayForResult(AddAuthActivity.class, RQC.ADD);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_title_more) {
            ((AccreditActivity) getActivity()).showAddAccredit();
        }
    }

    @Override
    public void onClickEdt(int position) {
        ((AccreditActivity) getActivity()).showEdtAccredit(accredits.get(position));
    }

    @Override
    public void onClickDel(final int position) {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setMessage("确认删除授权账户" + accredits.get(position).user_account + "?")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestDel(accredits.get(position).id);
                        accredits.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("取消", null)
                .create();
        dialog.show();
    }

    @Override
    public void onRefresh() {
        requestList();
    }




}
