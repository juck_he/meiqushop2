package cn.meiqu.mich.activity.me.realpay;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

public class RealPayActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_pay);
        initFragment();
    }

    @Override
    public void initFragment() {
        showFirst(new FragmentRealPay());
    }

    public void showHistory() {
        showAndPop(new FragmentRealPayHistroy());
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

}
