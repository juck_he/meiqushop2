package cn.meiqu.mich.activity.imageshow;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.BitListAdapter;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.BitListIpAddress;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.bean.UserShow;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/24.
 */
public class ActivityIpList extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMore;
    private String className = getClass().getName();
    private String action_getbitListInfo = className + API.getbitListInfo;
    String action_setsetTurnsPhoto = className + API.setTurnsPhoto;
    List<BitListIpAddress.BitDeviceList> device_list;//机器的所有信息
    private BitListAdapter adapter;
    ArrayList<String> list;                          //存放美屏的id信息
    private ListView mLvBitlist;
    private Context mContext;
    private boolean isShowList = false;
    private TextView mtvStatus;

    private UserShow userShow;
    private String titleName;
    private String titleConntent;
    private String imagePhoto;
    private String headImage;
    private String templateType;

    @Override
    public void onHttpHandle(String action, String data) {
        if (action_getbitListInfo.equals(action)) {
            //打印大屏列表数据
            dismissProgressDialog();
            if (data != null) {
                LogUtil.log("-------" + data);
                handlerBitData(data);
                if (isShowList) {

                } else {
                    showTipDialog();
                }
            } else {
                toast("网络连接失败...");
            }
        }
        if (action.equals(action_setsetTurnsPhoto)) {
            dismissProgressDialog();
            if (data != null) {
                LogUtil.log("--------上存完毕" + data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        ToastUtil.show(mContext, "形象展示上传成功...");
                        finish();
                    } else {
                        ToastUtil.show(mContext, "形象展示上传失败...");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ToastUtil.show(mContext, "形象展示上传失败...");
                }
            } else {
                toast("网络连接失败...");
            }
        }
    }

    /**
     * 处理大屏数据
     */
    private void handlerBitData(String data) {
        Gson gson = new Gson();
        BitListIpAddress bitListIpAddress = gson.fromJson(data, BitListIpAddress.class);
        //获取到所有机器的IP地址
        device_list = bitListIpAddress.info.device_list;
        if (isShowList) {
            adapter = new BitListAdapter(ActivityIpList.this, device_list, true);
        } else {
            adapter = new BitListAdapter(ActivityIpList.this, device_list, false);
        }
        mLvBitlist.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initReceiver(new String[]{action_getbitListInfo, action_setsetTurnsPhoto});
        setContentView(R.layout.activity_bit_iplist);
        this.mContext = this;
        //表示从形象展示界面打开的
        userShow = (UserShow) getIntent().getSerializableExtra("userShow");
        //表示从字幕广告界面打开的
        isShowList = getIntent().getBooleanExtra("isShowList", false);
        if (isShowList) {

        } else {
            imagePhoto = userShow.headPic;
            //
            titleName = userShow.titleName;
            //
            titleConntent = userShow.deceContent;
            //
            headImage = userShow.imgUrl;
            //
            templateType = userShow.currentType;
            LogUtil.log("--------userShow.currentType2" + templateType);
        }
        initView();
        initData();
        requestData();
    }


    @Override
    public void initFragment() {

    }

    /**
     * 显示提示对话框
     */
    AlertDialog.Builder builder = null;

    private void showTipDialog() {
        if (builder == null) {
            builder = new AlertDialog.Builder(mContext, R.style.myDialog);
        }
        View viewDialog = View.inflate(mContext, R.layout.view_tip_upto_bitscreen, null);
        builder.setView(viewDialog, 0, 0, 0, 0);
        final AlertDialog alertDialog = builder.create();
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
        LinearLayout llBtnOk = (LinearLayout) viewDialog.findViewById(R.id.ll_btnok);
        llBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    /**
     * 获取店内的美屏列表
     */
    private void requestData() {
        showProgressDialog("");
        HttpGetController instance = HttpGetController.getInstance();
        instance.getbitListInfo(PrefUtils.getUser(ActivityIpList.this).token, className);
    }

    private void initData() {
        list = new ArrayList<String>();
        mIvBack.setOnClickListener(this);
        if (isShowList) {
            mTvTitle.setText("字幕广告");
            mtvStatus.setText("状态");
            mtvStatus.setVisibility(View.VISIBLE);
            mTvMore.setVisibility(View.GONE);
        } else {
            mtvStatus.setVisibility(View.GONE);
            mTvTitle.setText("美屏列表");
            mTvMore.setVisibility(View.VISIBLE);
            mTvMore.setText("上传");
        }
        mTvMore.setOnClickListener(this);
        mLvBitlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LogUtil.log("--------position:" + position + "------" + id);
                if (isShowList == false) {
                    int thisnumber = position + 1;
                    String devicenumber = device_list.get(position).device_number;
                    if (!list.contains(devicenumber)) {
                        View childAt = mLvBitlist.getChildAt(position);
                        if (thisnumber % 2 != 0) {
                            childAt.findViewById(R.id.iv_sig_check).setVisibility(View.VISIBLE);
                        } else {
                            childAt.findViewById(R.id.iv_dou_check).setVisibility(View.VISIBLE);
                        }
                        list.add(devicenumber.trim());
                    } else {
                        list.remove(devicenumber);
                        View childAt = mLvBitlist.getChildAt(position);
                        if (thisnumber % 2 != 0) {
                            childAt.findViewById(R.id.iv_sig_check).setVisibility(View.GONE);
                        } else {
                            childAt.findViewById(R.id.iv_dou_check).setVisibility(View.GONE);
                        }
                    }
                    LogUtil.log("--------list:" + list);
                }
            }
        });
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        mLvBitlist = (ListView) findViewById(R.id.lv_bitlist);
        mtvStatus = (TextView) findViewById(R.id.tv_status);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_more:
                //上存广告到大屏列表
                if (TextUtils.isEmpty(getDevice())) {
                    ToastUtil.show(ActivityIpList.this, "请选择要上传到那个大屏...");
                    return;
                }
                upPhotoUrl();
                break;
        }
    }


    /**
     * 提交数据
     */
    public void upPhotoUrl() {
        showProgressDialog("");
        HttpGetController instance = HttpGetController.getInstance();
        User user = PrefUtils.getUser(mContext);
        LogUtil.log("-------user.token:" + user.token);
        LogUtil.log("--------------" + getDevice());
        LogUtil.log("templateType" + templateType);
        instance.setTurnsPhoto(user.token, headImage, getDevice(), titleName, titleConntent, imagePhoto, templateType, className);
    }


    /**
     * 从list集合里面把美屏的id号修改成 xxx,xxx
     *
     * @return
     */
    public String getDevice() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                stringBuffer.append(list.get(i));
            } else {
                stringBuffer.append(list.get(i) + ",");
            }
        }
        return stringBuffer.toString();
    }
}
