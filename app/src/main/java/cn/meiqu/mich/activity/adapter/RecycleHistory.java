package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.HistoryInfo;

/**
 * Created by Administrator on 2015/9/30.
 */
public class RecycleHistory extends RecyclerView.Adapter<RecycleHistory.MyViewHolderHistory>  {
    private Context mContext;
    private List<HistoryInfo.HistoryResult> list;

    public RecycleHistory(Context mCOntext, List<HistoryInfo.HistoryResult> listDate) {
        this.mContext = mCOntext;
        this.list = listDate;

    }

    @Override
    public MyViewHolderHistory onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderHistory(LayoutInflater.from(mContext).inflate(R.layout.item_histoty_item, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolderHistory holder, int position) {
        holder.instanceListView(position);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

        // void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    class MyViewHolderHistory extends RecyclerView.ViewHolder {

        private TextView mTvOddnumber;
        private TextView mTvUsetime;
        private TextView mTvContent;
        private TextView mTvMoney;

        private void assignViews() {
            mTvOddnumber = (TextView) findViewById(R.id.tv_oddnumber);
            mTvUsetime = (TextView) findViewById(R.id.tv_usetime);
            mTvContent = (TextView) findViewById(R.id.tv_content);
            mTvMoney = (TextView) findViewById(R.id.tv_money);
        }


        public MyViewHolderHistory(View itemView) {
            super(itemView);
        }
        public void instanceListView(int position) {
            assignViews();
            mTvOddnumber.setText(list.get(position).coupon_num.replaceAll("\\d{4}(?!$)", "$0 "));
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String date = sdf.format(Long.valueOf(list.get(position).use_time)*1000L);
            mTvUsetime.setText(date);
            mTvContent.setText(list.get(position).coupon_title);
            mTvMoney.setText("￥ " + list.get(position).pay_money);
        }


        public View findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}


