package cn.meiqu.mich.activity.imageshow;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.PhotoStylistAdapter;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.EditTextPhoto;
import cn.meiqu.mich.bean.TurnsPhotoData;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/23.
 */
public class ActivityTurnsPhoto extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMore;
    private Context mContext;
    private RelativeLayout mContainer;
    private ViewPager mViewpager;
    private ArrayList<View> views;
    private List<TurnsPhotoData.InfoEntity.ImgListEntity> img_list;
    PhotoStylistAdapter photoStylistAdapter;
    String className = getClass().getName();
    String action_getTurnsPhoto = className + API.getTurnsPhoto;
    String action_setDelTurnPhoro = className + API.setDelTurnPhoro;
    private int currentItem; //当前current photo的位置
    private String intentResultUrl;
    private int intentResultPosition;

    @Override
    public void onHttpHandle(String action, String data) {
        dismissProgressDialog();
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getTurnsPhoto)) {
                //打印获取到广告数据
                LogUtil.log("---------------打印获取到广告数据:" + data);
                handlerTurnsData(data);
                //缓存到本地
                PrefUtils.setTurnsPhotoJson(mContext, data);
            }
            if (action.equals(action_setDelTurnPhoro)) {
                LogUtil.log("---------------删除数据:" + data);
                if (!TextUtils.isEmpty(data)) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(data);
                        int status = jsonObject.getInt("status");
                        if (status == 1) {
                            //删除当前的图片
                            toDeletePhoto(currentItem);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    //处理获取到的大屏数据
    private void handlerTurnsData(String data) {
        Gson gson = new Gson();
        TurnsPhotoData turnsPhotoData = gson.fromJson(data, TurnsPhotoData.class);
        //把url设置给pageradapter
        views = new ArrayList<View>();
        img_list = turnsPhotoData.info.img_list;
        for (int i = 1; i <= img_list.size(); i++) {
            View view = View.inflate(mContext, R.layout.view_hairt_list, null);
            view.setVisibility(View.GONE);
            views.add(view);
        }
        //
        photoStylistAdapter = new PhotoStylistAdapter(mContext, img_list, views);
        photoStylistAdapter.setOnClickBtnListen(new PhotoStylistAdapter.OnClickPressListen() {
            @Override
            public void onClickDel(View view, int position) {
                //获取当前viewpager的Item位置
                showDialog("是否要删除当前形象展示图");
                //ToastUtil.show(mContext, "删除...");
            }

            @Override
            public void onClickEdit(View view, int position) {
                //ToastUtil.show(mContext, "编辑...");
                EditTextPhoto editTextPhoto = new EditTextPhoto();
                editTextPhoto.currentItem = position;
                editTextPhoto.deviceId = img_list.get(position).device_number;
                editTextPhoto.headUrl = img_list.get(position).head_pic;
                editTextPhoto.contentName = img_list.get(position).title;
                editTextPhoto.contentDesc = img_list.get(position).desc;
                //TODO 修改格式 和 模板的位置;
                editTextPhoto.typeId = img_list.get(position).template_id;
                //
                Intent intent = new Intent(mContext, ActivityStylistAd.class);
                intent.putExtra("editphoto", editTextPhoto);
                startActivityForResult(intent, 109);
            }
        });
        mViewpager.setAdapter(photoStylistAdapter);
        if (!TextUtils.isEmpty(intentResultUrl)) {
            mViewpager.setCurrentItem(intentResultPosition);
            if (intentResultUrl.equals("no_to_updata")) {
                //
            } else {
                img_list.get(intentResultPosition).img_url = intentResultUrl;
                photoStylistAdapter.notifyDataSetChanged();
            }
        }
        //
        if (img_list.size() > 0) {
            mTvTitle.setText("形象展示" + "(" + "1" + "/" + img_list.size() + ")");
        } else {
            mTvTitle.setText("形象展示" + "(" + "0" + "/" + img_list.size() + ")");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitturnsphoto);
        this.mContext = this;
        initReceiver(new String[]{action_getTurnsPhoto, action_setDelTurnPhoro});
        initView();
        initData();
        requestDate();
    }

    @Override
    public void initFragment() {

    }

    private void requestDate() {
        showProgressDialog("");
        //发送请求
        HttpGetController httpClient = HttpGetController.getInstance();
        //获取发型相册
        httpClient.getTurnsPhoto(PrefUtils.getUser(mContext).token, className);
    }

    private void initData() {
        mIvBack.setOnClickListener(this);
        //mTvMore.setOnClickListener(this);
        mViewpager.addOnPageChangeListener(this);
        mTvTitle.setText("形象展示");
        //mTvMore.setText("删除");
        /////////////////////主要配置//////////////////////////////////////
        // 1.设置幕后item的缓存数目
        mViewpager.setOffscreenPageLimit(3);
        // 2.设置页与页之间的间距
        mViewpager.setPageMargin(ScreenUtil.dip2px(mContext,40));
        // 3.将父类的touch事件分发至viewPgaer，否则只能滑动中间的一个view对象
        mContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mViewpager.dispatchTouchEvent(event);
            }
        });
    }


    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMore = (TextView) findViewById(R.id.tv_more);
        mTvMore.setVisibility(View.GONE);
        mContainer = (RelativeLayout) findViewById(R.id.container);
        mViewpager = (ViewPager) findViewById(R.id.viewpager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    AlertDialog alertDialog;

    public void showDialog(String str) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(mContext).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //TODO 删除图片
                    currentItem = mViewpager.getCurrentItem();
                    LogUtil.log("------currentItem" + currentItem);
                    //
                    setDelTurnsPhoto(currentItem);
                }
            }).setNegativeButton("取消", null).setMessage(str).create();
        }
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
    }

    /**
     * 删除远程服务器上的item
     * @param currentItem
     */
    private void setDelTurnsPhoto(int currentItem) {
        if ((mViewpager.getChildCount() > 0) || (img_list.size() > 0)) {
            showProgressDialog("");
            //发送请求
            HttpGetController httpClient = HttpGetController.getInstance();
            //获取发型相册
            httpClient.setDelTurnPhoro(PrefUtils.getUser(mContext).token, img_list.get(currentItem).id + "", className);
        } else {
            //TODO 没有数据的图标
            ToastUtil.show(mContext, "已经删除完了哦...");
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        LogUtil.log("---------当前位置：" + position);
        mTvTitle.setText("形象展示" + "(" + (position + 1) + "/" + img_list.size() + ")");
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void toDeletePhoto(int position) {
        // 图
        mViewpager.removeView(views.get(position));
        views.remove(position);
        // data
        img_list.remove(position);
        if (img_list.size() > 0) {
            mTvTitle.setText("形象展示" + "(" + (mViewpager.getCurrentItem() + 1) + "/" + img_list.size() + ")");
        } else {
            mTvTitle.setText("形象展示" + "(" + "0" + "/" + img_list.size() + ")");
        }
        LogUtil.log("img_list.:" + img_list.size());
        photoStylistAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 109) {
            // TODO 判断是不是取消编辑返回来的 ，不然就重新加载数据
            intentResultUrl = intent.getStringExtra("intent_result_url");
            intentResultPosition = intent.getIntExtra("intent_result_position", 0);
            if (!TextUtils.isEmpty(intentResultUrl)) {
                String turnsPhotoJson = PrefUtils.getTurnsPhotoJson(mContext);
                if (!TextUtils.isEmpty(turnsPhotoJson)) {
                    //解析json数据 , 跳转到上次打开的位置 ， 判断上次打开的位置还存在不
                    handlerTurnsData(turnsPhotoJson);
                    requestDate();
                } else {
                    requestDate();
                }
            }
        }
    }
}
