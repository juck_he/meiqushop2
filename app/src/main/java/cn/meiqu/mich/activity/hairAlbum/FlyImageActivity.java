package cn.meiqu.mich.activity.hairAlbum;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;

import com.umeng.analytics.MobclickAgent;
import com.yalantis.cameramodule.activity.CameraActivity;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSendController;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Fatel on 15-4-24.
 */
public class FlyImageActivity extends BaseActivity {
    public static String EXTRA_ACTION = "EXTRA_ACTION";
    FragmentHairAlbum fragmentHairAlbum;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fly_image);
        initFragment();
        //注册了一个本地广播
    }

    @Override
    public void initFragment() {
        setContainerId(containerId);
        fragmentHairAlbum = FragmentHairAlbum.newInstance();
        showFirst(fragmentHairAlbum);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onResume() {

        super.onResume();
        MobclickAgent.onResume(this);
        initReceiver();

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(returnBroadcase);

    }

    TakePhotoReturnBroadcase returnBroadcase = new TakePhotoReturnBroadcase();

    public void initReceiver() {
        IntentFilter filter = new IntentFilter(CameraActivity.ACTION_PHOTO_RETURN);
        filter.addAction(TcpResponController.action_tcp_respon);
        filter.addAction(CameraActivity.ACTION_PHOTO_FINISH);
        LocalBroadcastManager.getInstance(this).registerReceiver(returnBroadcase, filter);
    }

    Bitmap bitmap;
    String imageName;
    byte[] b;

    public void handleBitmap(Bitmap bitmap) {
        this.b = ImageUtil.compressImageByMaxSize(bitmap, 400);
        imageName = System.currentTimeMillis() + "";
        TcpSendController.getInstance().sendFeitu(imageName);
    }


    public void FlyImage() {
        LogUtil.log("FlyImage");
        showProgressDialog("");
        TcpSendController.getInstance().sendUpLoadImage(imageName, b);
    }
    class TakePhotoReturnBroadcase extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CameraActivity.ACTION_PHOTO_FINISH)) {
                TcpSendController.getInstance().sendLevelImage();
            } else if (CameraActivity.isRuning) {
                String action = intent.getStringExtra("data");
                String content = intent.getStringExtra("content");
                if (intent.getAction().equals(CameraActivity.ACTION_PHOTO_RETURN)) {
                    LogUtil.log("收到了图片广播");
                    Bitmap bitmap = intent.getBundleExtra("image").getParcelable("image");
                    handleBitmap(bitmap);
                }
                if (action != null) {
                    if (action.equals(TcpCommand.F_ImageSucceed)) {
                        dismissProgressDialog();
                    } else if (action.equals(TcpCommand.F_UploadImage)) {
                        FlyImage();
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       // LogUtil.log("---------requestCodeB:"+requestCode);
        if (fm.getFragments() != null) {
            for (Fragment f : fm.getFragments()) {
                if (f != null) {
                    f.onActivityResult(requestCode, resultCode, data);
                    break;
                }
            }
        }
    }
}
