package cn.meiqu.mich.activity.me.membermanger.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.ListTextPushHistoty;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.TextPushed;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/10/10.
 */
public class FragmentTextPush extends BaseFragment {
    private Context mContext ;
    String className = getClass().getName();
    String action_getTextPushed = className + API.getTextPushed;
    private List<TextPushed.InfoEntity.ListEntity> list ;
   // String url;
    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getTextPushed)){
            dismissProgressDialog();
            if(data!=null) {
                handleTextPush(data);
            }else {
                ToastUtil.show(mContext,"网络请求失败,请检查网络是否正常...");
            }
        }
    }

    /**
     * 处理用户未读信息
     * @param data
     */
    private void handleTextPush(String data) {

        TextPushed textPushed = new Gson().fromJson(data, TextPushed.class);
        list = textPushed.info.list;
        if (list.size()>0) {
            dismissMessage();
        } else {
            showNoMessage("没有内容哦!");
        }
        mLvTextPush.setAdapter(new ListTextPushHistoty(mContext,list));
    }

    public static FragmentTextPush newInstance() {
        FragmentTextPush fragmentTextPush = new FragmentTextPush();
        return fragmentTextPush;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_text_push, null);
        }
        //TODO 发送Http请求
        requestHttp();
        return contain;
    }

    /**
     * 获取已经发送的历史信息
     */
    private void requestHttp() {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getTextPushed(token,"2",null,className);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assignViews();
    }

    private void initReceiver() {
        initReceiver(new String[]{action_getTextPushed});

    }

    private ListView mLvTextPush;

    private void assignViews() {
        mLvTextPush = (ListView) findViewById(R.id.lv_text_push);
        mLvTextPush.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String url = list.get(position).url;
                HistoryActivity historyActivity = (HistoryActivity) getActivity();
                //historyActivity.getShowWebView();
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("url",url);
                historyActivity.startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }
//    //public String  getUrl(){
//        return url;
//    }
}
