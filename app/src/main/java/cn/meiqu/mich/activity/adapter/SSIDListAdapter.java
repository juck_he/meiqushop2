package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/10/30.
 */
public class SSIDListAdapter  extends BaseAdapter{
    private Context mContext;
    private List<ScanResult> resultList;

    public SSIDListAdapter(Context mContext,List<ScanResult> listSsid){
        this.mContext = mContext;
        resultList = listSsid;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView ==null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.view_item_text,null);
            holder.mTvSsidName = (TextView) convertView.findViewById(R.id.tv_ssid_name);
            holder.mRlSsid = (RelativeLayout) convertView.findViewById(R.id.rl_ssid);
            holder.mIvIsopend = (TextView) convertView.findViewById(R.id.iv_isopend);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80);
        String ssid = resultList.get(position).SSID;
        holder.mRlSsid.setLayoutParams(layoutParams);
        LogUtil.log(resultList.get(position).BSSID + "----" + resultList.get(position).capabilities + "----" + resultList.get(position).SSID);
        holder.mTvSsidName.setText(ssid);
        String capabilities = resultList.get(position).capabilities;
        if(capabilities.contains("WPA")){
            holder.mIvIsopend.setText("不开放");
        }else {
            holder.mIvIsopend.setText("开放");
        }
        return convertView;
    }

    class ViewHolder{
        private RelativeLayout mRlSsid;
        private TextView mTvSsidName;
        private TextView mIvIsopend;
    }
}
