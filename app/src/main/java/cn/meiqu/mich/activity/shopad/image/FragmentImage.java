package cn.meiqu.mich.activity.shopad.image;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.GridViewAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.MediaModel;
import cn.meiqu.mich.common.MediaChooser;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/11/26.
 */
public class FragmentImage extends BaseFragment {
    private Context mContext;
    private Cursor mImageCursor;
    private ArrayList<MediaModel> mGalleryModelList;
    private GridViewAdapter mImageAdapter;
    private ArrayList<String> mSelectedItems = new ArrayList<String>();
    private int photoCount;
    /**
     * 在Activity被销毁的时候，Fragment不会被销毁
     */
    public FragmentImage(){
        setRetainInstance(true);
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }
    private GridView mImageGridView;

    private void assignViews() {
        if(getActivity().getIntent()!=null){
            photoCount = getActivity().getIntent().getIntExtra("photoConut",0);
            initTitle("请选择10张图片("+photoCount+"/10)");
        }
        setTitleRight("确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 返回图片
                Intent imageIntent = new Intent();
                imageIntent.setAction(MediaChooser.IMAGE_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
                imageIntent.putStringArrayListExtra("list", mSelectedItems);
                getActivity().sendBroadcast(imageIntent);
                getActivity().finish();
            }
        });
        mImageGridView = (GridView) findViewById(R.id.gridViewFromMediaChooser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_video, null);
            assignViews();
            initPhoneImages();
        }
        mContext = getActivity();
        return contain;
    }

    /**
     * 获取到图片数据
     */
    private void initPhoneImages() {
        try {
            final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
            final String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
            mImageCursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy + " DESC");
            //
            setAdapter(mImageCursor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示图片
     * @param imagecursor
     */
    private void setAdapter(Cursor imagecursor) {
        LogUtil.log("------imagecursor"+imagecursor.getCount());
        //
        if(imagecursor.getCount() > 0){
            //
            mGalleryModelList = new ArrayList<MediaModel>();
            for (int i = 0; i < imagecursor.getCount(); i++) {
                imagecursor.moveToPosition(i);
                int dataColumnIndex       = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);
                MediaModel galleryModel   = new MediaModel(imagecursor.getString(dataColumnIndex).toString(), false);
                mGalleryModelList.add(galleryModel);
            }
            //
            mImageAdapter = new GridViewAdapter(getActivity(), 0, mGalleryModelList, false);
            mImageGridView.setAdapter(mImageAdapter);
        }else{
           //TODO　没有图片数据
        }
        //
        mImageGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                GridViewAdapter adapter = (GridViewAdapter) parent.getAdapter();
                //获取到当前这个item信息
                MediaModel galleryModel = (MediaModel) adapter.getItem(position);
                //状态取反
                galleryModel.status = ! galleryModel.status;
                if (galleryModel.status) {
                    photoCount ++;
                    if (photoCount>10){
                        photoCount --;
                        galleryModel.status = ! galleryModel.status;
                        ToastUtil.show(mContext,"最多只能选择10张图片!");
                        return;
                    }
                    mSelectedItems.add(galleryModel.url.toString());
                    mTvTitle.setText("请选择10张图片("+photoCount+"/10)");
                }else{
                    mSelectedItems.remove(galleryModel.url.toString());
                    photoCount --;
                    mTvTitle.setText("请选择10张图片(" + photoCount+"/10)");
                }
                adapter.notifyDataSetChanged();
            }
        });
    }






}
