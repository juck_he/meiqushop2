package cn.meiqu.mich.activity.me.setting;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

public class SettingActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initFragment();
    }

    @Override
    public void initFragment() {
        showFirst(new FragmentSetting());
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    public void showChangePwd() {
        showAndPop(new FragmentChangePwd());
    }
}
