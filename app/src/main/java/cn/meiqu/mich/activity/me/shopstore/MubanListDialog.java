package cn.meiqu.mich.activity.me.shopstore;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;

import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.MubanListAdapter;
import cn.meiqu.mich.basefatel.BaseFragmentDialog;
import cn.meiqu.mich.bean.ProMubanlist;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by juck on 15/12/8.
 */
public class MubanListDialog extends BaseFragmentDialog {
    private RecyclerView mSrvListMuban;
    private String className = getClass().getName();
    private String action_getProTemplateList = className + API.getProTemplateList;
    private List<ProMubanlist.InfoEntity.ListEntity> mubanList;
    private MubanListAdapter mMubanListAdapter;

    public MubanListDialog(int id) {
        super.id = id;
    }

    @Override
    protected void onHttpHandle(String action, String data) {
        if (action_getProTemplateList.equals(action)) {
            dismissProgressDialog();
            if (TextUtils.isEmpty(data)) {
                ToastUtil.show(getActivity(), "网络连接失败...");
            } else {
                LogUtil.log("-----------模板列表:" + data);
                handlerMubanList(data);
            }
        }
    }

    private void handlerMubanList(String data) {
        mubanList = new Gson().fromJson(data,ProMubanlist.class).info.list;
        mMubanListAdapter = new MubanListAdapter(getActivity(),mubanList);
        mSrvListMuban.setAdapter(mMubanListAdapter);
        mMubanListAdapter.setOnItemClickLitener(new MubanListAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                if (getActivity() instanceof OnUpdataListener){
                    OnUpdataListener onUpdataListener= (OnUpdataListener) getActivity();
                    onUpdataListener.getIdImageUrl(mubanList.get(position).id,mubanList.get(position).bg_image  );
                }
                dismiss();
            }
        });
    }

    @Override
    public void initData() {
        initReceiver(new String[]{action_getProTemplateList});
        mSrvListMuban = (RecyclerView) findViewById(R.id.srv_list_muban);
        mSrvListMuban.setHasFixedSize(true);
        mSrvListMuban.setLayoutManager(new LinearLayoutManager(getActivity()));
        getMubanList();
    }

    private void getMubanList() {
        showProgressDialog("");
        String token = PrefUtils.getUser(getActivity()).token;
        HttpGetController.getInstance().getProTemplateList(token, className);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
