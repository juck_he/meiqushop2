package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.CouponInfon;

/**
 * Created by Administrator on 2015/10/10.
 */
public class ListCuponPushHistoty extends BaseAdapter {
    private Context mContext;
    private List<CouponInfon.InfoEntity.VoucherListEntity> list;

    public ListCuponPushHistoty(Context mContext, List<CouponInfon.InfoEntity.VoucherListEntity> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
        //return 10;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CouponInfon.InfoEntity.VoucherListEntity userData = list.get(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.item_sendout, null);
            holder.mTvUsedata = (TextView) convertView.findViewById(R.id.tv_usedata);
            holder.mTvSendoutPrice = (TextView) convertView.findViewById(R.id.tv_sendout_price);
            holder.mTvSendoutName = (TextView) convertView.findViewById(R.id.tv_sendout_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag(); // 重新获取ViewHolder
        }
        holder.mTvUsedata.setText("使用期限："+userData.start_date+"~"+userData.end_date);
        holder.mTvSendoutPrice.setText(userData.money);
        holder.mTvSendoutName.setText(userData.title);

        return convertView;
    }

    class ViewHolder {
        private TextView mTvUsedata;
        private TextView mTvSendoutPrice;
        private TextView mTvSendoutName;
    }
}
