package cn.meiqu.mich.activity.me.expense;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;
import com.google.gson.Gson;

import java.util.ArrayList;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.RecycleExpendItemDetailAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.ExpenseItemDetail;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.view.superrecyclerview.SuperRecyclerView;

/**
 * Created by Fatel on 15-9-29.
 */
public class FragmentExpenseItemDetail extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    String className = getClass().getName();
    String action_detail = className + API.getPayItemDetail;
    public static String coupon_id = "";
    private ImageView mIvPic;
    private TextView mTvName;
    private TextView mTvPrice;
    private TextView mTvDate;
    private TextView mTvHasCosted;
    private TextView mTvHasSelled;
    private LinearLayout mLlLine;
    private TextView mTvSellMoney;
    private SuperRecyclerView mRecycleV;
    RecycleExpendItemDetailAdapter adapter;
    ExpenseItemDetail itemDetail;
    ArrayList<ExpenseItemDetail.Item> items = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_detail});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_expense_item_detail, null);
            assignViews();
            initRecycle();
            requestDetail();
        }
        return contain;
    }


    private void assignViews() {
        initTitle("项目每天消费量");
        mIvPic = (ImageView) findViewById(R.id.iv_pic);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvPrice = (TextView) findViewById(R.id.tv_price);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mTvHasCosted = (TextView) findViewById(R.id.tv_has_costed);
        mTvHasSelled = (TextView) findViewById(R.id.tv_has_selled);
        mLlLine = (LinearLayout) findViewById(R.id.ll_line);
        mTvSellMoney = (TextView) findViewById(R.id.tv_sell_money);
        mRecycleV = (SuperRecyclerView) findViewById(R.id.recycleV);
    }

    public void initRecycle() {
        mRecycleV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecycleV.setRefreshListener(this);
        adapter = new RecycleExpendItemDetailAdapter(getActivity(), items);
        mRecycleV.setAdapter(adapter);
    }

    public void refreshView() {
        if (itemDetail != null) {
            ExpenseItemDetail.Detail detail = itemDetail.coupon_info;
            //Glide.with(this).load(detail.coupon_img).into(mIvPic);
            ImageLoadHelper.displayImage(detail.coupon_img,mIvPic);
            mTvDate.setText("上线时间:" + detail.begin);
            mTvHasCosted.setText("" + detail.use_count);
            mTvHasSelled.setText("" + detail.sell_count);
            mTvName.setText("" + detail.coupon_title);
            mTvPrice.setText("￥ " + detail.coupon_money);
            if (itemDetail.list != null) {
                items.clear();
                items.addAll(itemDetail.list);
                adapter.notifyDataSetChanged();
            }
        }

    }

    public void requestDetail() {
        showProgressDialog("");
        HttpGetController.getInstance().getExpenseItemDetail(coupon_id, className);
    }

    public void handleDetail(String data) {
        itemDetail = new Gson().fromJson(JsonUtil.getJsonObject(data).optString("info"), ExpenseItemDetail.class);
        refreshView();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_detail)) {
                handleDetail(data);
            }
        }
    }

    @Override
    public void onRefresh() {
        requestDetail();
    }
}
