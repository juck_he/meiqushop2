package cn.meiqu.mich.activity.shopad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.MediaGridViewAdapter;
import cn.meiqu.mich.activity.shopad.image.ActivityImage;
import cn.meiqu.mich.activity.shopad.udp.Udp;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.BitScreenData;
import cn.meiqu.mich.bean.CompressBitmap;
import cn.meiqu.mich.bean.ImageAd;
import cn.meiqu.mich.bean.ImagePath;
import cn.meiqu.mich.common.MediaChooser;
import cn.meiqu.mich.httpGet.HttpGetBase;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.BitmapCompressUtils;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/11/25.
 */
public class FragmentImage extends BaseFragment {
    private GridView mGridImage;
    private Context mContext;
    private MediaGridViewAdapter adapter;
    private List<ImageAd> filePathList;
    private List<ImageAd> file;
    private int isFrist = 1;
    private ImagePath imagePath;
    //
    private ImagePath pathImage;
    private Udp mUdp;
    private TimerTask timerTask;
    private long currentTime;
    private boolean isRec = false;
    private List<String> listFileNames;
    private String className = getClass().getName();
    private String action_setUpdataPhoto = className + ":10003/uploadAdImage";
    //
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                dismissProgressDialog();
                ToastUtil.show(mContext, "手机和大屏不在同一个WiFi下");
            }
        }
    };

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_setUpdataPhoto)) {
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "图片上传失败");
            } else {
                ToastUtil.show(mContext, "图片上传成功");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_setUpdataPhoto});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_shopad, null);
            assignViews();
        }
        mContext = getActivity();
        IntentFilter imageIntentFilter = new IntentFilter(MediaChooser.IMAGE_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
        getActivity().registerReceiver(imageBroadcastReceiver, imageIntentFilter);
        // 初始化上次加载出来的图片
        String imagePath = PrefUtils.getImagePath(mContext);
        pathImage = new Gson().fromJson(imagePath, ImagePath.class);
        //
        if (pathImage != null) {
        } else {
            // 打开选择图片的界面
            Intent intentGetImage = new Intent(mContext, ActivityImage.class);
            intentGetImage.putExtra("photoConut", 0);
            startActivity(intentGetImage);
        }
        return contain;
    }

    private void assignViews() {
        initTitle("广告制作");
        mGridImage = (GridView) findViewById(R.id.grid_image);
        filePathList = new ArrayList<ImageAd>();
        filePathList.add(new ImageAd("isGetPhoto", true));
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new MediaGridViewAdapter(mContext, 0, filePathList);
        mGridImage.setAdapter(adapter);
        //把上一次的图片的加载出来
        if (pathImage != null) {
            List<ImageAd> iamgeFile = new ArrayList<ImageAd>();
            for (int i = 0; i < pathImage.imagePath.size(); i++) {
                iamgeFile.add(new ImageAd(pathImage.imagePath.get(i), false));
            }
            adapter.addAll(iamgeFile);
        }
        //
        mGridImage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean isCanAddPhtoto = filePathList.get(position).isCanAddPhtoto;
                if (isCanAddPhtoto) {
                    Intent intentGetImage = new Intent(mContext, ActivityImage.class);
                    intentGetImage.putExtra("photoConut", 0);
                    startActivity(intentGetImage);
                }
            }
        });
        setTitleRight("上传", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (filePathList.size() > 1) {
                            showProgressDialog("");
                            LogUtil.log("---------发送数据");
                            mUdp = Udp.newInstace(9001);
                            mUdp.isReceviceOk = false;
                            mUdp.setOnReceiveListen(new Udp.OnReceiveListen() {
                                @Override
                                public void getReceive(String data, String ip) {
                                    isRec = true;
                                    //判断是不是同一个WiFi
                                    LogUtil.log("------本地IP：" + getLocalIpAddress(BaseApp.mContext) + "--------服务ip" + ip);
                                    if (getLocalIpAddress(BaseApp.mContext).equals(ip)) {
                                    } else {
                                        LogUtil.log("--------data" + data);
                                        BitScreenData bitScreenData = new Gson().fromJson(data, BitScreenData.class);
                                        if (!TextUtils.isEmpty(bitScreenData.device_id)) {
                                            if (bitScreenData.currentTime != currentTime) {
                                                mUdp.isReceviceOk = true;
                                                mUdp.closeUdp();
                                                currentTime = bitScreenData.currentTime;
                                                //
                                                LogUtil.log("--------发达数据");
                                                HttpGetController.getInstance().setUpdataPhotoAd(ip, getPhotoList(), className);
                                            }
                                        }
                                    }
                                }
                            });
                            isFrist = 1;
                            timerTask = new TimerTask() {
                                @Override
                                public void run() {
                                    LogUtil.log("-------------运行一次");
                                    if (isRec) {
                                        //
                                        if (mUdp.isReceviceOk) {
                                            timerTask.cancel();
                                            LogUtil.log("---------验证成功");
                                            mUdp.closeUdp();
                                        } else {
                                            //再发一次UDP
                                            if (isFrist == 1) {
                                                isFrist = 2;
                                                LogUtil.log("-------在次发送UDP数据A");
                                                mUdp.toRunUdp();
                                            } else {
                                                timerTask.cancel();
                                                Message message = Message.obtain();
                                                message.what = 1;
                                                mHandler.sendMessage(message);
                                                mUdp.closeUdp();
                                                //ToastUtil.show(mContext, "不在同一个WiFi下");
                                            }
                                        }
                                    } else {
                                        //再发一次UDP
                                        if (isFrist == 1) {
                                            isFrist = 2;
                                            LogUtil.log("-------在次发送UDP数据B");
                                            mUdp.toRunUdp();
                                        } else {
                                            timerTask.cancel();
                                            Message message = Message.obtain();
                                            message.what = 1;
                                            mHandler.sendMessage(message);
                                            mUdp.closeUdp();
                                        }
                                    }
                                }
                            };
                            Timer timer = new Timer();
                            timer.schedule(timerTask, 3000, 3000);
                            //
                            LogUtil.log("--------第一次发送udp");
                            mUdp.toRunUdp();
                        } else {
                            ToastUtil.show(mContext, "至少要选择一张图片!");
                        }
                    }
                }
        );
    }


    /**
     * 获取到图片的广播
     */
    BroadcastReceiver imageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtil.log("------Image SIZE" + intent.getStringArrayListExtra("list").size());
            ArrayList<String> list = intent.getStringArrayListExtra("list");
            file = new ArrayList<ImageAd>();
            file.clear();
            for (int i = 0; i < list.size(); i++) {
                file.add(new ImageAd(list.get(i), false));
            }
            setAdapter(file);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (mUdp != null) {
            mUdp.closeUdp();
        }
        HttpGetBase.newInstance().disConnect();
        getActivity().unregisterReceiver(imageBroadcastReceiver);
    }

    private void setAdapter(List<ImageAd> filePathList) {
        adapter.addAll(filePathList);
        adapter.notifyDataSetChanged();
    }

    /**
     * 获取文件的路径,得到的时候压缩之后的图片
     *
     * @return
     */
    public List<String> getPhotoList() {
        listFileNames = new ArrayList<String>();
        List<String> imageFile = new ArrayList<String>();
        imagePath = new ImagePath();
        //拿到图片的路径
        for (int j = 0; j < filePathList.size(); j++) {
            if (filePathList.get(j).filePathList.equals("isGetPhoto")) {
            } else {
                listFileNames.add(filePathList.get(j).filePathList);
            }
        }
//        for (int i = 0; i < listFileNames.size(); i++) {
//            LogUtil.log("--------文件名1：" + listFileNames.get(i));
//        }
        //保存图片
        imagePath.imagePath = listFileNames;
        String jsonPath = new Gson().toJson(imagePath);
        PrefUtils.setImagePath(mContext, jsonPath);
        //进行压缩图片
        for (int i = 0; i < imagePath.imagePath.size(); i++) {
            //LogUtil.log("----------传递进来的文件的路径" + imagePath.imagePath.get(i));
            CompressBitmap bitmap = BitmapCompressUtils.compressForPath(imagePath.imagePath.get(i));
            String compressPatn = BitmapCompressUtils.toSaveImage(bitmap.mbitmap, bitmap.filepath);
            LogUtil.log("----------压缩之后文件名：" + compressPatn);
            LogUtil.log("----------压缩之后图片的大小:" + ImageUtil.getImageSize(new File(compressPatn)));
            imageFile.add(compressPatn);
        }
        return imageFile;
    }


    public void disConnect() {
        if (mUdp != null) {
            mUdp.closeUdp();
        }
        HttpGetBase.newInstance().disConnect();
    }

    /**
     * 获取当前ip地址
     *
     * @param context
     * @return
     */
    public static String getLocalIpAddress(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int i = wifiInfo.getIpAddress();
            return int2ip(i);
        } catch (Exception ex) {
            return " 获取IP出错鸟!!!!请保证是WIFI,或者请重新打开网络!\n" + ex.getMessage();
        }
    }

    /**
     * 将ip的整数形式转换成ip形式
     *
     * @param ipInt
     * @return
     */
    public static String int2ip(int ipInt) {
        StringBuilder sb = new StringBuilder();
        sb.append(ipInt & 0xFF).append(".");
        sb.append((ipInt >> 8) & 0xFF).append(".");
        sb.append((ipInt >> 16) & 0xFF).append(".");
        sb.append((ipInt >> 24) & 0xFF);
        return sb.toString();
    }
}
