package cn.meiqu.mich.activity.me.membermanger.history;


import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/10/10.
 */
public class WebViewActivity extends BaseActivity {
    public String url;

    @Override
    public void onHttpHandle(String action, String data) {

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_datail);
        initFragment();
        url = getIntent().getStringExtra("url");
    }

    @Override
    public void initFragment() {
        setContainerId(containerId);
        FragmentShowTextDetail fragmentShowTextDetail = FragmentShowTextDetail.newInstance();
        showFirst(fragmentShowTextDetail);
    }



}
