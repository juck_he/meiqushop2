package cn.meiqu.mich.activity.me.checkpay;


import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/9/28.
 */
public class PayTestActivity extends BaseActivity {
    private FragmentChenckPay fragmentChenckPay;
    @Override
    public void onHttpHandle(String action, String data) {

    }


    @Override
    public void initFragment() {
        setContainerId(containerId);
        fragmentChenckPay = FragmentChenckPay.newInstance();
        showFirst(fragmentChenckPay);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkpay);
        initFragment();
    }

    public void showHistory() {
        showAndPop(new FragmentHistory());
    }
}
