package cn.meiqu.mich.activity.me.checkpay;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.RecycleAdapterDate;
import cn.meiqu.mich.activity.adapter.RecycleHistory;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.DateInfo;
import cn.meiqu.mich.bean.HistoryInfo;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;

/**
 * Created by Administrator on 2015/9/29.
 */
public class FragmentHistory extends BaseFragment implements View.OnClickListener {
    private RecyclerView mRvDatashow;
    private TextView mTvHistoryDate;
    private TextView mTvHistoryCount;
    private RecyclerView mRvShowhistory;
    private Context mCOntext;
    private RecycleAdapterDate mRecycleAdapterDate;
    private String dateMonths[];
    private String dateWeeks[];
    private int currentPosition = 29;
    String className = getClass().getName();
    String action_getCheckUseHistory = className + API.getCheckUseHistory;
    private List<DateInfo> listDate;
    private List<String> listDateOrg;
    private HistoryInfo historyInfo;//获取到的历史信息

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mCOntext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_history_check, null);
            assignViews();
        }
        return contain;
    }

    public void initReceiver() {
        initReceiver(new String[]{action_getCheckUseHistory});
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        //第一个recycle view的设置
        mRvDatashow.setHasFixedSize(true);
        mRvDatashow.setVerticalScrollBarEnabled(false);
        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mCOntext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRvDatashow.setLayoutManager(linearLayoutManager);
        //第二个recycle view的设置
        mRvShowhistory.setHasFixedSize(true);
        mRvShowhistory.setVerticalScrollBarEnabled(false);
        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
        LinearLayoutManager linearLayout = new LinearLayoutManager(mCOntext);
        linearLayout.setOrientation(LinearLayoutManager.VERTICAL);
        mRvShowhistory.setLayoutManager(linearLayout);


        mRecycleAdapterDate = new RecycleAdapterDate(mCOntext, listDate);
        mRecycleAdapterDate.setOnItemClickLitener(new RecycleAdapterDate.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                LogUtil.log("------点击了--" + position);
//                //把上一次的给挂掉
//                LinearLayoutManager manager= (LinearLayoutManager) mRvDatashow.getLayoutManager();
//                int firsPosition= manager.findFirstCompletelyVisibleItemPosition();
//                LogUtil.log("currentPosition-firsPosition:"+(currentPosition-firsPosition));
//                View childAt =manager.findViewByPosition(currentPosition);
//                childAt.setSelected(false);
//                //把现在的给选中
//                currentPosition = position;
//                view.setSelected(true);
                if (position == currentPosition) {
                    return;
                }
                showToday(position);
                listDate.get(position).state = 1;
                mRecycleAdapterDate.notifyItemChanged(position);
                listDate.get(currentPosition).state = 0;
                mRecycleAdapterDate.notifyItemChanged(currentPosition);
                currentPosition = position;
                requestCheckPay(currentPosition);
            }
        });
        mRvDatashow.setAdapter(mRecycleAdapterDate);
        mRvDatashow.setSelected(true);
        mRvDatashow.setItemAnimator(new DefaultItemAnimator());

        //移动到第30item
        mRvDatashow.scrollToPosition(currentPosition);
        //选中第30个item
        listDate.get(currentPosition).state = 1;
        mRecycleAdapterDate.notifyItemChanged(currentPosition);
        //显示今天
        showToday(currentPosition);
        //发送Http请求查询今天验证了多少张
        requestCheckPay(currentPosition);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                LinearLayoutManager manager= (LinearLayoutManager) mRvDatashow.getLayoutManager();
//                int firsPosition= manager.findFirstCompletelyVisibleItemPosition();
//                LogUtil.log("currentPosition-firsPosition:"+(currentPosition-firsPosition));
//                View childAt =manager.findViewByPosition(currentPosition);
//                childAt.setSelected(true);
//            }
//        },0);

    }

    private void requestCheckPay(int currentPosition) {
        showProgressDialog("");
        String dateStr = listDateOrg.get(currentPosition);
        LogUtil.log("dateStr:" + dateStr);
        String[] split = dateStr.split("\\.");
        String time = split[0] + "-" + split[1] + "-" + split[2];
        LogUtil.log("-----打印提交的时间：" + time);
        HttpGetController.getInstance().getCheckUseHistory(PrefUtils.getUser(mCOntext).token, "1", "10", time, className);

    }


    /**
     * 显示具体那一天
     *
     * @param currentPosition
     */
    private void showToday(int currentPosition) {
        String dateStr = listDateOrg.get(currentPosition);
        LogUtil.log("dateStr:" + dateStr);
        String[] split = dateStr.split("\\.");
        LogUtil.log("dateStr:" + split.length);
        mTvHistoryDate.setText(split[0] + "-" + split[1] + "-" + split[2]);
    }

    private void assignViews() {
        initTitle("验证历史");
        listDate = new ArrayList<DateInfo>();
        listDateOrg = new ArrayList<String>();
        getToDay();
        mRvDatashow = (RecyclerView) findViewById(R.id.rv_datashow);
        mTvHistoryDate = (TextView) findViewById(R.id.tv_history_date);
        mTvHistoryCount = (TextView) findViewById(R.id.tv_history_count);
        mRvShowhistory = (RecyclerView) findViewById(R.id.rv_showhistory);
        mTvHistoryCount.setOnClickListener(this);
    }


    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getCheckUseHistory)) {
            dismissProgressDialog();
            if (data != null) {
                handleHistory(data);
                LogUtil.log("-------打印历史信息" + data);
            } else {
                toast("网络连接失败...");
            }
        }
    }

    private void handleHistory(String data) {
        Gson gson = new Gson();
        historyInfo = gson.fromJson(data, HistoryInfo.class);
        //显示今天验证了多少条历史信息
        mTvHistoryCount.setText("当天验证了" + historyInfo.info.list.size() + "张");
        if (historyInfo.info.list.size() > 0) {
            dismissMessage();
        } else {
            showNoMessage("没有内容哦!");
        }
        //创建一个recycleViewadapter
        RecycleHistory adapter = new RecycleHistory(mCOntext, historyInfo.info.list);
        //显示这些数据
        mRvShowhistory.setAdapter(adapter);
    }

    /**
     * 获取今天的具体时间
     */
    public void getToDay() {
//        Calendar c = Calendar.getInstance();
//        String[] mons = {"一月","二月","三月","四月"
//                ,"五月","六月","七月","八月"
//                ,"九月","十月","十一月","十二月"};

        String[] weeks = {
                "", "一", "二", "三", "四", "五", "六", "日"
        };
//        int indexMONTH = c.get(Calendar.MONTH);

//        int indexWEEK = c.get(Calendar.DAY_OF_WEEK);
//
//        LogUtil.log(c.get(Calendar.YEAR) + "年");
//        LogUtil.log((c.get(Calendar.MONTH)+1)+"月");
//       //LogUtil.log(mons[indexMONTH]);
//        LogUtil.log(c.get(Calendar.DAY_OF_MONTH) + "日");
//        //LogUtil.log("星期"+c.get(Calendar.DAY_OF_WEEK));
//        LogUtil.log(weeks[indexWEEK]);

        Calendar c = Calendar.getInstance();
        String[] months = new String[30];
        int[] weekNumbers = new int[30];
        StringBuilder date = new StringBuilder();
        String firstday = "";
        String lastday = "";
        for (int i = 0; i < 30; i++) {
            months[i] = new SimpleDateFormat("yyyy.MM.dd").format(new Date(
                    c.getTimeInMillis()));
            c.add(Calendar.DAY_OF_MONTH, -1);
            weekNumbers[i] = c.get(Calendar.DAY_OF_WEEK);
            Log.i("AAAAAAAA", "" + c.get(Calendar.DAY_OF_WEEK));
            if (i == 0) {
                lastday = months[i];
            }
            if (i == months.length - 1) {
                firstday = months[i];
            }
        }

        date.append(firstday);
        date.append("-");
        date.append(lastday);

        LogUtil.log("today---is:" + date);

        dateMonths = new String[30];
        dateWeeks = new String[30];

        for (int i = 0; i < 30; i++) {
            LogUtil.log("----" + months[i] + "-----" + weeks[weekNumbers[i]]);
            String month = months[29 - i];
            listDateOrg.add(month);
            int position = month.lastIndexOf(".");
            dateMonths[i] = month.substring(position + 1, month.length());
            dateWeeks[i] = weeks[weekNumbers[29 - i]];
            LogUtil.log("----A" + dateMonths[i] + "-----" + dateWeeks[i]);
            listDate.add(new DateInfo(dateMonths[i], dateWeeks[i], 0));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_history_count:
                LogUtil.log("----------------------");
                break;
        }
    }
}
