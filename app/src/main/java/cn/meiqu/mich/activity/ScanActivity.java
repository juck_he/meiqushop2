package cn.meiqu.mich.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import cn.meiqu.mich.R;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.zxing.camera.CameraManager;
import cn.meiqu.mich.zxing.ui.ScanQRCodeLinkActivity;


public class ScanActivity extends ScanQRCodeLinkActivity {

    private final static String pre = "mich_action=";
    public final static String result = "result";
    public final static int requestCode = 211;
    private CameraManager cm;
    private static final int CHECL_INTERVAL = 20;

    private static final int START_ANIM = 1;
    private static final int STOP_ANIM = 2;
    /**
     * 是否权限被禁
     */
    private boolean isRejuest = false;
    private Handler animHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assignViews();
        initHandle();
    }

    public void assignViews() {
        initTitle("扫一扫");

    }

    public void initHandle() {
        animHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//            super.handleMessage(msg);
                cm = CameraManager.get();
                if (cm == null)
                    return;
                Rect frame = null;
                try {
                    frame = cm.getFramingRect1();
                    View view = findViewById(R.id.capture_scan_line);
                    //把矩形的长宽搞进来，设置为capture的大小
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    if (lp == null) {
                        lp = new RelativeLayout.LayoutParams(frame.width(), frame.height());
                    }
                    lp.width = frame.width();
                    lp.height = frame.height();
                    //设置margins的距离
                    lp.setMargins(frame.left, frame.top, 0, 0);
                    view.setLayoutParams(lp);
                    startSaoAnimation(view);
                } catch (Exception e) {//失败时再尝试
                    e.printStackTrace();
                    animHandler.sendEmptyMessageDelayed(1, CHECL_INTERVAL);
                }
            }
        };
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void initFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        //启动handler里面的设置
        animHandler.sendEmptyMessageDelayed(1, 50);
    }

    public void startSaoAnimation(View mQrLineView) {
        if (mQrLineView == null)
            return;
        ScaleAnimation animation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f);
        animation.setRepeatCount(-1);//重复无限次
        animation.setRepeatMode(Animation.RESTART);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(2000);
        mQrLineView.startAnimation(animation);
    }

    @Override
    protected void onError(Exception ioe) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("权限被拒绝")
                .setMessage("拍照权限被拒绝了,通过 设置 -> 应用 -> 美渠店长 中设置允许拍照.")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(10);
                        finish();
                    }
                })
                .create();
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                finish();
                return true;
            }
        });
        dialog.show();
    }


    String ipString;
    String dev_id;

    @Override
    protected boolean onHandler(String resultText) {
//        if (resultText.contains("mich_action=")) {
//            Map<String, String> param = StringUtil.getParam(resultText);
//            if (param.get("mich_action").equals("member")) {
//                String open_id = param.get("open_id");
////                {
////                    HttpGetController.getInstance().addFriend(open_id, getClass().getName());
////                    ToastUtil.show(this, "好友添加申请已经发送");
////                }
////                Intent intent = new Intent(this, ChatActivity.class);
////                intent.putExtra(ChatActivity.EXTRA_ACTION, ChatActivity.ACTION_SHOWFRIENDINFO);
////                intent.putExtra("openid", open_id);
////                startActivity(intent);
////                StringUtil.getParam()
//            }
//        } else {
//            ToastUtil.show(this, "二维码格式错误");
//        }
        LogUtil.log("二维码内容＝" + resultText);
        Intent intent = new Intent();
        intent.putExtra(result, resultText);
        setResult(RESULT_OK, intent);
        finish();
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do something...
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.abc_fade_out);
    }


}
