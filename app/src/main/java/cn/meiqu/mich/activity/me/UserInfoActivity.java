package cn.meiqu.mich.activity.me;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.Fatel.utils.ImageLoadHelper;

import java.io.File;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.User;
import cn.meiqu.mich.dialogs.SelectImageDialog;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.FileUtil;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.UploadUtil;
import cn.meiqu.mich.view.CircleImageView;

public class UserInfoActivity extends BaseActivity implements View.OnClickListener {
    String className = getClass().getName();
    String action_edtInfo = className + API.edtUserInfo;
    private CircleImageView mIvLogo;
    private EditText mEtStoreName;
    private EditText mEtTel;
    private EditText mEtAddress;

    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
        assignViews();
        initReceiver(new String[]{action_edtInfo});
    }

    private void assignViews() {
        initTitle("修改资料");
        setTitleRight("保存", this);
        mUser = PrefUtils.getUser(this);
        mIvLogo = (CircleImageView) findViewById(R.id.iv_logo);
        mEtStoreName = (EditText) findViewById(R.id.et_store_name);
        mEtTel = (EditText) findViewById(R.id.et_tel);
        mEtAddress = (EditText) findViewById(R.id.et_address);
        //Glide.with(this).load(mUser.company_logo).into(mIvLogo);
        ImageLoadHelper.displayImage(mUser.company_logo,mIvLogo);
        mEtStoreName.setText(mUser.company_name);
        mEtTel.setText(mUser.telephone);
        mEtAddress.setText(mUser.address);
        mIvLogo.setOnClickListener(this);
    }

    @Override
    public void initFragment() {

    }

    public void requestEdit() {
        mUser.company_name = mEtStoreName.getText().toString().trim();
        mUser.address = mEtAddress.getText().toString().trim();
        mUser.telephone = mEtTel.getText().toString().trim();
        showProgressDialog("");
        if (!mUser.company_logo.startsWith("http") && !StringUtil.isEmpty(mUser.company_logo)) {
            new UploadUtil(this, new UploadUtil.QiuNiuUpLoadListener() {
                @Override
                public void onUpLoadSucceed(String url) {
                    mUser.company_logo = url;
                    HttpGetController.getInstance().edtUserInfo(mUser, className);
                }

                @Override
                public void onUpLoadFailure(String erroMessage) {
                    toast(erroMessage);
                    dismissProgressDialog();
                }
            }).upLoad(mUser.company_logo);
        } else {
            HttpGetController.getInstance().edtUserInfo(mUser, className);
        }
    }

    public void handleEdit(String data) {
        toast("修改成功");
        PrefUtils.setUser(this, mUser);
        finish();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_edtInfo)) {
                handleEdit(data);
            }
        } else {
            toast("修改失败");
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_title_more) {
            requestEdit();
        } else if (v.getId() == mIvLogo.getId()) {
            new SelectImageDialog(this).show();
        }
    }

    String imgPath = "";

    @Override
    public void onSelectImageReuslt(String path) {
        imgPath = path;
        mUser.company_logo = imgPath;
        //Glide.with(this).load(path).into(mIvLogo);
        ImageLoadHelper.displayImage(path,mIvLogo);
        Intent intent = new Intent();
        intent.setAction("com.android.camera.action.CROP");
        intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");// mUri是已经选择的图片Uri
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);// 裁剪框比例
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);// 输出图片大小
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 200);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 200) {
                Bitmap bitmap = data.getParcelableExtra("data");
                imgPath = imgPath + "1";
                mUser.company_logo = imgPath;
                FileUtil.writeBitmap(imgPath, ImageUtil.bitmapToByte(bitmap));
                //Glide.with(this).load(imgPath).into(mIvLogo);
                ImageLoadHelper.displayImage(imgPath,mIvLogo);
            }
        }
    }

}
