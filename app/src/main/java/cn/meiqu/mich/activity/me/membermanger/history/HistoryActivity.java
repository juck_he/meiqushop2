package cn.meiqu.mich.activity.me.membermanger.history;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/10/10.
 */
public class HistoryActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mImgVTitleBack;
    private TextView mTvTitle;
    private LinearLayout mLlTextpush;
    private LinearLayout mLlCoupohpush;
    private FrameLayout mFrameFragment;


    public FragmentTextPush fragmentTextPush;
    public FragmentCouponPush fragmentCouponPush;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void initFragment() {
        setContainerId(containerId);
        fragmentTextPush = FragmentTextPush.newInstance();
        fragmentCouponPush = FragmentCouponPush.newInstance();
        showFirst(fragmentTextPush);
        mLlTextpush.setSelected(true);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_history);
        assignViews();
        initData();
        initFragment();
        mLlTextpush.setSelected(true);
    }

    private void initData() {
        mImgVTitleBack.setOnClickListener(this);
        mLlTextpush.setOnClickListener(this);
        mLlCoupohpush.setOnClickListener(this);
    }


    private void assignViews() {
        mImgVTitleBack = (ImageView) findViewById(R.id.imgV_title_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mLlTextpush = (LinearLayout) findViewById(R.id.ll_textpush);
        mLlCoupohpush = (LinearLayout) findViewById(R.id.ll_coupohpush);
        mFrameFragment = (FrameLayout) findViewById(R.id.frame_fragment);
    }

//    public void getShowWebView(){
//        FragmentShowTextDetail fragmentShowTextDetail = FragmentShowTextDetail.newInstance();
//        showAndPop(fragmentShowTextDetail);
//    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgV_title_back:
                finish();
                break;
            case R.id.ll_textpush:
                //TODO 显示文字推送的Fragment
                mLlCoupohpush.setSelected(false);
                mLlTextpush.setSelected(true);
                showNoPop(fragmentTextPush);
                break;
            case R.id.ll_coupohpush:
                //TODO 显示优惠券推送Fragment
                mLlCoupohpush.setSelected(true);
                mLlTextpush.setSelected(false);
                showNoPop(fragmentCouponPush);
                break;
        }
    }
}
