package cn.meiqu.mich.activity.me.device;

import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.SSIDListAdapter;
import cn.meiqu.mich.activity.wifi.WifiAdmin;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.ToastUtil;
import cn.meiqu.mich.util.WifiUtil;


/**
 * Created by Fatel on 15-4-9.
 */
public class FragmentWIfiSetting extends BaseFragment implements View.OnClickListener {
    String className = getClass().getName();
    String action_setWifi = className + API.set_wifi_info;

    private EditText mEtUsername;
    private TextView mLabel1;
    private TextView mLabel2;
    private EditText mEtPwd;
    private EditText edtWifi1, edtWifi2, edtPwd1, edtPwd2;
    private WifiAdmin mWifiAdmin;
    private List<ScanResult> listInfo;
    private TextView mTvShowSsid;
    private TextView mTvShowSsid1;
    private TextView mTvShowSsid2;

    private void assignViews() {
        initTitle("设置Wi-Fi");
        setTitleRight("保存", this);
        mEtUsername = (EditText) findViewById(R.id.et_username);
        mLabel1 = (TextView) findViewById(R.id.label_1);

        mLabel2 = (TextView) findViewById(R.id.label_2);
        mEtPwd = (EditText) findViewById(R.id.et_pwd);
        edtWifi1 = (EditText) findViewById(R.id.et_username2);
        edtWifi2 = (EditText) findViewById(R.id.et_username3);
        edtPwd1 = (EditText) findViewById(R.id.et_pwd2);
        edtPwd2 = (EditText) findViewById(R.id.et_pwd3);
        mTvShowSsid = (TextView) findViewById(R.id.tv_show_ssid);
        mTvShowSsid1 = (TextView) findViewById(R.id.tv_show_ssid1);
        mTvShowSsid2 = (TextView) findViewById(R.id.tv_show_ssid2);
        getWifiList();
        mTvShowSsid.setOnClickListener(this);
        mTvShowSsid1.setOnClickListener(this);
        mTvShowSsid2.setOnClickListener(this);
        String ssid = WifiUtil.getCurrentWifiName();
        mEtUsername.setText(ssid);
    }

    public void instanceEdt() {
        if (!StringUtil.isEmpty(SettingDao.getInstance().getWifiSSid()))
            mEtUsername.setText(SettingDao.getInstance().getWifiSSid());
        mEtPwd.setText(SettingDao.getInstance().getWifiPwd());
        edtWifi1.setText(SettingDao.getInstance().getWifiSSid1());
        edtWifi2.setText(SettingDao.getInstance().getWifiSSid2());
        edtPwd1.setText(SettingDao.getInstance().getWifiPwd1());
        edtPwd2.setText(SettingDao.getInstance().getWifiPwd2());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_wifisetting, null);
            assignViews();
        } else {
            ((ViewGroup) contain.getParent()).removeView(contain);
        }
        instanceEdt();
        initReceiver();
        return contain;
    }

    //获取到WiFi
    public void getWifiList() {
        mWifiAdmin = new WifiAdmin(getActivity());
        listInfo = mWifiAdmin.getScanResultList();
        if (listInfo.size()>0) {
            for (int i = 0; i < listInfo.size(); i++) {
                LogUtil.log("--------ScanResult：第" + i + "个" + listInfo.get(i).SSID);
            }
        }
    }

    public void initReceiver() {
        initReceiver(new String[]{action_setWifi});
    }

    public void saveWifiData() {
        SettingDao.getInstance().setWifiSSid(ssid);
        SettingDao.getInstance().setWifiSSid1(ssid1);
        SettingDao.getInstance().setWifiSSid2(ssid2);
        SettingDao.getInstance().setWifiPwd(pwd);
        SettingDao.getInstance().setWifiPwd1(pwd1);
        SettingDao.getInstance().setWifiPwd2(pwd2);
    }

    String ssid;
    String pwd;
    String ssid1;
    String pwd1;
    String ssid2;
    String pwd2;

    public void requestWifi() {
        ssid = mEtUsername.getText().toString();
        pwd = mEtPwd.getText().toString();
        ssid1 = edtWifi1.getText().toString();
        pwd1 = edtPwd1.getText().toString();
        ssid2 = edtWifi2.getText().toString();
        pwd2 = edtPwd2.getText().toString();
        if (StringUtil.isEmpty(ssid)) {
            toast("wifi名字不能为空");
            return;
        }
        showProgressDialog("");
        HttpGetController.getInstance().setCompanyWiFi(getClass(), ssid, pwd, ssid1, pwd1, ssid2, pwd2);
    }

    public void handleWifi(String data) {
        dismissProgressDialog();
        if (data == null) {
            ToastUtil.showNetWorkFailure(getActivity());
        } else {
            if (JsonUtil.getStatusLegal(data)) {
                saveWifiData();
                showWifiSettingDialog();
            } else {
                toast("操作失败");
            }
        }
    }


    public void showWifiSettingDialog() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle("设置成功").setMessage("现在去设置美屏Wifi连接").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((DeviceActivity) getActivity()).showDeviceList();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((BaseActivity) getActivity()).popBack();
            }
        }).create();
        dialog.show();

    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_setWifi)) {
            handleWifi(data);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_title_more) {
            requestWifi();
        }else if(v.getId() == R.id.tv_show_ssid){
            //显示WiFi列表
            showWifiListDialog(1);
        }else if(v.getId() == R.id.tv_show_ssid1){
            //显示WiFi列表
            showWifiListDialog(2);
        }else if(v.getId() == R.id.tv_show_ssid2){
            //显示WiFi列表
            showWifiListDialog(3);
        }
    }

    AlertDialog.Builder builder = null;
    private void showWifiListDialog(final int setNumber) {
        if (builder == null) {
            builder = new AlertDialog.Builder(getActivity(), R.style.myDialog);
        }
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.view_ssid_list, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 800);

        ListView lvSsidList = (ListView) viewDialog.findViewById(R.id.lv_ssid_list);
        lvSsidList.setLayoutParams(layoutParams);
        lvSsidList.setAdapter(new SSIDListAdapter(getActivity(),listInfo));
        viewDialog.measure(0,0);
        builder.setView(viewDialog, 0, 0, 0, 0);
        final AlertDialog alertDialog = builder.create();
        if (alertDialog.isShowing()) {
            return;
        }
        alertDialog.show();
        lvSsidList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (setNumber == 1) {
                    mEtUsername.setText(listInfo.get(position).SSID);
                }else if(setNumber == 2){
                    edtWifi1.setText(listInfo.get(position).SSID);
                }else if(setNumber == 3){
                    edtWifi2.setText(listInfo.get(position).SSID);
                }

                alertDialog.dismiss();
            }
        });
    }


}
