package cn.meiqu.mich.activity.me.expense;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.adapter.RecycleExpendItemAdapter;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.Expense;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.JsonUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.view.superrecyclerview.OnMoreListener;
import cn.meiqu.mich.view.superrecyclerview.SuperRecyclerView;

/**
 * Created by Fatel on 15-9-29.
 */
public class FragmentExpenseItem extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener, RecycleExpendItemAdapter.OnRecycleClickListener {
    String className = getClass().getName();
    String action_getList = className + API.getPayItemList;
    int page = 1;
    int per_page = 20;
    private SuperRecyclerView mRecycleV;
    ArrayList<Expense.PayList> payLists = new ArrayList<>();
    RecycleExpendItemAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver(new String[]{action_getList});
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_expense_item, null);
            assignViews();
            requestList(page, per_page);
        }
        return contain;
    }

    private void assignViews() {
        initTitle("项目列表");
        mRecycleV = (SuperRecyclerView) findViewById(R.id.recycleV);
        mRecycleV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecycleV.setRefreshListener(this);
        mRecycleV.setupMoreListener(this, 1);
        adapter = new RecycleExpendItemAdapter(getActivity(), payLists);
        adapter.setOnRecycleClickListener(this);
        mRecycleV.setAdapter(adapter);
    }

    public void requestList(int page, int per_page) {
        showProgressDialog("");
        HttpGetController.getInstance().getExpenseItemList(page, per_page, className);
    }

    public void handleList(String data) {
        ArrayList<Expense.PayList> temps = new Gson().fromJson(JsonUtil.getJsonObject(data).optJSONObject("info").optString("list"), new TypeToken<ArrayList<Expense.PayList>>() {
        }.getType());
        if (page == 1) {
            payLists.clear();
        }
        payLists.addAll(temps);
        adapter.notifyDataSetChanged();
        if (temps.isEmpty() || temps.size() < per_page) {
            mRecycleV.removeMoreListener();
        }
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getList)) {
                handleList(data);
            }
        }
    }

    @Override
    public void onRefresh() {
        page = 1;
        mRecycleV.setupMoreListener(this, 1);
        showProgressDialog("");
        requestList(page, per_page);
    }

    @Override
    public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
        page++;
        requestList(page, per_page);
    }

    @Override
    public void onRecycleClick(int position) {
        LogUtil.log("onRecycleClick=" + position);
        ((ExpenseActivity) getActivity()).showItemDetail(payLists.get(position).coupon_id);
    }
}
