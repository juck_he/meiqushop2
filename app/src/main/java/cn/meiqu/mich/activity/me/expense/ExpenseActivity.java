package cn.meiqu.mich.activity.me.expense;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

public class ExpenseActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);
        initFragment();
    }

    @Override
    public void initFragment() {
        showFirst(new FragmentExpense());
    }

    public void showItem() {
        showAndPop(new FragmentExpenseItem());
    }

    public void showItemDetail(String coupon_id) {
        FragmentExpenseItemDetail.coupon_id = coupon_id;
        showAndPop(new FragmentExpenseItemDetail());
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

}
