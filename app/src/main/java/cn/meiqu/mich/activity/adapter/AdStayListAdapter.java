package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.Fatel.utils.ImageLoadHelper;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.List;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.TemplateList;
import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by Administrator on 2015/11/10.
 */
public class AdStayListAdapter extends RecyclerView.Adapter<AdStayListAdapter.MyViewHolderDate> {
    // private int[] images = new int[]{R.drawable.moban1,R.drawable.moban2,R.drawable.moban3,R.drawable.moban4,R.drawable.moban5,R.drawable.moban6};
    private Context mContext;
    private List<TemplateList.InfoEntity.TemplateListEntity> list;

    public AdStayListAdapter(Context mCOntext, List<TemplateList.InfoEntity.TemplateListEntity> listDate) {
        this.mContext = mCOntext;
        this.list = listDate;
    }

    @Override
    public MyViewHolderDate onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolderDate(LayoutInflater.from(mContext).inflate(R.layout.item_template_list, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolderDate holder, int position) {
        holder.instanceListView(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
        //return images.length;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    class MyViewHolderDate extends RecyclerView.ViewHolder {
        private RelativeLayout mLlClickData;
        private ImageView mIvMoban;
        private ImageView mIvTip;
        private ImageView mSpinnerImageView;

        private void assignViews() {
            mLlClickData = (RelativeLayout) findViewById(R.id.ll_click_data);
            mIvMoban = (ImageView) findViewById(R.id.iv_moban);
            mIvTip = (ImageView) findViewById(R.id.iv_tip);
            mSpinnerImageView = (ImageView) findViewById(R.id.spinner);
        }

        public MyViewHolderDate(View itemView) {
            super(itemView);
        }

        public void instanceListView(final int position) {
            assignViews();
            //ImageLoadHelper.displayImage(list.get(position).show_img,mIvMoban);

            if (list.get(position).isSelect) {
                mIvTip.setVisibility(View.VISIBLE);
                mIvMoban.setColorFilter(Color.parseColor("#77000000"));
            } else {
                mIvTip.setVisibility(View.GONE);
                mIvMoban.setColorFilter(null);
            }
            ImageLoadHelper.displayImage(list.get(position).show_img,null,new MyImgeViewLoader(mSpinnerImageView,mIvMoban));
            mLlClickData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickLitener != null) {
                        mOnItemClickLitener.onItemClick(mLlClickData, position);
                    }
                }
            });
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ScreenUtil.dip2px(mContext, 290));
            if (position != list.size()-1) {
                lp.setMargins(0, 0, 0, 20);
            }
            itemView.setLayoutParams(lp);
        }

        public View findViewById(int id) {
            return itemView.findViewById(id);
        }

        class  MyImgeViewLoader implements ImageLoadingListener {
            private ImageView loaderImageView;
            private AnimationDrawable spinner;
            private ImageView dataView;

            public MyImgeViewLoader(ImageView view,ImageView data){
                this.loaderImageView = view;
                this.dataView = data;
            }
            @Override
            public void onLoadingStarted(String s, View view) {
                loaderImageView.setVisibility(View.VISIBLE);
                // 获取ImageView上的动画背景
                spinner = (AnimationDrawable) loaderImageView.getBackground();
                // 开始动画
                spinner.start();
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                spinner.stop();
                loaderImageView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                spinner.stop();
                loaderImageView.setVisibility(View.GONE);
                dataView.setImageBitmap(bitmap);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        }
    }
}