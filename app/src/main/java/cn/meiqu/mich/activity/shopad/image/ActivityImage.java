package cn.meiqu.mich.activity.shopad.image;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;

/**
 * Created by Administrator on 2015/11/26.
 */
public class ActivityImage extends BaseActivity {
    private FragmentImage mFragmentImage;
    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void initFragment() {
        mFragmentImage = new FragmentImage();
        showFirst(mFragmentImage);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        initFragment();
    }

}
