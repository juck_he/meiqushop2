package cn.meiqu.mich.activity.me.shopstore;

import android.os.Bundle;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.MubanHistory;

/**
 * Created by Administrator on 2015/10/19.
 */
public class ShopStoreActivity extends BaseActivity implements OnUpdataListener{

    private FragmentShopStore fragmentShopStoreA;
    private FragmentShopStore fragmentShopStore;
    private FragmentHistory fragmentHistory;
    //private FragmentHistory fragmentHistory
    @Override
    public void onHttpHandle(String action, String data) {

    }


    @Override
    public void initFragment() {
        setContainerId(containerId);
         fragmentShopStoreA = FragmentShopStore.newInstance();
        showFirst(fragmentShopStoreA);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopstroe);
        initFragment();
    }

    @Override
    public void getIdImageUrl(String id, String imageUrl) {
        fragmentShopStoreA.setIdImageUrl(id, imageUrl);
    }

    public void openHistoryMuban(){
        fragmentHistory = new FragmentHistory();
        showAndPop(fragmentHistory);
    }

    public void openEditPro(MubanHistory.InfoEntity.ListEntity listEntity){
        fragmentShopStore = new FragmentShopStore();
        fragmentShopStore.isEditStatic = true;
        fragmentShopStore.proId = listEntity.id;
        fragmentShopStore.templateId = listEntity.template_id;
        fragmentShopStore.imageUrl = listEntity.coupon_img;
        fragmentShopStore.title = listEntity.coupon_title;
        fragmentShopStore.price = listEntity.coupon_money;
        showAndPop(fragmentShopStore);
    }

    //刷新历史列表
    public void refHistoryData(){
        fragmentHistory.refHistoryList();
    }

}
