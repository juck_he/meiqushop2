package cn.meiqu.mich.activity.me.membermanger.history;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.List;

import cn.meiqu.mich.API;
import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.adapter.ListCuponPushHistoty;
import cn.meiqu.mich.basefatel.BaseFragment;
import cn.meiqu.mich.bean.CouponInfon;
import cn.meiqu.mich.httpGet.HttpGetController;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.PrefUtils;
import cn.meiqu.mich.util.ToastUtil;

/**
 * Created by Administrator on 2015/10/10.
 */
public class FragmentCouponPush extends BaseFragment {
    private Context mContext;
    private ListView mLvCouponPush;
    String className = getClass().getName();
    String action_getCouponForMySelf = className + API.getCouponForMySelf;
    private List<CouponInfon.InfoEntity.VoucherListEntity> list;

    @Override
    public void onHttpHandle(String action, String data) {
        if (action.equals(action_getCouponForMySelf)) {
            dismissProgressDialog();
            if (data == null) {
                ToastUtil.show(mContext, "网络请求失败,请检查网络是否正常...");
                return;
            }
            LogUtil.log("-----优惠券信息:" + data);
            handlerCouponHistoty(data);
        }
    }

    /**
     * 显示发布的优惠劵的信息
     *
     * @param data
     */
    private void handlerCouponHistoty(String data) {
        CouponInfon couponInfon = new Gson().fromJson(data, CouponInfon.class);
        list = couponInfon.info.voucher_list;
        if (list.size()>0) {
            dismissMessage();
        } else {
            showNoMessage("没有内容哦!");
        }
        mLvCouponPush.setAdapter(new ListCuponPushHistoty(mContext, list));

    }

    public static FragmentCouponPush newInstance() {
        FragmentCouponPush fragmentCouponPush = new FragmentCouponPush();
        return fragmentCouponPush;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        this.mContext = getActivity();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_coupon_push, null);

        }
        //TODO 发送Http请求查询已经发布的优惠券
        requestHttp();
        return contain;
    }

    /**
     * 获取已经发送的历史信息
     */
    private void requestHttp() {
        showProgressDialog("");
        String token = PrefUtils.getUser(mContext).token;
        HttpGetController.getInstance().getCouponForMySelf(token, null, null, null, null, className);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        assignViews();
    }

    private void initReceiver() {
        initReceiver(new String[]{action_getCouponForMySelf});
    }


    private void assignViews() {
        mLvCouponPush = (ListView) findViewById(R.id.lv_coupon_push);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }

    }
}
