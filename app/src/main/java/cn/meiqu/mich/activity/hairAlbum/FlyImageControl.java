package cn.meiqu.mich.activity.hairAlbum;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;

import com.Fatel.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yalantis.cameramodule.activity.CameraActivity;

import java.util.ArrayList;
import java.util.Map;

import cn.meiqu.mich.R;
import cn.meiqu.mich.activity.ScanActivity;
import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.basefatel.BaseActivity;
import cn.meiqu.mich.bean.AblumImage;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.tcp.TcpCommand;
import cn.meiqu.mich.tcp.TcpResponController;
import cn.meiqu.mich.tcp.TcpSendController;
import cn.meiqu.mich.tcp.TcpSocket;
import cn.meiqu.mich.util.ImageUtil;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.StringUtil;
import cn.meiqu.mich.util.WifiUtil;

/**
 * Created by Fatel on 15-10-9.
 */
public class FlyImageControl {


    //    public static String action_takePhoto = "action_takePhoto";
    public boolean isTakePhoto = false;
    public static final int requestScan = 998;

    public interface FlyImageListener {

        public void onConnectSucceed();

        public void onConnectFailured();

        public void onFlyImageSucceed();

        public void onFlyImageFailured();

        public void onFlyImageUpLoad(String name);


    }

    private FlyImageListener flyImageListener;
    static FlyImageControl imageControl;
    static Context mContext;

    private FlyImageControl() {
        initRunMusic(); //飞图声音
        initReceiver();
    }


    public static FlyImageControl getInstance() {
        if (imageControl == null) {
            mContext = BaseApp.mContext;
            imageControl = new FlyImageControl();
        }
        return imageControl;
    }

    public FlyImageListener getFlyImageListener() {
        return flyImageListener;
    }

    public void setFlyImageListener(FlyImageListener flyImageListener) {
        this.flyImageListener = flyImageListener;
    }

    public void initReceiver() {
        IntentFilter filter = new IntentFilter(TcpResponController.action_tcp_respon);
        filter.addAction(CameraActivity.ACTION_PHOTO_RETURN);
        filter.addAction(CameraActivity.ACTION_PHOTO_FINISH);
        filter.addAction(CameraActivity.ACTION_TAKE_PHOTO);
        LocalBroadcastManager.getInstance(BaseApp.mContext).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(TcpResponController.action_tcp_respon)) {
                    String begin = intent.getStringExtra(TcpResponController.data_begin);
                    String content = intent.getStringExtra(TcpResponController.data_content);
                    handleTcp(begin, content);
                } else if (intent.getAction().equals(CameraActivity.ACTION_PHOTO_RETURN)) {
                    Bitmap bitmap = intent.getBundleExtra("image").getParcelable("image");
                    handleTakePhoto(bitmap);
                } else if (intent.getAction().equals(CameraActivity.ACTION_PHOTO_FINISH)) {
                    leaveImage();
                } else if (intent.getAction().equals(CameraActivity.ACTION_TAKE_PHOTO)) {
                    String image_id = intent.getStringExtra("image_id");
                    String image_path = intent.getStringExtra("image_path");
                    handleImageSave(image_id, image_path);
                }

            }
        }, filter);
    }

    private void handleImageSave(String id, String path) {
        String data = SettingDao.getInstance().getHairImages();
        ArrayList<AblumImage> ablumImages = new ArrayList<>();
        if (!StringUtils.isEmpty(data)) {
            ArrayList<AblumImage> temps = new Gson().fromJson(data, new TypeToken<ArrayList<AblumImage>>() {
            }.getType());
            ablumImages.addAll(temps);
        }
        AblumImage ablumImage = new AblumImage();
        ablumImage.image_id = id;
        ablumImage.image_path = path;
        ablumImages.add(0, ablumImage);
        SettingDao.getInstance().setHairImages(new Gson().toJson(ablumImages));
    }

    public void handleScanSucceed(BaseActivity activity, String result) {
        Map<String, String> map = StringUtil.getParam(result);
        String ip = map.get("ip");
        String device_id = map.get("device_id");
        String ssid = map.get("ssid");
        if (StringUtil.isEmpty(device_id)) {
            activity.toast("二维码不正确");
        } else if (!StringUtil.isEmpty(ssid) && !ssid.equals(WifiUtil.getCurrentWifiName())) {
            activity.toast("美屏和你手机不在同Wifi下，请检查");
        } else if (StringUtil.isEmpty(ip)) {
            activity.toast("请检查美屏是否已连接wifi");
        } else {
            //activity.showProgressDialog("");
            LogUtil.log("---------正在连接:"+ip);
            TcpSocket.getInstance().connect(ip);
        }
    }

    public void gotoScan(BaseActivity activity) {
        activity.jumpForResult(ScanActivity.class, requestScan);
    }

    AlertDialog dialog;

    public void showConnectDialog(final BaseActivity activity) {
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new AlertDialog.Builder(activity).setMessage("是否现在去扫二维码控制?").setTitle("美屏未控制").setPositiveButton("马上去", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gotoScan(activity);
            }
        }).setNegativeButton("不去", null).create();
        dialog.show();
    }

    public void handleTcp(String begin, String content) {
        if (isTakePhoto) {
            if (begin.equals(TcpCommand.F_UploadImage)) {
                TcpSendController.getInstance().sendUpLoadImage(imageName, b);
            }
        } else {
            if (flyImageListener != null) {
                if (begin.equals(TcpCommand.F_ImageSucceed)) {
                    flyImageListener.onFlyImageSucceed();
                } else if (begin.equals(TcpCommand.F_UploadImage)) {
                    flyImageListener.onFlyImageUpLoad(content);
                } else if (begin.equals(TcpCommand.B_ResponFailure)) {
                    flyImageListener.onFlyImageFailured();
                } else if (begin.equals(TcpCommand.H_ConnectSucceed)) {
                    flyImageListener.onConnectSucceed();
                } else if (begin.equals(TcpCommand.H_ConnectFailure)) {
                    flyImageListener.onConnectFailured();
                }
            }
        }
    }

    Bitmap bitmap;
    byte[] b;
    String imageName;

    public void handleTakePhoto(Bitmap bitmap) {
//        this.bitmap = bitmap;
        LogUtil.log("----------即拍即飞");
        this.b = ImageUtil.compressImageByMaxSize(bitmap, 400);
        imageName = System.currentTimeMillis() + "";
        TcpSendController.getInstance().sendFeitu(imageName);
    }

    SoundPool soundPool;
    int soundId;

    public void initRunMusic() {
        if (soundPool == null) {
            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
            soundId = soundPool.load(mContext, R.raw.fei, 1);
        }
    }


    public void playRunMusic() {
        soundPool.play(soundId, 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void flyImage(String name) {
        TcpSendController.getInstance().sendFeitu(name);
    }

    public void flyImageWithSound(String name) {
        playRunMusic();
        flyImage(name);
    }

//    public void upLoadImage(String name, Bitmap b) {
//        TcpSendController.getInstance().sendUpLoadImage(name, b);
//    }

    public void upLoadImage(String name, byte[] b) {
        TcpSendController.getInstance().sendUpLoadImage(name, b);
    }

    public void leaveImage() {
        TcpSendController.getInstance().sendLevelImage();
    }

}
