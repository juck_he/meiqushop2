package cn.meiqu.mich.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.DeviceFilm;
import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/9/21.
 */
public class RecyclerAdapterMoveLiset extends RecyclerView.Adapter<RecyclerAdapterMoveLiset.MyViewHolder> {
    private Context mContext;
    public   ArrayList<DeviceFilm> deviceFilms;
    public RecyclerAdapterMoveLiset(Context mContext, ArrayList<DeviceFilm> deviceFilms){
        this.mContext =mContext;
        this.deviceFilms =deviceFilms;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.view_move_list, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.instanceListView(position);
    }

    @Override
    public int getItemCount() {
        return deviceFilms.size();
    }


    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

        // void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;

    }






    class MyViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout mRlPalyMovelist;
        private ImageView mIvBtnPaly;
        private  TextView mTvMoveName;

        private void assignViews() {
            mRlPalyMovelist = (RelativeLayout) findViewById(R.id.rl_paly_movelist);
            mIvBtnPaly = (ImageView) findViewById(R.id.iv_btn_paly);
            mTvMoveName = (TextView) findViewById(R.id.tv_move_name);
        }

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        public void instanceListView(int position) {
            assignViews();
            mTvMoveName.setText(deviceFilms.get(position).ad_name);
            if (mOnItemClickLitener != null) {
                //final int finalPosition = position;
                mRlPalyMovelist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickLitener.onItemClick(mRlPalyMovelist, getLayoutPosition());
                        LogUtil.log("getLayoutPosition()"+ getLayoutPosition());
                    }
                });
            }
        }

        public View findViewById(int id) {
            return itemView.findViewById(id);
        }

    }

}


