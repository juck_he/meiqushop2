package cn.meiqu.mich.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.meiqu.mich.R;
import cn.meiqu.mich.view.CircleImageView;

/**
 * Created by Administrator on 2015/9/16.
 */
public class MySelfActivity extends Activity implements View.OnClickListener {
    private CircleImageView mTvMyhead;
    private TextView mTvShopname;
    private LinearLayout mLlPayMyself;
    private LinearLayout mLlReceiptMyself;
    private LinearLayout mLlConsumeMyself;
    private LinearLayout mLlManageMyself;
    private LinearLayout mLlSetupMyself;
    private LinearLayout mLlListMyself;
    private RelativeLayout mLlBitscreenMyself;
    private RelativeLayout mLlHotlineMyself;
    private ImageView mIvBack;
    private TextView mTvTitle;
    private TextView mTvMost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myself);
        initView();
        initEven();
        initData();
    }

    private void initData() {
        mTvTitle.setText("我的");
        mTvMost.setVisibility(View.VISIBLE);
        mTvMost.setText("更多");
    }

    private void initEven() {
        mIvBack.setOnClickListener(this);
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMost = (TextView) findViewById(R.id.tv_most);
        mTvMyhead = (CircleImageView) findViewById(R.id.tv_myhead);
        mTvShopname = (TextView) findViewById(R.id.tv_shopname);
        mLlPayMyself = (LinearLayout) findViewById(R.id.ll_pay_myself);
        mLlReceiptMyself = (LinearLayout) findViewById(R.id.ll_receipt_myself);
        mLlConsumeMyself = (LinearLayout) findViewById(R.id.ll_consume_myself);
        mLlManageMyself = (LinearLayout) findViewById(R.id.ll_manage_myself);
        mLlSetupMyself = (LinearLayout) findViewById(R.id.ll_setup_myself);
        mLlListMyself = (LinearLayout) findViewById(R.id.ll_list_myself);
        mLlBitscreenMyself = (RelativeLayout) findViewById(R.id.ll_bitscreen_myself);
        mLlHotlineMyself = (RelativeLayout) findViewById(R.id.ll_hotline_myself);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
