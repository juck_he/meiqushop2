package cn.meiqu.mich.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.Expense;

/**
 * Created by Fatel on 15-9-29.
 */
public class RecycleExpendAdapter extends RecyclerView.Adapter<RecycleExpendAdapter.Holder> {
    Context mContext;
    ArrayList<Expense.PayList> payLists;

    public RecycleExpendAdapter(Context mContext, ArrayList<Expense.PayList> payLists) {
        this.mContext = mContext;
        this.payLists = payLists;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.recycle_expense, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return payLists.size();
    }

    class Holder extends BaseHolder {

        public Holder(View itemView) {
            super(itemView);
        }

        private LinearLayout mLlLine;
        private TextView mTvCouponName;
        private TextView mTvPaytime;
        private TextView mTvPayMoney;

        public void assignViews() {
            mLlLine = (LinearLayout) findViewById(R.id.ll_line);
            mTvCouponName = (TextView) findViewById(R.id.tv_coupon_name);
            mTvPaytime = (TextView) findViewById(R.id.tv_paytime);
            mTvPayMoney = (TextView) findViewById(R.id.tv_pay_money);
        }

        @Override
        public void instanceView(int position) {
            Expense.PayList payList = payLists.get(position);
            if (position % 2 == 1) {
                mLlLine.setBackgroundResource(R.drawable.sel_white_gray);
            } else {
                mLlLine.setBackgroundResource(R.drawable.sel_gray_white);
            }
            mTvPayMoney.setText("实付金额:￥ " + payList.pay_money);
            mTvPaytime.setText(payList.true_time + "");
            mTvCouponName.setText(payList.show_name + "");
        }
    }
}
