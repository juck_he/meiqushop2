package cn.meiqu.mich.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.RealPayHistroy;
import cn.meiqu.mich.util.TimeUtil;

/**
 * Created by Fatel on 15-9-28.
 */
public class RecycleRealPayHistoryAdapter extends RecyclerView.Adapter<RecycleRealPayHistoryAdapter.Holder> {
    Context mContext;
    ArrayList<RealPayHistroy> realPayHistroys;

    public RecycleRealPayHistoryAdapter(Context mContext, ArrayList<RealPayHistroy> realPayHistroys) {
        this.mContext = mContext;
        this.realPayHistroys = realPayHistroys;
    }

    @Override
    public RecycleRealPayHistoryAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.recycle_realpay_history, null));
    }

    @Override
    public void onBindViewHolder(RecycleRealPayHistoryAdapter.Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return realPayHistroys.size();
    }

    class Holder extends BaseHolder {

        public Holder(View itemView) {
            super(itemView);
        }

        private LinearLayout mLlLine;
        private TextView mTvDate;
        private TextView mTvTodayDate;
        private TextView mTvGathering;
        private TextView mTvPayType;
        private TextView mTvUserNum;

        public void assignViews() {
            itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mLlLine = (LinearLayout) findViewById(R.id.ll_line);
            mTvDate = (TextView) findViewById(R.id.tv_date);
            mTvTodayDate = (TextView) findViewById(R.id.tv_today_date);
            mTvGathering = (TextView) findViewById(R.id.tv_gathering);
            mTvPayType = (TextView) findViewById(R.id.tv_pay_type);
            mTvUserNum = (TextView) findViewById(R.id.tv_user_num);
        }

        @Override
        public void instanceView(int position) {
            RealPayHistroy payHistroy = realPayHistroys.get(position);

            if (position % 2 == 0) {
                mLlLine.setBackgroundColor(0xFFFFFFFF);
            } else {
                mLlLine.setBackgroundColor(0xFFeeeeee);
            }
            mTvDate.setText(TimeUtil.getTime(Long.parseLong(payHistroy.pay_time) * 1000, TimeUtil.DATE_FORMAT_DATE) + "");
            mTvGathering.setText("￥" + payHistroy.pay_money);
            mTvUserNum.setText(payHistroy.user_account + "");
            String name = "";
            if (payHistroy.pay_type == null) {
                name = "在线支付";
            } else if (payHistroy.pay_type.equals("1")) {
                name = "支付宝";
            } else if (payHistroy.pay_type.equals("2")) {
                name = "财付通";
            } else if (payHistroy.pay_type.equals("3")) {
                name = "微信支付";
            } else if (payHistroy.pay_type.equals("4")) {
                name = "银联";
            }
            mTvPayType.setText(name + "");
        }
    }
}
