package cn.meiqu.mich.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.Device;


/**
 * Created by Fatel on 15-4-3.
 */
public class ListDeviceAdapter extends BaseAdapter {
    ArrayList<Device> infos;
    Context mContext;

    public ListDeviceAdapter(Context mContext, ArrayList<Device> infos) {
        this.mContext = mContext;
        this.infos = infos;
    }

    @Override
    public int getCount() {
        return infos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View contain, ViewGroup parent) {
        Holder holder;
        if (contain == null) {
            contain = LayoutInflater.from(mContext).inflate(R.layout.list_device, null);
            holder = new Holder();
            holder.tv_ip = (TextView) contain.findViewById(R.id.tv_list_device_ip);
            holder.tv_name = (TextView) contain.findViewById(R.id.tv_list_device_name);
            holder.tv_status = (TextView) contain.findViewById(R.id.tv_list_device_status);
            contain.setTag(holder);
        } else {
            holder = (Holder) contain.getTag();
        }
        Device device = infos.get(position);
        holder.tv_name.setText(device.device_alias);
        holder.tv_ip.setText(device.ip_addr);
        if (device.status == 0) {
            holder.tv_status.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
            holder.tv_status.setTextColor(Color.parseColor("#ff446b"));
            holder.tv_status.setText("离线");
        } else if (device.status == 1) {
            holder.tv_status.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
            holder.tv_status.setTextColor(Color.parseColor("#565656"));
            holder.tv_status.setText("在线");
        } else {
            holder.tv_status.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
            holder.tv_status.setTextColor(Color.parseColor("#0000FF"));
            holder.tv_status.setText("连接中");
        }
        if (position % 2 == 0) {
            contain.setBackgroundColor(Color.parseColor("#ffffff"));
        } else {
            contain.setBackgroundColor(Color.parseColor("#eeeeee"));
        }
        return contain;
    }

    class Holder {
        TextView tv_name, tv_ip, tv_status;

    }
}
