package cn.meiqu.mich.adapter;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.Fatel.utils.ImageLoadHelper;
import com.Fatel.utils.ScreenUtils;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.bean.AblumImage;
import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Fatel on 15-4-5.
 */
public class RecycleHairImageAdapter extends RecyclerView.Adapter<RecycleHairImageAdapter.MyViewHolder> {
    ArrayList<AblumImage> ablumImages;
    MyOnItemDeleteListener myOnItemDeleteListener;
    MyOnItemClickListener myOnItemClickListener;

    public interface MyOnItemClickListener {
        public void onItemClick(View v, int position);
    }

    public interface MyOnItemDeleteListener {
        public void onItemDelete(View v, int position);
    }

    public RecycleHairImageAdapter(ArrayList<AblumImage> ablumImages,
                                   MyOnItemDeleteListener myOnItemDeleteListener,
                                   MyOnItemClickListener myOnItemClickListener) {
        this.ablumImages = ablumImages;
        this.myOnItemClickListener = myOnItemClickListener;
        this.myOnItemDeleteListener = myOnItemDeleteListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_hair_ablum, null);
        MyViewHolder myViewHolder = new MyViewHolder(itemView, myOnItemDeleteListener, myOnItemClickListener);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.instanceView(ablumImages.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return ablumImages.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MyOnItemDeleteListener myOnItemDeleteListener;
        MyOnItemClickListener myOnItemClickListener;
        View mRelSelect;
        public ImageView imgV_hair, imgV_delete;

        public MyViewHolder(View itemView, MyOnItemDeleteListener myOnItemDeleteListener, MyOnItemClickListener myOnItemClickListener) {
            super(itemView);
            this.myOnItemClickListener = myOnItemClickListener;
            this.myOnItemDeleteListener = myOnItemDeleteListener;

            imgV_hair = (ImageView) findViewById(R.id.imgV_hair_album);
            imgV_delete = (ImageView) findViewById(R.id.imgV_hair_delete);
            mRelSelect = findViewById(R.id.rel_hairSelect);
            imgV_delete.setOnClickListener(this);
            itemView.setOnClickListener(this);

        }

        public View findViewById(int id) {
            return itemView.findViewById(id);
        }

        public void instanceView(AblumImage ablumImage) {
            int pTop = ScreenUtil.dip2px(itemView.getContext(), 6);
            int pLeft = ScreenUtil.dip2px(itemView.getContext(), 3);
            if (getPosition() % 2 == 0) {
                itemView.setPadding(0, 0,pLeft, pTop);
            } else {
                itemView.setPadding(pLeft, 0, 0, pTop);
            }
            int width = ScreenUtils.ScreenWidth(itemView.getContext()) / 3;
            imgV_hair.setTag(getPosition());
//            itemView.setVisibility(View.INVISIBLE);
            if (StringUtil.isEmpty(ablumImage.image_path)) {

            } else {
                ImageLoadHelper.displayImage("file://" + ablumImage.image_path, new ImageSize(width, width), new MyImageLoadingListener(itemView, imgV_hair, getPosition()));
                LogUtil.log("-----图片的路径：file://"+ablumImage.image_path);
            }
            //
            if (ablumImage.isSelect) {
                imgV_hair.setColorFilter(Color.parseColor("#77000000"));
                mRelSelect.setVisibility(View.VISIBLE);
            } else {
                mRelSelect.setVisibility(View.INVISIBLE);
                imgV_hair.setColorFilter(null);
            }
            //
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == itemView.getId()) {
                if (myOnItemClickListener != null) {
                    myOnItemClickListener.onItemClick(v, getPosition());
                }
            } else if (v.getId() == imgV_delete.getId()) {
                if (myOnItemDeleteListener != null) {
                    myOnItemDeleteListener.onItemDelete(v, getPosition());
                }
            }
        }

        class MyImageLoadingListener implements ImageLoadingListener {
            ImageView imageView = null;
            int position = 0;
            View itemView;

            public MyImageLoadingListener(View itemView, ImageView imageView, int position) {
                this.itemView = itemView;
                this.imageView = imageView;
                this.position = position;
                imageView.setImageBitmap(null);
            }

            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                LogUtil.log("bitmap.getwidth=" + bitmap.getWidth());
                if (imageView.getTag().equals(position)) {
                    itemView.setVisibility(View.VISIBLE);
                    imageView.setImageBitmap(bitmap);
                    imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(), com.example.myphotoalbum.R.anim.alpha_in));
                } else {

                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {
                LogUtil.log("onLoadingCancelled");
            }
        }
    }
}