package cn.meiqu.mich.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.Accredit;

/**
 * Created by Fatel on 15-9-23.
 */
public class ListAccreditAdapter extends BaseAdapter {


    public interface OnAccreditClickListener {
        public void onClickEdt(int position);

        public void onClickDel(int position);
    }

    public OnAccreditClickListener getOnAccreditClickListener() {
        return onAccreditClickListener;
    }

    public void setOnAccreditClickListener(OnAccreditClickListener onAccreditClickListener) {
        this.onAccreditClickListener = onAccreditClickListener;
    }

    private OnAccreditClickListener onAccreditClickListener;
    Context mContext;
    ArrayList<Accredit> accredits;

    public ListAccreditAdapter(Context mContext, ArrayList<Accredit> accredits) {
        this.mContext = mContext;
        this.accredits = accredits;
    }

    @Override
    public int getCount() {
        return accredits.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View contain, ViewGroup parent) {
        if (contain == null) {
            contain = LayoutInflater.from(mContext).inflate(R.layout.list_accredit, null);
            contain.setTag(new Holder(contain));
        }
        ((Holder) contain.getTag()).instanceView(position);
        return contain;
    }

    class Holder extends BaseHolder implements View.OnClickListener {

        private TextView mTvAccount;
        private Button mBtnDel;
        private Button mBtnEdit;
        int position = 0;

        public Holder(View itemView) {
            super(itemView);
        }

        public void assignViews() {
            mTvAccount = (TextView) findViewById(R.id.tv_account);
            mBtnDel = (Button) findViewById(R.id.btn_del);
            mBtnEdit = (Button) findViewById(R.id.btn_edit);
            mBtnDel.setOnClickListener(this);
            mBtnEdit.setOnClickListener(this);
        }

        @Override
        public void instanceView(int position) {
            this.position = position;
            Accredit accredit = accredits.get(position);
            mTvAccount.setText(accredit.user_account + "");
            //mTvPhone.setText(accredit.phone + "");

        }

        @Override
        public void onClick(View v) {
            if (onAccreditClickListener != null) {
                if (v.getId() == mBtnEdit.getId()) {
                    onAccreditClickListener.onClickEdt(position);
                } else if (v.getId() == mBtnDel.getId()) {
                    onAccreditClickListener.onClickDel(position);
                }
            }
        }
    }
}
