package cn.meiqu.mich.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Fatel.utils.ImageLoadHelper;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.Expense;
import cn.meiqu.mich.view.RippleView;

/**
 * Created by Fatel on 15-9-29.
 */
public class RecycleExpendItemAdapter extends RecyclerView.Adapter<RecycleExpendItemAdapter.Holder> {
        Context mContext;
        ArrayList<Expense.PayList> payLists;
private OnRecycleClickListener onRecycleClickListener;

public OnRecycleClickListener getOnRecycleClickListener() {
        return onRecycleClickListener;
        }

public void setOnRecycleClickListener(OnRecycleClickListener onRecycleClickListener) {
        this.onRecycleClickListener = onRecycleClickListener;
        }

public interface OnRecycleClickListener {
    public void onRecycleClick(int position);
}

    public RecycleExpendItemAdapter(Context mContext, ArrayList<Expense.PayList> payLists) {
        this.mContext = mContext;
        this.payLists = payLists;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.recycle_expense_item, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return payLists.size();
    }

class Holder extends BaseHolder implements RippleView.OnRippleCompleteListener {

    public Holder(View itemView) {
        super(itemView);
        ((RippleView) itemView).setOnRippleCompleteListener(this);
    }

    private ImageView mIvPic;
    private TextView mTvName;
    private TextView mTvPrice;
    private TextView mTvDate;
    private TextView mTvHasCosted;
    private TextView mTvHasSelled;

    public void assignViews() {
        mIvPic = (ImageView) findViewById(R.id.iv_pic);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvPrice = (TextView) findViewById(R.id.tv_price);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mTvHasCosted = (TextView) findViewById(R.id.tv_has_costed);
        mTvHasSelled = (TextView) findViewById(R.id.tv_has_selled);
    }


    @Override
    public void instanceView(int position) {
        Expense.PayList payList = payLists.get(position);
        ImageLoadHelper.displayImage(payList.coupon_img,mIvPic);
        //Glide.with(mContext).load(payList.coupon_img).into(mIvPic);
        mTvName.setText(payList.coupon_title + "");
        mTvPrice.setText("￥ " + payList.coupon_money);
        mTvDate.setText(payList.begin + "");
        mTvHasCosted.setText(payList.use_count + "");
        mTvHasSelled.setText(payList.sell_num + "");
    }

    @Override
    public void onComplete(RippleView rippleView) {
        if (onRecycleClickListener != null) {
            onRecycleClickListener.onRecycleClick(getAdapterPosition());
        }
    }
}
}
