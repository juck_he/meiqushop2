package cn.meiqu.mich.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.meiqu.mich.R;
import cn.meiqu.mich.basefatel.BaseHolder;
import cn.meiqu.mich.bean.ExpenseItemDetail;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Fatel on 15-9-29.
 */
public class RecycleExpendItemDetailAdapter extends RecyclerView.Adapter<RecycleExpendItemDetailAdapter.Holder> {
    Context mContext;
    ArrayList<ExpenseItemDetail.Item> items;


    public RecycleExpendItemDetailAdapter(Context mContext, ArrayList<ExpenseItemDetail.Item> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.recycle_expense_item_detail, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class Holder extends BaseHolder {

        public Holder(View itemView) {
            super(itemView);
            itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        private LinearLayout mLlLine;
        private TextView mTvDate;
        private TextView mTvCostedMun;
        private TextView mTvCostedMoney;
        private TextView mTvSellNum;
        private TextView mTvSellMoney;

        public void assignViews() {
            mLlLine = (LinearLayout) findViewById(R.id.ll_line);
            mTvDate = (TextView) findViewById(R.id.tv_date);
            mTvCostedMun = (TextView) findViewById(R.id.tv_costed_mun);
            mTvCostedMoney = (TextView) findViewById(R.id.tv_costed_money);
            mTvSellNum = (TextView) findViewById(R.id.tv_sell_num);
            mTvSellMoney = (TextView) findViewById(R.id.tv_sell_money);
        }


        @Override
        public void instanceView(int position) {
            ExpenseItemDetail.Item item = items.get(position);
//


            if (position % 2 == 0) {
                mLlLine.setBackgroundResource(R.drawable.sel_white_gray);//Color(0xFFFFFFFF);
            } else {
                mLlLine.setBackgroundResource(R.drawable.sel_gray_white);
            }
            mTvDate.setText("" + item.date);
            int usecount = Integer.parseInt(item.use_count);
            int sellcount = Integer.parseInt(item.sell_count);
            mTvCostedMun.setText(StringUtil.getLess(item.use_count) + "");
            mTvCostedMoney.setText(StringUtil.getLess(item.use_money) + "");
            mTvSellNum.setText(StringUtil.getLess(item.sell_count) + "");
            mTvSellMoney.setText(StringUtil.getLess(item.sell_money) + "");
            if (usecount <= 0) {
                mTvCostedMun.setTextColor(Color.parseColor("#ffbbbbbb"));
                mTvCostedMoney.setTextColor(Color.parseColor("#ffbbbbbb"));
            } else if (usecount < 200) {
                mTvCostedMun.setTextColor(Color.parseColor("#ff555555"));
                mTvCostedMoney.setTextColor(Color.parseColor("#ff555555"));
            } else {
                mTvCostedMun.setTextColor(Color.parseColor("#ffF12287"));
                mTvCostedMoney.setTextColor(Color.parseColor("#ff555555"));
            }

            if (sellcount <= 0) {
                mTvSellNum.setTextColor(Color.parseColor("#ffbbbbbb"));
                mTvSellMoney.setTextColor(Color.parseColor("#ffbbbbbb"));
            } else if (sellcount < 200) {
                mTvSellNum.setTextColor(Color.parseColor("#ff555555"));
                mTvSellMoney.setTextColor(Color.parseColor("#ff555555"));
            } else {
                mTvSellNum.setTextColor(Color.parseColor("#ffF12287"));
                mTvSellMoney.setTextColor(Color.parseColor("#ff555555"));
            }

        }


    }
}
