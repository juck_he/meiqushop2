package cn.meiqu.mich.view.wheelview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;


/**
 * Created by Administrator on 2015/11/6.
 */
public class MyRelativeLayout extends RelativeLayout {
    int startX;
    int startY;
    private int endX;
    private int endY;

    public MyRelativeLayout(Context context) {
        this(context,null);
    }

    public MyRelativeLayout(Context context, AttributeSet attrs) {
       this(context, attrs,0);
    }

    public MyRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
    }

}
