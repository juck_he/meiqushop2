package cn.meiqu.mich.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.os.Handler;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;

import cn.meiqu.mich.util.LogUtil;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.util.StringUtil;

/**
 * Created by Administrator on 2015/11/9.
 */
public class MyTextView extends EditText {
    TextPaint paintTop;
    private Context mContext;
    TextPaint paintBottom, paint;
    private String topPaintColor;
    private String bootomColor;
    private int paintSize= 0;
    int time = 0;
    Handler mHandler;
    private PaintFlagsDrawFilter paintFlagsDrawFilter;   //给画布设置锯齿

    public MyTextView(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        this.mContext = context;
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        this.mContext = context;
    }

    RreshRunable rreshRunable = new RreshRunable();

    class RreshRunable implements Runnable {
        @Override
        public void run() {
            postInvalidate();
        }
    }

    private void initView() {
        mHandler = new Handler();
//        Typeface iconfont = Typeface.createFromAsset(mContext.getAssets(), "fontawesome.ttf");
//        setTypeface(iconfont);
        LogUtil.log("------top:" + topPaintColor + "------bootom:" + bootomColor);
        paintTop = new TextPaint();
        paintBottom = new TextPaint();
        //


        //
        paintTop.setAntiAlias(true);
        paintBottom.setAntiAlias(true);
        //

        paintTop.setStyle(Paint.Style.FILL_AND_STROKE);
        paintBottom.setStyle(Paint.Style.FILL_AND_STROKE);

        paint = new TextPaint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(2);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.BLUE);

        //给画布设置锯齿
        paintFlagsDrawFilter = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
    }

    @Override
    public void setTextSize(float size) {
        super.setTextSize(size);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if(paintSize==0){
            paintTop.setStrokeWidth(ScreenUtil.dip2px(mContext, 4));
            paintBottom.setStrokeWidth(ScreenUtil.dip2px(mContext, 1));
        }else {
            paintTop.setStrokeWidth(ScreenUtil.dip2px(mContext, 4));
            paintBottom.setStrokeWidth(ScreenUtil.dip2px(mContext, 0.5f));
        }
        if (paintSize==0){
            paintBottom.setTextSize(ScreenUtil.dip2px(mContext, 19));
            paintTop.setTextSize(ScreenUtil.dip2px(mContext, 19));
        }else {
            paintBottom.setTextSize(paintSize);
            paintTop.setTextSize(paintSize);
        }
        //
        if (!TextUtils.isEmpty(topPaintColor)) {
            if(topPaintColor.equals(bootomColor)){
                paintTop.setColor(Color.TRANSPARENT);
            }else {
                String color = "#" + topPaintColor;
                paintTop.setColor(Color.parseColor(color));
            }
        } else {
            paintTop.setColor(Color.TRANSPARENT);
        }
        if (!TextUtils.isEmpty(bootomColor)) {
            String color = "#" + bootomColor;
            paintBottom.setColor(Color.parseColor(color));
        } else {
            paintBottom.setColor(Color.BLACK);
        }
        String text = getText().toString();
        if (!StringUtil.isEmpty(text)) {
            String[] split = text.split("");
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < split.length; i++) {
                split[i] = split[i];
                stringBuffer.append(split[i]);
            }
            text = stringBuffer.toString();
            float textWidth = paintTop.measureText(text);
            float startX = (getMeasuredWidth() - textWidth) / 2;

            LogUtil.log("--------startX"+startX+"------getTextSize()"+getTextSize());
            //
            if (paintSize==0) {
                canvas.drawText(text, startX,getTextSize(), paintTop);
                canvas.drawText(text, startX, getTextSize(), paintBottom);
            }else {
                canvas.drawText(text, 0,getTextSize(), paintTop);
                canvas.drawText(text, 0, getTextSize(), paintBottom);
            }

            float lineX = startX + paintTop.measureText(text.substring(0, getSelectionStart()));
            if (isFocused()) {
                time++;
                if (time % 2 == 0) {
                    canvas.drawLine(lineX, 0, lineX , getTextSize(), paint);
                }
                mHandler.removeCallbacks(rreshRunable);
                mHandler.postDelayed(rreshRunable, 1500);
            }
            canvas.setDrawFilter(paintFlagsDrawFilter);
        } else {
            super.onDraw(canvas);
        }
//        LogUtil.log("--------getSelecttionstart:" + getSelectionStart());
//        LogUtil.log("--------getSelecttionend:"+getSelectionEnd());
    }


    public void setPaintColor(String topPaintColor, String bootomColor) {
        this.topPaintColor = topPaintColor;
        this.bootomColor = bootomColor;
    }

    @Override
    public int getSelectionEnd() {
        return super.getSelectionEnd();
    }

    public void setTieleTextSize(int paintSize){
        this.paintSize = paintSize;
    }
}
