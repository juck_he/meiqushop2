package cn.meiqu.mich.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by juck on 15/12/29.
 */
public class ImageViewMuban extends ImageView {

    public ImageViewMuban(Context context) {
        super(context);
    }

    public ImageViewMuban(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewMuban(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //
//        setMeasuredDimension(scs);
        int newHeightSpec=MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec) / 2, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec,newHeightSpec);
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
