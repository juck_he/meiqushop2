package cn.meiqu.mich.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by juck on 15/12/30.
 */
public class MainFristLinearLayout extends RelativeLayout {
    private  Context mContext;
    public MainFristLinearLayout(Context context) {
        super(context);
        mContext = context;
    }

    public MainFristLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainFristLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(widthMeasureSpec)/10);

//        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//        int heightSize = ScreenUtil.ScreenHeight()/10;
//
//        LogUtil.log("-------widthSize:"+widthSize+"------heightSize:"+heightSize);
//        //
//        for (int i = 0 ;i<getChildCount(); i++){
//            measureChild(getChildAt(i),widthMeasureSpec,heightMeasureSpec);
//
//        }
//        setMeasuredDimension(widthSize,heightSize);
        heightMeasureSpec=MeasureSpec.makeMeasureSpec(ScreenUtil.ScreenHeight(getContext())/12,MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
    }

}
