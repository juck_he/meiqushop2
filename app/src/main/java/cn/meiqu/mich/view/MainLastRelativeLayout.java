package cn.meiqu.mich.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by juck on 15/12/30.
 */
public class MainLastRelativeLayout extends RelativeLayout {
    public MainLastRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MainLastRelativeLayout(Context context) {
        super(context);
    }

    public MainLastRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec=MeasureSpec.makeMeasureSpec(ScreenUtil.ScreenHeight(getContext())/12,MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
