package cn.meiqu.mich.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import cn.meiqu.mich.util.ScreenUtil;


/**
 * Created by Fatel on 15-4-3.
 */
public class LinSquareView extends RelativeLayout {

    // Paint mPaint = new Paint();

    public LinSquareView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // init();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(ScreenUtil.ScreenWidth(getContext()) / 2), MeasureSpec.EXACTLY);
        int heightSize = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize((int) (ScreenUtil.ScreenWidth(getContext()) / 3.0)), MeasureSpec.EXACTLY);
        super.onMeasure(widthSize, heightSize);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        // canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), mPaint);
    }
}
