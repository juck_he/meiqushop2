package cn.meiqu.mich.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import cn.meiqu.mich.util.LogUtil;

/**
 * Created by Administrator on 2015/11/2.
 */
public class ViewPagerTouch extends ViewPager {
    int startX;
    int startY;
    private int endX;
    private int endY;

    public ViewPagerTouch(Context context) {
        this(context, null);
    }

    public ViewPagerTouch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
//


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean isGetFouce = true;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                LogUtil.log("juck-----down");
                startX = (int) ev.getRawX();
                startY = (int) ev.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                LogUtil.log("juck-----move");
                endX = (int) ev.getRawX();
                endY = (int) ev.getRawY();
                if(onSlideListen!=null){
                    onSlideListen.slide();
                }
                break;
            case MotionEvent.ACTION_UP:
                LogUtil.log("juck-----Math.abs(endX - startX)" + Math.abs(endX - startX) + "--------" + Math.abs(endY - startY));
                if (Math.abs(endX - startX) > 0 || (Math.abs(endY - startY) > 0)) {
                    isGetFouce = true;
                } else {
                    isGetFouce = false;
                }
                if(onSlideListen!=null){
                    onSlideListen.slideImage(Math.abs(endX - startX),Math.abs(endY - startY));
                }
                break;
        }
        LogUtil.log("juck-----是否拦截" + isGetFouce);
        return  super.dispatchTouchEvent(ev);
    }

    public interface OnSlideListen{
        void slideImage(int x,int y);
        void slide();
    }

    public OnSlideListen onSlideListen;

    public void setOnSlideListen(OnSlideListen onSlideListen){
        this.onSlideListen = onSlideListen;
    }


}




