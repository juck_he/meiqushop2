package cn.meiqu.mich.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import cn.meiqu.mich.util.ScreenUtil;

/**
 * Created by juck on 15/12/30.
 */
public class MainMidLinearlayout extends LinearLayout {
    public MainMidLinearlayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MainMidLinearlayout(Context context) {
        super(context);
    }

    public MainMidLinearlayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = ((ScreenUtil.ScreenHeight(getContext())/12)*10)/3;
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height,MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
