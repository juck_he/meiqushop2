package cn.meiqu.mich.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cn.meiqu.mich.R;
import cn.meiqu.mich.dao.SettingDao;
import cn.meiqu.mich.view.RippleView;


/**
 * Created by Fatel on 15-4-9.
 */
public class SetDeviceOnlineDialog extends Dialog implements View.OnClickListener, RippleView.OnRippleCompleteListener {
    private OnDialogClickListener onDialogClickListener;

    public interface OnDialogClickListener {
        public void onClickCommit(boolean b);
    }

    Button btn_cansel;
    Button btn_comit;
    EditText edt_ssid, edt_pwd;
    RippleView rippleViewCansel, rippleViewComit;

    public SetDeviceOnlineDialog(Context context) {
        super(context, R.style.defaultDialog);
        init();
    }

    public SetDeviceOnlineDialog(Context context, int theme) {
        super(context, theme);
    }

    protected SetDeviceOnlineDialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public Dialog setOnDialogClickListener(OnDialogClickListener onDialogClickListener) {
        this.onDialogClickListener = onDialogClickListener;
        return this;
    }

    public void init() {
        setContentView(R.layout.dialog_setdevice_online);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        btn_cansel = (Button) findViewById(R.id.btn_dialog_cansel);
        btn_comit = (Button) findViewById(R.id.btn_dialog_commit);
        edt_ssid = (EditText) findViewById(R.id.et_username);
        edt_pwd = (EditText) findViewById(R.id.et_pwd);
        btn_cansel.setOnClickListener(this);
        btn_comit.setOnClickListener(this);
        edt_ssid.setText(SettingDao.getInstance().getWifiSSid());
        edt_pwd.setText(SettingDao.getInstance().getWifiPwd());

        rippleViewComit = (RippleView) findViewById(R.id.rippleView_dialogCommit);
        rippleViewCansel = (RippleView) findViewById(R.id.rippleView_dialogCansel);
        rippleViewCansel.setOnRippleCompleteListener(this);
        rippleViewComit.setOnRippleCompleteListener(this);
    }

    public String getSSid() {
        return edt_ssid.getText().toString();
    }

    public String getPwd() {
        return edt_pwd.getText().toString();
    }

    @Override
    public void onClick(View v) {
        // dismiss();

    }

    @Override
    public void onComplete(RippleView v) {
        if (v.getId() == rippleViewComit.getId()) {
            if (onDialogClickListener != null) {
                onDialogClickListener.onClickCommit(true);
            }
        } else if (v.getId() == rippleViewCansel.getId()) {
            if (onDialogClickListener != null) {
                onDialogClickListener.onClickCommit(false);
            }
        }
    }
}
