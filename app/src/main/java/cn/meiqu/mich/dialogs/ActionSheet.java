package cn.meiqu.mich.dialogs;

/**
 * Created by Administrator on 2015/9/16.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.meiqu.mich.R;

public class ActionSheet {

    public interface OnActionSheetSelected {
        void onClickAction(int whichButton);
    }

    private ActionSheet() {
    }

    public static Dialog showSheet(Context context,
                                   final OnActionSheetSelected actionSheetSelected,
                                   DialogInterface.OnCancelListener cancelListener,String titleNam,String funtion1,String funtion2) {
        final Dialog dlg = new Dialog(context, R.style.ActionSheet);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(
                R.layout.view_actionsheet, null);
        final int cFullFillWidth = 10000;
        layout.setMinimumWidth(cFullFillWidth);

        TextView mTitle = (TextView) layout.findViewById(R.id.title);
        mTitle.setText(titleNam);
        TextView mTvTakingphoto = (TextView) layout.findViewById(R.id.tv_takingphoto);
        TextView mTvFromphotoablum = (TextView) layout.findViewById(R.id.tv_fromphotoablum);
        if (funtion1==null){
            mTvTakingphoto.setVisibility(View.GONE);
        }else {
            mTvTakingphoto.setText(funtion1);
        }
        mTvFromphotoablum.setText(funtion2);
        TextView mCancel = (TextView) layout.findViewById(R.id.cancel);

        mTvTakingphoto.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionSheetSelected.onClickAction(0);
                dlg.dismiss();
            }
        });
        mTvFromphotoablum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                actionSheetSelected.onClickAction(1);
                dlg.dismiss();
            }
        });
        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionSheetSelected.onClickAction(2);
                dlg.dismiss();
            }
        });

        Window w = dlg.getWindow();
        WindowManager.LayoutParams lp = w.getAttributes();
        lp.x = 0;
        final int cMakeBottom = -1000;
        lp.y = cMakeBottom;
        lp.gravity = Gravity.BOTTOM;
        dlg.onWindowAttributesChanged(lp);
        dlg.setCanceledOnTouchOutside(false);
        if (cancelListener != null)
            dlg.setOnCancelListener(cancelListener);
        dlg.setContentView(layout);
        dlg.show();

        return dlg;
    }

}
