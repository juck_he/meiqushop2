package cn.meiqu.mich;

/**
 * Created by Administrator on 2015/10/15.
 */
public class KeyCountAnalytics {

    public static String Key_Play = "Key_Play"; //广告播放,0
    public static String Key_Text_Ad = "Key_Text_Ad";//,字幕广告,0
    public static String Key_Screen_Control = "Key_Screen_Control";//,美屏控制,0
    public static String Key_HairStyle_Album = "Key_HairStyle_Album";//,发型相册,0
    public static String Key_Take_Fly_Soon = "Key_Take_Fly_Soon";//,即拍即飞,0
    public static String Key_BeautyScreen_Move = "Key_BeautyScreen_Move";//,美屏电影,0
    public static String Key_HairStyleer_Ad = "Key_HairStyleer_Ad";//,发型师广告,0
    public static String Key_Open_Myself = "Key_Open_Myself";//打开我的界面
    public static String Key_Home_Scan = "Key_Home_Scan";//主页扫一扫

    public static String Key_Pay_Verify = "Key_Pay_Verify";//,支付验证,0
    public static String Key_Real_Time_Gathering = "Key_Real_Time_Gathering";//,实时收款,0
    public static String Key_Custom_Count = "Key_Custom_Count";//,消费统计,0
    public static String Key_Beauty_Setup = "Key_Beauty_Setup";//,美屏设置,0
    public static String Key_SuperUser = "Key_SuperUser";//,授权管理,0
    public static String Key_WiFi_SetUp = "Key_WiFi_SetUp";//,设置WiFi,0
    public static String Key_Beauty_List = "Key_Beauty_List";//,大屏列表,0
    public static String Key_Bingding_Screen = "Key_Bingding_Screen";//,绑定大屏,0
    public static String Key_Member_Manger = "Key_Member_Manger";//,会员管理,0

    public static String  Key_Shop_Store = "Key_Shop_Store";//店家商铺

    public static String Movie_Play = "Movie_Play";//,,电影_开始播放,0
    public static String Album_AddPic = "Album_AddPic";//,发型相册_添加照片,0
    public static String Album_HairBeforeAfter = "Album_HairBeforeAfter";//,发型相册_美发前后,0
    public static String ADManager_UpdateADTitle = "ADManager_UpdateADTitle";//,广告管理字幕更新,0
    public static String Album_BeginCompare = "Album_BeginCompare";//,发型相册_开始对比,0
    public static String ADManager_BigScreenList = "ADManager_BigScreenList";//,广告管理大屏列表,0
    public static String ADManager_UploadMemberBannerPic = "ADManager_UploadMemberBannerPic";//,广告管理上传会员广告图片,0

}
