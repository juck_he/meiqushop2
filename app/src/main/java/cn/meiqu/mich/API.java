package cn.meiqu.mich;

public class API {
//
//    public static int staticNow = 1;// 1:表示正式环境
//    public final static String baseUrl = "http://v2.mich-china.com/api/";

    public static int staticNow = 2;  //2.表示测试环境
    public final static String baseUrl = "http://test.v2.mich-china.com/api/";

    // ----------用户 100-999 ------
    public final static String downLoadAppUrl = "http://baidu.com?";

    //
    //
    public final static String home = "member/get_member_app_banner_v26";

    public final static String get_qiniu_token = "url/get_qiniu_uploadtoken";
    public final static String login = "user/login";

    //上传广告
    public final static String changePwd = "user/set_password";

    public final static String getUserInfo = "manage/get_company_baseinfo";
    public final static String edtUserInfo = "manage/set_baseinfo";
    public final static String setAdToScreen = "marquee/update_marquee";
    //获取广告的内容
    public final static String sgetAdToScreen = "marquee/get_marquee";

    //上存店家广告图
    public final static String setShopAdPhoto = "company/edit_company_member_ad";
    //获取店家广告图
    public final static String getShopAdPhoto = "member/get_member_app_banner_v26";

    //上存轮番图url
    public final static String setTurnsPhoto = "device/add_img";
    //获取轮番图url
    public final static String getTurnsPhoto = "device/get_img_list";

    //删除轮番图url
    public final static String setDelTurnPhoro = "device/del_img";
    //获取大屏列表
    public final static String getbitListInfo = "manage/get_device_list";
    //验证并使用券
    public final static String getCheckUseCoupons = "manage/set_coupon_use";
    //验证券历史列表
    public final static String getCheckUseHistory = "manage/get_coupon_use_list";
    //发布优惠券
    public final static String setCoupon = "voucher/add";
    //优惠券赠送给会员
    public final static String setCouponToMember = "exchange/give_to_users";
    //获取会员列表
    public final static String getUserList = "manage/get_member_list";
    //店家发送推送
    public final static String sendTextPush = "company_news/set_news_v2_5";
    //获取商家自己发布优惠券
    public final static String getCouponForMySelf = "voucher/get_company_voucher";
    //获取已经推送的信息
    public final static String getTextPushed = "manage/get_news_list";
    //获取会员详情
    public final static String getMemberDetail = "manage/get_member_detail";
    //设置大屏工作时间
    public final static String setScreenWorkTime = "manage/set_screen_work_time";
    //店长端banner
    public final static String getBeannerPhoto = "company/get_company_app_banner";
    //发送短信验证码2.0
    public final static String getVerificationCode = "user/set_user_reg_code_v2";
    //设置信息的密码
    public final static String setNewPwd = "user/set_new_password";
    //获取大屏上次设置的过的开关机时间
    public final static String getBitScreenLastSetup = "manage/get_screen_info";
    //获取一体机形象展示模板列表
    public final static String getTemplateList = "device/get_template_list";
    //获取项目上传模板列表
    public final static String getProTemplateList = "company/coupon_template_list";
    //上传模板
    public final static String setUpdataTemplateList = "company/add_coupon";
    //获取历史模板
    public final static String getHistoryTemplateList = "manage/get_coupon";
    //编辑项目模板
    public final static String setEditTemplateList = "company/edit_coupon";
    //删除模板
    public final static String setDeteleMuban = "company/down_coupon";


    public final static String getStyList = "company/get_account_list";
    public final static String addStyList = "company/add_account";
    public final static String edtStyList = "company/edit_account";
    public final static String delStylist = "company/del_account";
    public final static String set_wifi_info = "manage/set_wifi";
    public final static String get_company_devices = "manage/get_device_list";
    public final static String set_device_info = "getWIfiInfo";
    public final static String bindDevice = "manage/set_binding_screen";
    public final static String setRealPay = "manage/set_quick_pay";
    public final static String checkRealPay = "manage/get_order_info_by_company";
    public final static String getRealPayHistroy = "manage/get_quick_pay_list";
    public final static String getPayList = "manage/get_pay_list_v3";
    public final static String getPayItemList = "manage/get_coupon_use_statistics_list_v2";
    public final static String getPayItemDetail = "manage/get_coupon_statistics";
    public final static String checkDeviceHasAd = "getComanyAd";
    public final static String getDeviceFilmList = "screen/get_video_list";

    public static String getAbsolutePath(String url) {
        return baseUrl + url;
    }
}
