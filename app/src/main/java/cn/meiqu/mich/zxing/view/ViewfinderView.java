/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.meiqu.mich.zxing.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.google.zxing.ResultPoint;

import java.util.Collection;
import java.util.HashSet;

import cn.meiqu.mich.R;
import cn.meiqu.mich.util.ScreenUtil;
import cn.meiqu.mich.zxing.camera.CameraManager;


public final class ViewfinderView extends View {

    private static final int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192,
            128, 64};
    private static final long ANIMATION_DELAY = 100L;
    private static final int OPAQUE = 0xFF;

    private final Paint paint;
    private Bitmap resultBitmap;
    private final int maskColor;
    private final int resultColor;
    private final int frameColor;
    private final int laserColor;
    private final int resultPointColor;
    private int scannerAlpha;
    private Collection<ResultPoint> possibleResultPoints;
    private Collection<ResultPoint> lastPossibleResultPoints;
    int broderWidth = 6;
    int broderLength = 12;

    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Initialize these once for performance rather than calling them every
        // time in onDraw().
        paint = new Paint();
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.viewfinder_mask);
        resultColor = resources.getColor(R.color.result_view);
        frameColor = resources.getColor(R.color.pink);
        laserColor = resources.getColor(R.color.viewfinder_laser);
        resultPointColor = resources.getColor(R.color.transparent);
        scannerAlpha = 0;
        possibleResultPoints = new HashSet<ResultPoint>(5);
        broderWidth = ScreenUtil.dip2px(getContext(), broderWidth);
        broderLength = ScreenUtil.dip2px(getContext(), broderLength);
    }

    CameraManager cm;

    @Override
    public void onDraw(Canvas canvas) {
        cm = CameraManager.get();
        if (cm == null)
            return;
        Rect frame = null;
        try {
            frame = cm.getFramingRect1();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (frame == null) {
            return;
        }
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        // Draw the exterior (i.e. outside the framing rect) darkened
        paint.setColor(resultBitmap != null ? resultColor : maskColor);
        canvas.drawRect(0, 0, width, frame.top, paint);
        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1,
                paint);
        canvas.drawRect(0, frame.bottom + 1, width, height, paint);

        if (resultBitmap != null) {
            // Draw the opaque result bitmap over the scanning rectangle
            paint.setAlpha(OPAQUE);
            canvas.drawBitmap(resultBitmap, frame.left, frame.top, paint);
        } else {

            Rect frame2 = new Rect();
//            frame2.left = frame.left - broderWidth;
//            frame2.right = frame.right + broderWidth;
//            frame2.top = frame.top - broderWidth;
//            frame2.bottom = frame.bottom + broderWidth;
            frame2.left = frame.left ;
            frame2.right = frame.right ;
            frame2.top = frame.top ;
            frame2.bottom = frame.bottom ;
            drawBroder(canvas, frame2);
            // Draw a two pixel solid black border inside the framing rect

            // Draw a red "laser scanner" line through the middle to show
            // decoding is active

            //画十字
            /*
            paint.setColor(laserColor);
			paint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
			scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
			int middle = frame.height() / 2 + frame.top;
			int m2 = frame.left + (frame.right -frame.left) /2;
            int l = 50;
            int w  = 2;
//			canvas.drawRect(frame.left + 2, middle - 1, frame.right - 1,
//					middle + 2, paint);
            canvas.drawRect(m2 - l ,middle -w,m2 + l,middle + w,paint);
            canvas.drawRect(m2 - w ,middle -l,m2 + w,middle + l,paint);
			Collection<ResultPoint> currentPossible = possibleResultPoints;
			Collection<ResultPoint> currentLast = lastPossibleResultPoints;
			if (currentPossible.isEmpty()) {
				lastPossibleResultPoints = null;
			} else {
				possibleResultPoints = new HashSet<ResultPoint>(5);
				lastPossibleResultPoints = currentPossible;
				paint.setAlpha(OPAQUE);
				paint.setColor(resultPointColor);
				for (ResultPoint point : currentPossible) {
					canvas.drawCircle(frame.left + point.getX(), frame.top
							+ point.getY(), 6.0f, paint);
				}
			}
			if (currentLast != null) {
				paint.setAlpha(OPAQUE / 2);
				paint.setColor(resultPointColor);
				for (ResultPoint point : currentLast) {
					canvas.drawCircle(frame.left + point.getX(), frame.top
							+ point.getY(), 3.0f, paint);
				}
			}*/

            // Request another update at the animation interval, but only
            // repaint the laser line,
            // not the entire viewfinder mask.
            postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top,
                    frame.right, frame.bottom);
        }
    }

    public void drawBroder(Canvas canvas, Rect frame2) {
        paint.setColor(frameColor);
        canvas.drawRect(frame2.left, frame2.top, frame2.left + broderWidth + broderLength,
                frame2.top + broderWidth, paint);
        canvas.drawRect(frame2.left, frame2.top, frame2.left + broderWidth,
                frame2.top + broderWidth + broderLength, paint);

        canvas.drawRect(frame2.left, frame2.bottom - broderLength - broderWidth, frame2.left + broderWidth,
                frame2.bottom + 1, paint);
        canvas.drawRect(frame2.left, frame2.bottom - broderWidth, frame2.left + broderWidth + broderLength,
                frame2.bottom + 1, paint);


        canvas.drawRect(frame2.right - broderLength - broderWidth + 2, frame2.top, frame2.right + 2,
                frame2.top + broderWidth, paint);
        canvas.drawRect(frame2.right - broderWidth + 2, frame2.top, frame2.right + 2,
                frame2.top + broderWidth + broderLength, paint);

        canvas.drawRect(frame2.right - broderLength - broderWidth + 2, frame2.bottom - broderWidth, frame2.right + 2,
                frame2.bottom + 1, paint);
        canvas.drawRect(frame2.right - broderWidth + 2, frame2.bottom - broderLength - broderWidth, frame2.right + 2,
                frame2.bottom + 1, paint);

    }

    public void drawViewfinder() {
        resultBitmap = null;
        invalidate();
    }

    /**
     * Draw a bitmap with the result points highlighted instead of the live
     * scanning display.
     *
     * @param barcode An image of the decoded barcode.
     */
    public void drawResultBitmap(Bitmap barcode) {
        resultBitmap = barcode;
        invalidate();
    }

    public void addPossibleResultPoint(ResultPoint point) {
        possibleResultPoints.add(point);
    }

}
