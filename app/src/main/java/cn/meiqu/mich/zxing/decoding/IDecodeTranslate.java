package cn.meiqu.mich.zxing.decoding;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

import cn.meiqu.mich.zxing.view.ViewfinderView;


public interface IDecodeTranslate {
	/**
	 * 获取handler
	 * 
	 * @return
	 */
	public Handler getHandler();

	/**
	 * 获取view
	 * 
	 * @return
	 */
	public ViewfinderView getViewfinderView();

	/**
	 * draw view
	 */
	public void drawViewfinder();

	/**
	 * 获取activitity
	 * 
	 * @return
	 */
	public Activity getActivity();

	/**
	 * 处理返回信息
	 * 
	 * @param result
	 * @param barcode
	 */
	public void handleDecode(Result result, Bitmap barcode);
}
