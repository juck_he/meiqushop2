package cn.meiqu.mich.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import cn.meiqu.mich.base.BaseApp;
import cn.meiqu.mich.bean.User;

public class SettingDao {
    private static SettingDao dao;
    private String settingName = "entrepreneur";
    private String account = "account";
    private String pwd = "pwd";
    private String userJson = "userJson";
    private String wifi_ssid = "wifi_ssid";
    private String wifi_pwd = "wifi_pwd";
    private String wifi_ssid1 = "wifi_ssid1";
    private String wifi_pwd1 = "wifi_pwd1";
    private String wifi_ssid2 = "wifi_ssid2";
    private String wifi_pwd2 = "wifi_pwd2";
    private User user;
    public static String lastDeviceIp = "lastDeviceIp";
    private String hairImages = "hairImages";

    private SettingDao() {
    }


    public static SettingDao getInstance() {
        if (dao == null)
            dao = new SettingDao();
        return dao;
    }

    private SharedPreferences getPreferences() {
        return BaseApp.mContext.getSharedPreferences(settingName,
                Context.MODE_PRIVATE);
    }

    public void set(String key, String value) {
        Editor editor = getPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void set(String key, boolean value) {
        Editor editor = getPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String get(String key) {
        return getPreferences().getString(key, "");
    }


    public void setAccount(String name) {
        set(account, name);
    }

    public String getAccount() {
        return get(account);
    }

    public void setPwd(String name) {
        set(pwd, name);
    }

    public String getPwd() {
        return get(pwd);
    }

    public void setUserJson(String json) {
        set(userJson, json);
    }

    public String getUserJson() {
        return get(userJson);
    }

    public User getUser() {
        String userJson = getUserJson();

        User user = new Gson().fromJson(userJson, User.class);
        return user;
    }

    public void setUser(User user) {
        if (user != null) {
            String userJson = new Gson().toJson(user);
            setUserJson(userJson);
        } else {
            setUserJson("");
        }

    }

    public void setWifiSSid(String value) {
        set(wifi_ssid, value);
    }

    public String getWifiSSid() {
        return getPreferences().getString(wifi_ssid, "");
    }

    public void setWifiSSid1(String value) {
        set(wifi_ssid1, value);
    }

    public String getWifiSSid1() {
        return getPreferences().getString(wifi_ssid1, "");
    }

    public void setWifiSSid2(String value) {
        set(wifi_ssid2, value);
    }

    public String getWifiSSid2() {
        return getPreferences().getString(wifi_ssid2, "");
    }

    public void setWifiPwd(String value) {
        set(wifi_pwd, value);
    }

    public String getWifiPwd() {
        return getPreferences().getString(wifi_pwd, "");
    }

    public void setWifiPwd1(String value) {
        set(wifi_pwd1, value);
    }

    public String getWifiPwd1() {
        return getPreferences().getString(wifi_pwd1, "");
    }

    public void setWifiPwd2(String value) {
        set(wifi_pwd2, value);
    }

    public String getWifiPwd2() {
        return getPreferences().getString(wifi_pwd2, "");
    }

    public void setLastDeviceIp(String value) {
        set(lastDeviceIp, value);
    }

    public String getLastDeviceIp() {
        return getPreferences().getString(lastDeviceIp, "");
    }

    public String getHairImages() {
        return getPreferences().getString(hairImages, "");
    }

    public void setHairImages(String value) {
        set(hairImages, value);
    }
}

