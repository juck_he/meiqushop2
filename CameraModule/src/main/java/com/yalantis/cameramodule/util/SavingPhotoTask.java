/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Zillow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.yalantis.cameramodule.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;

import com.yalantis.cameramodule.activity.CameraActivity;
import com.yalantis.cameramodule.interfaces.PhotoSavedListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import timber.log.Timber;

public class SavingPhotoTask extends AsyncTask<Void, Void, File> {

    private byte[] data;
    private String name;
    private String path;
    private int orientation;
    private PhotoSavedListener callback;
    Context mContext;

    public SavingPhotoTask(Context mContext, byte[] data, String name, String path, int orientation) {
        this(mContext, data, name, path, orientation, null);
    }

    public SavingPhotoTask(Context mContext, byte[] data, String name, String path, int orientation, PhotoSavedListener callback) {
        this.data = data;
        this.name = name;
        this.path = path;
        this.orientation = orientation;
        this.callback = callback;
        this.mContext = mContext;
    }

    @Override
    protected File doInBackground(Void... params) {
        File photo = getOutputMediaFile();
        if (photo == null) {
            Timber.e("Error creating media file, check storage permissions");
            return null;
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(photo);
//            if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
//                saveByteArray(fos, data);
//            } else {
            saveByteArrayWithOrientation(fos, data, orientation);
            //  }

        } catch (Exception e) {
            Timber.e(e, "File not found: " + e.getMessage());
        }  finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                Timber.e(e, e.getMessage());
            }
        }

        return photo;
    }

    private void saveByteArray(FileOutputStream fos, byte[] data) throws IOException {
        long time = System.currentTimeMillis();
        fos.write(data);
        Timber.d("saveByteArray: %1dms", System.currentTimeMillis() - time);
    }

    private void saveByteArrayWithOrientation(FileOutputStream fos, byte[] data, int orientation) {
        long totalTime = System.currentTimeMillis();
        long time = System.currentTimeMillis();
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            if (orientation != 0 && bitmap.getWidth() > bitmap.getHeight()) {
                Timber.e("decodeByteArray: %1dms", System.currentTimeMillis() - time);
                time = System.currentTimeMillis();
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                Timber.e("createBitmap: %1dms", System.currentTimeMillis() - time);
                time = System.currentTimeMillis();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, CameraConst.COMPRESS_QUALITY, fos);
                Timber.e("compress: %1dms", System.currentTimeMillis() - time);
            } else {
                saveByteArray(fos, data);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("image", bitmap);
            Intent intent = new Intent(CameraActivity.ACTION_PHOTO_RETURN);
            intent.putExtra("image", bundle);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            // bitmap.recycle();
            Timber.e("saveByteArrayWithOrientation: %1dms", System.currentTimeMillis() - totalTime);
        } catch (Exception e) {
            e.printStackTrace();
            System.gc();
            try {
                saveByteArray(fos, data);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        photoSaved(file);
    }

    private void photoSaved(File photo) {
        if (photo != null) {
            if (callback != null) {
                callback.photoSaved(photo.getPath(), photo.getName());
            }
        }
    }

    /**
     * Create a File for saving an image
     */
    private File getOutputMediaFile() {
        // To be safe, we should check that the SDCard is mounted
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Timber.e("External storage " + Environment.getExternalStorageState());
            return null;
        }

        File dir = new File(path);
        // Create the storage directory if it doesn't exist
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Timber.e("Failed to create directory");
                return null;
            }
        }

        return new File(dir.getPath() + File.separator + name);
    }

}
