package com.Fatel.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class MListView extends ListView {

	public MListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void setYScrollTo(int y) {
		scrollTo(0, y);
	}
}
