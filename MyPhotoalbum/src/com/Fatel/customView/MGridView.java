package com.Fatel.customView;

import com.Fatel.utils.ScreenUtils;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.GridView;

public class MGridView extends GridView {
	float lastY = 0;
	float deltaY = 0;
	float moveMinY = 20;
	int windowHeight = ScreenUtils.ScreenHeight(getContext());

	public MGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MGridView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub

		if (ev.getAction() == MotionEvent.ACTION_DOWN)
			lastY = ev.getY();

		else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
			float currentdeltaY = ev.getY() - lastY;
			if (currentdeltaY - deltaY > moveMinY) {

				System.out.println("deltaY----" + deltaY);
				// System.out.println("deltaY----" + getChildAt(0).getTop());
				if (getFirstVisiblePosition() == 0 && currentdeltaY > 0
						&& getChildAt(0).getTop() == 0) {
					for (int i = 0; i < getChildCount(); i++) {
						rotation3D(getChildAt(i), currentdeltaY);
					}
					//return true;
				}

			}
		} else if (ev.getAction() == MotionEvent.ACTION_UP) {
			System.out.println("MotionEvent.ACTION_UP===");
			if (getFirstVisiblePosition() == 0) {
				for (int i = 0; i < getChildCount(); i++) {
					rotation3DEnd(getChildAt(i));
				}
			}
			deltaY = 0;
		}

		return super.onTouchEvent(ev);
	}

	public void rotation3D(View view, float deltaY) {
		float start = this.deltaY;
		this.deltaY = deltaY;
		Rotate3D rotate3d = new Rotate3D(-start / windowHeight * 45,
				-this.deltaY / windowHeight * 45, 10, view.getWidth() / 2,
				view.getHeight() / 2);
		rotate3d.setFillAfter(true);
		rotate3d.setInterpolator(new OvershootInterpolator(2F));
		view.startAnimation(rotate3d);
	}

	public void rotation3DEnd(View view) {
		Rotate3D rotate3d = new Rotate3D(-(deltaY) / windowHeight * 45,
				0, 0, view.getWidth() / 2, view.getHeight() / 2);
		rotate3d.setFillAfter(true);
		rotate3d.setDuration(800);
		rotate3d.setInterpolator(new OvershootInterpolator(2F));
		view.startAnimation(rotate3d);
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		return super.dispatchTouchEvent(ev);
	}
}
