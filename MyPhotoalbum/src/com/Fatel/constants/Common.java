package com.Fatel.constants;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Common implements Runnable {
	/**
	 * 程序文件夹名称
	 */
	public final static String AppName = "MeiquManager";
	/**
	 * 主文件夹目录
	 */
	public final static String AppRootDir = Environment
			.getExternalStorageDirectory() + File.separator + AppName;
    /**
     * 缓存文件夹
     */
    public final static String Image_cache_Dir = AppRootDir + File.separator
            + ".cache" + File.separator + "image";

	/**
	 * 用户的文件夹
	 */
	public final static String User_Root_Dir = AppRootDir + File.separator
			+ "User";
	/**
	 * 本地数据库路径
	 */
	public final static String DB = AppRootDir + File.separator + "." + AppName
			+ ".db";
	/**
	 * ShareParams的主文件名配置
	 */
	public final static String P_MainSetting = AppName + "_main_setting";
	

	public final static String Client_Key = "pdatkMFt";//客户端Key
	public final static String StyList_Client_Key = "a9issIfB";//客户端Key

	public final static String P_ScreenWidth = "P_ScreenWidth";// 屏幕宽度
	public final static String P_ScreenHeight = "P_ScreenHeight";// 屏幕高度
	public final static String P_Use_Last_Time = "P_Use_Last_Time";// 最后一次使用的时间
	public final static String P_Use_Current_Time = "P_Use_Current_Time";// 当前时间
	public final static String P_VersionCode = "P_VersionCode";// 版本编码
	public final static String P_VersionName = "P_VersionName";// 版本名称
	private static boolean isLoaded = false;// 是否需要重新初始化
	private List<String> _dirList = null;// 需要创建的文件夹列表

	public Common() {
	}




	/**
	 * 创建应用所需文件夹
	 * 
	 */
	public static void createDir(List<String> dirs) {
		Common cmm = new Common();
		cmm.setDirList(dirs);
		Thread thread = new Thread(cmm);
		thread.start();
	}

	/**
	 * 创建文件夹
	 * 
	 */
	public static void createDir(String dir) {
		List<String> dirs = new ArrayList<String>() {
		};
		dirs.add(dir);
		createDir(dirs);
	}

	protected void setDirList(List<String> dirs) {
		this._dirList = dirs;
	}

	/**
	 * 获取当前包的信息
	 * 
	 * @param context
	 * @return
	 */
	public static PackageInfo getPackageInfo(Context context) {
		try {
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(context.getPackageName(),
					0);
			return info;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据文件路径创建目录
	 * 
	 * @param dir
	 * @return
	 */
	public static boolean mkdir(String dir) {
		File directory = new File(dir);
		if (directory.exists()) {
			if (directory.isDirectory()) {
				return true;
			} else {
				Log.i("command", "新建目录失败，因为给定的目录路径已经存在同名的文件");
				return false;
			}
		} else {
			return directory.mkdirs();
		}
	}

	//需要写删除目录以及目录下文件的方法

	@Override
	public void run() {
		// TODO Auto-generated method stub

		if (_dirList == null) {
			return;
		}
		boolean success;
		for (String item : _dirList) {
			try {
				success = mkdir(item);
				if (!success) {
					System.err.println("Dir '" + item + "' create fail! ");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("create dir fail:" + e.getMessage());
			}
		}
	}

}
