package com.Fatel.constants;

import android.os.Environment;

public class UrlConstants {
	// sd卡路径
	public static String SD_PATH = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	// 图片缓存路径
	public static String BASE_IMAGE_CACHE = "MyPhotoAlbum";
	// 图片缓存路径
	public static String SAVE_IMAGE_PATH = SD_PATH + "/茵曼";
}
