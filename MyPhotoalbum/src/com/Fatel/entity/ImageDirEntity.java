package com.Fatel.entity;

import java.io.Serializable;
public class ImageDirEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dirName = "";// 文件名
	private String dirPath = "";// 文件绝对路径
	private int imgCount = 0;// 文件图片的个数
	private long modifiedDate = 0;// 最后修改日期
	private boolean isSelect = false;// 是否被选择
	private String imgPath="";//首张图片的路径
	
	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}

	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}

	public int getImgCount() {
		return imgCount;
	}

	public void setImgCount(int imgCount) {
		this.imgCount = imgCount;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "ImageDirEntity [dirName=" + dirName + ", dirPath=" + dirPath
				+ ", imgCount=" + imgCount + ", modifiedDate=" + modifiedDate
				+ "]";
	}

}
