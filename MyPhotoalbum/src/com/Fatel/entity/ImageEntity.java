package com.Fatel.entity;

public class ImageEntity {
	private String id = "";
	private String name = "";// 图片名
	private String size = "";// 图片大小
	private long modifiedDate = 0;// 更改日期
	private String path = "";// 绝对路径
	private String parentPath = "";// 上级文件夹的路径
	private String parentDirName = "";// 上级文件夹的名字
	private boolean isSelect = false;// 是否选中了
	private boolean isOrginal = true;
	private String path_small="";
	
	public String getPath_small() {
		return path_small;
	}

	public void setPath_small(String path_small) {
		this.path_small = path_small;
	}

	public boolean isOrginal() {
		return isOrginal;
	}

	public void setOrginal(boolean isOrginal) {
		this.isOrginal = isOrginal;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}

	public String getParentPath() {
		return parentPath;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getParentDirName() {
		return parentDirName;
	}

	public void setParentDirName(String parentDirName) {
		this.parentDirName = parentDirName;
	}

	@Override
	public String toString() {
		return "ImageEntity [id=" + id + ", name=" + name + ", size=" + size
				+ ", modifiedDate=" + modifiedDate + ", path=" + path
				+ ", parentPath=" + parentPath + ", parentDirName="
				+ parentDirName + "]";
	}

}
