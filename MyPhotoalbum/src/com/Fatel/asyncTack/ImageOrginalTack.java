package com.Fatel.asyncTack;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;

import com.Fatel.utils.ImageUtils;
import com.Fatel.utils.ScreenUtils;
import com.example.myphotoalbum.R;

import uk.co.senab.photoview1.PhotoViewAttacher1;

public class ImageOrginalTack extends AsyncTask<String, Integer, Bitmap> {
	Context mContext;
	ImageView imgV;
	ProgressBar pBar;
	ImageTackCallBack callBack;
	String path = "";

	public ImageOrginalTack(Context mContext, ImageTackCallBack callBack,
			ImageView imgV, ProgressBar pBar) {
		this.mContext = mContext;
		this.imgV = imgV;
		this.pBar = pBar;
		this.callBack = callBack;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		System.out.println("执行异步---onPreExecute");
		pBar.setVisibility(View.VISIBLE);
		imgV.setImageBitmap(null);
		super.onPreExecute();
	}

	@Override
	protected Bitmap doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		path = arg0[0];
		Bitmap b = ImageUtils.compressBitmapByPreLoad(arg0[0],
				ScreenUtils.ScreenWidth(mContext));
		return b;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}

	@Override
	protected void onCancelled() {
		// TODO Auto-generated method stub
		System.out.println("图片加载失败");
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		// TODO Auto-generated method stub
		System.out.println("图片加载完成-------");
		if (imgV.getTag().equals(path)) {
			pBar.setVisibility(View.GONE);
			if (result.getHeight() >= ScreenUtils.ScreenHeight(mContext)) {
				imgV.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			} else {
				imgV.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, ScreenUtils
								.ScreenHeight(mContext)
								- ScreenUtils.statusBarHeight(mContext)));
			}

			imgV.setImageBitmap(result);
			 imgV.startAnimation(AnimationUtils.loadAnimation(mContext,
			 R.anim.image_in));
			PhotoViewAttacher1 mAttacher = new PhotoViewAttacher1(imgV);
			if (callBack != null) {
				callBack.loadFinish((String) imgV.getTag());
				System.out.println("imgV.getTag()---" + imgV.getTag());
			}
		}
	}

	public interface ImageTackCallBack {
		public void loadFinish(String position);
	}
}
