package com.Fatel.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Thumbnails;
import android.util.Log;

import com.Fatel.entity.ImageEntity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ImageBuilder {
    private static ImageBuilder builder;
    private Context mContext;
    DecimalFormat df = new DecimalFormat("###.00");

    private ImageBuilder(Context mContext) {
        this.mContext = mContext;
    }

    public synchronized static ImageBuilder build(Context mContext) {
        if (builder == null) {
            builder = new ImageBuilder(mContext);
        }
        return builder;
    }

    public List<ImageEntity> getImagesFromSdcard() {
        return getImages(mContext, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

    }

    public List<ImageEntity> getImagesFromContent() {
        return getImages(mContext, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
    }

    public List<ImageEntity> getImagesFromAll() {
        List<ImageEntity> lists = getImagesFromContent();
        lists.addAll(getImagesFromSdcard());
        Collections.sort(lists, new FileComparator());
        return lists;
    }

    private List<ImageEntity> getImages(Context mContext, Uri uri) {
        // 获取ContentResolver
        Log.e("DDDDD", System.currentTimeMillis() / 1000 + "");
        ContentResolver contentResolver = mContext.getContentResolver();
        // 查询的字段
        String[] projection = {Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.DATA, MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATE_MODIFIED,
                MediaStore.Images.Media.MINI_THUMB_MAGIC};
        // 条件
        String selection = MediaStore.Images.Media.MIME_TYPE + "=?  or "
                + MediaStore.Images.Media.MIME_TYPE + "=?";
        // 条件值(這裡的参数不是图片的格式，而是标准，所有不要改动)
        String[] selectionArgs = {"image/png", "image/jpeg"};
        // 排序
        String sortOrder = MediaStore.Images.Media.DATE_MODIFIED + " desc";
        // 查询sd卡上的图片
        Cursor cursor = contentResolver.query(uri, projection, selection,
                selectionArgs, sortOrder);
        List<ImageEntity> imageList = new ArrayList<ImageEntity>();
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                ImageEntity imageEntity = getImageEntity(cursor);
                if (imageEntity != null) {
                    imageList.add(getImageEntity(cursor));
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        Log.e("DDDDD", System.currentTimeMillis() / 1000 + "");
        return imageList;
    }

    public ImageEntity getImageEntity(Cursor cursor) {
        ImageEntity entity = new ImageEntity();
        try {
            entity.setId(cursor.getString(cursor.getColumnIndex(Images.Media._ID)));
        }catch (Exception e){
            return null;
//            Toast.makeText(Context,"",1).show();
        }

        // System.out.println("entity-------" + entity.getId());
        try {
            entity.setModifiedDate(Long.parseLong(cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))));
        } catch (Exception e) {
            e.printStackTrace();
            entity.setModifiedDate(0);
        }

        entity.setName(cursor.getString(cursor
                .getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)));
        String path = cursor.getString(cursor
                .getColumnIndex(MediaStore.Images.Media.DATA));
        if (!FileUtils.isFileExist(path)) {
            return null;
        }
        String parentPath = FileUtils.getFolderName(path);
        String parentName = FileUtils.getFileName(parentPath);
        entity.setPath(path);
        entity.setParentPath(parentPath);
        entity.setParentDirName(parentName);
        entity.setPath_small(getSmallImage(entity.getId()));
        // System.out.println("------small----" + entity.getPath_small());
        long size = cursor.getLong(cursor
                .getColumnIndex(MediaStore.Images.Media.SIZE)) / 1024;
        if (size >= 1024) {
            entity.setSize(df.format((float) size / 1024) + "M");
        } else {
            entity.setSize(size + "K");
        }
        // System.out.println(entity.getPath());
        return entity;
    }

    public String getSmallImage(String id) {
        // System.out.println();
        String path = "";
        // System.out.println(" Thumbnails.IMAGE_ID----" + Thumbnails.IMAGE_ID
        // + "---mmmmmm---" + Media._ID);
        ContentResolver cr = mContext.getContentResolver();
        String[] projection = {Thumbnails._ID, Thumbnails.IMAGE_ID,
                Thumbnails.DATA};
        String selection = Thumbnails.IMAGE_ID + "= " + id;
        Cursor cursor = cr.query(Thumbnails.EXTERNAL_CONTENT_URI, projection,
                selection, null, null);
        // if (cursor != null) {
        if (cursor == null || cursor.getCount() == 0) {
            cursor.close();
            return path;
        }
        cursor.moveToFirst();
        path = cursor.getString(2);
        System.out.println(path);
        cursor.close();
        return path;
    }

    // // Intent.ACTION_MEDIA_MOUNTED 进行全扫描 4.4已经禁止，再搞就报错
    // public void allScan() {
    // mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri
    // .parse("file://" + Environment.getExternalStorageDirectory())));
    // }
    //
    // 通过 Intent.ACTION_MEDIA_SCANNER_SCAN_FILE 扫描某个文件
    public void fileScan(String fName) {
        Uri data = Uri.parse("file://" + fName);
        mContext.sendBroadcast(new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, data));
    }

    public void updateGallery(String filename)// filename是我们的文件全名，包括后缀哦
    {
        MediaScannerConnection.scanFile(mContext, new String[]{filename},
                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        // System.out.println("ExternalStorage---Scanned " +
                        // path
                        // + ":");
                        // System.out.println("ExternalStorage -> uri=" + uri);
                    }

                });
    }

    class FileComparator implements Comparator<ImageEntity> {

        @Override
        public int compare(ImageEntity lhs, ImageEntity rhs) {
            // TODO Auto-generated method stub
            if (lhs.getModifiedDate() > rhs.getModifiedDate()) {
                return -1;
            } else if (lhs.getModifiedDate() < rhs.getModifiedDate()) {
                return 0;
            }
            return 0;
        }

    }
}
