package com.Fatel.utils;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory.Options;
import android.provider.MediaStore;
import android.text.GetChars;
import android.widget.ImageView;

import com.example.myphotoalbum.R;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

public class ImageLoaderConfig {

	public static ImageLoaderConfiguration.Builder initDefault(Context context,
			String cacheDisc) {
		return initImageConfig(context, cacheDisc);
	}

	public static ImageLoaderConfiguration.Builder initAvanced(Context context,
			String cacheDisc, int cacheWidth, int quilty) {
//		return initImageConfig(context, cacheDisc).discCacheExtraOptions(
//				cacheWidth, cacheWidth, CompressFormat.JPEG, quilty, null)
//				.memoryCacheExtraOptions(cacheWidth, cacheWidth);
		return initImageConfig(context, cacheDisc);
	}

	/**
	 * 返回默认的参数配置
	 * 
	 * @param isDefaultShow
	 *            true：显示默认的加载图片 false：不显示默认的加载图片
	 * @return
	 */
	public static DisplayImageOptions initDisplayOptions(boolean isOrginal) {
		DisplayImageOptions.Builder displayImageOptionsBuilder = new DisplayImageOptions.Builder();
		// 设置图片缩放方式
		// EXACTLY: 图像将完全按比例缩小的目标大小
		// EXACTLY_STRETCHED: 图片会缩放到目标大小
		// IN_SAMPLE_INT: 图像将被二次采样的整数倍
		// IN_SAMPLE_POWER_OF_2: 图片将降低2倍，直到下一减少步骤，使图像更小的目标大小
		// NONE: 图片不会调整
		displayImageOptionsBuilder.imageScaleType(ImageScaleType.EXACTLY);

		// 设置图片的编码格式为RGB_565，此格式比ARGB_8888快
		if (isOrginal) {
			// 开启内存缓存
			displayImageOptionsBuilder.cacheInMemory(false);
			// 开启SDCard缓存
			displayImageOptionsBuilder.cacheOnDisc(false);
			displayImageOptionsBuilder.bitmapConfig(Bitmap.Config.ARGB_8888);
		} else {
			// 开启内存缓存
			displayImageOptionsBuilder.cacheInMemory(true);
			// 开启SDCard缓存
			displayImageOptionsBuilder.cacheOnDisc(true);
			displayImageOptionsBuilder.bitmapConfig(Bitmap.Config.RGB_565);
		}
	
		return displayImageOptionsBuilder.build();
	}

	/**
	 * 异步图片加载ImageLoader的初始化操作，在Application中调用此方法
	 * 
	 * @param context
	 *            上下文对象
	 * @param cacheDisc
	 *            图片缓存到SDCard的目录，只需要传入SDCard根目录下的子目录即可，默认会建立在SDcard的根目录下
	 */
	private static ImageLoaderConfiguration.Builder initImageConfig(
			Context context, String cacheDisc) {
		// 配置ImageLoader
		// 获取本地缓存的目录，该目录在SDCard的根目录下
		File cacheDir = StorageUtils.getOwnCacheDirectory(context, cacheDisc);
		// 实例化Builder
		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
				context);
		// 设置线程数量
		builder.threadPoolSize(3);
		// 设定线程等级比普通低一点
		builder.threadPriority(Thread.NORM_PRIORITY);
//		builder.discCacheExtraOptions(context.getApplicationContext().getResources().getDisplayMetrics().widthPixels,
//				context.getApplicationContext().getResources().getDisplayMetrics().heightPixels, CompressFormat.JPEG, 80, null);
		// 设定内存缓存为弱缓存
		// builder.memoryCache(new WeakMemoryCache());
		// 设置缓存大小，这里为10M
		// builder.memoryCacheSize(10 * 1024 * 1024);

		// 设定只保存同一尺寸的图片在内存
		builder.denyCacheImageMultipleSizesInMemory();
		// 设定缓存的SDcard目录，UnlimitDiscCache速度最快
		builder.discCache(new UnlimitedDiscCache(cacheDir));
		// 设定缓存到SDCard目录的文件命名
		builder.discCacheFileNameGenerator(new HashCodeFileNameGenerator());
		// 设定网络连接超时 timeout: 10s 读取网络连接超时read timeout: 60s
		builder.imageDownloader(new BaseImageDownloader(context, 10000, 60000));
		// 设置ImageLoader的配置参数
		builder.defaultDisplayImageOptions(initDisplayOptions(false));
		return builder;
	}

}

//class SimpleImageDisplayer implements BitmapDisplayer {
//
//	private int targetWidth;
//
//	public SimpleImageDisplayer(int targetWidth) {
//		this.targetWidth = targetWidth;
//	}
//
//	public Bitmap display(Bitmap bitmap, ImageView imageView,
//			LoadedFrom loadedFrom) {
//		// TODO Auto-generated method stub
//		System.out.println("bitmap.getWidth()" + bitmap.getWidth());
//		System.out.println("bitmap.getHeight()" + bitmap.getHeight());
//		// if (bitmap != null) {
//		// bitmap = ImageUtils.toRoundCorner(bitmap, targetWidth);
//		// }
//		// System.out.println("dasdfsa" + bitmap.getWidth());
//		imageView.setImageBitmap(bitmap);
//		return bitmap;
//	}
//}
