package com.Fatel.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.myphotoalbum.BaseApplication;
import com.example.myphotoalbum.R;
import com.example.myphotoalbum.R.drawable;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;


public class ImageLoador {
	public static void displayDefault(String uri, ImageView imageView) {
		display(uri, imageView, false);
	}

	public static void displayOrginal(String uri, ImageView imageView) {
		display(uri, imageView, true);
	}

	// public static void displayWithScale(String uri, ImageView imageView,
	// int targetWidth) {
	// ImageLoader.getInstance().displayImage(uri, imageView,
	// ImageLoaderConfig.initDisplayOptions(targetWidth, false));
	// }

	private static void display(String uri, ImageView imageView, boolean b) {
		if (!ImageLoader.getInstance().isInited())
			((BaseApplication)imageView.getContext().getApplicationContext()).initImageLoader();
		if (b) {
			ImageLoader.getInstance().displayImage(uri, imageView,
					ImageLoaderConfig.initDisplayOptions(b));
		} else {
			ImageLoader.getInstance().displayImage(uri, imageView,
					ImageLoaderConfig.initDisplayOptions(b));
		}
	}

//	public static ImageLoaderListener getListener() {
//		return new ImageLoaderListener();
//	}


}
