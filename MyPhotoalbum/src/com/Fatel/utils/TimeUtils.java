package com.Fatel.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TimeUtils
 *
 * @author Trinea 2013-8-24
 */
public class TimeUtils {

    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(
            "yyyy/MM");
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT2 = new SimpleDateFormat(
            "yyyy/MM/dd");
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT3 = new SimpleDateFormat(
            "yyyy-MM-dd");
    public static long dTime1 = 7 * 24 * 60 * 60;
    public static long dTime2 = 30 * 24 * 60 * 60;
    public static long dTime3 = 60 * 24 * 60 * 60;

    public static String getTime(long timeInMillis, SimpleDateFormat dateFormat) {
        return dateFormat.format(new Date(timeInMillis));
    }

    public static String getTime(long timeInMillis) {
        return getTime(timeInMillis * 1000, DEFAULT_DATE_FORMAT3);
    }

    public static String getImageTime(long currentTime, long imgTime) {
        String time = "";
        if ((currentTime) < (dTime1 + imgTime)) {
            time = "本周";
        } else if ((currentTime) < (dTime2 + imgTime)) {
            time = "这个月";
        } else if (currentTime < (dTime3 + imgTime)) {
            time = "两个月内";
        } else {
            time = getTime(imgTime * 1000);
        }
        return time;
    }
}
