package com.Fatel.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

  public static boolean isBlank(String str) {
    return (str == null || str.trim().length() == 0);
}
  public static boolean isEmpty(String s) {
    if (s == null)
      return true;
    else if ("".equals(s)) return true;
    return false;
  }

  public static boolean isMobileNO(String mobiles) {
    Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
    Matcher m = p.matcher(mobiles);
    return m.matches();
  }

  public static boolean isEmail(String strEmail) {
    String strPattern =
        "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    Pattern p = Pattern.compile(strPattern);
    Matcher m = p.matcher(strEmail);
    return m.matches();
  }


  public static boolean isIP(String str) {
    String num = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
    String regex = "^" + num + "\\." + num + "\\." + num + "\\." + num + "$";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(str);
    return m.matches();
  }


  public static boolean IsUrl(String str) {
    String regex = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(str);
    return m.matches();
  }


}
