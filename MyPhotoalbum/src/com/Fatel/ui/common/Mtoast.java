package com.Fatel.ui.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

public class Mtoast {
  private static Toast toast;

  public static void show(Context context) {
    show(context, "网络连接超时");
  }

  @SuppressLint("ShowToast")
  public static void show(Context context, String text) {
    if (toast == null) {
      toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
    } else {
      toast.setText(text);
    }
    toast.show();
  }

  public static void show(Context context, int id) {
    if (toast == null) {
      toast = Toast.makeText(context, id, Toast.LENGTH_SHORT);
    } else {
      toast.setText(id);
    }
    toast.show();
  }
}
