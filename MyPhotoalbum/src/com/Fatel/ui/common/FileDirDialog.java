package com.Fatel.ui.common;

import java.util.ArrayList;
import java.util.List;

import com.Fatel.adapter.FileListAdapter;
import com.Fatel.entity.ImageDirEntity;
import com.Fatel.entity.ImageEntity;
import com.Fatel.utils.ScreenUtils;
import com.example.myphotoalbum.PhotoAlbumActivity;
import com.example.myphotoalbum.R;

import android.R.integer;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;

public class FileDirDialog extends Dialog implements OnItemClickListener {

	private List<ImageEntity> imageEntities;
	private ArrayList<ImageDirEntity> dirEntities;
	private Context mContext;
	private ListView mListView;
	private FileListAdapter adapter;

	public FileDirDialog(Context context, List<ImageEntity> list) {
		super(context, R.style.downInDialog);
		this.mContext = context;
		this.imageEntities = list;
		initDirEntities();
		initDialog();
		// TODO Auto-generated constructor stub
	}

	public void initDialog() {
		this.setCancelable(true);
		this.setCanceledOnTouchOutside(true);
		View contain = LayoutInflater.from(mContext).inflate(
				R.layout.popur_file, null);
		mListView = (ListView) contain.findViewById(R.id.listV_popur);
		adapter = new FileListAdapter(mContext, dirEntities);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);
		this.setContentView(contain);
		Window dialogWindow = this.getWindow();
		WindowManager.LayoutParams lp = dialogWindow.getAttributes();
		lp.height = ScreenUtils.dip2px(mContext, 350);
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.gravity = Gravity.BOTTOM;
		this.getWindow().setAttributes(lp);
	}

	public void initDirEntities() {
		dirEntities = new ArrayList<ImageDirEntity>();
		ImageDirEntity dirEntity = new ImageDirEntity();
		// 初始化全部图片
		dirEntity.setDirName("所有图片");
		dirEntity.setSelect(true);
		dirEntity.setImgCount(imageEntities.size());
		if (!imageEntities.isEmpty()) {
			dirEntity.setImgPath(imageEntities.get(0).getPath());
			dirEntity.setDirPath(imageEntities.get(0).getParentPath());
		}
		dirEntities.add(dirEntity);
		// 获取各文件夹图片
		if (!imageEntities.isEmpty()) {
			String pFileName = "";
			dirEntity = initDirEntity(0);
			if (imageEntities.size() < 2) {
				dirEntities.add(dirEntity);
			} else {
				for (int i = 1; i < imageEntities.size(); i++) {
					pFileName = imageEntities.get(i).getParentDirName();
			//		System.out.println(pFileName);
					if (dirEntity.getDirName().equals(pFileName)) {
						dirEntity.setImgCount(dirEntity.getImgCount() + 1);
						// 如果是最后一个，加入到列表中
						if (i == imageEntities.size() - 1) {
							qurcyDirEntity(dirEntity);
						}
					} else {
						// 如果几个都不相同，加入到列表中
						qurcyDirEntity(dirEntity);
						dirEntity = initDirEntity(i);
						if (i == imageEntities.size() - 1) {
							qurcyDirEntity(dirEntity);
						}
					}
				}
			}
		}
	//	System.out.println("文件夹的个数----" + dirEntities.size());
	}

	public void qurcyDirEntity(ImageDirEntity dirEntity) {
		boolean b = true;
		for (int j = 0; j < dirEntities.size(); j++) {
			if (dirEntities.get(j).getDirName().equals(dirEntity.getDirName())) {
				dirEntities.get(j).setImgCount(
						dirEntity.getImgCount()
								+ dirEntities.get(j).getImgCount());
				b = false;
				break;
			}
		}
		if (b) {
			dirEntities.add(dirEntity);
		}
	}

	private ImageDirEntity initDirEntity(int position) {
		ImageDirEntity dirEntity = new ImageDirEntity();
		dirEntity.setDirName(imageEntities.get(position).getParentDirName());
		dirEntity.setDirPath(imageEntities.get(position).getParentPath());
		dirEntity.setImgCount(1);
		dirEntity.setImgPath(imageEntities.get(position).getPath());
		dirEntity.setSelect(false);
		return dirEntity;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub'
		if (arg2 == 0)
			((PhotoAlbumActivity) mContext).onListFileSelectListener(
					dirEntities.get(arg2), true);
		else {
			((PhotoAlbumActivity) mContext).onListFileSelectListener(
					dirEntities.get(arg2), false);
		}
		refreshSelect(dirEntities.get(arg2));
		this.dismiss();
	}

	public void refreshSelect(ImageDirEntity dirEntity) {
		for (int i = 0; i < dirEntities.size(); i++) {
			if (dirEntity.getDirName().equals(dirEntities.get(i).getDirName())) {
				dirEntities.get(i).setSelect(true);
			} else {
				dirEntities.get(i).setSelect(false);
			}
		}
		adapter.notifyDataSetChanged();
	}
	// public void refresh() {
	//
	// }
	// public void show() {
	// //this.show();
	// }
	// @Override
	// public void update() {
	// // TODO Auto-generated method stub
	// super.update();
	// }
}
