package com.Fatel.ui.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.Fatel.adapter.PagerAlbumImageAdapter;
import com.Fatel.asyncTack.ImageOrginalTack.ImageTackCallBack;
import com.Fatel.entity.ImageEntity;
import com.Fatel.utils.ScreenUtils;
import com.example.myphotoalbum.PhotoAlbumActivity;
import com.example.myphotoalbum.R;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.util.ArrayList;
import java.util.List;

public class ReSeeDialog extends Dialog implements
        OnPageChangeListener, android.view.View.OnClickListener,
        ImageTackCallBack, DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {
    private Context mContext;
    private List<ImageEntity> images;
    private ViewPager viewP_image;
    private PagerAlbumImageAdapter adapter;
    private TextView tv_title, tv_finsh;
    private ImageView imgV_back;
    private TextView checkB_orginal, checkB_select;
    private ViewGroup vGroup_Top, vGroup_Bottom;
    private ImageView imgV_small;
    int currentPosition;
    int selectPosition;
    private boolean isAll = false;
    private boolean isResee = false;
    private boolean isFirstAnimation = true;

    public ReSeeDialog(Context context, ArrayList<ImageEntity> images,
                       int currentPosition, int theme) {
        super(context, theme);
        setOnDismissListener(this);
        setOnCancelListener(this);
        this.mContext = context;
        this.images = images;
        this.currentPosition = currentPosition;
        initDialog();
        // TODO Auto-generated constructor stub
    }

    public ReSeeDialog(Context context) {
        super(context, R.style.defaultDialog);
        this.mContext = context;
        initDialog();
        // TODO Auto-generated constructor stub
    }

    public void initDialog() {
        this.setCancelable(true);
        this.setCanceledOnTouchOutside(true);
        View contain = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_resee, null);
        this.setContentView(contain);
        Window dialogWindow = this.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.height = ScreenUtils.ScreenHeight(mContext);
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        this.onWindowAttributesChanged(lp);
        this.getWindow().setAttributes(lp);
        viewP_image = (ViewPager) contain.findViewById(R.id.viewP_photo);
        tv_title = (TextView) contain.findViewById(R.id.tv_title);
        tv_finsh = (TextView) contain.findViewById(R.id.tv_titleC);
        vGroup_Top = (ViewGroup) contain.findViewById(R.id.rel_reseeTop);
        vGroup_Bottom = (ViewGroup) contain.findViewById(R.id.rel_reseeBottom);
//        int padding = ScreenUtils.dip2px(mContext, 7);
//        vGroup_Bottom.setPadding(padding, padding, padding,
//                ScreenUtils.statusBarHeight(mContext) + padding);
        imgV_back = (ImageView) contain.findViewById(R.id.imgV_titleback);
        imgV_small = (ImageView) contain.findViewById(R.id.imgV_dialogResee);
        checkB_orginal = (TextView) contain.findViewById(R.id.checkB_original);
        checkB_select = (TextView) contain.findViewById(R.id.checkB_select);
        tv_title.setOnClickListener(this);
        viewP_image.setOnPageChangeListener(this);
        imgV_back.setOnClickListener(this);
    }

    private float touchX = 0f;
    private float touchY = 0f;
    private boolean isVisiable = true;
    private long tapDuarantion = 0l;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // TODO Auto-generated method stub

        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            if (ev.getY() > vGroup_Top.getBottom()
                    && ev.getY() < vGroup_Bottom.getTop()) {
                touchX = ev.getX();
                touchY = ev.getY();
            }
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
            if ((ev.getX() - 5 <= touchX && touchX <= ev.getX() + 5) && (ev.getY() - 5 <= touchY && touchY <= ev.getY() + 5)) {
                if (System.currentTimeMillis() - tapDuarantion > 200) {
                    if (isVisiable) {
                        vGroup_Bottom.setVisibility(View.GONE);
                        vGroup_Top.setVisibility(View.GONE);
                        vGroup_Top.startAnimation(AnimationUtils.loadAnimation(
                                mContext, R.anim.up_out));
                        vGroup_Bottom.startAnimation(AnimationUtils
                                .loadAnimation(mContext, R.anim.bottom_out));
                    } else {
                        vGroup_Bottom.setVisibility(View.VISIBLE);
                        vGroup_Top.setVisibility(View.VISIBLE);
                        vGroup_Top.startAnimation(AnimationUtils.loadAnimation(
                                mContext, R.anim.up_in));
                        vGroup_Bottom.startAnimation(AnimationUtils
                                .loadAnimation(mContext, R.anim.bottom_in));
                    }
                    isVisiable = !isVisiable;
                    System.out.println("---------MotionEvent.单机");
                    tapDuarantion = System.currentTimeMillis();
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public void refreshAndShowWithAnimation(ViewGroup imgV,
                                            List<ImageEntity> tempImages, int currentPosition, boolean isAll,
                                            boolean isResee) {

        show(tempImages, currentPosition, isAll, isResee, true);
        startAnimtion(imgV);
    }

    public void refreshAndShow(List<ImageEntity> tempImages,
                               int currentPosition, boolean isAll, boolean isResee) {
        show(tempImages, currentPosition, isAll, isResee, false);
    }

    public void show(List<ImageEntity> tempImages, int currentPosition,
                     boolean isAll, boolean isResee, boolean animation) {
        this.images = tempImages;
        this.currentPosition = currentPosition;
        this.selectPosition = currentPosition;
        this.isAll = isAll;
        this.isResee = isResee;
        adapter = new PagerAlbumImageAdapter(mContext, images);
        if (animation) {
            isFirstAnimation = true;
            imgV_small.setVisibility(View.VISIBLE);
            vGroup_Top.setVisibility(View.INVISIBLE);
            vGroup_Bottom.setVisibility(View.INVISIBLE);
            viewP_image.setAdapter(null);
            adapter.addCallBack(this);
        } else {
            imgV_small.setVisibility(View.GONE);
            vGroup_Top.setVisibility(View.VISIBLE);
            vGroup_Bottom.setVisibility(View.VISIBLE);
            adapter.addCallBack(null);
            viewP_image.setAdapter(adapter);
            viewP_image.setCurrentItem(currentPosition);
        }

        tv_title.setText((currentPosition + 1) + "/" + images.size());
        tv_finsh.setText("选择(" + ((PhotoAlbumActivity) (mContext)).selectCount
                + "/" + ((PhotoAlbumActivity) (mContext)).imageCount + ")");
        if (((PhotoAlbumActivity) (mContext)).selectCount != 0) {
            tv_finsh.setTextColor(mContext.getResources().getColor(
                    R.color.tv_sel));

        } else {
            tv_finsh.setTextColor(mContext.getResources().getColor(
                    R.color.enable));
        }
        checkB_orginal.setText("原图(" + images.get(currentPosition).getSize()
                + ")");
        initCheckTextView();
        checkB_orginal.setOnClickListener(this);
        checkB_select.setOnClickListener(this);
        vGroup_Top.clearAnimation();
        vGroup_Bottom.clearAnimation();
        viewP_image.clearAnimation();
        this.show();

    }

    public void initCheckTextView() {
        if (images.get(currentPosition).isOrginal()) {
            checkB_orginal.setCompoundDrawablesWithIntrinsicBounds(R.drawable.image_select, 0, 0, 0);
        } else {
            checkB_orginal.setCompoundDrawablesWithIntrinsicBounds(R.drawable.image_unselect, 0, 0, 0);
        }
        if (images.get(currentPosition).isSelect()) {
            checkB_select.setCompoundDrawablesWithIntrinsicBounds(R.drawable.image_select, 0, 0, 0);
        } else {
            checkB_select.setCompoundDrawablesWithIntrinsicBounds(R.drawable.image_unselect, 0, 0, 0);
        }
    }

    public void refreshTitle() {
        tv_title.setText((currentPosition + 1) + "/" + images.size());
        initCheckTextView();
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub
        // System.out.println("onPageSelected-----" + arg0);
        currentPosition = arg0;
        checkB_orginal.setText("原图(" + images.get(arg0).getSize() + ")");
        refreshTitle();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == tv_title.getId()) {
            this.dismiss();
        } else if (v.getId() == checkB_orginal.getId()) {
            images.get(currentPosition).setOrginal(!images.get(currentPosition).isOrginal());
            initCheckTextView();
        } else if (v.getId() == checkB_select.getId()) {
            ((PhotoAlbumActivity) (mContext)).onItemSelect(null, currentPosition);
            if (((PhotoAlbumActivity) (mContext)).selectCount != 0) {
                tv_finsh.setTextColor(mContext.getResources().getColor(
                        R.color.tv_sel));
            } else {
                tv_finsh.setTextColor(mContext.getResources().getColor(
                        R.color.enable));
            }

            if (!images.isEmpty()) {
                refreshTitle();
            }
            tv_finsh.setText("选择("
                    + ((PhotoAlbumActivity) (mContext)).selectCount + "/"
                    + ((PhotoAlbumActivity) (mContext)).imageCount + ")");
            initCheckTextView();
        }
    }

    public void startAnimtion(ViewGroup imgV) {
        LayoutParams params = new LayoutParams(imgV.getWidth(),
                imgV.getHeight());
        imgV_small.setLayoutParams(params);
        System.out.println("imgV.getleft()---" + imgV.getLeft()
                + "-------imgV.getTop()------" + imgV.getTop());
        imgV_small.setImageDrawable(((ImageView) imgV.getChildAt(0))
                .getDrawable());
        AnimatorSet animationSet = new AnimatorSet();
        ObjectAnimator tX = ObjectAnimator.ofFloat(imgV_small, "translationX",
                imgV.getLeft(),
                (ScreenUtils.ScreenWidth(mContext) - imgV.getWidth()) / 2);
        ObjectAnimator tY = ObjectAnimator.ofFloat(
                imgV_small,
                "translationY",
                imgV.getTop()
                        + mContext.getResources().getDimension(
                        R.dimen.titleHeight),
                ((ScreenUtils.ScreenHeight(mContext)) - imgV.getHeight()) / 2);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(
                imgV_small,
                "alpha",
                0.2f,
                0.6f);
        animationSet.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator arg0) {
                System.out.println("动画播放完毕");
                //
                viewP_image.setAdapter(adapter);
                viewP_image.setCurrentItem(currentPosition);
            }

            @Override
            public void onAnimationCancel(Animator arg0) {
                // TODO Auto-generated method stub

            }
        });
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.setDuration(500);
        animationSet.play(tX).with(tY).with(alpha);
        animationSet.start();
    }

    @Override
    public void loadFinish(String path) {
        // TODO Auto-generated method stub
        if (path.equals(images.get(selectPosition).getPath())
                && isFirstAnimation) {
            isFirstAnimation = false;
            imgV_small.setVisibility(View.INVISIBLE);
            vGroup_Top.setVisibility(View.VISIBLE);
            vGroup_Bottom.setVisibility(View.VISIBLE);
            vGroup_Top.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.up_in));
            vGroup_Bottom.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.bottom_in));
            imgV_small.clearAnimation();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        // ((PhotoAlbumActivity) mContext).onReseeDimiss(currentPosition);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        onDismiss(dialog);
    }

    @Override
    public void onBackPressed() {
        ((PhotoAlbumActivity) mContext).onReseeDimiss(currentPosition);
        super.onBackPressed();
    }
}
