package com.Fatel.ui.common;

import android.app.Dialog;
import android.content.Context;

import com.example.myphotoalbum.R;


/**
 * Created by Fatel on 15-4-9.
 */
public class LoadingDialog extends Dialog {

    public LoadingDialog(Context context) {
        super(context, R.style.defaultDialog);
        getWindow().setWindowAnimations(R.style.defaultDialog);
        init();
    }

    public LoadingDialog(Context context, int theme) {
        super(context, theme);
    }

    protected LoadingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void init() {
        setContentView(R.layout.imagebig2);
        setCanceledOnTouchOutside(false);
    }
}
