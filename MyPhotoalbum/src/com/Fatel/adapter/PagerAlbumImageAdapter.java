package com.Fatel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.Fatel.asyncTack.ImageOrginalTack.ImageTackCallBack;
import com.Fatel.entity.ImageEntity;
import com.Fatel.utils.ImageLoadHelper;
import com.example.myphotoalbum.R;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.util.List;
import java.util.concurrent.ExecutorService;

import uk.co.senab.photoview1.PhotoViewAttacher1;

public class PagerAlbumImageAdapter extends PagerAdapter {
    Context mContext;
    List<ImageEntity> images;
    ViewGroup imageViews[];
    ExecutorService e;
    private boolean isRefresh = false;
    private ImageTackCallBack callBack;
    private int threadSize = 5;
    private int containViewSize = 5;

    public PagerAlbumImageAdapter(Context mContext, List<ImageEntity> images) {
        this.mContext = mContext;
        this.images = images;
        containViewSize = images.size();
        //initImages();
//        e = Executors.newFixedThreadPool(threadSize);
//        System.out.println("所用内存：" + Runtime.getRuntime().totalMemory() / 1024
//                + " KB");
    }

    public void addCallBack(ImageTackCallBack callBack) {
        this.callBack = callBack;
        System.out.println("addCallBack=-----" + callBack);
    }

    public void initImages() {
        imageViews = new ViewGroup[containViewSize];
        for (int i = 0; i < imageViews.length; i++) {
            imageViews[i] = (ViewGroup) LayoutInflater.from(mContext).inflate(
                    R.layout.imagealbum, null);
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        // System.out.println("PagerImageAdapter--getCount-"+images.size());
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        // TODO Auto-generated method stub
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // TODO Auto-generated method stub
        System.out.println("instantiateItem---" + position);
        ViewGroup imageViews = (ViewGroup) LayoutInflater.from(mContext)
                .inflate(R.layout.imagealbum, null);
        imageViews.getChildAt(0).setTag(images.get(position).getPath());
        //1032,581
        ImageLoadHelper.displayImage("file://" + images.get(position).getPath(), new ImageSize(400, 600), new MyImageLoadingListener(mContext, callBack,
                (ImageView) (imageViews.getChildAt(0)),
                (ProgressBar) (imageViews.getChildAt(1)), images.get(position).getPath()));
        ((ViewPager) container).addView(imageViews);
        return imageViews;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        System.out.println("destroyItem----" + position);
        ((ViewPager) container).removeView((View) object);
    }

    public void setIsRefresh(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }

    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub
        if (isRefresh) {
            return POSITION_NONE;
        } else {
            return super.getItemPosition(object);
        }
    }

    class MyImageLoadingListener implements ImageLoadingListener {
        Context mContext;
        ImageView imgV;
        ProgressBar pBar;
        ImageTackCallBack callBack;
        String path = "";

        public MyImageLoadingListener(Context mContext, ImageTackCallBack callBack,
                                      ImageView imgV, ProgressBar pBar, String path) {
            this.mContext = mContext;
            this.imgV = imgV;
            this.pBar = pBar;
            this.callBack = callBack;
            this.path = path;
            pBar.setVisibility(View.VISIBLE);
            imgV.setImageBitmap(null);
        }


        @Override
        public void onLoadingStarted(String s, View view) {

        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap loadedImage) {

            Log.e("pp", loadedImage.getWidth() + "," + loadedImage.getHeight());
            Log.e("pp", loadedImage.getDensity() + "");
            Log.e("pp", loadedImage.getByteCount() / 1024 + "");
            if (imgV.getTag().equals(path)) {
                pBar.setVisibility(View.GONE);
//                if (loadedImage.getHeight() >= ScreenUtils.ScreenHeight(mContext)) {
//                    imgV.setLayoutParams(new RelativeLayout.LayoutParams(
//                            RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
//                } else {
//                    imgV.setLayoutParams(new RelativeLayout.LayoutParams(
//                            RelativeLayout.LayoutParams.MATCH_PARENT, ScreenUtils
//                            .ScreenHeight(mContext)
//                            - ScreenUtils.statusBarHeight(mContext)));
//                }
                imgV.setImageBitmap(loadedImage);
                imgV.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.alpha_in));
                PhotoViewAttacher1 mAttacher = new PhotoViewAttacher1(imgV);
                if (callBack != null) {
                    callBack.loadFinish((String) imgV.getTag());
                }
            }
        }

        @Override
        public void onLoadingCancelled(String s, View view) {
        }
    }


}
