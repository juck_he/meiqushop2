package com.Fatel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;

import com.Fatel.entity.ImageEntity;
import com.Fatel.ui.common.Mtoast;
import com.Fatel.utils.ImageLoador;
import com.example.myphotoalbum.PhotoAlbumActivity;
import com.example.myphotoalbum.R;

import java.util.List;

public class GridAdapter extends BaseAdapter {

	private Context mContext;
	private List<ImageEntity> infos;
	private int photoSize = 0;

	public GridAdapter(Context mContext, int photoSize, List<ImageEntity> infos) {
		this.mContext = mContext;
		this.infos = infos;
		this.photoSize = photoSize;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// System.out.println("infos.size--------"+infos.size());
		return infos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View contain, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		ImageEntity imageEntity = infos.get(position);
		// System.out.println("getView----" + "--position--" + position
		// + "----firstPosition--"
		// + ((GridView) parent).getFirstVisiblePosition()
		// + "---parent.getCount---" + parent.getChildCount());

		if (contain == null) {
			holder = new Holder();
			contain = LayoutInflater.from(mContext).inflate(R.layout.image,
					null);
			holder.imgV = (ImageView) contain.findViewById(R.id.imgV);
			holder.foreView = (View) contain.findViewById(R.id.view_fore);
			holder.checkB = (CheckBox) contain.findViewById(R.id.checkB_select);
			contain.setTag(holder);
		} else {
			holder = (Holder) contain.getTag();
		}
		if (contain.getLayoutParams() == null) {
			LayoutParams params = new LayoutParams(photoSize, photoSize);
			contain.setLayoutParams(params);
		}
		if (imageEntity.getName().equals("")) {
			holder.imgV.setImageResource(R.drawable.take_photo);
			holder.imgV.setTag(0);
			holder.foreView.setVisibility(View.INVISIBLE);
			holder.checkB.setVisibility(View.INVISIBLE);
			// System.out.println("重载了第一个");
		} else {
			holder.imgV.setTag(position + 1);
			if (imageEntity.getPath_small().length() > 5) {
				ImageLoador.displayDefault(
						"file://" + imageEntity.getPath_small(), holder.imgV);
			} else {
				ImageLoador.displayDefault("file://" + imageEntity.getPath(),
						holder.imgV);
			}
			holder.checkB.setVisibility(View.VISIBLE);
			holder.checkB.setOnCheckedChangeListener(null);
			holder.checkB.setChecked(imageEntity.isSelect());
			holder.checkB.setOnCheckedChangeListener(new MyOnCheckListener(
					holder, position));
			// holder.checkB.setOnClickListener(new
			// MyOnClickListener(position));
			if (imageEntity.isSelect()) {
				holder.foreView.setVisibility(View.VISIBLE);
			} else {
				holder.foreView.setVisibility(View.INVISIBLE);
			}
			// System.out.println("重载了第" + position + "个");
		}
		return contain;
	}

	class MyOnClickListener implements OnClickListener {
		int position = 0;

		public MyOnClickListener(int position) {
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("onClick-------" + position);
			if (mContext instanceof OnSelectPhotoChangeListener) {
				((OnSelectPhotoChangeListener) mContext)
						.onSelectPhotoChangeLister(position);
			}

		}
	}

	class MyOnCheckListener implements OnCheckedChangeListener {
		int position = 0;
		Holder holder;

		public MyOnCheckListener(Holder holder, int position) {
			this.position = position;
			this.holder = holder;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			System.out.println("onCheck-------" + position);
			((PhotoAlbumActivity) (mContext)).isReSee = false;
			if (isChecked
					&& ((PhotoAlbumActivity) (mContext)).selectCount == ((PhotoAlbumActivity) (mContext)).imageCount) {
				buttonView.setOnCheckedChangeListener(null);
				buttonView.setChecked(false);
				buttonView.setOnCheckedChangeListener(this);
				Mtoast.show(mContext, "你最多只能选择"
						+ ((PhotoAlbumActivity) (mContext)).imageCount + "张图片");
			} else {
				if (mContext instanceof OnSelectPhotoChangeListener) {
					((OnSelectPhotoChangeListener) mContext)
							.onSelectPhotoChangeLister(position);
				}
			}
			if (infos.get(position).isSelect()) {
				holder.foreView.setVisibility(View.VISIBLE);
			} else {
				holder.foreView.setVisibility(View.GONE);
			}
		}

	}

	static class Holder {
		ImageView imgV;
		View foreView;
		CheckBox checkB;
	}

	public interface OnSelectPhotoChangeListener {
		public boolean onSelectPhotoChangeLister(int position);
	}
}
