package com.Fatel.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.Fatel.entity.ImageEntity;
import com.Fatel.utils.ImageLoadHelper;
import com.Fatel.utils.StringUtils;
import com.example.myphotoalbum.R;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.util.List;

/**
 * Created by Fatel on 15-4-22.
 */
public class RecyclerAblumAdapter extends RecyclerView.Adapter<RecyclerAblumAdapter.Holder> {
    private List<ImageEntity> images;
    private int photoSize = 0;
    MyOnItemSelectListener myOnItemSelectListener;
    MyOnItemClickListener myOnItemClickListener;
    ImageSize imageSize = null;

    public interface MyOnItemClickListener {
        public void onItemClick(View v, int position);
    }

    public interface MyOnItemSelectListener {
        public void onItemSelect(View v, int position);
    }

    public RecyclerAblumAdapter(List<ImageEntity> images, int photoSize, MyOnItemSelectListener myOnItemSelectListener
                               ) {
        this.images = images;
        this.photoSize = photoSize;
        //this.myOnItemClickListener = myOnItemClickListener;
        this.myOnItemSelectListener = myOnItemSelectListener;
        imageSize = new ImageSize((int) (photoSize / 2), (int) (photoSize / 2));
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Holder mHolder = new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image, null));
        return mHolder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.instanceView();
    }


    @Override
    public int getItemCount() {
        return images.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgV;
        View foreView;
        ImageView checkB;

        public Holder(View itemView) {
            super(itemView);
            Log.e("uu", "onCreateViewHolder=" + getPosition());
            initView();
            itemView.setOnClickListener(this);
        }

        public View findViewById(int id) {
            return itemView.findViewById(id);

        }

        public void initView() {
            imgV = (ImageView) findViewById(R.id.imgV);
            foreView = (View) findViewById(R.id.view_fore);
            checkB = (ImageView) findViewById(R.id.checkB_select);
            checkB.setOnClickListener(this);
        }

        public void instanceView() {
            ImageEntity imageEntity = images.get(getPosition());
            if (itemView.getLayoutParams() == null) {
                AbsListView.LayoutParams params = new AbsListView.LayoutParams(photoSize, photoSize);
                itemView.setLayoutParams(params);
            }
            //ImageLoadHelper.getImageLoader().cancelDisplayTask(imgV);
            imgV.setTag(getPosition());
            if (StringUtils.isEmpty(imageEntity.getPath())) {
                imgV.setImageResource(R.drawable.take_photo);
                foreView.setVisibility(View.INVISIBLE);
                checkB.setVisibility(View.INVISIBLE);
            } else {
                checkB.setVisibility(View.VISIBLE);
                if (imageEntity.getPath_small().length() > 5) {
                    ImageLoadHelper.displayImage("file://" + imageEntity.getPath_small(),
                            imageSize, new MyImageLoadingListener(imgV, getPosition()));
                    // ImageLoadHelper.displayImage("file://" + imageEntity.getPath_small(),imgV);
                    //  Log.e("ii", "有缩略图");
                } else {
                    ImageLoadHelper.displayImage("file://" + imageEntity.getPath(),
                            imageSize, new MyImageLoadingListener(imgV, getPosition()));
                    //  ImageLoadHelper.displayImage("file://" + imageEntity.getPath(),imgV);
                    //   Log.e("ii", "无缩略图");

                }
                if (imageEntity.isSelect()) {
                    foreView.setVisibility(View.VISIBLE);
                    checkB.setImageResource(R.drawable.image_check);
                } else {
                    foreView.setVisibility(View.INVISIBLE);
                    checkB.setImageResource(R.drawable.image_uncheck);
                }
            }
        }

        @Override
        public void onClick(View v) {
//            if (v instanceof ViewGroup) {
//                if (myOnItemClickListener != null) {
//                    myOnItemClickListener.onItemClick(v, getPosition());
//                }
//            } else {

                if (myOnItemSelectListener != null) {
                    myOnItemSelectListener.onItemSelect(v, getPosition());
                }
//            }
        }
    }

    class MyImageLoadingListener implements ImageLoadingListener {
        ImageView imageView = null;
        int position = 0;

        public MyImageLoadingListener(ImageView imageView, int position) {
            this.imageView = imageView;
            this.position = position;
            imageView.setImageBitmap(null);
        }

        @Override
        public void onLoadingStarted(String s, View view) {

        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            Log.e("ss", "bitmap.getwidth=" + bitmap.getWidth());
            if (imageView.getTag().equals(position)) {
                imageView.setImageBitmap(bitmap);
                imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(), R.anim.alpha_in));
            } else {

            }
        }

        @Override
        public void onLoadingCancelled(String s, View view) {
            Log.e("uu", "onLoadingCancelled");
        }
    }
}
