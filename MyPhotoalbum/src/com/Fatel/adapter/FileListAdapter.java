package com.Fatel.adapter;

import java.net.ContentHandler;
import java.util.ArrayList;

import com.Fatel.entity.ImageDirEntity;
import com.Fatel.utils.ImageLoador;
import com.example.myphotoalbum.R;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileListAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<ImageDirEntity> dirEntities;

	public FileListAdapter(Context mContext,
			ArrayList<ImageDirEntity> dirEntities) {
		this.mContext = mContext;
		this.dirEntities = dirEntities;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dirEntities.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View contain, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder mHolder;
		ImageDirEntity dirEntity = dirEntities.get(position);
		if (contain == null) {
			mHolder = new Holder();
			contain = LayoutInflater.from(mContext).inflate(R.layout.list_file,
					null);
			mHolder.imgV_photo = (ImageView) contain
					.findViewById(R.id.imgV_listFile);
			mHolder.tv_FileName = (TextView) contain
					.findViewById(R.id.tv_listFile_Name);
			mHolder.tv_FileCount = (TextView) contain
					.findViewById(R.id.tv_listFile_count);
			mHolder.imgV_select = (ImageView) contain
					.findViewById(R.id.imgV_listFile_select);
			contain.setTag(mHolder);
		} else {
			mHolder = (Holder) contain.getTag();
		}
		ImageLoador.displayDefault("file://" + dirEntity.getImgPath(),
				mHolder.imgV_photo);
		mHolder.tv_FileName.setText(dirEntity.getDirName());
		mHolder.tv_FileCount.setText(dirEntity.getImgCount()+"");
		if (dirEntity.isSelect())
			mHolder.imgV_select.setVisibility(View.VISIBLE);
		else
			mHolder.imgV_select.setVisibility(View.GONE);
		return contain;
	}

	class Holder {
		ImageView imgV_photo;
		TextView tv_FileName, tv_FileCount;
		ImageView imgV_select;
	}
}
