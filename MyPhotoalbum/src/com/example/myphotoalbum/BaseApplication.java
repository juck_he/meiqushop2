package com.example.myphotoalbum;

import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.Fatel.constants.Common;
import com.Fatel.constants.UrlConstants;
import com.Fatel.utils.ImageLoaderConfig;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;

public class BaseApplication extends Application {
    public static Context mContext;
    private static BaseApplication mInstance = null;
    public static int imageQuilaty = 50;// 图片压缩大小

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mContext = this;
        initImageLoader(this);

    }

    public void initImageLoader(Context context) {
        File cacheDir = new File(Common.Image_cache_Dir);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 1)//设置线程优先级,越低越高优先权
                .denyCacheImageMultipleSizesInMemory() //当同一个Uri获取不同大小的图片，缓存到内存时，只缓存一个。默认会缓存多个不同的大小的相同图片
                .discCache(new UnlimitedDiscCache(cacheDir))// 设置缓存的目录
                .discCacheFileNameGenerator(new Md5FileNameGenerator())// 缓存命名规则,MD5FIleName是url的md5
//                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .tasksProcessingOrder(QueueProcessingType.FIFO) //设置读图为先进先出
                .imageDownloader(new BaseImageDownloader(context, 3000, 8000))// 设置超时 connectTimeout 和 readTimeout
                .threadPoolSize(3)//下载线程数量
                .build();
        ImageLoader.getInstance().init(config);
    }

    public void initImageLoader() {
        WindowManager wm = (WindowManager) mContext
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;// 手机屏幕的宽度
        ImageLoader.getInstance().init(
                ImageLoaderConfig.initAvanced(mContext,
                        UrlConstants.BASE_IMAGE_CACHE, width / 3, imageQuilaty)
                        .build());
    }
}
