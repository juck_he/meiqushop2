package com.example.myphotoalbum;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Fatel.adapter.RecyclerAblumAdapter;
import com.Fatel.entity.ImageDirEntity;
import com.Fatel.entity.ImageEntity;
import com.Fatel.ui.common.FileDirDialog;
import com.Fatel.ui.common.LoadingDialog;
import com.Fatel.ui.common.ReSeeDialog;
import com.Fatel.utils.FileUtils;
import com.Fatel.utils.ImageBuilder;
import com.Fatel.utils.ScreenUtils;
import com.nineoldandroids.animation.ObjectAnimator;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PhotoAlbumActivity extends Activity implements OnClickListener, RecyclerAblumAdapter.MyOnItemSelectListener {
    // private ArrayList<String> imagePaths;
    public static final String EXTRS_IMAGE_COUNT = "imageCount";
    public static final String EXTRS_IMAGE_PATH = "imagePath";
    public static final String EXTRS_IMAGE_ID = "imageID";
    String[] imgUrls;
    boolean[] orginals;
    private Context mContext;
    private ImageView imgV_back;
    private TextView tv_title, tv_time, tv_finished, tv_reSee, tv_file;
    private ProgressBar pBar;
    List<ImageEntity> images;
    List<ImageEntity> tempImages;
    List<ImageEntity> selectImages;// 选择的图片集
    RecyclerView mRecyclerView;
    public RecyclerAblumAdapter adapter;
    private int photoSize = 0;
    public int imageCount = 1000;
    public int selectCount = 0;
    String cameraPath = "/mnt/sdcard/DCIM/Camera";
    Long currentTime = 0l;
    private ObjectAnimator alphaIn;
    private int currentFirstVisiableCount = 0;
    private FileDirDialog fileDirPopurWindow;
    private ReSeeDialog reSeeDialog;
    private ImageDirEntity dirEntity;
    DecimalFormat df = new DecimalFormat("###.00");
    public boolean isReSee = true;
    public boolean isPhotoSucceed = false;
    Handler mHandler;
    int colunm = 4;
    LoadingDialog dialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_album);
        initView();
        initListener();
        initAnimation();
        new ImageQueryThread().start();
//        dialog = new LoadingDialog(mContext);
//        dialog.show();
        mHandler = new Handler(new Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                pBar.setVisibility(View.GONE);
                // TODO Auto-generated method stub
                //  dialog.dismiss();
                // images.add(0, new ImageEntity());
                tempImages = new ArrayList<ImageEntity>();
                tempImages.addAll(images);
                adapter = new RecyclerAblumAdapter(tempImages, photoSize, PhotoAlbumActivity.this);
                mRecyclerView.setAdapter(adapter);
                mRecyclerView.setItemViewCacheSize(100);
                initImageDirEntity();
                return true;
            }
        });
    }

    public void initView() {
        mContext = this;
        imageCount = getIntent().getIntExtra("imageCount", imageCount);
        imgV_back = (ImageView) findViewById(R.id.imgV_titleback);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_time = (TextView) findViewById(R.id.tv_albumTime);
        tv_finished = (TextView) findViewById(R.id.tv_titleC);
        tv_reSee = (TextView) findViewById(R.id.tv_bottomCount);
        tv_file = (TextView) findViewById(R.id.tv_bottomFile);
        tv_finished.setText("选择(" + selectCount + "/" + imageCount + ")");
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerV_ablum);
        pBar = (ProgressBar) findViewById(R.id.pBar_imageload);
        photoSize = (int) ((ScreenUtils.ScreenWidth(mContext)) / colunm);
        selectImages = new ArrayList<ImageEntity>();

    }

    public void initListener() {
        imgV_back.setOnClickListener(this);
        tv_file.setOnClickListener(this);
        tv_finished.setOnClickListener(this);
        tv_reSee.setOnClickListener(null);
        tv_title.setOnClickListener(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, colunm));
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                                              @Override
                                              public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                                  super.onScrollStateChanged(recyclerView, newState);
                                                  switch (newState) {
                                                      case RecyclerView.SCROLL_STATE_SETTLING:
                                                          System.out.println("alphaIn----" + alphaIn.getCurrentPlayTime());
                                                          if (!alphaIn.isRunning()) {
                                                              alphaIn.setCurrentPlayTime(0);
                                                              alphaIn.start();
                                                          } else {
                                                              alphaIn.setCurrentPlayTime(1000);
                                                          }
                                                          break;
                                                      case 0:
                                                          break;
                                                  }
                                              }

                                              @Override
                                              public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                                  super.onScrolled(recyclerView, dx, dy);
                                                  currentScrollY += dy;
                                                  if (tempImages != null && tempImages.size() > 1) {
//                                                      tv_time.setText(TimeUtils.getImageTime(
//                                                              System.currentTimeMillis() / 1000,
//                                                              tempImages.get(currentScrollY / photoSize).getModifiedDate()));
                                                  }
                                              }
                                          }

        );

//        mGridView.setOnScrollListener(this);
    }

    int currentScrollY = 0;

    public void initAnimation() {
        alphaIn = ObjectAnimator.ofFloat(tv_time, "alpha", 0.5f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f);
        alphaIn.setDuration(5000);
    }

    public void initImageDirEntity() {
        dirEntity = new ImageDirEntity();
        // 初始化全部图片
        dirEntity.setDirName("所有图片");
        dirEntity.setSelect(true);
        dirEntity.setImgCount(images.size());
        if (!images.isEmpty()) {
            dirEntity.setImgPath(images.get(0).getPath());
            dirEntity.setDirPath(images.get(0).getParentPath());
        }
    }

    public void refreshTextWithView() {
        tv_finished.setText("选择(" + selectCount + "/" + imageCount + ")");
        if (selectCount != 0) {
            tv_finished.setTextColor(getResources().getColor(R.color.tv_sel));
//            tv_reSee.setText("预览(" + selectCount + ")");
            tv_reSee.setTextColor(getResources().getColor(R.color.tv_sel));
            tv_reSee.setOnClickListener(this);

        } else {
            tv_finished.setTextColor(getResources().getColor(R.color.enable));
//            tv_reSee.setText("预览");
            tv_reSee.setTextColor(getResources().getColor(R.color.enable));
            tv_reSee.setOnClickListener(null);
        }
    }

    /**
     * 手机拍照
     */
    public void photograph() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        currentTime = System.currentTimeMillis();
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(new File(cameraPath, currentTime + ".jpg")));
        startActivityForResult(intent, 2);
    }

    public void photographSucceed() {
        ImageEntity entity = new ImageEntity();
        entity.setModifiedDate(currentTime);
        entity.setName(currentTime + ".jpg");
        String path = cameraPath + "/" + currentTime + ".jpg";
        String parentPath = FileUtils.getFolderName(path);
        String parentName = FileUtils.getFileName(parentPath);
        entity.setPath(path);
        entity.setParentPath(parentPath);
        entity.setParentDirName(parentName);
        entity.setSelect(true);
        float size = FileUtils.getFileSize(path) / 1024;
        if (size >= 1024) {
            entity.setSize(df.format((float) size / 1024) + "M");
        } else {
            entity.setSize(size + "K");
        }
        images.add(1, entity);
        tempImages.add(1, entity);
        selectImages.add(selectCount, entity);
        selectCount++;
        ImageBuilder.build(mContext).fileScan(path);
        isReSee = true;
        isPhotoSucceed = true;
        adapter.notifyDataSetChanged();
        refreshTextWithView();
    }

    public void onReseeDimiss(int currentPosition) {
        Log.e("dismiss", currentPosition + "");
        mRecyclerView.smoothScrollToPosition(currentPosition);
    }

//    @Override
//    public void onItemClick(View v, int position) {
//        System.out.println("onItemClick---" + position);
//        isReSee = false;
//        if (reSeeDialog == null)
//            reSeeDialog = new ReSeeDialog(mContext);
//        ImageView imgV = (ImageView) ((ViewGroup) v).getChildAt(0);
//        reSeeDialog.refreshAndShowWithAnimation((ViewGroup) v,
//                tempImages, position,
//                false, isReSee);
//
//    }

    @Override
    public void onItemSelect(View v, int position) {
        System.out.println("onCheck-------" + position);
        if (!tempImages.get(position).isSelect()) {
            selectCount++;
            selectImages.add(tempImages.get(position));
        } else {
            selectCount--;
            selectImages.remove(tempImages.get(position));
        }
        refreshTextWithView();
        tempImages.get(position).setSelect(!tempImages.get(position).isSelect());
        Log.e("dd", "onItemSelect=postion=" + position + "," + tempImages.get(position).isSelect());
        adapter.notifyItemChanged(position);
    }

    public void setResult() {
        String imagePaths[] = new String[selectImages.size()];
        String imageIds[] = new String[selectImages.size()];

        for (int i = 0; i < selectImages.size(); i++) {
            imagePaths[i] = selectImages.get(i).getPath();
            imageIds[i] = selectImages.get(i).getModifiedDate() + "";
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRS_IMAGE_ID, imageIds);
        intent.putExtra(EXTRS_IMAGE_PATH, imagePaths);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == tv_finished.getId()) {
            System.out.println(selectImages.toString());
            //testToBigImage();
            //  setResult();
        } else if (v.getId() == tv_file.getId()) {
            if (tempImages != null) {
                fileDirPopurWindow = new FileDirDialog(mContext,
                        tempImages.subList(1, tempImages.size()));
                fileDirPopurWindow.show();
            }
        } else if (v.getId() == tv_reSee.getId()) {
//            isReSee = true;
//            if (reSeeDialog == null)
//                reSeeDialog = new ReSeeDialog(mContext);
//            reSeeDialog.refreshAndShow(selectImages, 0, false, isReSee);
            setResult();
        } else if (v.getId() == tv_title.getId() || v.getId() == imgV_back.getId()) {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    public boolean onSelectPhotoChangeLister(int position) {
        onItemSelect(null, position);
        return true;
    }

    public void onListFileSelectListener(ImageDirEntity dirEntity, boolean isAll) {
        if (!dirEntity.getDirName().equals(this.dirEntity.getDirName())) {
            this.dirEntity = dirEntity;
            tv_file.setText(dirEntity.getDirName());
            tempImages.clear();
            if (!isAll) {
                for (int i = 0; i < images.size(); i++) {
                    if (dirEntity.getDirName().equals(
                            images.get(i).getParentDirName())) {
                        tempImages.add(images.get(i));
                    }
                }
            } else {
                tempImages.addAll(images);
            }
            currentScrollY = 0;
            adapter.notifyDataSetChanged();
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 2:
                    photographSucceed();
                    fileDirPopurWindow = new FileDirDialog(mContext,
                            images.subList(1, images.size()));
                    fileDirPopurWindow.refreshSelect(dirEntity);
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onPostResume() {
        // TODO Auto-generated method stub
        super.onPostResume();
        if (isPhotoSucceed) {
            getWindow().getDecorView().setVisibility(View.INVISIBLE);
            tv_file.post(new Runnable() {

                @Override
                public void run() {
                    if (reSeeDialog == null)
                        reSeeDialog = new ReSeeDialog(mContext);
                    reSeeDialog.refreshAndShow(selectImages, selectCount - 1,
                            false, isReSee);
                    isPhotoSucceed = false;
                    getWindow().getDecorView().setVisibility(View.VISIBLE);
                }
            });

        }

    }


    class ImageQueryThread extends Thread {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            images = ImageBuilder.build(mContext).getImagesFromSdcard();
            mHandler.sendEmptyMessage(0);
        }
    }

}
